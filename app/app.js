var jbsApp = angular.module('jbsApp', ['ngRoute']);
global.SystemData = require('./app/lib/SystemData.js');
global.Controls = require('./app/lib/Controls.js');
global.Settings = require('./app/models/ApplicationSettings.js');

jbsApp.config(function ($routeProvider) {

    $routeProvider
    
        .when('/', {
            templateUrl: './app/views/Login.html',
            controller: 'LoginController'
        })

        .when('/resetpassword', {
            templateUrl: './app/views/ResetPassword.html',
            controller: 'ResetPasswordController'
        })
        
        .when('/dashboard', {
            templateUrl: './app/views/MainDashboard.html',
            controller: 'mainDashboardController'
        })

        .when('/login', {
            templateUrl: './app/views/Login.html',
            controller: 'LoginController'
        })

        .when('/createinvoice', {
            templateUrl: './app/views/Invoice.html',
            controller: 'InvoiceController'
        })

        .when('/createinvoice/:InvoiceID/:Action', {
            templateUrl: './app/views/Invoice.html',
            controller: 'InvoiceController'
        })

        .when('/customers', {
            templateUrl: './app/views/Customers.html',
            controller: 'CustomersController'
        })

        .when('/invoices', {
            templateUrl: './app/views/InvoicesDashboard.html',
            controller: 'invoicesController'

        })

        .when('/productsandservices', {
            templateUrl: './app/views/ProductsAndServices.html',
            controller: 'ProductsAndServicesController'
        })

        .when('/applicationsettings', {
            templateUrl: './app/views/ApplicationSettings.html',
            controller: 'AppSettingsRouteController'
        })

        .when('/salestax', {
            templateUrl: './app/views/SalesTax.html',
            controller: 'SalesTaxController'
        })

        .when('/expenses', {
            templateUrl: './app/views/ExpensesDashboard.html',
            controller: 'ExpensesDashboardController'
        })

        .when('/addexpense', {
            templateUrl: './app/views/Expense.html',
            controller: 'ExpenseController'
        })

        .when('/addexpense/:ExpenseID/:Action', {
            templateUrl: './app/views/Expense.html',
            controller: 'ExpenseController'
        })

        .when('/suppliers', {
            templateUrl: './app/views/Suppliers.html',
            controller: 'SuppliersController'
        })

        .when('/profitandloss', {
            templateUrl: './app/views/ProfitAndLoss.html',
            controller: 'ProfitAndLossController'
        })

        .when('/chartofaccounts', {
            templateUrl: './app/views/ChartOfAccounts.html',
            controller: 'ChartOfAccountsController'
        })

        .when('/transactions', {
            templateUrl: './app/views/Transactions.html',
            controller: 'TransctionsController'
        })

        .when('/CustomerStatements', {
            templateUrl: './app/views/CustomerStatements.html',
            controller: 'CustomerStatementsController'
        })
        

        .otherwise({ redirectTo: '/' });

});

jbsApp.directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});