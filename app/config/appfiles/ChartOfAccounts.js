exports.ChartOfAccounts = [{
    label: "Assets",
    value: [
      {
        "Group": "Assets",
        "Type": "Cash and Bank"
      },
      {
        "Group": "Assets",
        "Type": "Money in Transit"
      },
      {
        "Group": "Assets",
        "Type": "Expected Payments from Customers"
      },
      {
        "Group": "Assets",
        "Type": "Property, Plant, Equipment"
      },
      {
        "Group": "Assets",
        "Type": "Depreciation and Amortization"
      },
      {
        "Group": "Assets",
        "Type": "Vendor Prepayments and Vendor Credits"
      },
      {
        "Group": "Assets",
        "Type": "Other Short-Term Asset"
      },
      {
        "Group": "Assets",
        "Type": "Other Long-Term Asset"
      },
      
    ]
  },
  {
    label: "Liabilities",
    value: [
      {
        "Group": "Liabilities",
        "Type": "Credit Card",
        
      },
      {
        "Group": "Liabilities",
        "Type": "Loan and Line of Credit",
        
      },
      {
        "Group": "Liabilities",
        "Type": "Expected Payments to Vendors",
        
      },
      {
        "Group": "Liabilities",
        "Type": "Sales Taxes",
        
      },
      {
        "Group": "Liabilities",
        "Type": "Due For Payroll",
        
      },
      {
        "Group": "Liabilities",
        "Type": "Due to You and Other Business Owners",
        
      },
      {
        "Group": "Liabilities",
        "Type": "Customer Prepayments and Customer Credits",
        
      },
      {
        "Group": "Liabilities",
        "Type": "Other Short-Term Liability",
        
      },
      {
        "Group": "Liabilities",
        "Type": "Other Long-Term Liability",
        
      },
      
    ]
  },
  {
    label: "Income",
    value: [
      {
        "Group": "Income",
        "Type": "Income",
        
      },
      {
        "Group": "Income",
        "Type": "Discount",
        
      },
      {
        "Group": "Income",
        "Type": "Other Income",
        
      },
      {
        "Group": "Income",
        "Type": "Uncategorized Income",
        
      },
      {
        "Group": "Income",
        "Type": "Uncategorized Income",
        
      }
    ]
  },
  {
    label: "Expenses",
    value: [
      {
        "Group": "Expenses",
        "Type": "Operating Expense",
        
      },
      {
        "Group": "Expenses",
        "Type": "Cost of Goods Sold",
        
      },
      {
        "Group": "Expenses",
        "Type": "Payment Processing Fee",
        
      },
      {
        "Group": "Expenses",
        "Type": "Payroll Expense",
        
      },
      {
        "Group": "Expenses",
        "Type": "Uncategorized Expense",
        
      },
      {
        "Group": "Expenses",
        "Type": "Loss On Foreign Exchange",
        
      }
    ]
  },
  {
    label: "Equity",
    value: [
      {
        "Group": "Equity",
        "Type": "Business Owner Contribution and Drawing",
        
      },
      {
        "Group": "Equity",
        "Type": "Retained Earnings: Profit"
      }
    ]
  }]