exports.Profiles = [
    {
        Name: "Admin",
        Description: "Ideal for the business owner/partners and or trusted accountants",
        PermissionsDetails: [
            ["Purchases", "Full access"],
            ["Sales", "Full access"],
            ["Accounting", "Full access"],
            ["Reports", "Full access"],
            ["User management", "Full access"],
            ["Data export", "Full access"],
            ["App Settings", "Full access"]
        ],
        InvoiceModule: {
            "Read": true,
            "Write": true,
            "Delete": true,
            "Download": true,
        },
        ExpenseModule: {
            "Read": true,
            "Write": true,
            "Delete": true,
            "Download": true,
        },
        AccountingModule: {
            "Read": true,
            "Write": true,
            "Delete": true,
            "Download": true,
        },
        ReportsModule: {
            "Read": true,
            "Write": true,
            "Delete": true,
            "Download": true,
        },
        ApplicationSettings: {
            "Read": true,
            "Write": true,
            "Delete": true,
            "Download": true,
        }
    },
    {
        Name: "Accountant",
        Description: "Ideal for an accountant or bookkeeper",
        PermissionsDetails: [

            ["Purchases", "Full access"],
            ["Sales", "Full access"],
            ["Accounting", "Full access"],
            ["Reports", "Full access"],
            ["User management", "No access"],
            ["Data export", "Full access"],
            ["App Settings", "No access"]
        ],
        InvoiceModule: {
            "Read": true,
            "Write": true,
            "Delete": false,
            "Download": true,
        },
        ExpenseModule: {
            "Read": true,
            "Write": true,
            "Delete": false,
            "Download": true,
        },
        AccountingModule: {
            "Read": true,
            "Write": true,
            "Delete": true,
            "Download": true,
        },
        ReportsModule: {
            "Read": true,
            "Write": true,
            "Delete": false,
            "Download": true,
        },
        ApplicationSettings: {
            "Read": true,
            "Write": false,
            "Delete": false,
            "Download": false,
        }
    },
    {
        Name: "Creator",
        Description: "Ideal for bookerpers who create and send invoices and expenses",
        PermissionsDetails: [
            ["Purchases", "Full access"],
            ["Sales", "Full access"],
            ["Accounting", "No access"],
            ["Reports", "No access"],
            ["User management", "No access"],
            ["Data export", "No access"],
            ["App Settings", "No access"]
        ],
        InvoiceModule: {
            "Read": true,
            "Write": true,
            "Delete": true,
            "Download": true,
        },
        ExpenseModule: {
            "Read": true,
            "Write": true,
            "Delete": true,
            "Download": true,
        },
        AccountingModule: {
            "Read": false,
            "Write": false,
            "Delete": false,
            "Download": false,
        },
        ReportsModule: {
            "Read": false,
            "Write": false,
            "Delete": false,
            "Download": false,
        },
        ApplicationSettings: {
            "Read": false,
            "Write": false,
            "Delete": false,
            "Download": false,
        }
    },
    {
        Name: "Viewer",
        Description: "Ideal for onyone who needs view-only access to invoices, expenses and reports",
        PermissionsDetails: [
            ["Purchases", "View only"],
            ["Sales", "View only"],
            ["Accounting", "View only"],
            ["Reports", "View only"],
            ["User management", "No access"],
            ["Data export", "No access"],
            ["App Settings", "No access"]
        ],
        InvoiceModule: {
            "Read": true,
            "Write": false,
            "Delete": false,
            "Download": false,
        },
        ExpenseModule: {
            "Read": true,
            "Write": false,
            "Delete": false,
            "Download": false,
        },
        AccountingModule: {
            "Read": true,
            "Write": false,
            "Delete": false,
            "Download": false,
        },
        ReportsModule: {
            "Read": true,
            "Write": false,
            "Delete": false,
            "Download": false,
        },
        ApplicationSettings: {
            "Read": false,
            "Write": false,
            "Delete": false,
            "Download": false,
        }
    }
]