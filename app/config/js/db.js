//Persistent datastore with automatic loading
var Datastore = require('nedb');
var db = {};
var dbpath = __dirname + '/app/data/';

//create db if it does not exist
db.customers = new Datastore(dbpath+'customers.db');

//load each database (do it asynchronously)
db.customers.loadDatabase();

//export all the dbs created and or loaded
module.exports = db;