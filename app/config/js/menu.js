/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function(event) {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
    
    for (i = 0; i < dropdown.length; i++) {
      if(dropdown[i] === event.target){
        console.log("Test menu items!");
      }else{
        dropdown[i].classList.remove("active");
        dropdown[i].nextElementSibling.style.display = "none";
        console.log("Hide Others!");
      }
    }  
  });
}

$('#mainDashboardBtn').on("click",HideMenuBtns);
$('#SettingsMenu').on("click",HideMenuBtns);
$('#LogoutBtn').on("click",HideMenuBtns);
$('#DashboardMenu').on("click",HideMenuBtns);

function HideMenuBtns(){
    for (i = 0; i < dropdown.length; i++) {    
        dropdown[i].classList.remove("active");
        dropdown[i].nextElementSibling.style.display = "none";
        console.log("Hide All Other Buttons!");    
    }
}