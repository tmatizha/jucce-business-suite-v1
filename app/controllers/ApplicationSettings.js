jbsApp.controller("AppSettingsRouteController", function ($scope, $routeParams, $rootScope, $location) {

    //Check if user is signed-in
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {
        //Global variables
        var License = require('./app/lib/SystemData.js');
        var LicenseFunc = require('./app/models/License.js');
        var UserFunc = require('./app/models/Users.js');
        var TransactionFunc = require('./app/models/Transactions.js');
        var PaymentTermFunc = require('./app/models/PaymentTerms.js');
        var CountriesList = require('./app/config/appfiles/Countries.js');
        $scope.Profiles = require('./app/config/appfiles/UserProfiles.js').Profiles;
        $scope.SecurityQuestions = UserFunc.SecurityQuestons;
        var CurrenciesList = require('./app/config/appfiles/Currencies.js');
        //var RequiredFields = "";//["CompanyName", "AddressLine1", "City", "Country"]
        var remote = require('electron').remote;
        var fs = remote.require('fs');
        const app = remote.app
        var filePath = null;
        var path = require('path');
        var md5 = require('md5');

        $(function () {
            // Basic instantiation:
            $('#AccentColor').colorpicker();
            $('[data-toggle="popover"]').popover();

            // Example using an event, to change the color of the .jumbotron background:
            $('#AccentColor').on('colorpickerChange', function (event) {
                $('#AccentColor').css('background-color', event.color.toString());
            });
        });

        //Initialise scope variables
        $scope.Countries = CountriesList.Countries;
        $scope.Currencies = CurrenciesList.Currencies;

        TransactionFunc.getTransaction({}, function (transaction) {

            if (transaction.length > 0) {
                $('#Currency').attr("disabled", true);

            } else {
                console.log("Currency still changable")
            }

        });

        //load Invoice settings
        Settings.getSettings(function (err, AppSettings) {
            //GetAllUsers
            GetUsers();

            $scope.Settings = AppSettings;
            $scope.$apply();
            $("#ExpiryDateDisplay").datepicker("setDate", $scope.Settings.ExpiryDate);
            $scope.Settings.Logo ? renderImage("CompanyLogo", $scope.Settings.Logo) : console.log("no company logo");
            $scope.Settings.EnableDailyExchangeRate ? $("#DailyExchangePane").show() : $("#DailyExchangePane").hide();
            $("#Currency").val($scope.Settings.Currency).change();
            $("#Country").val($scope.Settings.Country).change();
            $("#ReferenceCurrency").val($scope.Settings.ReferenceCurrency).change();
            $('#AccentColor').css('background-color', $scope.Settings.AccentColor);

            if (!$scope.Settings.ApplicationID) {
                License.setMachineUUID(function (data) {

                    console.log(data);
                    $scope.Settings.ApplicationID = data;
                    $scope.$apply();
                });
            }
        });

        console.log(app.getPath('userData'));
        //scope variables
        $scope.imagePath = null;
        ///Form layout
        $("#Alert").hide();
        $('#UserSettings').hide();
        $('#FinancialSettings').hide();


        //Allow searching for dropdown
        $("#Country").select2({
            placeholder: "Select Country",
            allowClear: true
        });
        $("#Currency").select2({
            placeholder: "Select Currency",
            allowClear: true
        });
        $("#ReferenceCurrency").select2({
            placeholder: "Select Currency",
            allowClear: true
        });

        $("#ApplicationKey").mask('AAAA-SSSS-YYYY-ZZZZ', {
            'translation': {
                A: { pattern: /[A-Za-z0-9]/ },
                S: { pattern: /[A-Za-z0-9]/ },
                Y: { pattern: /[A-Za-z0-9]/ },
                Z: { pattern: /[A-Za-z0-9]/ }
            }
        });

        $('#ApplicationKey').keyup(function () {
            $(this).val($(this).val().toUpperCase());
        });

        $scope.UserSettingsOnclick = () => {
            $('#CompanySettings').hide();
            $('#UserSettings').show();
            $('#FinancialSettings').hide();

        };
        $scope.FinancialSettingsOnclick = () => {
            $('#FinancialSettings').show();
            $('#CompanySettings').hide();
            $('#UserSettings').hide();
            GetPaymentTerms();
        };
        $scope.CompanySettingsOnclick = () => {
            $('#CompanySettings').show();
            $('#UserSettings').hide();
            $('#FinancialSettings').hide();
        };

        $scope.MySettingsOnclick = () => {

            if ($rootScope.CurrentUser != null) {
                UserFunc.getUserById($rootScope.CurrentUser.UniqueID, function (err, docs) {
                    if (err != null) {
                        console.log("Error retriving User: " + err);
                    } else {
                        $scope.NewUser = docs[0];
                        $scope.UserTitle = "Update My Details";
                        $scope.$apply();
                        $('#UserModal').modal('toggle');
                    }

                });
            } else {
                $location.path('login');
            }
        };

        $scope.ProfileOnChange = () => {
            $scope.NewUser.Profile = $scope.Profiles.find(function (obj) { return obj.Name === $scope.NewUser.ProfileName; })
            console.log("Profile is:" + JSON.stringify($scope.Profile));
        }

        //Save settings
        $scope.Save = function () {

            var ValidateFields = Controls.CheckRequiredFields(["CompanyName", "AddressLine1", "City", "Country", "Currency"]);

            if (ValidateFields) {
                $scope.Settings.UniqueID == "" ? Settings.CreateSettings($scope.Settings, SaveSettingsCallback) : Settings.UpdateSettings($scope.Settings.UniqueID, $scope.Settings, SaveSettingsCallback);
            } else {
                Controls.showAlert("Alert", "alert-danger", "Enter all the required fields", "message", true);
            }
        }

        $scope.pickFile = function () {
            console.log("pickFile");
            var { dialog } = remote;
            dialog.showOpenDialog({
                properties: ['openFile'],
                filters: [{
                    name: 'Images',
                    extensions: ['jpg', 'jpeg', 'png']
                }]
            }, function (file) {
                if (!!file) {
                    var path = file[0];
                    filePath = path;
                    var target = "CompanyLogo";
                    img = "data:image/png;base64," + fs.readFileSync(path).toString('base64');
                    renderImage(target, img);
                }
            });
        };

        $scope.EnableDailyExchangeRate = function () {
            var display = $scope.Settings.EnableDailyExchangeRate ? "none" : "block";
            document.getElementById("DailyExchangePane").style.display = display;
        }

        /****************************************** Call Backs and Other Functions ***********************************************************/
        function renderImage(target, img) {
            var canvas = document.getElementById(target),
                context = canvas.getContext('2d');
            logo_image = new Image();
            logo_image.src = img;
            logo_image.onload = drawImageScaled.bind(null, logo_image, context);
        }

        function drawImageScaled(img, ctx) {
            console.log("Drawing Image" + img);
            var canvas = ctx.canvas;
            var hRatio = canvas.width / img.width;
            var vRatio = canvas.height / img.height;
            var ratio = Math.min(hRatio, vRatio);
            var centerShift_x = (canvas.width - img.width * ratio) / 2;
            var centerShift_y = (canvas.height - img.height * ratio) / 2;
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(img, 0, 0, img.width, img.height, centerShift_x, centerShift_y, img.width * ratio, img.height * ratio);
            $scope.Settings.Logo = canvas.toDataURL('image/png');
        }

        function SaveSettingsCallback(err, doc) {
            if (err) {
                Controls.showAlert("Alert", "alert-danger", "Error Saving Settings", "message", true);
                console.log(err);
            } else {

                var LicenseData = new LicenseFunc.License();
                LicenseData.Main = md5($scope.Settings.ApplicationKey);
                LicenseData.Log1 = $scope.Settings.ExpiryDate;
                LicenseFunc.SaveLicense(LicenseData, function (err, docs) {
                    if (err != null) {
                        console.error("Error saving Licence: " + err);
                    } else {
                        Controls.showAlert("Alert", "alert-success", "Settings Saved", "message", true);
                        doc.UniqueID ? $("#SettingsID").val(doc.UniqueID) : console.log("settings saved");
                    }
                });

            }
        }


        /************************************************* User Management *****************************************************************/

        //$scope.Countries = CountriesList.Countries;
        $scope.NewUser = new UserFunc.User();
        function GetUsers() {

            if ($scope.UsersTable != null) {
                $scope.UsersTable.destroy();
                $('#UsersTable tbody').off('click');
            }

            UserFunc.getUser({ $not: { Username: "admin@juccetech.com" } }, function (err, Users) {
                console.log("Getting Users");

                if (err != null) {

                    console.log("Error Getting Users :" + err);
                }
                else if (Users.length == 0) {
                    console.log("No Users found");
                } else {

                    $scope.UsersList = Users;
                    $scope.$apply();



                    $('#UsersTable tbody').unbind("click").on('click', 'button', function () {
                        //var UserID = table.row(this).id();

                        console.log("button clicked is:" + $(this).attr('id'));
                        var buttonType = $(this).attr('id').split("_")[0];
                        var UserId = $(this).attr('id').split("_")[1];
                        console.log(buttonType);
                        console.log("User id is: " + UserId);


                        switch (buttonType) {
                            case "edit":
                                UserFunc.getUserById(UserId, function (err, docs) {
                                    if (err != null) {
                                        console.log("Error retriving User: " + err);
                                    } else {
                                        $scope.NewUser = docs[0];
                                        $scope.UserTitle = "Edit User Details";
                                        $scope.$apply();
                                        Controls.SetError("Name", "");
                                        Controls.SetError("Username", "");
                                        Controls.SetError("Email", "");
                                        Controls.SetError("Profile", "");
                                        Controls.SetError("JobTitle", "");
                                        Controls.SetError("Password", "");
                                        Controls.SetError("ConfirmPassword", "");
                                        $('#UserModal').modal('toggle');
                                    }

                                });
                                break;
                            case "delete":
                                console.log("delete action");
                                UserFunc.getUserById(UserId, function (err, docs) {
                                    if (err != null) {
                                        console.log("Error retriving User: " + JSON.stringify(err));
                                    } else {
                                        $scope.DeleteUser = docs[0];
                                        $scope.$apply();
                                        $('#DeleteUser').modal('toggle');
                                    }
                                });

                                break;
                            default:
                                console.log("Unknown action");
                                break;
                        }
                    });
                }
            });

        }

        $scope.AddUser = function () {
            $scope.UserTitle = "Add New User";
            $scope.NewUser = new UserFunc.User();
            Controls.SetError("Name", "");
            Controls.SetError("Username", "");
            Controls.SetError("Email", "");
            Controls.SetError("Profile", "");
            Controls.SetError("JobTitle", "");
            Controls.SetError("Password", "");
            Controls.SetError("ConfirmPassword", "");
        }

        $scope.SaveUser = function () {

            var requiredFields = ["Name", "Username", "UserEmail", "Profile", "JobTitle", "Password", "ConfirmPassword", "Question1", "Question2", "Answer1", "Answer2"], validateFields = [{ Name: "UserEmail", Type: "email" }];
            if (!Controls.CheckRequiredFields(requiredFields) || !Controls.Validatefields(validateFields)) {
                console.log("Invalid/missing info!")
            } else {

                if ($scope.NewUser.ConfirmPassword === $scope.NewUser.Password) {
                    $('#ConfirmPassword_error').hide();

                    UserFunc.addUser(JSON.parse(angular.toJson($scope.NewUser)), function (err, User) {
                        if (err != null) {
                            console.log("Error is:" + err)
                        } else {
                            $('#UserModal').modal('toggle');
                            console.log("User Creation Successful");
                            Controls.showAlert("Alert", "alert-success", "User " + "'" + $scope.NewUser.Name + "'" + " succesfully created", "message", true);
                            //$scope.User = User; //Update Current User
                            console.log("Created User: " + JSON.stringify(User));
                            $scope.NewUser = new UserFunc.User(); //Clear New User
                            $scope.$apply();
                            GetUsers();
                            $('#User_error').hide();
                        }
                    });
                } else {
                    $('#ConfirmPassword_error').show();
                }
            }
        }

        $scope.UpdateUser = function () {

            var requiredFields = ["Name", "Username", "UserEmail", "Profile", "JobTitle", "Password", "ConfirmPassword", "Question1", "Question2", "Answer1", "Answer2"], validateFields = [{ Name: "UserEmail", Type: "email" }];
            if (!Controls.CheckRequiredFields(requiredFields) || !Controls.Validatefields(validateFields)) {
                console.log("Invalid/missing info!")
            } else {
                if ($scope.NewUser.ConfirmPassword === $scope.NewUser.Password) {
                    $('#ConfirmPassword_error').hide();

                    UserFunc.updateUser($scope.NewUser.UniqueID, JSON.parse(angular.toJson($scope.NewUser)), function (err, num, User) {
                        err ? console.log("Error is:" + err) : console.log("User Update Successful");
                        if (err != null) {
                            console.log("Error deleting User: " + err);
                        } else {
                            $('#UserModal').modal('toggle');
                            Controls.showAlert("Alert", "alert-success", "User " + "'" + User.Name + "'" + " succesfully updated", "message", true);
                            $scope.User = User; //Update Current User
                            console.log("Updated User: " + User);
                            $scope.NewUser = new UserFunc.User(); //Clear New User
                            $scope.$apply();
                            GetUsers();
                        }
                    });
                } else {
                    $('#ConfirmPassword_error').show();
                }
            }
        };

        $scope.RemoveUser = function () {

            console.log("Delete User" + JSON.stringify($scope.DeleteUser));
            UserFunc.deleteUser($scope.DeleteUser.UniqueID, function (err, numRemoved) {
                if (err != null) {
                    console.log("Error deleting User: " + err);
                } else {
                    $('#DeleteUser').modal('toggle');
                    console.log("Deleted " + numRemoved + " Users");
                    Controls.showAlert("Alert", "alert-success", "User " + "'" + $scope.DeleteUser.Name + "'" + " succesfully deleted", "message", true);
                    $scope.DeleteUser = new UserFunc.User(); //Clear New User
                    $scope.$apply();
                    GetUsers();
                }
            });
        };


        /************************************************* Payment Term Management *****************************************************************/

        $scope.NewPaymentTerm = new PaymentTermFunc.PaymentTerm();
        function GetPaymentTerms() {

            if ($scope.PaymentTermsTable != null) {
                $scope.PaymentTermsTable.destroy();
                $('#PaymentTermsTable tbody').off('click');
            }

            PaymentTermFunc.getPaymentTerms({}, function (err, PaymentTerms) {
                console.log("Getting PaymentTerms");

                if (err != null) {

                    console.log("Error Getting PaymentTerms :" + err);
                }
                else if (PaymentTerms.length == 0) {
                    console.log("No PaymentTerms found");
                } else {

                    $scope.PaymentTermsList = PaymentTerms;
                    $scope.$apply();



                    $('#PaymentTermsTable tbody').unbind("click").on('click', 'button', function () {
                        //var PaymentTermID = table.row(this).id();

                        console.log("button clicked is:" + $(this).attr('id'));
                        var buttonType = $(this).attr('id').split("_")[0];
                        var PaymentTermId = $(this).attr('id').split("_")[1];
                        console.log(buttonType);
                        console.log("PaymentTerm id is: " + PaymentTermId);


                        switch (buttonType) {
                            case "edit":
                                PaymentTermFunc.getPaymentTermById(PaymentTermId, function (err, docs) {
                                    if (err != null) {
                                        console.log("Error retriving PaymentTerm: " + err);
                                    } else {
                                        $scope.NewPaymentTerm = docs[0];
                                        $scope.PaymentTermTitle = "Edit PaymentTerm Details";
                                        $scope.$apply();
                                        Controls.SetError("PaymentTerm", "");
                                        Controls.SetError("PaymentPeriod", "");
                                        $('#PaymentTermModal').modal('toggle');
                                    }
                                });
                                break;
                            case "delete":
                                console.log("delete action");
                                PaymentTermFunc.getPaymentTermById(PaymentTermId, function (err, docs) {
                                    if (err != null) {
                                        console.log("Error retriving PaymentTerm: " + JSON.stringify(err));
                                    } else {
                                        $scope.DeletePaymentTerm = docs[0];
                                        $scope.$apply();
                                        $('#DeletePaymentTerm').modal('toggle');
                                    }
                                });

                                break;
                            default:
                                console.log("Unknown action");
                                break;
                        }
                    });
                }
            });

        }

        $scope.AddPaymentTerm = function () {
            $scope.PaymentTermTitle = "Add New PaymentTerm";
            $scope.NewPaymentTerm = new PaymentTermFunc.PaymentTerm();
            Controls.SetError("PaymentTerm", "");
            Controls.SetError("PaymentPeriod", "");
        }

        $scope.SavePaymentTerm = function () {

            var requiredFields = ["PaymentTerm", "PaymentPeriod"];
            if (!Controls.CheckRequiredFields(requiredFields)) {
                console.log("Invalid/missing info!")
            } else {

                PaymentTermFunc.addPaymentTerm(JSON.parse(angular.toJson($scope.NewPaymentTerm)), function (err, PaymentTerm) {
                    if (err != null) {
                        console.log("Error is:" + err)
                    } else {
                        $('#PaymentTermModal').modal('toggle');
                        console.log("PaymentTerm Creation Successful");
                        Controls.showAlert("Alert", "alert-success", "PaymentTerm " + "'" + $scope.NewPaymentTerm.PaymentTerm + "'" + " succesfully created", "message", true);
                        //$scope.PaymentTerm = PaymentTerm; //Update Current PaymentTerm
                        console.log("Created PaymentTerm: " + JSON.stringify(PaymentTerm));
                        $scope.NewPaymentTerm = new PaymentTermFunc.PaymentTerm(); //Clear New PaymentTerm
                        $scope.$apply();
                        GetPaymentTerms();
                        $('#PaymentTerm_error').hide();
                    }
                });

            }
        }

        $scope.UpdatePaymentTerm = function () {

            var requiredFields = ["PaymentTerm", "PaymentPeriod"];
            if (!Controls.CheckRequiredFields(requiredFields)) {
                console.log("Invalid/missing info!")
            } else {
                if ($scope.NewPaymentTerm.ConfirmPassword === $scope.NewPaymentTerm.Password) {
                    $('#ConfirmPassword_error').hide();

                    PaymentTermFunc.updatePaymentTerm($scope.NewPaymentTerm.UniqueID, JSON.parse(angular.toJson($scope.NewPaymentTerm)), function (err, num, PaymentTerm) {
                        err ? console.log("Error is:" + err) : console.log("PaymentTerm Update Successful");
                        if (err != null) {
                            console.log("Error deleting PaymentTerm: " + err);
                        } else {
                            $('#PaymentTermModal').modal('toggle');
                            Controls.showAlert("Alert", "alert-success", "PaymentTerm " + "'" + PaymentTerm.PaymentTerm + "'" + " succesfully updated", "message", true);
                            $scope.PaymentTerm = PaymentTerm; //Update Current PaymentTerm
                            console.log("Updated PaymentTerm: " + PaymentTerm);
                            $scope.NewPaymentTerm = new PaymentTermFunc.PaymentTerm(); //Clear New PaymentTerm
                            $scope.$apply();
                            GetPaymentTerms();
                        }
                    });
                } else {
                    $('#ConfirmPassword_error').show();
                }
            }
        };

        $scope.RemovePaymentTerm = function () {

            console.log("Delete PaymentTerm" + JSON.stringify($scope.DeletePaymentTerm));
            PaymentTermFunc.deletePaymentTerm($scope.DeletePaymentTerm.UniqueID, function (err, numRemoved) {
                if (err != null) {
                    console.log("Error deleting PaymentTerm: " + err);
                } else {
                    $('#DeletePaymentTerm').modal('toggle');
                    console.log("Deleted " + numRemoved + " PaymentTerms");
                    Controls.showAlert("Alert", "alert-success", "PaymentTerm " + "'" + $scope.DeletePaymentTerm.Name + "'" + " succesfully deleted", "message", true);
                    $scope.DeletePaymentTerm = new PaymentTermFunc.PaymentTerm(); //Clear New PaymentTerm
                    $scope.$apply();
                    GetPaymentTerms();
                }
            });
        };

        $scope.validatekey = function () {

            console.log("$scope.Settings.ApplicationKey", $scope.Settings.ApplicationKey.replace("-", ""));
            License.getMachineUUID($scope.Settings.ApplicationKey/*.replace(/-/g, "")*/, function (data) {

                LicenseFunc.CheckLicense({ Main: md5($scope.Settings.ApplicationKey) }, function (err, docs) {
                    if (err != null) {
                        console.error("Error checking Licence: " + err);
                    } else {

                        if (docs.length > 0) {
                            console.log("Key Already Used");
                            document.getElementById('ApplicationKey').classList.add("is-invalid");
                            $('#ApplicationKey_error').show();
                            $('#ApplicationKey_error').text("Key Already Used");

                        } else {

                            console.log("Key is valid " + $scope.Settings.ExpiryDate);
                            console.log("Key still valid " + ($scope.Settings.ExpiryDate > moment()));
                            //$scope.Settings.ExpiryDate = false;
                            if (data.valid) {

                                if ($scope.Settings.ExpiryDate && $scope.Settings.ExpiryDate > moment()) {
                                    $scope.Settings.DisplayExpiryDate = moment($scope.Settings.ExpiryDate).add(data.months, 'month').format('MMMM Do YYYY, h:mm:ss a');
                                    $scope.Settings.ExpiryDate = moment($scope.Settings.ExpiryDate).add(data.months, 'month').toDate();
                                    console.log("Key is valid" + data.valid);
                                    //$scope.$apply();
                                } else {
                                    $scope.Settings.ExpiryDate = moment().add(data.months, 'month').toDate();
                                    $scope.Settings.DisplayExpiryDate = moment().add(data.months, 'month').format('MMMM Do YYYY, h:mm:ss a');
                                    console.log("1st time Key is valid" + data.valid);
                                }

                                $scope.$apply();
                                keyisValid();
                                $scope.Save();

                            } else {
                                console.log("Key is not valid" + data.valid);
                                document.getElementById('ApplicationKey').classList.add("is-invalid");
                                $('#ApplicationKey_error').show();
                                $('#ApplicationKey_error').text("Key Is Invalid");
                            }
                        }
                    }
                });

            });

            function keyisValid() {
                document.getElementById('ApplicationKey').classList.remove("is-invalid");
                document.getElementById('ApplicationKey').classList.add("is-valid");
                $('#ApplicationKey_error').hide();
                $('#ApplicationKey_error').text('');
                $("#subscriptionAlert").hide();
            }
        };
    }
});

