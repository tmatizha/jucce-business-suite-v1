jbsApp.controller("ChartOfAccountsController", function ($scope, $routeParams, $rootScope, $location) {

    //Check if user is signed-in
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {


        var AccountFunc = require('./app/models/ChartOfAccounts.js');
        var CountriesList = require('./app/config/appfiles/Countries.js');
        var AccountTypes = require('./app/config/appfiles/ChartOfAccounts.js');
        var CurrenciesList = require('./app/config/appfiles/Currencies.js');
        //Initialise scope variables
        $scope.Countries = CountriesList.Countries;
        $scope.Currencies = CurrenciesList.Currencies;
        $scope.AcountList = AccountTypes.ChartOfAccounts;
        $scope.NewAccount = new AccountFunc.Account();
        function Run() {
            $("#Alert").hide();
            $("#Preview_Alert").hide();
            GetAccount();
        };
        Run();


        function GetAccount() {

            if ($scope.AccountTable != null) {
                $scope.AccountTable.destroy();
                $('#AccountsTable tbody').off('click');
            }

            AccountFunc.getAccount({"DefaultAccount":false}, function (Account) {
                console.log("Getting Account");

                if (Account.length == 0) {

                    console.log("No Account found");
                }

                $scope.AccountTable = $('#AccountsTable').DataTable({
                    data: Account,
                    rowId: "UniqueID",
                    columns: [
                        { 'data': 'AccountName' },
                        { 'data': 'AccountGroup' },
                        { 'data': 'AccountType' },
                        { 'data': 'AccountCurrency' },
                        {
                            'data': 'Description', 'render': function (data) {
                                return data || "";
                            }
                        },
                        {
                            'data': 'UniqueID', 'sortable': false, 'render': function (data, type, row) {

                                var actions = "";
                                if (!row.ReadOnly) {
                                    actions = '<div>' +
                                        '<button type="button" id="edit_' + row.UniqueID + '"  style="margin-right:10px;" class="btn btn-outline-info"><span class="glyphicon glyphicon-pencil"></span></button></div>'
                                }
                                return actions;
                            }
                        }
                    ],
                    bDestroy: true
                });

                $('#AccountsTable tbody').unbind("click").on('click', 'button', function () {
                    console.log("button clicked is:" + $(this).attr('id'));
                    var buttonType = $(this).attr('id').split("_")[0];
                    var AccountId = $(this).attr('id').split("_")[1];
                    console.log(buttonType);
                    console.log("Account id is: " + AccountId);


                    switch (buttonType) {
                        case "edit":
                            AccountFunc.getAccountById(AccountId, function (err, docs) {
                                if (err != null) {
                                    console.log("Error retriving Account: " + err);
                                } else {
                                    $scope.NewAccount = docs[0];
                                    $scope.$apply();
                                    $('#AccountModal').modal('toggle');                                    
                                    Controls.SetError("Currency", "");""
                                    Controls.SetError("AccountName", "");
                                    Controls.SetError("AccountType", "");
                                }
                            });
                            break;
                        default:
                            console.log("Unknown action");
                            break;
                    }
                });
            });

            AccountFunc.getAccount({"DefaultAccount":true}, function (Account) {
                console.log("Getting Account");

                if (Account.length == 0) {

                    console.log("No Account found");
                }

                $scope.AccountTable = $('#DefaultAccountsTable').DataTable({
                    data: Account,
                    rowId: "UniqueID",
                    columns: [
                        { 'data': 'AccountName' },
                        { 'data': 'AccountGroup' },
                        { 'data': 'AccountType' },
                        { 'data': 'AccountCurrency' },
                        {
                            'data': 'Description', 'render': function (data) {
                                return data || "";
                            }
                        }
                    ],
                    bDestroy: true
                });
            });
        }

        $scope.AddAccount = function () {
            $scope.NewAccount = new AccountFunc.Account();
            Controls.SetError("Currency", "");""
            Controls.SetError("AccountName", "");
            Controls.SetError("AccountType", "");
        }

        $scope.accountChange = function () {

            console.log("Selected account" + JSON.stringify($scope.NewAccount.AccountType));
            var selectedgroup = $("#AccountType option:selected").attr("name");
            $scope.NewAccount.AccountGroup = selectedgroup;
            console.log("Selected group" + selectedgroup);
        }

        $scope.SaveAccount = function () {

            if (Controls.CheckRequiredFields(["AccountName", "AccountType", "Currency"])) {


                console.log("save Account" + JSON.stringify($scope.NewAccount));
                AccountFunc.addAccount($scope.NewAccount, function (err, Account) {
                    if (err != null) {
                        console.log("Error is:" + err)
                    } else {
                        $('#AccountModal').modal('toggle');
                        console.log("Account Creation Successful");
                        Controls.showAlert("Alert", "alert-success", "Account " + "'" + $scope.NewAccount.AccountName + "'" + " succesfully created", "message", true);
                        //$scope.Account = Account; //Update Current Account
                        console.log("Created Account: " + JSON.stringify(Account));
                        $scope.NewAccount = new AccountFunc.Account(); //Clear New Account
                        $scope.$apply();
                        GetAccount();
                        $('#Account_error').hide();
                    }
                });
            } else {
                $('#Account_error').show();
            }

        }

        $scope.UpdateAccount = function () {

            console.log("Update Account" + JSON.stringify($scope.NewAccount));
            AccountFunc.updateAccount($scope.NewAccount.UniqueID, $scope.NewAccount, function (err, num, Account) {

                if (err != null) {
                    console.log("Error deleting Account: " + err);
                } else {
                    $('#AccountModal').modal('toggle');
                    Controls.showAlert("Alert", "alert-success", "Account " + "'" + Account.AccountName + "'" + " succesfully updated", "message", true);
                    $scope.Account = Account; //Update Current Account
                    console.log("Updated Account: " + Account);
                    $scope.NewAccount = new AccountFunc.Account(); //Clear New Account
                    $scope.$apply();
                    GetAccount();
                }
            });
        };
    }
});