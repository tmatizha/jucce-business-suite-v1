const { Callbacks } = require('jquery');

jbsApp.controller("CustomerStatementsController", function ($scope, $routeParams, $rootScope, $location) {

    //Check if user is signed-in
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {
        var CustomerFunc = require('./app/models/Customer.js');
        var CountriesList = require('./app/config/appfiles/Countries.js');
        var InvoiceFunc = require('./app/models/invoice.js');
        var CurrenciesList = require('./app/config/appfiles/Currencies.js');
        var LatePayments = require('./app/lib/LatePayments.js');

        //Initialise scope variables
        $scope.Countries = CountriesList.Countries;
        $scope.NewCustomer = new CustomerFunc.Customer();
        function Run() {
            $("#Alert").hide();
            $("#Preview_Alert").hide();
            GetInvoices();
            //GetCustomers();
        }

        Settings.getSettings(function (err, AppSettings) {
            $scope.Settings = AppSettings;
            $scope.CurrencySymbol = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Settings.Currency; }).Symbol;
            $('#itemsHeader').css('background-color', $scope.Settings.AccentColor);
            Run();
        });

        function GetInvoices() {
            //Late Invoices
            InvoiceFunc.searchInvoice({ "InvoiceState": "Unpaid" }, function (err, invoices) {
                if (err != null) {
                    console.log("Error:" + err);
                    $scope.TotalAmountOverDue = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, 0);
                    $scope.TotalAmountNotDue = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, 0);
                } else {
                    $scope.TotalUnpaidInvoices = invoices.length;
                    $scope.TotalAmountOverDue = 0;
                    $scope.TotalAmountNotDue = 0;
                    var groupedInvoices = _.groupBy(invoices, function (invoices) {
                        return invoices.Customer.UniqueID;
                    });

                    console.log("grouped Late Invoices: " + JSON.stringify(groupedInvoices));

                    var arrayOfKyes = [];
                    for (var key in groupedInvoices) {
                        arrayOfKyes.push(key);
                    }

                    console.log("Account Groups: " + arrayOfKyes.length);



                    if (arrayOfKyes.length > 0) {
                        $scope.CustomerLateInvoice = [];
                        var z = 0;
                        GetCustomerInfo(groupedInvoices[arrayOfKyes[z]]);

                    } else {
                        console.log("No Account groups");
                    }

                    function GetCustomerInfo(Invoices) {


                        CustomerFunc.getCustomer(arrayOfKyes[z], function (customers) {
                            var Customer = customers[0];

                            var customerInvoiceInfo = {
                                CustomerId: arrayOfKyes[z],
                                CustomerName: Customer.CustomerName,
                                TotalAmountNotDue: 0,
                                TotalAmountOverDue: 0,
                                TotalAmountOutstanding: 0,
                                InvoiceDetails: []
                            }

                            var item = 0;

                            Invoices.forEach(invoice => {


                                LatePayments.CalculateLatePaymentCharge(invoice.Customer.UniqueID, invoice.InvoicePayment.Balance, invoice.InvoiceHeader.PaymentDate, invoice.InvoiceHeader.PaymentTerms, "Invoice", function (LatePaymentDetails) {
                                    // var DaysOverDue = LatePaymentDetails.DaysOverDue;
                                    var LateFee = LatePaymentDetails.LateFee;
                                    var InvoiceBalance = invoice.InvoicePayment.Balance + LatePaymentDetails.LateFee;
                                    console.log("Late Payment Details: " + JSON.stringify(LatePaymentDetails));
                                    //invoice.InvoicePayment.Balance = InvoiceBalance;

                                    var invoiceDetails = {
                                        InvoiceID: invoice.UniqueID,
                                        InvoiceNumber: invoice.InvoiceHeader.InvoiceNumber,
                                        InvoiceDate: invoice.InvoiceHeader.InvoiceDate,
                                        DueDate: invoice.InvoiceHeader.PaymentDate,
                                        Total: invoice.InvoiceHeader.InvoiceAmount,
                                        Paid: invoice.InvoiceHeader.InvoiceAmount - invoice.InvoicePayment.Balance,
                                        LateFee: LateFee,
                                        AmountDue: InvoiceBalance,
                                        Currency: invoice.InvoiceHeader.InvoiceCurrency,
                                        CurrencySymbol: CurrenciesList.Currencies.find(function (obj) { return obj.ISO === invoice.InvoiceHeader.InvoiceCurrency; }).Symbol
                                    };

                                    customerInvoiceInfo.InvoiceDetails.push(invoiceDetails);
                                    customerInvoiceInfo.TotalAmountOutstanding += (invoice.InvoiceHeader.InvoiceAmount * invoice.InvoiceHeader.ExchangeRate);

                                    if (moment(invoice.InvoiceHeader.PaymentDate) <= moment().startOf('day')) {
                                        customerInvoiceInfo.TotalAmountOverDue += (invoice.InvoiceHeader.InvoiceAmount * invoice.InvoiceHeader.ExchangeRate);
                                        $scope.TotalAmountOverDue += (invoice.InvoiceHeader.InvoiceAmount * invoice.InvoiceHeader.ExchangeRate);
                                    } else {
                                        customerInvoiceInfo.TotalAmountNotDue += (invoice.InvoiceHeader.InvoiceAmount * invoice.InvoiceHeader.ExchangeRate);
                                        $scope.TotalAmountNotDue += (invoice.InvoiceHeader.InvoiceAmount * invoice.InvoiceHeader.ExchangeRate);
                                    }

                                    item++

                                    console.log("item: " + item);
                                    console.log("invoices.length: " + invoices.length);

                                    if (item == Invoices.length) {
                                        console.log("All Late Payment Details: " + JSON.stringify(LatePaymentDetails));
                                        callback();
                                    }

                                });
                            });

                            function callback() {
                                console.log("Call back");
                                $scope.CustomerLateInvoice.push(customerInvoiceInfo);
                                z++;
                                if (z < arrayOfKyes.length) {
                                    GetCustomerInfo(groupedInvoices[arrayOfKyes[z]]);
                                } else {
                                    $scope.TotalAmountOverDue = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, $scope.TotalAmountOverDue);
                                    $scope.TotalAmountNotDue = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, $scope.TotalAmountNotDue);
                                    console.log("Customer Invoices: " + JSON.stringify($scope.CustomerLateInvoice));
                                    $scope.CustomersTable = $('#CustomersTable').DataTable({
                                        data: $scope.CustomerLateInvoice,
                                        rowId: "CustomerId",
                                        columns: [
                                            { 'data': 'CustomerName' },
                                            { 'data': 'TotalAmountNotDue', 'render': function (data, type, row) { return $scope.CurrencySymbol + Controls.ToCurrency(row.TotalAmountNotDue) } },
                                            { 'data': 'TotalAmountOverDue', 'render': function (data, type, row) { return $scope.CurrencySymbol + Controls.ToCurrency(row.TotalAmountOverDue) } },
                                            {
                                                'data': 'CustomerId', 'sortable': false, 'render': function (id) {

                                                    return '<div>' +
                                                        '<button type="button" id="view_' + id + '"  style="margin-right:10px;" class="btn btn-outline-info btn-sm"><span class="glyphicon glyphicon-eye-open"></span> view</button>' +
                                                        '<button type="button" id="download_' + id + '" class="btn btn-outline-primary btn-sm"><span class="glyphicon glyphicon-download-alt"></span> donwload</button></div>'
                                                }
                                            }
                                        ],
                                        bDestroy: true
                                    });
                                    $('#CustomersTable tbody').unbind("click").on('click', 'button', function () {
                                        //var customerID = table.row(this).id();

                                        console.log("button clicked is:" + $(this).attr('id'));
                                        var buttonType = $(this).attr('id').split("_")[0];
                                        var customerId = $(this).attr('id').split("_")[1];
                                        console.log(buttonType);
                                        console.log("customer id is: " + customerId);


                                        switch (buttonType) {
                                            case "view":
                                                CustomerFunc.getCustomerById(customerId, function (err, docs) {
                                                    if (err != null) {
                                                        console.log("Error retriving customer: " + err);
                                                    } else {
                                                        $scope.Customer = docs[0];
                                                        $scope.InvoiceData = $scope.CustomerLateInvoice.find(function (obj) { return obj.CustomerId === customerId });
                                                        console.log("Invoice Data is : " + JSON.stringify($scope.InvoiceData));
                                                        $scope.GernerateStatementData($scope.InvoiceData.InvoiceDetails)
                                                        $scope.$apply();
                                                        $scope.CurrencyKeys.forEach(key => {
                                                            $('#currencyItemsHeader_' + key).css('background-color', $scope.Settings.AccentColor);
                                                        });
                                                        $('#CustomerModal').modal('toggle');
                                                    }
                                                });
                                                break;
                                            case "download":
                                                console.log("download action");
                                                CustomerFunc.getCustomerById(customerId, function (err, docs) {
                                                    if (err != null) {
                                                        console.log("Error retriving customer: " + JSON.stringify(err));
                                                    } else {
                                                        $scope.Customer = docs[0];
                                                        $scope.InvoiceData = $scope.CustomerLateInvoice.find(function (obj) { return obj.CustomerId === customerId });
                                                        console.log("Invoice Data is : " + JSON.stringify($scope.InvoiceData));
                                                        $scope.GernerateStatementData($scope.InvoiceData.InvoiceDetails)
                                                        $scope.$apply();
                                                        $scope.DownloadStatement();
                                                    }
                                                });

                                                break;
                                            default:
                                                console.log("Unknown action");
                                                break;
                                        }
                                    });
                                    $scope.$apply();
                                }
                            }
                        });
                    }
                }
            });

            //Late Invoices
            InvoiceFunc.CountInvoices({ $and: [{ "InvoiceState": "Unpaid" }, { "InvoiceHeader.PaymentDate": { $lt: moment().startOf('day') } }] }, function (err, total) {
                if (err) {
                    console.log("Error:" + err);
                } else {
                    //Controls.updateCounters(total, "LateInvoices");
                    $scope.TotalOverdueInvoices = total;
                    console.log("Late Invoices: " + total);
                    $scope.$apply();
                }
            });
        }

        $scope.GernerateStatementData = function (invoices) {
            var CustomerCurrrencyStatementSet = false;
            $scope.StatementDate = new Date().toDateString();
            $scope.CustomerCurrrencyStatement = {};
            $scope.OtherCurrencyStatements = [];

            var InvoicesByCurrency = _.groupBy(invoices, function (invoices) {
                return invoices.Currency;
            });

            console.log("Invoices By Currency : " + JSON.stringify(InvoicesByCurrency));

            var arrayOfKyes = [];
            $scope.CurrencyKeys = [];
            for (var key in InvoicesByCurrency) {
                arrayOfKyes.push(key);
                $scope.CurrencyKeys.push(key);
            }

            $scope.TableCurrencies = [];
            for (var i = arrayOfKyes.length - 1; i >= 0; i--) {
                $scope.TableCurrencies.push(arrayOfKyes[i]);
            }

            console.log("TableCurrencies " + $scope.TableCurrencies);
            var z = 0;
            SortInvoiceInfo(InvoicesByCurrency[arrayOfKyes[z]]);
            function SortInvoiceInfo(Invoices) {

                var currency = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === arrayOfKyes[z]; });

                var currencyInvoiceInfo = {
                    Currency: arrayOfKyes[z],
                    CurrencySymbol: currency.Symbol,
                    CurrencyName: currency.Currency,
                    TotalAmountNotDue: 0,
                    TotalAmountOverDue: 0,
                    TotalNetAmountOverDue:0,
                    TotalLateFees:0,
                    TotalAmountOutstanding: 0,
                    InvoiceDetails: []
                }

                Invoices.forEach(invoice => {

                    var invoiceCurrency = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === invoice.Currency; });

                    var invoiceDetails = {
                        InvoiceID: invoice.InvoiceID,
                        InvoiceNumber: invoice.InvoiceNumber,
                        InvoiceDate: invoice.InvoiceDate.toDateString(),
                        DueDate: invoice.DueDate.toDateString(),
                        Total: Controls.ToCurrencyWithSymbol(invoiceCurrency.Symbol, invoice.Total),
                        Paid: Controls.ToCurrencyWithSymbol(invoiceCurrency.Symbol, invoice.Paid),
                        LateFee: Controls.ToCurrencyWithSymbol(invoiceCurrency.Symbol, invoice.LateFee),
                        AmountDue: Controls.ToCurrencyWithSymbol(invoiceCurrency.Symbol, invoice.AmountDue),
                        Currency: invoice.Currency,
                        CurrencySymbol: invoiceCurrency.Symbol,
                        CurrencyName: invoiceCurrency.Currency
                    };

                    currencyInvoiceInfo.InvoiceDetails.push(invoiceDetails);
                    currencyInvoiceInfo.TotalAmountOutstanding += (invoice.AmountDue);                    
                    currencyInvoiceInfo.TotalLateFees  += invoice.LateFee;

                    if (moment(invoice.DueDate) <= moment().startOf('day')) {
                        currencyInvoiceInfo.TotalAmountOverDue += (invoice.AmountDue);
                        currencyInvoiceInfo.TotalNetAmountOverDue  += (invoice.AmountDue - invoice.LateFee);
                    } else {
                        currencyInvoiceInfo.TotalAmountNotDue += (invoice.AmountDue);
                    }
                });
                currencyInvoiceInfo.TotalNetAmountOverDue = Controls.ToCurrencyWithSymbol(currency.Symbol, currencyInvoiceInfo.TotalNetAmountOverDue);
                currencyInvoiceInfo.TotalAmountNotDue = Controls.ToCurrencyWithSymbol(currency.Symbol, currencyInvoiceInfo.TotalAmountNotDue);
                currencyInvoiceInfo.TotalAmountOverDue = Controls.ToCurrencyWithSymbol(currency.Symbol, currencyInvoiceInfo.TotalAmountOverDue);
                currencyInvoiceInfo.TotalLateFees = Controls.ToCurrencyWithSymbol(currency.Symbol, currencyInvoiceInfo.TotalLateFees);
                currencyInvoiceInfo.TotalAmountOutstanding = Controls.ToCurrencyWithSymbol(currency.Symbol, currencyInvoiceInfo.TotalAmountOutstanding);


                if (arrayOfKyes[z] == $scope.Customer.Currency) {
                    console.log("arrayOfKyes[z] = $scope.Customer.Currency" + (arrayOfKyes[z] == $scope.Customer.Currency));
                    $scope.CustomerCurrrencyStatement = JSON.parse(JSON.stringify(currencyInvoiceInfo));
                    CustomerCurrrencyStatementSet = true;

                } else {
                    $scope.OtherCurrencyStatements.push(currencyInvoiceInfo);

                }
                z++;
                if (z < arrayOfKyes.length) {
                    SortInvoiceInfo(InvoicesByCurrency[arrayOfKyes[z]]);
                } else {

                    if (CustomerCurrrencyStatementSet == false) {
                        $scope.CustomerCurrrencyStatement = JSON.parse(JSON.stringify($scope.OtherCurrencyStatements[0]));
                        $scope.OtherCurrencyStatements.splice(0, 1);
                    }
                    console.log("$scope.CustomerCurrrencyStatement" + JSON.stringify($scope.CustomerCurrrencyStatement));
                    console.log("$scope.OtherCurrencyStatements" + JSON.stringify($scope.OtherCurrencyStatements));
                }
            }
        }

        //*********************************************************************** export Statement *********************************************************** */

        $scope.DownloadStatement = function () {
            var date = new Date();
            $scope.InvoiceSendDate = date;

            console.log("Generating PDF");
            var doc = new jsPDF('p', 'px', [595, 842], 'a4', true);
            if ($scope.Settings.Logo) {
                doc.addImage($scope.Settings.Logo, 'base64', 30, 20, 100, 50);
            }
            doc.setFontSize(30);
            doc.text("Statement Of Account", 200, 40);
            doc.autoTable(
                {
                    html: '#print_CompanyInfo',

                    theme: 'plain',
                    headStyles: { fillColor: [255, 255, 255], textColor: [0, 0, 0], fontSize: 14, fontStyle: "bold", halign: 'right' },
                    footStyles: { fillColor: [255, 255, 255], fontSize: 10, textColor: [0, 0, 0] },
                    columnStyles: { 0: { halign: 'right', fontSize: 11 } },
                    styles: { cellPadding: 1 },
                    startY: 50,
                    margin: { left: 250 }
                });

            doc.setFontSize(12);
            doc.text("Bill To:", 30, doc.lastAutoTable.finalY + 10);

            doc.autoTable(
                {
                    html: '#print_StatementCustomerInfo',
                    theme: 'plain',
                    headStyles: { fillColor: [255, 255, 255], textColor: [0, 0, 0], fontSize: 14, fontStyle: "bold" },
                    footStyles: { fillColor: [255, 255, 255], fontSize: 11, textColor: [0, 0, 0] },
                    columnStyles: { 0: { halign: 'left', fontSize: 11 } },
                    styles: { cellPadding: 1 },
                    startY: doc.lastAutoTable.finalY + 15,
                    margin: { right: 230 }
                });


            $scope.TableCurrencies.forEach(function (currency, index) {
                doc.autoTable(
                    {
                        html: '#print_StatementHeaderInfo_' + currency,

                        didParseCell: data => {
                            if (data.section === 'foot' && data.column.index === 1) {
                                data.cell.styles.halign = 'right';
                                data.cell.styles.fontSize = 10;
                            }

                            if (data.section === 'foot' && data.column.index === 0) {
                                data.cell.styles.halign = 'right';
                                data.cell.styles.fontSize = 10;
                            }

                            if (data.section === 'body' && data.row.index === 0) {
                                data.cell.styles.fontStyle = 'bold';
                                data.cell.styles.fontSize = 15;
                            }


                            if (data.section === 'body' && data.row.index === 1) {
                                data.cell.styles.fontStyle = 'bold';
                                data.cell.styles.fontSize = 11;
                            }

                            if (data.section === 'body' && data.column.index === 1) {
                                data.cell.styles.halign = 'right';
                            }
                        },
                        theme: 'plain',
                        headStyles: { fillColor: [255, 5, 133], fontSize: 17 },
                        footStyles: { fillColor: [255, 255, 255], fontSize: 10, textColor: [0, 0, 0], halign: 'right' },
                        columnStyles: { 0: { halign: 'right', fontSize: 10, cellPadding: { top: 1, right: 1 } }, 1: { halign: 'left', fontSize: 10 } },
                        styles: { cellPadding: 1 },
                        startY: doc.lastAutoTable.finalY + (index == 0 ? -65 : 22),
                        margin: { left: 200 }
                    });

                doc.autoTable(
                    {
                        html: '#print_StatementItems_' + currency,
                        didParseCell: data => {
                            if (data.section === 'head' && data.column.index != 0) {
                                data.cell.styles.halign = 'right';
                            }
                        },
                        theme: 'striped',
                        headStyles: { fillColor: $scope.Settings.AccentColor, fontSize: 12 },
                        footStyles: { fillColor: [255, 255, 255], fontSize: 10, textColor: [0, 0, 0] },
                        columnStyles: { 1: { halign: 'right' }, 2: { halign: 'right' }, 3: { halign: 'right' }, 4: { halign: 'right' }, 5: { halign: 'right' }, 6: { halign: 'right' } },
                        startY: doc.lastAutoTable.finalY + 10
                    });


                doc.autoTable(
                    {
                        html: '#print_StatementFooter_' + currency,
                        theme: 'plain',
                        headStyles: { fillColor: [255, 5, 133], fontSize: 15 },
                        footStyles: { fillColor: [255, 255, 255], fontSize: 12, textColor: [0, 0, 0], halign: 'right' },
                        columnStyles: { 0: { halign: 'right', fontSize: 11 }, 1: { halign: 'right', fontSize: 11 } },
                        styles: { cellPadding: { top: 2 } },
                        startY: doc.lastAutoTable.finalY + 10,
                        margin: { left: 200 }
                    });
            });

            var dateTime = new Date().toDateString();
            var file = $scope.Customer.CustomerName + '- Statement -' + dateTime + '.pdf'
            doc.save(file);
            // Open a local file in the default app
            //shell.openItem('c:\\'+file);
        }

        /***************************************************************************************************************************************************************/
    }
});

