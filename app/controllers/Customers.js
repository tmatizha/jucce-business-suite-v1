jbsApp.controller("CustomersController", function ($scope, $routeParams, $rootScope, $location) {

    //Check if user is signed-in
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {

        var CustomerFunc = require('./app/models/Customer.js');
        var CountriesList = require('./app/config/appfiles/Countries.js');
        var CurrenciesList = require('./app/config/appfiles/Currencies.js');

        //Initialise scope variables
        $scope.Countries = CountriesList.Countries;
        $scope.Currencies = CurrenciesList.Currencies;
        $scope.NewCustomer = new CustomerFunc.Customer();
        function Run() {
            $("#Alert").hide();
            $("#Preview_Alert").hide();
            GetCustomers();
        }

        Run();

        function GetCustomers() {

            if ($scope.CustomersTable != null) {
                $scope.CustomersTable.destroy();
                $('#CustomersTable tbody').off('click');
            }

            CustomerFunc.getCustomer("", function (customers) {
                console.log("Getting customers");

                if (customers.length == 0) {

                    console.log("No customers found");
                }

                $scope.CustomersTable = $('#CustomersTable').DataTable({
                    data: customers,
                    rowId: "UniqueID",
                    columns: [
                        { 'data': 'CustomerName' },
                        { 'data': 'ContactFname', 'render': function (data, type, row) { return row.ContactFname + " " + row.ContactLname; } },
                        { 'data': 'Phone' },
                        {
                            'data': 'UniqueID', 'sortable': false, 'render': function (id) {

                                return '<div>' +
                                    '<button type="button" id="edit_' + id + '"  style="margin-right:10px;" class="btn btn-outline-info"><span class="glyphicon glyphicon-pencil"></span></button>' +
                                    '<button type="button" id="delete_' + id + '" class="btn btn-outline-danger"><span class="glyphicon glyphicon-trash"></span></button></div>'
                            }
                        }
                    ],
                    bDestroy: true
                });


                $('#CustomersTable tbody').unbind("click").on('click', 'button', function () {
                    //var customerID = table.row(this).id();

                    console.log("button clicked is:" + $(this).attr('id'));
                    var buttonType = $(this).attr('id').split("_")[0];
                    var customerId = $(this).attr('id').split("_")[1];
                    console.log(buttonType);
                    console.log("customer id is: " + customerId);


                    switch (buttonType) {
                        case "edit":
                            CustomerFunc.getCustomerById(customerId, function (err, docs) {
                                if (err != null) {
                                    console.log("Error retriving customer: " + err);
                                } else {
                                    $scope.NewCustomer = docs[0];
                                    $scope.$apply();
                                    $('#CustomerModal').modal('toggle');
                                }

                            });
                            break;
                        case "delete":
                            console.log("delete action");
                            CustomerFunc.getCustomerById(customerId, function (err, docs) {
                                if (err != null) {
                                    console.log("Error retriving customer: " + JSON.stringify(err));
                                } else {
                                    $scope.DeleteCustomer = docs[0];
                                    $scope.$apply();
                                    $('#DeleteCustomer').modal('toggle');
                                }
                            });

                            break;
                        default:
                            console.log("Unknown action");
                            break;
                    }
                });
            });
        }

        $scope.AddCustomer = function () {
            $scope.NewCustomer = null;
            $scope.NewCustomer = new CustomerFunc.Customer();
        }

        $scope.SaveCustomer = function () {

            var requiredFields = ["Customer"], validateFields = [{ Name: "Email", Type: "email" }, { Name: "Phone", Type: "phone" }]

            if (Controls.CheckRequiredFields(requiredFields) && Controls.Validatefields(validateFields)) {
                console.log("save customer" + JSON.stringify($scope.NewCustomer));
                CustomerFunc.addCustomer($scope.NewCustomer, function (err, Customer) {
                    if (err != null) {
                        console.log("Error is:" + err)
                    } else {
                        $('#CustomerModal').modal('toggle');
                        console.log("Customer Creation Successful");
                        Controls.showAlert("Alert", "alert-success", "Customer " + "'" + $scope.NewCustomer.CustomerName + "'" + " succesfully created", "message", true);
                        //$scope.Customer = Customer; //Update Current Customer
                        console.log("Created Customer: " + JSON.stringify(Customer));
                        $scope.NewCustomer = new CustomerFunc.Customer(); //Clear New Customer
                        $scope.$apply();
                        GetCustomers();
                        $('#Customer_error').hide();
                    }
                });
            } else {
                console.log("Missing fileds/invalid data entered")
            }
        }

        $scope.EditCustomer = function () {

            console.log("Edit.NewCustomer");
        }

        $scope.UpdateCustomer = function () {

            console.log("Update Customer" + JSON.stringify($scope.NewCustomer));
            var requiredFields = ["Customer"], validateFields = [{ Name: "Email", Type: "email" }, { Name: "Phone", Type: "phone" }]

            if (Controls.CheckRequiredFields(requiredFields) && Controls.Validatefields(validateFields)) {
                CustomerFunc.updateCustomer($scope.NewCustomer.UniqueID, $scope.NewCustomer, function (err, num, Customer) {
                    if (err != null) {
                        console.log("Error is:" + err)
                    } else {
                        $('#CustomerModal').modal('toggle');
                        Controls.showAlert("Alert", "alert-success", "Customer " + "'" + $scope.NewCustomer.CustomerName + "'" + " succesfully updated", "message", true);
                        $scope.Customer = Customer; //Update Current Customer
                        console.log("Updated Customer: " + Customer);
                        $('#CustomerModal').modal('toggle');
                        $scope.NewCustomer = new CustomerFunc.Customer(); //Clear New Customer
                        $scope.$apply();
                        GetCustomers();
                    }
                });
            } else {
                console.log("Missing fileds/invalid data entered")
            }
        };

        $scope.RemoveCustomer = function () {

            console.log("Delete Customer" + JSON.stringify($scope.DeleteCustomer));
            CustomerFunc.deleteCustomer($scope.DeleteCustomer.UniqueID, function (err, numRemoved) {
                if (err != null) {
                    console.log("Error deleting customer: " + err);
                } else {
                    $('#DeleteCustomer').modal('toggle');
                    console.log("Deleted " + numRemoved + " customers");
                    Controls.showAlert("Alert", "alert-success", "Customer " + "'" + $scope.DeleteCustomer.CustomerName + "'" + " succesfully deleted", "message", true);
                    $scope.DeleteCustomer = new CustomerFunc.Customer(); //Clear New Customer
                    $scope.$apply();
                    GetCustomers();
                }
            });
        };


        $scope.SameAsBillingOnChange = function () {

            console.log("Same As Billing" + $scope.NewCustomer.Shipping.SameAsBilling);

            if ($scope.NewCustomer.Shipping.SameAsBilling == true) {

                $scope.NewCustomer.Shipping.AddressLine1 = $scope.NewCustomer.Billing.AddressLine1
                $scope.NewCustomer.Shipping.AddressLine2 = $scope.NewCustomer.Billing.AddressLine2
                $scope.NewCustomer.Shipping.City = $scope.NewCustomer.Billing.City
                $scope.NewCustomer.Shipping.ZipCode = $scope.NewCustomer.Billing.ZipCode
                $scope.NewCustomer.Shipping.Country = $scope.NewCustomer.Billing.Country
            }
        }
    }
});

