//App contant variables
//const { shell } = require('electron');
jbsApp.controller("ExpenseController", function ($scope, $routeParams, $rootScope, $location) {

    //Check if user is signed-in
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {

        var ExpenseFunc = require('./app/models/Expense.js');
        //Global variables
        var SupplierFunc = require('./app/models/Suppliers.js');
        var ProdctFunc = require('./app/models/ProductsAndServices.js');
        var ExpensePaymentFunc = require('./app/models/ExpensePayments.js');
        var SalesTaxFunc = require('./app/models/SalesTax.js');
        var CountriesList = require('./app/config/appfiles/Countries.js');
        var CurrenciesList = require('./app/config/appfiles/Currencies.js');
        var AccountFunc = require('./app/models/ChartOfAccounts.js');
        var LatePayments = require('./app/lib/LatePayments.js');
        var CurrentSupplier = ExpenseFunc.Supplier;
        var productSrchObj = null;
        var edit = false;
        var AccountFunc = require('./app/models/ChartOfAccounts.js');
        var TransactionFunc = require('./app/models/Transactions.js');
        //var ExpenseID = null;

        //Initialise all Scope Variables:
        console.log("$routeParams.ExpenseID: " + $routeParams.Action);
        $scope.ExpenseID = $routeParams.ExpenseID;
        $scope.Action = $routeParams.Action;
        $scope.Supplier = new SupplierFunc.Supplier();
        //$scope.NewSupplier = new SupplierFunc.Supplier();
        $scope.Expense = new ExpenseFunc.Expense();
        $scope.Countries = CountriesList.Countries;
        $scope.Currencies = CurrenciesList.Currencies;
        $scope.ExpensePayment = ExpensePaymentFunc.ExpensePayment;

        console.log("Expense Object:  " + JSON.stringify($scope.Expense));
        //load Expense settings
        Settings.getSettings(function (err, AppSettings) {
            $scope.Settings = AppSettings;
            $scope.Expense.ExpenseHeader.ExpenseCurrency = $scope.Settings.Currency;
            $scope.Expense.ExpenseHeader.CompanyCurrency = $scope.Settings.Currency;
            $scope.Settings.DailyExchangeRate = ($scope.Settings.DailyExchangeRate == null || $scope.Settings.DailyExchangeRate == "") ? 1 : $scope.Settings.DailyExchangeRate;
            $scope.Expense.ExpenseHeader.ReferenceCurrencyRate = $scope.Settings.DailyExchangeRate
            $scope.Expense.ExpenseHeader.ReferenceCurrency = $scope.Settings.ReferenceCurrency;
            $scope.ExpenseCurrency = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Settings.Currency; });
            //$("#ExpenseCurrency").val($scope.ExpenseCurrency).change();
            $scope.CurrencySymbol = $scope.ExpenseCurrency.Symbol;
            $scope.$apply();

            //Initialise APP functions
            GetSuppliers();
            GetTaxes();
            GetAPAccount();
            $('[data-toggle="popover"]').popover();

            //Get Expense Accounts values
            AccountFunc.getAccount({ AccountGroup: "Expenses" }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Expenses Accounts found");
                    $scope.ExpenseAccount = [];
                } else {

                    $scope.ExpenseAccount = Accounts;
                }
            });
        });


        //************************************* Other Functions ****************************************************************************************************************/

        //Get Tax values
        function GetTaxes() {
            SalesTaxFunc.getSalesTax("", function (SalesTax) {
                if (SalesTax.length == 0) {

                    console.log("No SalesTax found");
                    $scope.Tax = [];
                } else {

                    $scope.Tax = SalesTax;
                    console.log("SalesTax foun: " + $scope.Tax);
                }
                Run();
            });
        }

        //Get Account for Accounts Payable
        function GetAPAccount() {
            AccountFunc.getAccount({ AccountName: "Accounts Payable" }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Accounts Payable Account found");
                    AccountFunc.CheckAccounts(GetAPAccount);
                } else {
                    $scope.AccountsPayableAccountId = Accounts[0].UniqueID;
                    console.log("Accounts Payable Accounts found:" + Accounts[0].UniqueID);
                }
            });
        }

        //Get/Refresh Expense Data
        function GetExpense() {

            $scope.ExpenseLineItems = [];
            $scope.ExpenseTaxes = [];
            $scope.CurrentProduct = ProdctFunc.product;
            $scope.Expense.ExpenseHeader.ExpenseNetAmount = 0.00;
            $scope.Expense.ExpenseHeader.ExpenseAmount = 0.00;


            if ($scope.ExpenseID) {
                console.log("Expense ID is: " + $scope.ExpenseID);
                //AddExpenseDetailsToForm(ExpenseID, Expense, true);

                ExpenseFunc.getExpenseByID($scope.ExpenseID, function (err, expense) {

                    if (err != null) {
                        console.log("Error retriving expense: " + err);
                    } else {

                        //Retrive expense data
                        $scope.Expense = expense[0];

                        //Retrive Expense taxes
                        var lines = expense[0].ExpenseLines
                        for (var i = 0; i < Object.keys(lines).length; i++) {
                            $scope.ExpenseLineItems.push(expense[0].ExpenseLines[i]);
                        }

                        //Get supplier based on expense data
                        SupplierFunc.getSupplier($scope.Expense.Supplier.UniqueID, function (suppliers) {
                            $scope.Supplier = suppliers[0];
                            $scope.NewSupplier = $scope.Supplier;

                            //Supplier Payment terms
                            $scope.SupplierPaymentTerms = {
                                PaymentTerms: $scope.Supplier.PaymentTerms,
                                PaymentPeriod: $scope.Supplier.PaymentPeriod,
                                LatePaymentRate: $scope.Supplier.LatePaymentRate
                            }

                            if (($scope.Expense.ExpensePayment.PaymentStatus == "Unpaid"|| $scope.Action == "duplicate") && $scope.Supplier.PaymentTerms && $scope.Supplier.PaymentPeriod && $scope.Supplier.LatePaymentRate) {
                                $scope.Expense.ExpenseHeader.PaymentTerms = {
                                    PaymentTerm: $scope.Supplier.PaymentTerms,
                                    PaymentPeriod: $scope.Supplier.PaymentPeriod,
                                    LatePaymentRate: $scope.Supplier.LatePaymentRate
                                }
                            }
                            
                            LatePayments.CalculateLatePaymentCharge($scope.Expense.Supplier.UniqueID, $scope.Expense.ExpensePayment.Balance, $scope.Expense.ExpenseHeader.PaymentDate, $scope.Expense.ExpenseHeader.PaymentTerms,"Expense", function (LatePaymentDetails) {
                                //$scope.DaysOverDue = LatePaymentDetails.DaysOverDue;
                                $scope.ExpenseLateFees = LatePaymentDetails.LateFee;
                                $scope.LateFees = LatePaymentDetails.LateFee;
                                $scope.AmountDue = $scope.Expense.ExpensePayment.Balance + LatePaymentDetails.LateFee;
                                console.log("Late Payment Details: " + JSON.stringify(LatePaymentDetails));
                                $scope.Expense.ExpensePayment.Balance = $scope.AmountDue;
                                //format Expense amounts
                                $scope.FormartAllAmounts(Controls.ToCurrency);
                                $scope.$apply();
                            });

                            if ($scope.Action == "duplicate") {

                                $scope.Expense.ExpensePayment.PaymentStatus = "Unpaid";
                                console.log("Duplicate Expense");
                                delete $scope.Expense.UniqueID;
                                delete $scope.Expense._id;
                                $scope.ExpenseID = null;
                                $scope.Expense.ExpensePayment = new ExpenseFunc.Expense().ExpensePayment;
                                GenerateExpenseNumber();
                                $scope.Action = "view";
                                console.log("Action is now: " + $scope.Action);
                                //set dates to today
                                $("#expenseDate").datepicker({
                                    format: 'dd/mm/yyyy'
                                }).datepicker("setDate", new Date());


                                if ($scope.Supplier.PaymentTerms && $scope.Supplier.PaymentPeriod > 0) {

                                    var dateMomentObject = moment(new Date(), "DD/MM/YYYY").add($scope.Supplier.PaymentPeriod, 'day');
                                    $scope.Expense.ExpenseHeader.PaymentDate = dateMomentObject.toDate(); // convert moment.js object to Date object
                                    $("#paymentDate").datepicker({
                                        format: 'dd/mm/yyyy'
                                    }).datepicker("setDate", dateMomentObject.toDate());
                                } else {

                                    $("#paymentDate").datepicker({
                                        format: 'dd/mm/yyyy'
                                    }).datepicker("setDate", new Date());
                                }
                            } else {
                                GetPayments();
                                console.log("Getting payments");
                            }
                            formLayout("Unpaid");
                            $scope.$apply();
                        });

                        //Load Expense LineItems
                        $scope.ExpenseCurrency = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Expense.ExpenseHeader.ExpenseCurrency; });
                        //$("#ExpenseCurrency").val($scope.ExpenseCurrency ).change();
                        $scope.CurrencySymbol = $scope.ExpenseCurrency.Symbol;
                        $scope.ExpenseCompanyCurrency = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Expense.ExpenseHeader.CompanyCurrency; });

                        //Expense Preview date formats
                        if ($scope.Expense.ExpenseCreationDate != "" && $scope.Expense.ExpenseCreationDate != null) {
                            $scope.Preview_ExpenseCreationDate = $scope.Expense.ExpenseCreationDate.toDateString();
                        }
                        if ($scope.Expense.ExpenseSendDate != "" && $scope.Expense.ExpenseSendDate != null) {
                            $scope.Preview_ExpenseSendDate = $scope.Expense.ExpenseSendDate.toDateString();
                        }

                        if ($scope.Expense.ExpensePayment.PaymentDate != "" && $scope.Expense.ExpensePayment.PaymentDate != null) {
                            $scope.Preview_ActualPaymentDate = $scope.Expense.ExpensePayment.PaymentDate.toDateString();
                        }

                        //$scope.Preview_DueDate = $scope.Expense.ExpenseHeader.PaymentDate.toDateString();

                        /*Handle Date formats
                        $("#expenseDate").datepicker({
                            format: 'dd/mm/yyyy'
                        });
                        $("#paymentDate").datepicker({
                            format: 'dd/mm/yyyy'
                        });
                        */
                        $("#expenseDate").datepicker("setDate", Controls.FormatDate($scope.Expense.ExpenseHeader.ExpenseDate));
                        $("#paymentDate").datepicker("setDate", Controls.FormatDate($scope.Expense.ExpenseHeader.PaymentDate));

                        //calculate Expense amounts
                        //$scope.CalculateLineAmounts();

                        //Calculate expense Amounts
                        $scope.ExpenseBalance = $scope.Expense.ExpensePayment.Balance;
                        //Calculate Amounts Due & paid
                        $scope.AmountPaid = parseFloat($scope.Expense.ExpenseHeader.ExpenseAmount - $scope.Expense.ExpensePayment.Balance).toFixed(2);
                        $scope.AmountDue = parseFloat($scope.Expense.ExpenseHeader.ExpenseAmount - $scope.AmountPaid).toFixed(2);
                        $scope.ConvertedAmount = parseFloat($scope.Expense.ExpenseHeader.ExpenseAmount * $scope.Expense.ExpenseHeader.ExchangeRate).toFixed(2);

                        //format expense amounts
                        //$scope.FormartAllAmounts(Controls.ToCurrency);


                        $scope.$apply();
                        //Set Layout based on the expense status
                        console.log("Setting Layout:" + $scope.Expense.ExpensePayment.PaymentStatus);
                        formLayout($scope.Expense.ExpensePayment.PaymentStatus);
                    }
                });

            } else {
                GenerateExpenseNumber();
                formLayout("Unpaid");

                //set dates to today
                $("#expenseDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", new Date());
                $("#paymentDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", new Date());
            }
        }

        //Get All Products
        function GetProducts() {
            ProdctFunc.getProductToBuy("", function (items) {

                productSrchObj = items;

                $scope.ProductsTable = $('#ItemsRslt').DataTable({
                    data: items,
                    rowId: "UniqueID",
                    columns: [
                        { 'data': 'Name' },
                        { 'data': 'Description' },
                        { 'data': 'Cost', 'render': function (amount) { return $scope.CurrencySymbol + Controls.ToCurrency(amount); } }
                    ],
                    select: { style: 'multi' },
                    buttons: [
                        'selectRows'
                    ]
                });

                $('#ItemsRslt tbody').on('click', 'tr', function () {

                    var selectedRow = $scope.ProductsTable.row(this).id();
                    var id = selectedRow;
                    console.log("Selected product: " + selectedRow);
                    selectProduct(selectedRow);
                    $scope.ProductsTable.search('').draw();
                });
            });
        }

        //Set document layout
        function Run() {
            GetExpense();
            $("#Alert").hide();
            $("#Preview_Alert").hide();

            /*$("#Currency").select2({
                placeholder: "Select Currency",
                allowClear: true
            });*/
        }
        //Add rows to items table >>
        $(document).ready(function () {

        });

        //Set layout for expense form based on status
        function formLayout(status) {

            $("#SelectItems").hide();
            $("#addrowBtn").show();
            $("#AddSuppliers").hide();

            if (!$scope.Settings.ExpiryDate || $scope.Settings.ExpiryDate < new Date()) {
                $("#subscriptionAlert").show();
            } else {
                $("#subscriptionAlert").hide();
            }

            switch (status) {
                case "Unpaid":

                    if (!$scope.Settings.ExpiryDate || $scope.Settings.ExpiryDate < new Date()) {
                        $("#ExpenseCreate").hide();
                        $("#Cancel").show();
                        $("#addDifSupplier").hide();
                        $("#editSupplier").hide();
                        $("#addrow").hide();
                        $("#ExpenseHederSection *").attr("disabled", "disabled").off('click');
                        $("#ExpenseLinesSection *").attr("disabled", "disabled").off('click');
                        $("i").hide();
                        $("#addrowBtn").hide();
                    } else {
                        $("#SaveDraft").show();
                        $("#ProcessPayment").hide();
                        $("#ExpenseCreate").show();
                        $("#Cancel").show();
                        $("#addDifSupplier").show();
                        $("#editSupplier").show();
                        $("#addrow").show();
                        $("#ExpenseHederSection *").removeAttr("disabled", "disabled").on('click');
                        $("#ExpenseLinesSection *").removeAttr("disabled", "disabled").on('click');
                        $("i").show();
                        $("#addrowBtn").show();
                        //$("input, textarea").prop("readonly", false);
                    }
                    break;

                case "Paid":
                    $("#ExpenseCreate").hide();
                    $("#Cancel").show();
                    $("#addDifSupplier").hide();
                    $("#editSupplier").hide();
                    $("#addrow").hide();
                    $("#ExpenseHederSection *").attr("disabled", "disabled").off('click');
                    $("#ExpenseLinesSection *").attr("disabled", "disabled").off('click');
                    $("i").hide();
                    $("#addrowBtn").hide();
                    break;

                default:
                    console.log("Error setting layout");
            }
        }

        //Reset the modal popup
        function ResetModal() {
            $("#AddSuppliers").hide();
            $("#displaySuppliers").show();
            $("#AddSupplierFooter").show();

            //Clear all supplier details fields
            $('#SearchSupplier').val("");
            $('#displaySuppliers').find('input[type=search]').val('').change();
            $('#sameASbilling').prop('checked', false);
            (function () {
                $('#tab1-tab').tab('show');
            })();

            $("#billingInfo").show();
            document.getElementById("Supplier").classList.remove("is-invalid");
            edit = false;
            console.log("Reset the supplier modal");
        }
        //***********************************************************************************************************************************************************************************/
        //************************************* Supplier Functions ****************************************************************************************************************/
        function GetSuppliers() {
            SupplierFunc.getSupplier("", function (suppliers) {

                if (suppliers.length == 0) {

                    console.log("No suppliers found");
                }

                var table = $('#supplierRslt').DataTable({
                    data: suppliers,
                    rowId: "UniqueID",
                    columns: [
                        { 'data': 'SupplierName' },
                        { 'data': 'ContactFname', 'render': function (data, type, row) { return row.ContactFname + " " + row.ContactLname; } },
                        { 'data': 'Phone' },
                        { 'data': 'Currency' }
                    ],
                    bDestroy: true
                });

                $('#supplierRslt tbody').unbind("click").on('click', 'tr', function () {
                    var selectedRow = table.row(this).id();
                    console.log(selectedRow);

                    var id = selectedRow;
                    $scope.Expense.Supplier = suppliers.find(function (obj) { return obj.UniqueID === id; });
                    $scope.Expense.ExpenseHeader.ExpenseCurrency = $scope.Expense.Supplier.Currency;
                    $scope.Expense.ExpenseHeader.PaymentTerms = {
                        PaymentTerm: $scope.Expense.Supplier.PaymentTerms,
                        PaymentPeriod: $scope.Expense.Supplier.PaymentPeriod,
                        LatePaymentRate: $scope.Expense.Supplier.LatePaymentRate
                    }
                    $scope.CurrencyChange();

                    var dateMomentObject = moment($scope.FormExpenseDate, "DD/MM/YYYY").add($scope.Expense.ExpenseHeader.PaymentTerms.PaymentPeriod, 'day')
                    $scope.Expense.ExpenseHeader.PaymentDate = dateMomentObject.toDate(); // convert moment.js object to Date object
                    $("#paymentDate").datepicker({
                        format: 'dd/mm/yyyy'
                    }).datepicker("setDate", dateMomentObject.toDate());

                    //$("#ExpenseCurrency").val($scope.ExpenseCurrency).change();
                    $scope.$apply();
                    ResetModal();
                    $('#SupplierModal').modal('hide');
                    table.search('').draw();
                    console.log("$scope.Supplier: " + JSON.stringify($scope.Expense.Supplier));
                });

            });
        }
        $scope.AddSupplier = function () {
            GetSuppliers();
            $("#AddSuppliers").hide();
            $("#displaySuppliers").show();
            $("#AddSupplierFooter").show();
            console.log("add Supplier btn");
        }

        $scope.SaveSupplier = function () {

            var requiredFields = ["Currency", "SupplierName"], validateFields = [{ Name: "Email", Type: "email" }, { Name: "Phone", Type: "phone" }];

            if (!Controls.CheckRequiredFields(requiredFields) || !Controls.Validatefields(validateFields)) {
                console.log("Invalid/missing info!")
            } else {

                SupplierFunc.addSupplier($scope.Supplier, function (err, Supplier) {
                    if (err != null) {
                        console.log("Error retriving supplier: " + err);
                    } else {
                        Controls.showAlert("Alert", "alert-success", "Supplier " + "'" + Supplier.SupplierName + "'" + " succesfully created", "message", true);
                        console.log("$scope.Expense b4: " + JSON.stringify($scope.Expense));
                        $scope.Expense.Supplier = Supplier; //Update Current Supplier
                        $scope.Expense.ExpenseHeader.ExpenseCurrency = $scope.Expense.Supplier.Currency;
                        $scope.CurrencyChange();
                        console.log("$scope.Expense: " + JSON.stringify($scope.Expense));
                        $scope.Supplier = new SupplierFunc.Supplier(); //Clear New Supplier
                        $('#SupplierModal').modal("toggle");

                        GetSuppliers();
                    }
                    $scope.$apply();
                });
            }
        }

        $scope.EditSupplier = function () {

            SupplierFunc.getSupplierById($scope.Expense.Supplier.UniqueID, function (err, docs) {
                if (err != null) {
                    console.log("Error retriving supplier: " + err);
                } else {

                    $scope.Supplier = docs[0];
                    console.log("Edit: " + $scope.Supplier.SupplierName);
                    $scope.$apply();
                    $("#Currency").val($scope.Supplier.Currency).change();
                    $('#SupplierModal').modal('toggle');
                    $("#AddSuppliers").show();
                    $("#displaySuppliers").hide();
                    $("#AddSupplierFooter").hide();
                }

            });
        }

        $scope.UpdateSupplier = function () {

            console.log("Update Supplier" + JSON.stringify($scope.Supplier));
            ///$scope.NewSupplier = JSON.parse(JSON.stringify($scope.Supplier));
            var requiredFields = ["SupplierName", "Currency"], validateFields = [{ Name: "Email", Type: "email" }, { Name: "Phone", Type: "phone" }]

            if (!Controls.CheckRequiredFields(requiredFields) || !Controls.Validatefields(validateFields)) {
                console.log("Invalid/missing info!")
            } else {


                SupplierFunc.updateSupplier($scope.Supplier.UniqueID, $scope.Supplier, function (err, num, Supplier) {
                    if (err != null) {
                        console.log("Error retriving supplier: " + err);
                    } else {
                        Controls.showAlert("Alert", "alert-success", "Supplier " + "'" + Supplier.SupplierName + "'" + " succesfully updated", "message", true);
                        $scope.Expense.Supplier = Supplier; //Update Current Supplier
                        $scope.Expense.ExpenseHeader.ExpenseCurrency = $scope.Expense.Supplier.Currency;
                        $scope.CurrencyChange();
                        //$("#ExpenseCurrency").val($scope.Expense.ExpenseHeader.ExpenseCurrency).change();
                        console.log("Updated Supplier: " + Supplier);
                        $scope.Supplier = new SupplierFunc.Supplier(); //Clear New Supplier            
                        $('#SupplierModal').modal('toggle');
                        $scope.$apply();
                        GetSuppliers();
                    }
                });
            }
        };


        $scope.showAddNewSupplier = function () {

            $("#AddSuppliers").show();
            $("#displaySuppliers").hide();
            $("#AddSupplierFooter").hide();
            $('#Supplier_error').hide();
            document.getElementById("Supplier").classList.remove("is-invalid");
            $scope.Supplier = new SupplierFunc.Supplier();
        }
        //************************************************************************************************************************************************************************************
        //***************************************************************************************** Expense Header Functions *****************************************************************
        function GenerateExpenseNumber() {

            Date.prototype.yyyymmdd = function () {
                var mm = this.getMonth() + 1; // getMonth() is zero-based
                var dd = this.getDate();

                return [this.getFullYear(),
                (mm > 9 ? '' : '0') + mm,
                (dd > 9 ? '' : '0') + dd
                ].join('');
            };

            var date = new Date();
            getExpenseCount(date.yyyymmdd());
            function getExpenseCount(date) {
                ExpenseFunc.CheckExpenseNum(date, callback);
                function callback(count) {
                    console.log("Expense Number Count:" + count);
                    $scope.Expense.ExpenseHeader.ExpenseNumber = date + " - " + (count + 1);
                    $scope.$apply();
                }
            }
        }


        $scope.CurrencyChange = function () {

            $scope.CurrencySymbol = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Expense.ExpenseHeader.ExpenseCurrency; }).Symbol;
            if ($scope.Expense.ExpenseHeader.ExpenseCurrency == $scope.Expense.ExpenseHeader.CompanyCurrency) {
                $scope.Expense.ExpenseHeader.ExchangeRate = 1
            } else {
                $scope.Expense.ExpenseHeader.ExchangeRate = "";
            }
            $scope.exchangeRateChange();
        }

        $scope.exchangeRateChange = function (index) {

            console.log("$scope.Expense.ExpenseHeader.ExchangeRate: " + $scope.Expense.ExpenseHeader.ExchangeRate);

            if ($scope.Expense.ExpenseHeader.ExchangeRate == null || $scope.Expense.ExpenseHeader.ExchangeRate == "" || $scope.Expense.ExpenseHeader.ExchangeRate == 0) {
                $scope.ConvertedAmount = 0.00;
                Controls.SetError("exchangeRate", "Invalid Exchange Rate");
                console.log("Currency change: ");
            } else {
                $scope.ConvertedAmount = Controls.ToCurrency(Controls.ToNumber($scope.Expense.ExpenseHeader.ExpenseAmount) * $scope.Expense.ExpenseHeader.ExchangeRate);
                Controls.SetError("exchangeRate", "");
            }
        }


        $scope.ExpenseDateOnChange = function () {
            if ($("#expenseDate").val() == "" && !$scope.Expense.ExpenseHeader.ExpenseDate) {
                $("#expenseDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", new Date());
            }

            var dateMomentObject = moment($scope.FormExpenseDate, "DD/MM/YYYY"); // 1st argument - string, 2nd argument - format
            $scope.Expense.ExpenseHeader.ExpenseDate = dateMomentObject.toDate(); // convert moment.js object to Date object

            if ($scope.Expense.ExpenseHeader.PaymentTerms && $scope.Expense.ExpenseHeader.PaymentTerms.PaymentPeriod > 0) {
                console.log("$scope.Expense.ExpenseHeader.PaymentTerms.PaymentPeriod" + $scope.Expense.ExpenseHeader.PaymentTerms.PaymentPeriod);
                var DueDateMomentObject = moment($scope.FormExpenseDate, "DD/MM/YYYY").add($scope.Expense.ExpenseHeader.PaymentTerms.PaymentPeriod, 'day');
                $scope.Expense.ExpenseHeader.PaymentDate = DueDateMomentObject.toDate(); // convert moment.js object to Date object
                $("#paymentDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", DueDateMomentObject.toDate());
            }
        }

        $scope.PaymentDateOnChange = function () {
            if ($("#paymentDate").val() == "" && !$scope.Expense.ExpenseHeader.PaymentDate) {
                $("#paymentDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", new Date());
            }
            var dateMomentObject = moment($scope.FormPaymentDate, "DD/MM/YYYY"); // 1st argument - string, 2nd argument - format
            $scope.Expense.ExpenseHeader.PaymentDate = dateMomentObject.toDate(); // convert moment.js object to Date object
        }

        //*****************************************************************************************************************************************************************************************
        //*************************************************************************************************** Expense LineItem Functions **********************************************************

        //Add Item from catalog
        function selectProduct(id) {
            var item = null;
            $scope.CurrentProduct = productSrchObj.find(function (obj) { return obj.UniqueID === id; });
            item = new ExpenseFunc.LineItem();
            console.log("Selected product: " + JSON.stringify($scope.CurrentProduct));
            item.Item = $scope.CurrentProduct.Name;
            item.Description = $scope.CurrentProduct.Description;
            item.Cost = $scope.CurrentProduct.Cost;
            item.ProductId = $scope.CurrentProduct.UniqueID;
            item.ExpenseAccount = $scope.CurrentProduct.ExpenseAccount;
            if ($scope.CurrentProduct.SalesTax != "" && $scope.CurrentProduct.SalesTax != null) {
                item.TaxName = $scope.CurrentProduct.SalesTax;
                item.TaxRate = parseFloat($scope.Tax.find(function (obj) { return obj.Name === item.TaxName; }).TaxRate);
                item.TaxId = $scope.Tax.find(function (obj) { return obj.Name === item.TaxName; }).UniqueID;
                item.TaxAccountId = $scope.Tax.find(function (obj) { return obj.Name === item.TaxName; }).AccountId;
            }

            $scope.ExpenseLineItems.push(item);
            console.log("Added item: " + JSON.stringify(item));
            $("#SelectItems").hide();
            $("#addrowBtn").show();
            $scope.CalculateLineAmounts();
            $scope.$apply();
        }

        $scope.addItem = function () {

            if ($scope.ProductsTable != null) {
                $scope.ProductsTable.destroy();
                $('#ItemsRslt tbody').off('click');
            }
            GetProducts();

            console.log("show products");
            $("#SelectItems").show();
            $("#addrowBtn").hide();
            document.getElementById("SelectItems").scrollIntoView();
        };

        $scope.AddNonCatalogItem = function () {

            var newLineItem = new ExpenseFunc.LineItem();
            console.log("ExpenseLine Obj: " + JSON.stringify(newLineItem));
            newLineItem.ExpenseAccount = $scope.ExpenseAccount.find(function (obj) { return obj.AccountName == "Uncategorized Expense"; }).UniqueID;
            $scope.ExpenseLineItems.push(newLineItem);
            console.log("ExpenseLine Obj: " + JSON.stringify(newLineItem));
            $("#SelectItems").hide();
            $("#addrowBtn").show();
        };

        $scope.CancelAddNewPrdct = function () {

            $("#SelectItems").hide();
            $("#addrowBtn").show();
        };

        $scope.deleteItem = function (index) {
            //Remove the item from Array using Index.
            $scope.ExpenseLineItems.splice(index, 1);
            $scope.CalculateLineAmounts();
        }

        $scope.lineValueChange = function (index) {
            $scope.CalculateLineAmounts();
        }

        //Culculate totals and Taxes
        $scope.CalculateLineAmounts = function () {
            var ExpenseSubtotal = 0, ExpenseTotal = 0;
            $scope.Expense.ExpenseTaxes = [];
            console.log("ExpenseLineItems:" + JSON.stringify($scope.ExpenseLineItems));
            $scope.ExpenseLineItems.forEach(item => {
                var itemTax = item.TaxName;
                var ItemTotal = 0;
                item.Amount = 0;
                var itemCost = Controls.ToNumber(item.Cost);

                if (item.Quantity != 0 && typeof itemCost == "number" && itemCost != 0) {

                    ItemTotal = itemCost * item.Quantity;

                    if ($scope.Tax && item.TaxName != "" && item.TaxName != null) {
                        item.TaxRate = parseFloat($scope.Tax.find(function (obj) { return obj.Name === itemTax; }).TaxRate);
                        item.TaxAmount = (ItemTotal * (item.TaxRate / 100)).toFixed(2);
                        item.TaxId = $scope.Tax.find(function (obj) { return obj.Name === item.TaxName; }).UniqueID;
                        item.TaxAccountId = $scope.Tax.find(function (obj) { return obj.Name === item.TaxName; }).AccountId;
                    } else {
                        item.TaxAmount = (0).toFixed(2);
                        item.TaxName = "";
                        item.TaxRate = "";
                        item.TaxId = "";
                        item.TaxAccountId = "";
                        console.log("Line has no tax");
                    }

                    item.Amount = ItemTotal.toFixed(2);
                    ExpenseSubtotal += parseFloat(item.Amount);
                    ExpenseTotal += parseFloat(item.Amount) + parseFloat(item.TaxAmount);
                    console.log("check ExpenseTotal: " + ExpenseTotal);
                    var itemTax = item.TaxName;
                    var itemTaxObj = $scope.Tax.find(function (obj) { return obj.Name === itemTax; });
                    if (!$scope.Expense.ExpenseTaxes.includes(itemTaxObj) && itemTaxObj != null && itemTax != "") {
                        itemTaxObj.TotalTaxAmount = parseFloat(item.TaxAmount).toFixed(2);
                        $scope.Expense.ExpenseTaxes.push(itemTaxObj);

                    } else if ($scope.Expense.ExpenseTaxes.length > 0 && itemTax != "") {
                        itemTaxObj = $scope.Expense.ExpenseTaxes.find(function (obj) { return obj.Name === itemTax; });
                        console.log("itemTaxObj:" + JSON.stringify(itemTaxObj));
                        itemTaxObj.TotalTaxAmount = (parseFloat(itemTaxObj.TotalTaxAmount) + parseFloat(item.TaxAmount)).toFixed(2);
                    }
                }
            });

            $scope.Expense.ExpenseHeader.ExpenseNetAmount = parseFloat(ExpenseSubtotal).toFixed(2);
            $scope.Expense.ExpenseHeader.ExpenseAmount = parseFloat(ExpenseTotal).toFixed(2);
            $scope.AmountDue = parseFloat($scope.Expense.ExpenseHeader.ExpenseAmount - $scope.AmountPaid).toFixed(2);
            $scope.ConvertedAmount = parseFloat($scope.Expense.ExpenseHeader.ExpenseAmount * $scope.Expense.ExpenseHeader.ExchangeRate).toFixed(2);
            $scope.FormartAllAmounts(Controls.ToCurrency);
            $scope.Expense.ExpenseTaxes = JSON.parse(angular.toJson($scope.Expense.ExpenseTaxes));
        }

        //Formart Expense Ammounts
        $scope.FormartAllAmounts = (Format, action) => {
            var taxLines = $scope.Expense.ExpenseTaxes
            for (var i = 0; i < Object.keys(taxLines).length; i++) {
                taxLines[i].TaxRate = Controls.ToNumber(taxLines[i].TaxRate)
                taxLines[i].TotalTaxAmount = Format(taxLines[i].TotalTaxAmount); // format amount as currency
            }
            //Retrive Expense taxes
            var lines = $scope.ExpenseLineItems;
            for (var i = 0; i < Object.keys(lines).length; i++) {
                if (action == "Save") {
                    lines[i].Quantity = Controls.ToNumber(lines[i].Quantity);
                    lines[i].Cost = Format(lines[i].Cost);
                }

                lines[i].TaxAmount = Format(lines[i].TaxAmount);
                lines[i].Amount = Format(lines[i].Amount); // format amount as currency
            }


            $scope.Expense.ExpenseHeader.ReferenceCurrencyRate = Controls.ToNumber($scope.Expense.ExpenseHeader.ReferenceCurrencyRate);
            $scope.Expense.ExpenseHeader.ExchangeRate = Controls.ToNumber($scope.Expense.ExpenseHeader.ExchangeRate);

            console.log("$scope.Expense.ExpenseHeader.ExpenseNetAmount b4: " + $scope.Expense.ExpenseHeader.ExpenseNetAmount);
            $scope.Expense.ExpenseHeader.ExpenseNetAmount = Format($scope.Expense.ExpenseHeader.ExpenseNetAmount);
            $scope.ExpenseBalance = Format($scope.ExpenseBalance);
            console.log("$scope.Expense.ExpenseHeader.ExpenseNetAmount: " + $scope.Expense.ExpenseHeader.ExpenseNetAmount);
            $scope.Expense.ExpenseHeader.ExpenseAmount = Format($scope.Expense.ExpenseHeader.ExpenseAmount);
            $scope.AmountPaid = Format($scope.AmountPaid);
            $scope.AmountDue = Format($scope.AmountDue);
            $scope.ExpenseLateFees = Format($scope.ExpenseLateFees);

            console.log("ConvertedAmount" + $scope.Expense.ExpenseHeader.ExpenseAmount + " * " + $scope.Expense.ExpenseHeader.ExchangeRate);
            $scope.ConvertedAmount = Format($scope.ConvertedAmount);
        };

        //*****************************************************************************************************************************************************************************************
        //*****************************************************************  Button Functions *****************************************************************************************************
        $scope.DiscardChanges = function () {
            GetExpense();
        };

        /*********************************************************************** Create Expense *********************************************************** */

        $scope.CreateExpense = function () {


            //format amounts to numbers
            $scope.FormartAllAmounts(Controls.ToNumber, "Save");
            console.log("CreateExpense: " + JSON.stringify($scope.Expense));

            if ($scope.Expense.Supplier.SupplierName != "" && !isNaN($scope.Expense.ExpenseHeader.ExchangeRate) && $scope.Expense.ExpenseHeader.ExchangeRate > 0) {
                console.log("No Error");
                $scope.Expense.ExpenseHeader.PaymentDate = new Date($scope.Expense.ExpenseHeader.PaymentDate);
                $scope.Expense.ExpenseHeader.ExpenseDate = new Date($scope.Expense.ExpenseHeader.ExpenseDate);
                $scope.Expense.ExpenseLines = $scope.ExpenseLineItems;

                $scope.Expense.ExpenseTaxes = JSON.parse(angular.toJson($scope.Expense.ExpenseTaxes));

                $scope.Expense.ExpensePayment.PaymentStatus = "Unpaid";
                $scope.Expense.ExpensePayment.Balance = $scope.Expense.ExpenseHeader.ExpenseAmount;
                $scope.Expense.ExpenseCreationDate = new Date();

                var expenseTocreate = JSON.parse(JSON.stringify($scope.Expense));

                expenseTocreate.ExpenseHeader.PaymentDate = new Date(expenseTocreate.ExpenseHeader.PaymentDate);
                expenseTocreate.ExpenseHeader.ExpenseDate = new Date(expenseTocreate.ExpenseHeader.ExpenseDate);

                expenseTocreate.CreatedBy = $rootScope.CurrentUser.Username;

                $scope.Expense.UniqueID == null ? ExpenseFunc.CreateExpense(expenseTocreate, ExpenseinsertCallback) : ExpenseFunc.updateExpense(expenseTocreate.UniqueID, expenseTocreate, ExpenseinsertCallback);
                document.getElementById("AddCstBtn").classList.remove("is-invalid");
                document.getElementById("paymentDate").classList.remove("is-invalid");
                document.getElementById("AddCstBtn").classList.remove("is-invalid");

            } else {
                if ($scope.Expense.Supplier.SupplierName == "") {
                    console.log("Error 1");
                    Controls.showAlert("Alert", "alert-danger", "A supplier is required!", "message", true);
                    //$("#AddCstBtn").css({ style: "border-style: solid; border-color: red;" });
                }
                else if ($scope.Expense.ExpenseHeader.PaymentDate == "") {
                    console.log("Error 2");
                    document.getElementById("paymentDate").classList.add("is-invalid");
                    Controls.showAlert("Alert", "alert-danger", "Payment date is required if payment is not made!", "message", true);
                }
                else if ($scope.Expense.ExpenseHeader.ExpenseDate == "") {
                    console.log("Error 3");
                    document.getElementById("expenseDate").classList.add("is-invalid");
                    Controls.showAlert("Alert", "alert-danger", "Expense Date is required", "message", true);
                } else {
                    Controls.showAlert("Alert", "alert-danger", "Check Exchange Rate", "message", true);
                }
                console.log("Error Saving expense");
            }

            function ExpenseinsertCallback(err, doc) {
                if (err) {
                    alerttype = "alert-danger";
                    message = "Error Saving Expense: " + err;
                    console.log("Error saving expsense" + err)
                    Controls.showAlert("Alert", "alert-danger", "Expense: not succesfully saved", "message", true);
                } else {


                    console.log("Created Expense : ", JSON.stringify(doc));

                    $scope.Expense.UniqueID = doc.UniqueID;
                    // Create transaction for each line
                    var linesUpdated = 0;
                    doc.ExpenseLines.forEach(function (item, index, arr) {
                        console.log("line :" + JSON.stringify(item));
                        console.log("Ïtem", item.Item);

                        var LineItemTransaction = {
                            AccountId: item.ExpenseAccount,
                            Date: doc.ExpenseHeader.ExpenseDate,
                            DocumentReference: doc.ExpenseHeader.ExpenseNumber,
                            DocumentId: doc.UniqueID,
                            Details: item.Item + " " + item.Description,
                            Credit_CompanyCurrencyAmount: "",
                            Credit_AccountCurrencyAmount: "",
                            Credit_ReferenceCurrencyAmount: "",
                            Debit_CompanyCurrencyAmount: item.Amount * doc.ExpenseHeader.ExchangeRate,
                            Debit_AccountCurrencyAmount: item.Amount * doc.ExpenseHeader.ExchangeRate,
                            Debit_ReferenceCurrencyAmount: item.Amount * doc.ExpenseHeader.ReferenceCurrencyRate,
                        }

                        var LineTaxTransaction = {
                            AccountId: item.TaxAccountId,
                            Date: doc.ExpenseHeader.ExpenseDate,
                            DocumentReference: doc.ExpenseHeader.ExpenseNumber,
                            DocumentId: doc.UniqueID,
                            Details: item.Item + " " + item.Description,
                            Credit_CompanyCurrencyAmount: "",
                            Credit_AccountCurrencyAmount: "",
                            Credit_ReferenceCurrencyAmount: "",
                            Debit_CompanyCurrencyAmount: item.TaxAmount * doc.ExpenseHeader.ExchangeRate,
                            Debit_AccountCurrencyAmount: item.TaxAmount * doc.ExpenseHeader.ExchangeRate,
                            Debit_ReferenceCurrencyAmount: item.TaxAmount * doc.ExpenseHeader.ReferenceCurrencyRate,
                        }

                        TransactionFunc.RecordTransaction(LineItemTransaction, LineTaxTransaction, function (err, LineItemTransactionId, LineTaxTransactionId) {

                            if (err) {
                                console.log("error creating transaction :" + err);
                            } else {
                                linesUpdated++;
                                arr[index].TransactionID = LineItemTransactionId;
                                arr[index].TaxTransactionID = LineTaxTransactionId;
                                console.log("Updated " + item.Item + " with ID " + item.TransactionID);

                                //Update Expense once all items are done
                                if (linesUpdated == doc.ExpenseLines.length) {
                                    //Set date to correct format
                                    doc.ExpenseHeader.PaymentDate = new Date(doc.ExpenseHeader.PaymentDate);
                                    doc.ExpenseHeader.ExpenseDate = new Date(doc.ExpenseHeader.ExpenseDate);
                                    doc.ExpenseCreationDate = new Date();

                                    //Record Accounts RecievableTransaction
                                    var AccountsPayableTransaction = {
                                        AccountId: $scope.AccountsPayableAccountId,
                                        Date: doc.ExpenseHeader.ExpenseDate,
                                        DocumentReference: "AP-" + doc.ExpenseHeader.ExpenseNumber,
                                        DocumentId: doc.UniqueID,
                                        Details: doc.Supplier.SupplierName + " - " + doc.ExpenseHeader.ExpenseNumber,
                                        Credit_CompanyCurrencyAmount: doc.ExpenseHeader.ExpenseAmount * doc.ExpenseHeader.ExchangeRate,
                                        Credit_AccountCurrencyAmount: doc.ExpenseHeader.ExpenseAmount * doc.ExpenseHeader.ExchangeRate,
                                        Credit_ReferenceCurrencyAmount: doc.ExpenseHeader.ExpenseAmount * doc.ExpenseHeader.ReferenceCurrencyRate,
                                        Debit_CompanyCurrencyAmount: "",
                                        Debit_AccountCurrencyAmount: "",
                                        Debit_ReferenceCurrencyAmount: "",
                                    }

                                    TransactionFunc.RecordTransaction(AccountsPayableTransaction, null, function (err, TransactionId, TaxTransactionId) {
                                        if (err) {
                                            console.log("error creating Accounts Payable  transaction :" + err);
                                        } else {

                                            doc.AP_TransactionId = TransactionId;

                                            //$scope.FormartAllAmounts(Controls.ToNumber, "Save");
                                            ExpenseFunc.updateExpense(doc.UniqueID, doc, function (err, doc) {
                                                if (err) {
                                                    console.log("error updating Expense :" + err);
                                                } else {
                                                    console.log("Expense Transaction complete! " + doc);
                                                    console.log("Created Expense with UniqueID: " + doc.UniqueID);
                                                    doc.UniqueID = doc.UniqueID;
                                                    $scope.ExpenseID = doc.UniqueID;
                                                    $scope.$apply();
                                                    Controls.showAlert("Alert", "alert-success", "Expense: " + "'" + doc.ExpenseHeader.ExpenseNumber + "'" + " succesfully saved", "message", true);
                                                    console.log(JSON.stringify(CurrentSupplier));
                                                    GetExpense();
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        });
                    });
                }
            }
        }

        /***************************************************************************************************************************************************************/
        //************************************************************* Process Expense Payment **************************************************************************

        //Remove Payment
        $scope.RemovePayment = function () {

            $scope.Expense.ExpensePayment.Balance = Controls.ToNumber($scope.Expense.ExpensePayment.Balance) + Controls.ToNumber($scope.DeletePayment.AmountRecieved);
            //$scope.Expense.ExpensePayment.AmountRecieved = $scope.Expense.ExpensePayment.AmountRecieved - $scope.DeletePayment.AmountRecieved;

            if ($scope.Expense.ExpensePayment.Balance > 0.00) {
                $scope.Expense.ExpensePayment.PaymentStatus = "Unpaid";
                $scope.Expense.ExpenseState = "Unpaid";
                $scope.Expense.ExpensePayment.NextPaymentDate = new Date($scope.Expense.ExpenseHeader.PaymentDate);
                console.log("partial payment");
            } else {
                $scope.Expense.ExpensePayment.PaymentStatus = "Paid";
                $scope.Expense.ExpenseState = "Paid";
                $scope.Expense.ExpensePayment.NextPaymentDate = "";
            }

            console.log("Payment details:" + JSON.stringify($scope.Expense.ExpensePayment));
            TransactionFunc.deleteTransaction({ UniqueID: $scope.DeletePayment.TransactionId }, function (err, numRemoved) {

                if (err) {
                    console.log("error deleting transaction :" + err);
                } else {
                    console.log("Removed " + numRemoved + " Transactions");
                    //Delete Accounts Payable transaction
                    TransactionFunc.deleteTransaction({ DocumentReference: $scope.DeletePayment.TransactionId, DocumentId: $scope.DeletePayment.TransactionId }, function (err, numRemoved) {
                        if (err) {
                            console.log("error deleting transaction :" + err);
                        } else {
                            console.log("Removed " + numRemoved + " Transactions");
                        }
                    });
                    ExpensePaymentFunc.deletePayment($scope.DeletePayment.UniqueID, RemovePaymentCallBack, $scope.Expense.ExpensePayment, $scope.Expense.ExpenseState)
                }
            });

        };

        function RemovePaymentCallBack(numRemoved, PaymentInfo, ExpenseState, error) {
            if (error || numRemoved == 0) {
                Controls.showAlert("Alert", "alert-danger", "Error deleting payment: " + error, "message", false);
                GetExpense();

            } else {
                ExpenseFunc.expensePayment($scope.Expense.UniqueID, PaymentInfo, ExpenseState, UpdateExpensePaymentCallback);
            }
        }

        function UpdateExpensePaymentCallback(error) {
            if (error) {
                Controls.showAlert("Alert", "alert-danger", "Error updating payment: " + error, "message", false);
                GetExpense();
            } else {
                $('#DeletePayment').modal('toggle');
                Controls.showAlert("Alert", "alert-success", "Expense: " + "'" + $scope.Expense.ExpenseHeader.ExpenseNumber + "'" + " payment deleted", "message", true);
                GetExpense();
            }
        }
        function GetPayments() {

            //if ($scope.PaymentsTable != null) {
            //  $scope.PaymentsTable.destroy();
            //}

            ExpensePaymentFunc.getPayments($scope.ExpenseID, function (payments) {

                $scope.PaymentsTable = $('#ExpensePayments').DataTable({

                    data: payments,
                    rowId: "UniqueID",
                    columns: [
                        { 'data': 'PaymentDate', 'render': function (date) { return Controls.FormatDate(date); } },
                        { 'data': 'PaymentMethod', 'render': function (method) { return method ? method : ""; } },
                        { 'data': 'PaymentDetails', 'render': function (PaymentDetails) { return PaymentDetails ? PaymentDetails : ""; } },
                        { 'data': 'AmountRecieved', 'render': function (amount) { return $scope.CurrencySymbol + Controls.ToCurrency(amount); } },
                        {
                            'data': 'UniqueID', 'sortable': false, 'render': function (id) {

                                return '<div>' +
                                    '<i id="delete_' + id + '"><span class="glyphicon glyphicon-trash"></span></i></div>'
                            }
                        }
                    ],
                    paging: false,
                    searching: false,
                    lengthChange: false,
                    info: false,
                    bDestroy: true
                });
                $('#ExpensePayments tbody').unbind("click").on('click', 'i', function () {
                    //var supplierID = table.row(this).id();
                    console.log("button clicked is:" + $(this).attr('id'));
                    var buttonType = $(this).attr('id').split("_")[0];
                    var paymentId = $(this).attr('id').split("_")[1];
                    console.log(buttonType);
                    console.log("payment id is: " + paymentId);

                    switch (buttonType) {
                        case "delete":
                            console.log("delete action");
                            ExpensePaymentFunc.getPaymentById(paymentId, function (err, docs) {
                                if (err != null) {
                                    console.log("Error retriving payment: " + JSON.stringify(err));
                                } else {
                                    $scope.DeletePayment = docs[0];
                                    $scope.DeletePayment.AmountRecieved = Controls.ToCurrency($scope.DeletePayment.AmountRecieved);
                                    $scope.$apply();
                                    $('#DeletePayment').modal('toggle');
                                    console.log("delete payment: " + $scope.DeletePayment.AmountRecieved);
                                }
                            });
                            break;
                        default:
                            console.log("Unknown action");
                            break;
                    }
                });
            });
        }
        //******************************************************************************************************************************************************************************************
    }
});