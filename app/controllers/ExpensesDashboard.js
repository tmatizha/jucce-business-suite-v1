jbsApp.controller("ExpensesDashboardController", function ($scope, $rootScope, $timeout, $location) {

    //Check if user is signed-in
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {

        // Controller variables:
        var ExpenseFunc = require('./app/models/Expense.js');
        var ExpensePaymentFunc = require('./app/models/ExpensePayments.js');
        var SupplierFunc = require('./app/models/Suppliers.js');
        var AccountFunc = require('./app/models/ChartOfAccounts.js');
        var TransactionFunc = require('./app/models/Transactions.js');
        var CurrenciesList = require('./app/config/appfiles/Currencies.js');
        var LatePayments = require('./app/lib/LatePayments.js');
        var edit = false;

        //load Expense settings
        Settings.getSettings(function (err, AppSettings) {
            $scope.Settings = AppSettings;
            $scope.Settings.DailyExchangeRate = ($scope.Settings.DailyExchangeRate == null || $scope.Settings.DailyExchangeRate == "") ? 1 : $scope.Settings.DailyExchangeRate;
            GetSuppliers();
            GetAccounts();
            GetAPAccount();
            $scope.$apply();
        });

        //Get Expense Accounts values
        function GetAccounts() {
            AccountFunc.getAccount({ AccountType: "Cash and Bank" }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Cash and Bank Accounts found");
                    $scope.ExpenseAccounts = [];
                } else {

                    $scope.ExpenseAccounts = Accounts;
                }
            });
        }

        //Get Account for Accounts Payable
        function GetAPAccount() {
            AccountFunc.getAccount({ AccountName: "Accounts Payable" }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Accounts Payable Account found");
                    AccountFunc.CheckAccounts(GetAPAccount);
                } else {
                    $scope.AccountsPayableAccountId = Accounts[0].UniqueID;
                    console.log("Accounts Payable Accounts found:" + Accounts[0].UniqueID);
                }
            });
        }

        //******************To delete ***********************************
        ExpenseFunc.searchExpense({}, function (err, expenses) {
            if (err) {
                console.log("Error Getting Expense:" + err);
            } else {
                console.log("Get All expenses", JSON.stringify(expenses));
            }
        });
        //**************************************************************/





        //var Chart = require("chart.js");
        $("#Supplier").select2({
            placeholder: "Select Supplier",
            allowClear: true
        });
        $("#Alert").hide();
        var Expenses = [];
        //$scope.SearchTxt = "";
        ExpenseFunc.searchExpense({}, GetExpensesCallback);
        function GetExpensesCallback(err, expenses) {

            var LatepaymentAmounts = [];

            if (err) {
                console.log("Error Getting Expense:" + err);
            } else {

                var item = 0;
                expenses.forEach((expense, index) => {

                    var expenseLatePayment = {
                        expenseUniqueID: "",
                        latePaymentFee: 0,
                        amountDue: 0
                    }                    

                    LatePayments.CalculateLatePaymentCharge(expense.Supplier.UniqueID, expense.ExpensePayment.Balance, expense.ExpenseHeader.PaymentDate, expense.ExpenseHeader.PaymentTerms, "Expense", function (LatePaymentDetails) {

                        expenseLatePayment.UniqueID = expense.UniqueID;
                        expenseLatePayment.latePaymentFee = LatePaymentDetails.LateFee;
                        expenseLatePayment.amountDue = expense.ExpensePayment.Balance + LatePaymentDetails.LateFee;
                        LatepaymentAmounts.push(expenseLatePayment);

                        item++;
                        if (item == expenses.length) {

                            console.log("Late Payment Details: " + JSON.stringify(LatePaymentDetails));
                            callback();
                        }                        
                    });

                });

                function callback() {
                    $('#ExpensesTable').dataTable({
                        data: expenses,
                        scrollY: 500,
                        columns: [
                            { 'data': 'ExpenseHeader.ExpenseNumber', 'render': function (data, type, row) { return Controls.AsLink("addexpense", row.UniqueID, row.ExpenseHeader.ExpenseNumber); } },
                            { 'data': 'ExpenseHeader.ExpenseDate', 'render': function (data, type, row) { return Controls.AsLink("addexpense", row.UniqueID, Controls.FormatDate(row.ExpenseHeader.ExpenseDate)); } },
                            { 'data': 'Supplier.SupplierName', 'render': function (data, type, row) { return Controls.AsLink("addexpense", row.UniqueID, row.Supplier.SupplierName); } },
                            { 'data': 'ExpenseHeader.ExpenseAmount', 'render': function (data, type, row) { return Controls.AsLink("addexpense", row.UniqueID, CurrenciesList.Currencies.find(function (obj) { return obj.ISO === row.ExpenseHeader.ExpenseCurrency; }).Symbol + Controls.ToCurrency(row.ExpenseHeader.ExpenseAmount)); } },
                            {
                                'data': 'ExpenseHeader.ExpenseAmount', 'render': function (data, type, row) {
                                    var lateFee = LatepaymentAmounts.find(function (obj) {
                                        return obj.UniqueID == row.UniqueID;
                                    });

                                    lateFee = lateFee ? lateFee.latePaymentFee : 0;
                                    return Controls.AsLink("addexpense", row.UniqueID, CurrenciesList.Currencies.find(function (obj) { return obj.ISO === row.ExpenseHeader.ExpenseCurrency; }).Symbol + Controls.ToCurrency(lateFee));
                                }
                            },
                            {
                                'data': 'ExpensePayment.Balance', 'render': function (data, type, row) {
                                    var amountDue = LatepaymentAmounts.find(function (obj) {
                                        return obj.UniqueID == row.UniqueID;
                                    })

                                    amountDue = amountDue ? amountDue.amountDue : row.ExpensePayment.Balance;
                                    return Controls.AsLink("addexpense", row.UniqueID, CurrenciesList.Currencies.find(function (obj) { return obj.ISO === row.ExpenseHeader.ExpenseCurrency; }).Symbol + Controls.ToCurrency(amountDue));
                                }
                            },
                            // { 'data': 'ExpenseHeader.ExpenseAmount', 'render': function (data, type, row) { return Controls.AsLink("addexpense", row.UniqueID, CurrenciesList.Currencies.find(function (obj) { return obj.ISO === row.ExpenseHeader.ExpenseCurrency; }).Symbol + Controls.ToCurrency(row.ExpenseHeader.ExpenseAmount)); } },
                            // { 'data': 'ExpensePayment.Balance', 'render': function (data, type, row) { return Controls.AsLink("addexpense", row.UniqueID, CurrenciesList.Currencies.find(function (obj) { return obj.ISO === row.ExpenseHeader.ExpenseCurrency; }).Symbol + Controls.ToCurrency(row.ExpensePayment.Balance)); } },
                            {
                                'data': 'ExpensePayment.PaymentStatus', 'render': function (status) {
                                    var badge;
                                    status == "Paid" ? badge = '<span class="badge badge-pill badge-success" style="width:100%">Paid</span>' : badge = '<span class="badge badge-pill badge-secondary" style="width:100%">Unpaid</span>'
                                    return badge;
                                }
                            },
                            {
                                'data': 'UniqueID', 'sortable': false, 'render': function (data, type, row) {

                                    var disablepayment = row.ExpensePayment.PaymentStatus == "Paid" ? "disabled" : "";
                                    var mainButton;

                                    if (row.ExpensePayment.PaymentStatus == "Paid") {
                                        mainButton = '<a ng-reflect-router-link = "#!addexpense/" href="#!addexpense/' + row.UniqueID + '/view" class="btn btn-outline-default" style="color:#007bff;padding-left: 62px;">View</a>';
                                        view = '';
                                    } else {

                                        mainButton = '<button type="button" id="pay_' + row.UniqueID + '" ' + disablepayment + ' class="btn btn-outline-default" style="color: green;"><span class="glyphicon glyphicon-plus"style="margin-right: 5px;"></span><span >Payment</span></button>';
                                        view = '<li><a ng-reflect-router-link = "#!addexpense/" href="#!addexpense/' + row.UniqueID + '/view">View</a></li>';
                                    }

                                    return '<div class="btn-group">' +
                                        mainButton +
                                        '<button class="btn btn-sm btn-outline-default" type="button" data-toggle="dropdown">' +
                                        '<span class="glyphicon glyphicon-chevron-down"></span></button>' +
                                        '<ul class="dropdown-menu jc-dropdown link">' +
                                        view +
                                        '<li><a ng-reflect-router-link = "#!addexpense/" href="#!addexpense/' + row.UniqueID + '/duplicate">duplicate</a></li>' +
                                        '<li class="divider"></li>' +
                                        '<li><button type="button" id="delete_' + row.UniqueID + '" class="btn btn-outline-default" style="color: red; padding:0"><span class="glyphicon glyphicon-trash"style="margin-right: 5px;"></span><span >delete</span></button></div></li>' +
                                        '</ul>' +
                                        '</div></div>';
                                }
                            },
                        ],
                        bDestroy: true,
                    });

                    $('#ExpensesTable tbody').unbind("click").on('click', 'button', function () {
                        if ($(this).attr('id') && $(this).attr('id').split("_")[0]) {
                            console.log("button clicked is:" + $(this).attr('id'));
                            var buttonType = $(this).attr('id').split("_")[0];
                            var ExpenseId = $(this).attr('id').split("_")[1];
                            console.log(buttonType);
                            console.log("supplier id is: " + ExpenseId);


                            switch (buttonType) {
                                case "pay":
                                    ExpenseFunc.getExpenseByID(ExpenseId, function (err, docs) {
                                        if (err != null) {
                                            console.log("Error retriving expense: " + err);
                                        } else {
                                            $scope.Expense = docs[0];
                                            $scope.ExpensePayment = new ExpensePaymentFunc.ExpensePayment;

                                            LatePayments.CalculateLatePaymentCharge($scope.Expense.Supplier.UniqueID, $scope.Expense.ExpensePayment.Balance, $scope.Expense.ExpenseHeader.PaymentDate, $scope.Expense.ExpenseHeader.PaymentTerms, "Expense", function (LatePaymentDetails) {
                                                //$scope.DaysOverDue = LatePaymentDetails.DaysOverDue;
                                                $scope.ExpenseLateFees = LatePaymentDetails.LateFee;
                                                $scope.LateFees = LatePaymentDetails.LateFee;
                                                $scope.AmountDue = $scope.Expense.ExpensePayment.Balance + LatePaymentDetails.LateFee;
                                                console.log("Late Payment Details: " + JSON.stringify(LatePaymentDetails));
                                                $scope.Expense.ExpensePayment.Balance = $scope.AmountDue;

                                                $("#ActualPaymentDate").datepicker({
                                                    format: 'dd/mm/yyyy'
                                                }).datepicker("setDate", new Date());
                                                $scope.ExpensePayment.PaymentDate = new Date(); // convert moment.js object to Date object
                                                $("#NextPaymentDate").datepicker({
                                                    format: 'dd/mm/yyyy'
                                                }).datepicker("setDate", new Date());
                                                $scope.Expense.ExpensePayment.NextPaymentDate = new Date(); // convert moment.js object to Date object
                                                $("#PaymentDetails").val("");
                                                $("#PaymentMethod").val("Select");
                                                $scope.ExpensePayment.AmountRecieved = $scope.Expense.ExpensePayment.Balance;
                                                $("#NextPaymentDateDiv").hide();
                                                $("#PaymentError").hide();
                                                Controls.SetError("exchangeRate", "");
                                                Controls.SetError("AmountRecieved", "");
                                                Controls.SetError("NextPaymentDate", "");
                                                Controls.SetError("PaymentMethod", "");
                                                Controls.SetError("ExpenseAccount", "");
                                                $scope.CurrencySymbol = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Expense.ExpenseHeader.ExpenseCurrency; }).Symbol;
                                                $scope.Expense.ExpensePayment.Balance = Controls.ToCurrency($scope.Expense.ExpensePayment.Balance);
                                                $scope.ExpensePayment.ExchangeRate = $scope.Expense.ExpenseHeader.ExchangeRate;
                                                $scope.$apply();
                                                $('#ExpensePaymentModal').modal('toggle');
                                            });
                                        }
                                    });
                                    break;
                                case "delete":
                                    console.log("delete action");
                                    ExpenseFunc.getExpenseByID(ExpenseId, function (err, docs) {
                                        if (err != null) {
                                            console.log("Error retriving expense: " + err);
                                        } else {
                                            $scope.DeleteExpense = docs[0];
                                            $scope.$apply();
                                            $('#DeleteExpense').modal('toggle');
                                            console.log("Delete Expense" + $scope.DeleteExpense.ExpenseHeader.ExpenseNumber);
                                        }
                                    });

                                    break;
                                default:
                                    console.log("Unknown action");
                                    break;
                            }
                        }
                    });
                }
            }
        }
        $scope.RemoveExpense = function () {

            var ExpenseID = $scope.DeleteExpense.UniqueID;

            console.log("Deleting Expense" + JSON.stringify($scope.DeleteExpense));
            ExpenseFunc.deleteExpense($scope.DeleteExpense, function (err, numRemoved) {
                if (err != null) {
                    console.log("Error deleting Expense: " + err);
                } else {

                    console.log("Deleted " + numRemoved + " Expense");
                    $scope.DeleteExpense.DeletedBy = $rootScope.CurrentUser.Username;
                    TransactionFunc.deleteRelatedTransactions($scope.DeleteExpense.UniqueID, function (err, numRemoved) {

                        if (err != null) {
                            console.log("Error deleting Expense Transactions: " + err);
                        } else {
                            $('#DeleteExpense').modal('toggle');
                            console.log("Deleted " + numRemoved + " Transactions");
                            Controls.showAlert("Alert", "alert-success", "Expense #" + "'" + $scope.DeleteExpense.ExpenseHeader.ExpenseNumber + "'" + " succesfully deleted", "message", true);
                            $scope.DeleteExpense = new ExpenseFunc.Expense(); //Clear New Expense
                            $scope.$apply();
                            ExpenseFunc.getExpense("", GetExpensesCallback);
                        }

                    });

                }
            });

            ExpensePaymentFunc.getPayments(ExpenseID, function (payments) {

                if (payments == null) {
                    console.log("Error getting expense payments: " + err);
                } else {

                    ExpensePaymentFunc.deleteAllPayments(ExpenseID, function (err, numRemoved) {
                        if (err != null) {
                            console.log("Error deleting Expense payments: " + err);
                        } else {

                            console.log("Deleted " + numRemoved + " Expense payments");
                            payments.forEach(payment => {

                                TransactionFunc.deleteRelatedTransactions(payment.UniqueID, function (err, numRemoved) {

                                    if (err != null) {
                                        console.log("Error deleting payment Transactions: " + err);
                                    } else {
                                        console.log("Deleted " + numRemoved + " Transactions");
                                    }

                                });
                            });
                        }
                    });
                }
            });
        };

        function GetSuppliers() {
            SupplierFunc.getSupplier("", function (suppliers) {
                if (suppliers.length == 0) {
                    console.log("No suppliers found");
                } else {
                    $scope.Suppliers = suppliers;
                    $scope.$apply();
                }
            });
        }

        $scope.Applyfilter = function () {
            var search = {};
            var searchArray = [];
            var filter = null;
            var includefilter = false;

            if ($scope.Supplier) {
                searchArray.push({ "Supplier.UniqueID": $scope.Supplier });
            }

            if ($scope.Status) {
                if ($scope.Status != "All") {
                    searchArray.push({ "ExpenseState": $scope.Status });
                }
            }

            if ($scope.FromDate) {
                searchArray.push({ "ExpenseHeader.ExpenseDate": { $gte: moment(new Date($scope.FromDate)).startOf('day') } });
            }

            if ($scope.ToDate) {
                searchArray.push({ "ExpenseHeader.ExpenseDate": { $lte: moment(new Date($scope.ToDate)).endOf('day') } });
            }

            if ($scope.ExpenseNumber) {
                searchArray.push({ "ExpenseHeader.ExpenseNumber": $scope.ExpenseNumber });
            }
            searchArray.length > 0 ? filter = { $and: searchArray } : filter = "";

            console.log("Search Filter is: " + JSON.stringify(filter));

            ExpenseFunc.searchExpense(filter, GetExpensesCallback);
        }

        $scope.ClearFilters = function () {
            ExpenseFunc.searchExpense({}, GetExpensesCallback);
            $scope.Status = "";
            $scope.FromDate = "";
            $scope.ToDate = "";
            $scope.ExpenseNumber = "";

            $timeout(function () {
                $("#Supplier").select2("val", "");
            }, 1);
        }

        //************************************************************* Process Expense Payment **************************************************************************
        $scope.exchangeRateChange = function (index) {
            if (Expense.ExpenseHeader.ExpenseCurrency == Settings.Currency) {
                Expense.ExpenseHeader.ExchangeRate = 1
            }
        }
        $scope.PaymentAmntOnChange = function () {
            var Balance = Controls.ToNumber($scope.Expense.ExpensePayment.Balance) - $scope.ExpensePayment.AmountRecieved;
            console.log("Baance is: " + Balance);
            console.log("more than amount: " + $scope.ExpensePayment.AmountRecieved + "   " + $scope.Expense.ExpensePayment.Balance);
            $("#NextPaymentDateDiv").hide();
            Controls.SetError("AmountRecieved", "");
            if (isNaN($scope.ExpensePayment.AmountRecieved) || $scope.ExpensePayment.AmountRecieved == null) {
                console.log("Is not a number");
                //$scope.ExpensePayment.AmountRecieved = 0.00;
            } else if (Balance < 0) {
                Controls.SetError("AmountRecieved", "Invalid Balance");

            } else if (Balance > 0.00) {
                $("#NextPaymentDateDiv").show();
                Controls.SetError("NextPaymentDate", "Enter Next Payment Date");
            }
            else {
                $("#NextPaymentDateDiv").hide();
                Controls.SetError("NextPaymentDate", "");
            }
            //$scope.ExpensePayment.AmountRecieved = parseFloat($scope.ExpensePayment.AmountRecieved).toFixed(2);
        }

        $scope.PaymentDateChange = function () {
            if ($("#ActualPaymentDate").val() == "") {
                $("#ActualPaymentDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", new Date());
            }
            var dateMomentObject = moment($scope.FormActualPaymentDate, "DD/MM/YYYY"); // 1st argument - string, 2nd argument - format
            $scope.ExpensePayment.PaymentDate = dateMomentObject.toDate(); // convert moment.js object to Date object
        }

        $scope.NextPaymentDateChange = function () {
            console.log("Payment date changed");
            if ($("#NextPaymentDate").val() == "") {
                $("#NextPaymentDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", new Date());
            }
            var dateMomentObject = moment($scope.FormNextPaymentDate, "DD/MM/YYYY"); // 1st argument - string, 2nd argument - format
            $scope.Expense.ExpensePayment.NextPaymentDate = dateMomentObject.toDate(); // convert moment.js object to Date object
        }

        //Record Payment
        $scope.SubmitPayment = function () {

            var Balance = Controls.ToNumber($scope.Expense.ExpensePayment.Balance) - $scope.ExpensePayment.AmountRecieved;
            Controls.SetError("exchangeRate", "");
            Controls.SetError("AmountRecieved", "");
            Controls.SetError("NextPaymentDate", "");
            Controls.SetError("PaymentMethod", "");

            if ($scope.ExpensePayment.PaymentMethod == null || $scope.ExpensePayment.PaymentMethod == "" || $scope.ExpensePayment.PaymentMethod == "Select") {
                Controls.SetError("PaymentMethod", "Select Payment Method");
            }
            else if ($scope.ExpensePayment.ExpenseAccount == null || $scope.ExpensePayment.ExpenseAccount == "" || $scope.ExpensePayment.ExpenseAccount == "Select") {
                Controls.SetError("ExpenseAccount", "Select Account");
            }
            else if ($scope.ExpensePayment.AmountRecieved <= 0 || $scope.ExpensePayment.AmountRecieved == null) {
                Controls.SetError("AmountRecieved", "Enter Amount Recieved");
            }
            else if (Balance < 0) {
                Controls.SetError("AmountRecieved", "Amount Recieved Is Greater Than Amount Due");
            }
            else if (Balance > 0.00 && ($scope.Expense.ExpensePayment.NextPaymentDate == "" || $scope.Expense.ExpensePayment.NextPaymentDate == null)) {
                Controls.SetError("NextPaymentDate", "Enter Next Payment Date");
            }
            else if (isNaN($scope.ExpensePayment.ExchangeRate) || !($scope.ExpensePayment.ExchangeRate > 0)) {
                Controls.SetError("exchangeRate", "Enter a valid exchange rate");
            }
            else {

                $scope.ExpensePayment.AmountRecieved = Controls.ToNumber($scope.ExpensePayment.AmountRecieved);
                $scope.Expense.ExpensePayment.Balance = Controls.ToNumber($scope.Expense.ExpensePayment.Balance) - $scope.ExpensePayment.AmountRecieved;
                $scope.Expense.ExpensePayment.AmountRecieved = $scope.Expense.ExpenseHeader.ExpenseAmount - $scope.Expense.ExpensePayment.Balance;


                $scope.ExpensePayment.ExpenseCurrency = $scope.Expense.ExpenseHeader.ExpenseCurrency;
                $scope.ExpensePayment.CompanyCurrency = $scope.Expense.ExpenseHeader.CompanyCurrency;
                $scope.ExpensePayment.ReferenceCurrencyRate = $scope.Settings.DailyExchangeRate;
                $scope.ExpensePayment.ReferenceCurrency = $scope.Settings.ReferenceCurrency;

                if ($scope.Expense.ExpensePayment.Balance > 0.00) {
                    $scope.Expense.ExpensePayment.PaymentStatus = "Unpaid";
                    $scope.ExpensePayment.PaymentType = "Partial Payment";
                    $scope.Expense.ExpenseState = "Unpaid";
                    $scope.Expense.ExpensePayment.NextPaymentDate = new Date($scope.Expense.ExpensePayment.NextPaymentDate);
                    console.log("partial payment");
                } else {
                    $scope.Expense.ExpensePayment.PaymentStatus = "Paid";
                    $scope.Expense.ExpenseState = "Paid";
                    $scope.ExpensePayment.PaymentType = "Full Payment";
                    console.log("Payment Date: " + $scope.ExpensePayment.PaymentDate)
                    $scope.Expense.ExpensePayment.PaymentDate = new Date($scope.ExpensePayment.PaymentDate);
                    $scope.Expense.ExpensePayment.NextPaymentDate = "",
                        console.log("Full payment:");
                }
                $scope.Expense.ExpensePayment.Balance = $scope.Expense.ExpensePayment.Balance;
                $scope.ExpensePayment.Balance = $scope.Expense.ExpensePayment.Balance;
                $scope.ExpensePayment.PaymentDate = new Date($scope.ExpensePayment.PaymentDate);
                console.log("Payment Date: " + $scope.ExpensePayment.PaymentDate)

                $scope.ExpensePayment.ExpenseNumber = $scope.Expense.ExpenseHeader.ExpenseNumber;
                $scope.ExpensePayment.ExpenseID = $scope.Expense.UniqueID;
                $scope.ExpensePayment.SupplierID = $scope.Expense.Supplier.UniqueID;
                $scope.ExpensePayment.AmountDue = $scope.Expense.ExpenseHeader.ExpenseAmount;


                console.log("Payment details:" + JSON.stringify($scope.Expense.ExpensePayment));
                console.log("Expense payment info:" + JSON.stringify($scope.ExpensePayment));

                $scope.ExpensePayment.CreatedBy = $rootScope.CurrentUser.Username;

                ExpensePaymentFunc.recordPayment($scope.ExpensePayment, RecordPaymentCallBack, $scope.Expense.ExpensePayment, $scope.Expense.ExpenseState);
            }

        };

        function RecordPaymentCallBack(payment, PaymentInfo, ExpenseState, error) {
            if (error) {
                Controls.showAlert("PaymentError", "alert-danger", "Error updating payment: " + error, "message", false);
                ExpenseFunc.getExpense("", GetExpensesCallback);

            } else {

                console.log("payment info is: " + payment.ExpenseAccount);
                var PaymentTransaction = {
                    AccountId: payment.ExpenseAccount,
                    Date: payment.PaymentDate,
                    DocumentReference: payment.ExpenseNumber,
                    DocumentId: payment.UniqueID,
                    Details: "Payment for Expense:" + payment.ExpenseNumber + "\n" + (payment.PaymentDetails || ""),
                    Credit_CompanyCurrencyAmount: payment.AmountRecieved * payment.ExchangeRate,
                    Credit_AccountCurrencyAmount: payment.AmountRecieved * payment.ExchangeRate,
                    Credit_ReferenceCurrencyAmount: payment.AmountRecieved * payment.ReferenceCurrencyRate,
                    Debit_CompanyCurrencyAmount: "",
                    Debit_AccountCurrencyAmount: "",
                    Debit_ReferenceCurrencyAmount: "",
                }

                TransactionFunc.RecordTransaction(PaymentTransaction, null, function (err, PaymentTransactionId) {

                    if (err) {
                        console.log("error creating transaction :" + err);
                    } else {
                        console.log("Updated payment with ID " + PaymentTransactionId);

                        //Record Accounts Payable transaction
                        var AccountsPayableTransaction = {
                            AccountId: $scope.AccountsPayableAccountId,
                            Date: payment.PaymentDate,
                            DocumentReference: PaymentTransactionId,
                            DocumentId: payment.UniqueID,
                            Details: "Payment for Expense:" + payment.ExpenseNumber + "\n" + (payment.PaymentDetails || ""),
                            Credit_CompanyCurrencyAmount: "",
                            Credit_AccountCurrencyAmount: "",
                            Credit_ReferenceCurrencyAmount: "",
                            Debit_CompanyCurrencyAmount: payment.AmountRecieved * payment.ExchangeRate,
                            Debit_AccountCurrencyAmount: payment.AmountRecieved * payment.ExchangeRate,
                            Debit_ReferenceCurrencyAmount: payment.AmountRecieved * payment.ReferenceCurrencyRate,
                        }
                        TransactionFunc.RecordTransaction(AccountsPayableTransaction, null, function (err, APTransactionId) {

                            if (err) {
                                console.log("error creating Accounts Payable Transaction :" + err);
                            } else {
                                console.log("Recorded Accounts Payable Transaction with ID " + APTransactionId);

                                ExpensePaymentFunc.updatePaymentTransactionID(payment.UniqueID, PaymentTransactionId, function (err, doc) {
                                    if (err) {
                                        console.log("error updating Expense :" + err);
                                    } else {
                                        console.log("Payment Transaction complete! " + JSON.stringify(doc));

                                    }
                                });
                            }
                        });
                    }
                });

                ExpenseFunc.expensePayment($scope.Expense.UniqueID, PaymentInfo, ExpenseState, UpdateExpensePaymentCallback);

            }
        }

        function UpdateExpensePaymentCallback(error) {
            if (error) {
                Controls.showAlert("PaymentError", "alert-danger", "Error updating payment: " + error, "PaymentMessage", false);
                ExpenseFunc.getExpense("", GetExpensesCallback);

            } else {
                $('#ExpensePaymentModal').modal('hide');
                Controls.showAlert("Alert", "alert-success", "Expense: " + "'" + $scope.Expense.ExpenseHeader.ExpenseNumber + "'" + " payment recorded", "message", true);
                ExpenseFunc.getExpense("", GetExpensesCallback);
            }
        }
    }
    //******************************************************************************************************************************************************************************************
});

$(document).ready(function () {
    $("#Alert").hide();

});
//************************************************************************************************************************************************************ */

