//App contant variables
const { shell } = require('electron');
const { create } = require('lodash');
const electron = require('electron');
const { ipcRenderer } = require('electron')
const fs = require('fs');
const CustomCss = require('./app/config/appfiles/cssText.js');
const bootstrapCss = "";//CustomCss.bootstrapCss;
const formCss = "";//CustomCss.formCss;

jbsApp.controller("InvoiceController", function ($scope, $routeParams, $rootScope, $location) {

    //Check if user is signed-in
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {
        //Global variables
        var CustomerFunc = require('./app/models/Customer.js');
        var InvoiceFunc = require('./app/models/invoice.js');
        var ProdctFunc = require('./app/models/ProductsAndServices.js');
        var InvPaymentFunc = require('./app/models/InvoicePayments.js');
        var SalesTaxFunc = require('./app/models/SalesTax.js');
        var CountriesList = require('./app/config/appfiles/Countries.js');
        var CurrenciesList = require('./app/config/appfiles/Currencies.js');
        var AccountFunc = require('./app/models/ChartOfAccounts.js');
        var TransactionFunc = require('./app/models/Transactions.js');
        var LatePayments = require('./app/lib/LatePayments.js');
        var PaymentTermFunc = require('./app/models/PaymentTerms.js');
        var CurrentCustomer = CustomerFunc.Customer;
        var productSrchObj = null;
        var edit = false;
        var InvoiceID = null;


        //Initialise all Scope Variables:
        InvoiceID = $scope.InvoiceID = $routeParams.InvoiceID;
        console.log("$routeParams.InvoiceID: " + $routeParams.InvoiceID);
        $scope.Action = $routeParams.Action;
        $scope.Customer = CustomerFunc.Customer;
        $scope.Customer = CustomerFunc.Customer;
        $scope.Invoice = new InvoiceFunc.Invoice();
        $scope.Countries = CountriesList.Countries;
        $scope.Currencies = CurrenciesList.Currencies;
        $scope.InvoicePayment = new InvPaymentFunc.InvoicePayment();


        //Initialise APP functions
        Run();

        //Set document layout
        function Run() {

            $("#Alert").hide();
            $("#Preview_Alert").hide();
            (function () {
                $('.btn-tab-prev').on('click', function (e) {
                    e.preventDefault()
                    $('#' + $('.nav-item > .active').parent().prev().find('a').attr('id')).tab('show')
                })
                $('.btn-tab-next').on('click', function (e) {

                    var requiredFields = ["Customer"], validateFields = [{ Name: "Email", Type: "email" }, { Name: "Phone", Type: "phone" }]

                    e.preventDefault()
                    if ($(this).attr('id') == "nextTab" && (!Controls.CheckRequiredFields(requiredFields) || !Controls.Validatefields(validateFields))) {

                        console.log("Invalid/missing info!")

                    } else {
                        $('#' + $('.nav-item > .active').parent().next().find('a').attr('id')).tab('show')
                        document.getElementById("Customer").classList.remove("is-invalid");
                        $('#Customer_error').hide();
                        $scope.SameAsBillingOnChange();
                    }
                })
            })();

            $("#invoiceDate").datepicker({
                format: 'dd/mm/yyyy'
            });
            $("#paymentDate").datepicker({
                format: 'dd/mm/yyyy'
            });

            //load Invoice settings
            Settings.getSettings(function (err, AppSettings) {
                $scope.Settings = AppSettings;
                $scope.DefaultPaymentTerms = $scope.Settings.DefaultPaymentTerm
                $scope.Settings.DailyExchangeRate = ($scope.Settings.DailyExchangeRate == null || $scope.Settings.DailyExchangeRate == "") ? 1 : $scope.Settings.DailyExchangeRate;
                $scope.Invoice.InvoiceHeader.InvoiceCurrency = $scope.Settings.Currency;
                $scope.Invoice.InvoiceHeader.CompanyCurrency = $scope.Settings.Currency;
                $scope.Invoice.InvoiceHeader.ReferenceCurrencyRate = $scope.Settings.DailyExchangeRate;
                $scope.Invoice.InvoiceHeader.ReferenceCurrency = $scope.Settings.ReferenceCurrency;
                $scope.InvoiceCurrency = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Settings.Currency; });
                $scope.CurrencySymbol = $scope.InvoiceCurrency.Symbol;
                $scope.CompanyCurrency = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Settings.Currency; });
                $('#itemsHeader').css('background-color', $scope.Settings.AccentColor);
                $scope.$apply();
                GetCustomers();
                GetTaxes();
                GetInvoice();
                GetAccounts();
                GetARAccount();

                //Get Payment Terms
                PaymentTermFunc.getPaymentTerms({}, function (err, PaymentTerms) {
                    console.log("Getting PaymentTerms");

                    if (err != null) {

                        console.log("Error Getting PaymentTerms :" + err);
                    }
                    else if (PaymentTerms.length == 0) {
                        console.log("No PaymentTerms found");
                    } else {
                        $scope.PaymentTermsList = PaymentTerms;
                        if ($scope.Invoice.InvoiceHeader.PaymentTerms && $scope.Invoice.InvoiceHeader.PaymentTerms.PaymentTerm == '' && $scope.DefaultPaymentTerms) {

                            $scope.Invoice.InvoiceHeader.PaymentTerms = PaymentTerms.find(function (obj) { return obj.UniqueID === $scope.DefaultPaymentTerms; });
                            $scope.InvoicePaymentTerms = $scope.Invoice.InvoiceHeader.PaymentTerms;
                            $scope.PaymentTermOnChange();
                            console.log("$scope.Invoice.InvoiceHeader.PaymentTerms: " + $scope.Invoice.InvoiceHeader.PaymentTerms);
                        }

                        $scope.$apply();
                    }
                });

                //Get Account for Sales
                AccountFunc.getAccount({ AccountGroup: "Income", AccountName: "Sales" }, function (Accounts) {
                    if (Accounts.length == 0) {
                        console.log("No Expenses Accounts found");
                        $scope.SalesAccount = [];
                    } else {

                        $scope.SalesAccount = Accounts;
                        console.log("Sales Accounts found:" + Accounts[0].UniqueID);
                    }
                });


            });
        }

        //************************************* Other Functions ****************************************************************************************************************/
        //Get Tax values
        function GetTaxes() {
            SalesTaxFunc.getSalesTax("", function (SalesTax) {
                if (SalesTax.length == 0) {
                    console.log("No SalesTax found");
                    $scope.Tax = [];
                } else {
                    $scope.Tax = SalesTax;
                }
            });
        }

        //Get Account for Accounts Receivable
        function GetARAccount() {
            AccountFunc.getAccount({ AccountName: "Accounts Receivable" }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Accounts Receivable Account found");
                    AccountFunc.CheckAccounts(GetARAccount);
                } else {
                    $scope.AccountsReceivableAccountId = Accounts[0].UniqueID;
                    console.log("Accounts Receivable Accounts found:" + Accounts[0].UniqueID);
                }
            });
        }


        //Get Income Accounts values
        function GetAccounts() {
            AccountFunc.getAccount({ AccountType: "Cash and Bank" }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Income Accounts found");
                    $scope.IncomeAccounts = [];
                } else {
                    $scope.IncomeAccounts = Accounts;
                }
            });
        }

        //Get/Refresh Invoice Data
        function GetInvoice(id) {
            $scope.InvoiceLineItems = [];
            $scope.InvoiceTaxes = [];
            $scope.CurrentProduct = ProdctFunc.product;
            $scope.Invoice.InvoiceHeader.InvoiceNetAmount = "0.00";
            $scope.Invoice.InvoiceHeader.InvoiceAmount = "0.00";

            if (id) {
                InvoiceID = id;
            } else if (InvoiceID) {

            }

            if (InvoiceID) {

                $scope.Invoice = new InvoiceFunc.Invoice();
                //$scope.$apply();
                console.log("Invoice ID is: " + InvoiceID);
                //AddInvoiceDetailsToForm(InvoiceID, Invoice, true);

                InvoiceFunc.getInvoiceByID(InvoiceID, function (err, invoice) {
                    if (err != null) {
                        console.log("Error retriving invoice: " + err);
                    } else {

                        //Retrive invoice data
                        $scope.Invoice = invoice[0];

                        console.log("Invoice is: " + JSON.stringify(invoice));

                        //Retrive Invoice lines
                        var lines = invoice[0].InvoiceLines
                        for (var i = 0; i < Object.keys(lines).length; i++) {
                            //var  = "Line" + i;

                            $scope.InvoiceLineItems.push(invoice[0].InvoiceLines[i]);
                        }

                        //Get customer based on invoice data
                        $scope.Invoice.Customer ? CustomerFunc.getCustomer($scope.Invoice.Customer.UniqueID, function (customers) { $scope.Customer = customers[0]; $scope.$apply(); }) : Console.log("no customer defined");

                        $scope.InvoicePaymentTerms = $scope.Invoice.InvoiceHeader.PaymentTerms

                        //Load Invoice LineItems
                        $scope.InvoiceCurrency = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Invoice.InvoiceHeader.InvoiceCurrency; })
                        $scope.CurrencySymbol = $scope.InvoiceCurrency.Symbol;
                        $scope.InvoiceCompanyCurrency = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Invoice.InvoiceHeader.CompanyCurrency; });

                        //Invoice Preview date formats
                        if ($scope.Invoice.InvoiceCreationDate != "" && $scope.Invoice.InvoiceCreationDate != null) {
                            $scope.Preview_InvoiceCreationDate = $scope.Invoice.InvoiceCreationDate.toDateString();
                        }
                        if ($scope.Invoice.InvoiceSendDate != "" && $scope.Invoice.InvoiceSendDate != null) {
                            $scope.Preview_InvoiceSendDate = $scope.Invoice.InvoiceSendDate.toDateString();
                        }

                        if ($scope.Invoice.InvoicePayment.PaymentDate != "" && $scope.Invoice.InvoicePayment.PaymentDate != null) {
                            $scope.Preview_ActualPaymentDate = $scope.Invoice.InvoicePayment.PaymentDate.toDateString();
                        }

                        $scope.Preview_DueDate = $scope.Invoice.InvoiceHeader.PaymentDate != null ? $scope.Invoice.InvoiceHeader.PaymentDate.toDateString() : "";

                        //Handle Date formats
                        console.log("1 Invoice date is : " + $scope.Invoice.InvoiceHeader.InvoiceDate);
                        console.log("1 PaymentDate date is : " + $scope.Invoice.InvoiceHeader.PaymentDate);

                        //$scope.$apply();
                        //$scope.FormartAllAmounts(Controls.ToCurrency);
                        // $("#invoiceDate").datepicker({
                        //     dateFormat: 'dd-mm-yy'
                        // });
                        // $("#paymentDate").datepicker({
                        //     dateFormat: 'dd-mm-yy'
                        // });

                        console.log("2: Invoice date is : " + $scope.Invoice.InvoiceHeader.InvoiceDate);
                        console.log("2: PaymentDate date is : " + $scope.Invoice.InvoiceHeader.PaymentDate);


                        $("#invoiceDate").datepicker("setDate", /*Controls.FormatDate(*/$scope.Invoice.InvoiceHeader.InvoiceDate);//));
                        $("#paymentDate").datepicker("setDate", Controls.FormatDate($scope.Invoice.InvoiceHeader.PaymentDate));


                        //Calculate Amounts Due
                        $scope.Invoice.InvoicePayment.Balance = $scope.Invoice.InvoicePayment.Balance;

                        //Calculate Amounts Due & paid
                        $scope.AmountPaid = $scope.Invoice.InvoiceHeader.InvoiceAmount - $scope.Invoice.InvoicePayment.Balance;
                        $scope.AmountDue = $scope.Invoice.InvoiceHeader.InvoiceAmount - $scope.AmountPaid;
                        $scope.ConvertedAmount = $scope.Invoice.InvoiceHeader.InvoiceAmount * $scope.Invoice.InvoiceHeader.ExchangeRate;

                        //format all dates
                        $scope.InvoiceDisplayDate = Controls.FormatDate($scope.Invoice.InvoiceHeader.InvoiceDate);
                        $scope.PaymentDisplayDate = Controls.FormatDate($scope.Invoice.InvoiceHeader.PaymentDate);

                        if ($scope.Action == "duplicate") {

                            console.log("Duplicate Invoice");
                            delete $scope.Invoice.UniqueID;
                            delete $scope.Invoice._id;
                            $scope.InvoiceID = null;
                            $scope.Invoice.InvoicePayment = new InvoiceFunc.Invoice().InvoicePayment;
                            $scope.Invoice.InvoiceState = "Draft";
                            GenerateInvoiceNumber();
                            formLayout("Draft");
                            $scope.Action = "view";
                            console.log("Action is now: " + $scope.Action);
                        } else {
                            //Set Layout based on the invoice status
                            formLayout($scope.Invoice.InvoiceState);
                        }


                        if ($scope.Invoice.InvoiceState != "Draft" && $scope.Invoice.InvoiceState != "To send") {
                            if ($scope.PaymentsTable != null) {
                                $scope.PaymentsTable.destroy();
                            }
                            GetPayments();
                        }

                        console.log("payments date is finally: " + $scope.Invoice.InvoiceHeader.PaymentDate);

                        //Handle late payment fees
                        LatePayments.CalculateLatePaymentCharge($scope.Invoice.Customer.UniqueID, $scope.Invoice.InvoicePayment.Balance, $scope.Invoice.InvoiceHeader.PaymentDate, $scope.Invoice.InvoiceHeader.PaymentTerms, "Invoice", function (LatePaymentDetails) {
                            $scope.DaysOverDue = LatePaymentDetails.DaysOverDue;
                            $scope.LateFee = LatePaymentDetails.LateFee;
                            $scope.InvoiceBalance = $scope.Invoice.InvoicePayment.Balance + LatePaymentDetails.LateFee;
                            console.log("Late Payment Details: " + JSON.stringify(LatePaymentDetails));
                            $scope.Invoice.InvoicePayment.Balance = $scope.InvoiceBalance;
                            //format invoice amounts
                            $scope.FormartAllAmounts(Controls.ToCurrency);

                            if (!$scope.Invoice.InvoiceHeader.PaymentTerms && $scope.Settings.DefaultPaymentTerm && $scope.Settings.DefaultPaymentTerm != "" && $scope.PaymentTermsList) {

                                $scope.Invoice.InvoiceHeader.PaymentTerms = $scope.PaymentTermsList.find(function (obj) { return obj.UniqueID === $scope.Settings.DefaultPaymentTerm; });
                                console.log("Set Default Payment Terms: " + JSON.stringify($scope.Invoice.InvoiceHeader.PaymentTerms));
                            }

                            $scope.$apply();
                        });

                    }
                });

            } else {


                if (!$scope.Settings.ExpiryDate || $scope.Settings.ExpiryDate < new Date()) {

                    $("#subscriptionAlert").show();

                } else {
                    $("#subscriptionAlert").hide();
                    $("#invoiceDate").datepicker({
                        format: 'dd/mm/yyyy'
                    }).datepicker("setDate", new Date());
                    $("#paymentDate").datepicker({
                        format: 'dd/mm/yyyy'
                    }).datepicker("setDate", new Date());
                    GenerateInvoiceNumber();
                }
                formLayout("Draft");
            }
        }

        //Get All Products
        function GetProducts() {
            ProdctFunc.getProductToSell("", function (items) {

                productSrchObj = items;

                $scope.ProductsTable = $('#ItemsRslt').DataTable({
                    data: items,
                    rowId: "UniqueID",
                    columns: [
                        { 'data': 'Name' },
                        { 'data': 'Description' },
                        { 'data': 'Price', 'render': function (amount) { return $scope.CurrencySymbol + Controls.ToCurrency(amount); } }
                    ],
                    select: { style: 'multi' },
                    buttons: [
                        'selectRows'
                    ]
                });

                $('#ItemsRslt tbody').on('click', 'tr', function () {

                    var selectedRow = $scope.ProductsTable.row(this).id();
                    var id = selectedRow;
                    console.log("Selected product: " + selectedRow);
                    selectProduct(selectedRow);
                    $scope.ProductsTable.search('').draw();
                });
            });
        }


        //Add rows to items table >>
        $(document).ready(function () {

        });

        //Set layout for invoice form based on status
        function formLayout(status) {

            $("#SelectItems").hide();
            $("#addrowBtn").show();
            $("#AddCustomers").hide();
            $('#Preview_DiscardChanges').hide();
            $('#Preview_InvoiceCreate').hide();

            if (!$scope.Settings.ExpiryDate || $scope.Settings.ExpiryDate < new Date()) {
                $("#subscriptionAlert").show();
            } else {
                $("#subscriptionAlert").hide();
            }

            status == "Draft" ? $("#displayState").text("New Invoice") : $("#displayState").text("Invoice #" + $scope.Invoice.InvoiceHeader.InvoiceNumber);
            switch (status) {
                case "Draft":

                    if (!$scope.Settings.ExpiryDate || $scope.Settings.ExpiryDate < new Date()) {

                        $("#SaveDraft").hide();
                        $("#ProcessPayment").hide();
                        $("#InvoiceCreate").hide();
                        $("#Cancel").show();
                        $("#addDifCustomer").hide();
                        $("#addrow").hide();
                        $(".ibtnDel").hide();
                        $("#invoiceDate").css('pointer-events', 'none');
                        $("#paymentDate").css('pointer-events', 'none');
                        $("input, textarea").prop("readonly", true);
                        $("#PrintInvoice").hide();
                        $("#InvoiceSummary").hide();
                        $("#Preview_PrintInvoice").hide();
                        $('#Preview_InvoiceCreate').hide();
                        $('#InvoicePreview').hide();
                        document.getElementById("NewInvoice").style.display = "block";
                        document.getElementById("viewInvoice").style.display = "none";
                        $("#InvoiceHederSection *").attr("disabled", "disabled").off('click');
                        $("#InvoiceLinesSection *").attr("disabled", "disabled").off('click');
                        $("#InvoiceTotalsSection *").attr("disabled", "disabled").off('click');

                    } else {
                        document.getElementById("NewInvoice").style.display = "block";
                        document.getElementById("viewInvoice").style.display = "none";
                        $("#SaveDraft").show();
                        $("#ProcessPayment").hide();
                        $("#InvoiceCreate").show();
                        $("#Cancel").show();
                        $("#addDifCustomer").show();
                        $("#addrow").show();
                        $(".ibtnDel").show();
                        $("#invoiceDate").css('pointer-events', 'auto');
                        $("#paymentDate").css('pointer-events', 'auto');
                        $("input, textarea").prop("readonly", false);
                        $("#PrintInvoice").hide();
                        $("#InvoiceSummary").hide();
                        $("#Preview_PrintInvoice").hide();

                        $('#Preview_InvoiceCreate').show();
                    }
                    break;
                case "To send":
                    document.getElementById("NewInvoice").style.display = "none";
                    document.getElementById("viewInvoice").style.display = "block";
                    $("#SaveDraft").hide();
                    $("#ProcessPayment").hide();
                    $("#InvoiceCreate").show();
                    $("#InvoicePaymentDetails").hide();
                    $("#InvoiceSummary").show();
                    $("#InvoiceCreate").text("Save Changes");
                    document.getElementById("DiscardChanges").style.display = "block";
                    $("#Preview_PrintInvoice").show();
                    $("#Cancel").show();
                    $("#addrow").hide();
                    $('#PrintInvoice').show();
                    break;
                case "Unpaid":
                    document.getElementById("NewInvoice").style.display = "none";
                    document.getElementById("viewInvoice").style.display = "block";
                    $("#SaveDraft").hide();
                    $("#ProcessPayment").show();
                    $("#InvoiceCreate").show();
                    $("#Cancel").show();
                    $("#addDifCustomer").hide();
                    $("#addrow").hide();
                    $('#PrintInvoice').show();
                    $('#InvoicePaymentDetails').show();
                    $('#DiscardChanges').show();
                    //$("#invoiceDate").css('pointer-events', 'none');
                    //$("#paymentDate").css('pointer-events', 'none');
                    //$("input, textarea").prop("readonly", true);
                    //$("#ActualPaymentDate").removeAttr("readonly");
                    //$("#PaymentDetails").removeAttr("readonly");
                    //$("#AmountRecieved").removeAttr("readonly");
                    break;
                case "Paid":
                    document.getElementById("NewInvoice").style.display = "none";
                    document.getElementById("viewInvoice").style.display = "block";
                    $("#SaveDraft").hide();
                    $("#ProcessPayment").hide();
                    $("#InvoiceCreate").hide();
                    $("#Cancel").show();
                    $("#addDifCustomer").hide();
                    $("#addrow").hide();
                    $('#PrintInvoice').show();
                    $(".ibtnDel").hide();
                    $("#invoiceDate").css('pointer-events', 'none');
                    $("#paymentDate").css('pointer-events', 'none');
                    $("input, textarea").prop("readonly", true);
                    break;
                case "Canceled":
                    break;
                case "Edit Preview":
                    document.getElementById("NewInvoice").style.display = "none";
                    document.getElementById("viewInvoice").style.display = "block";
                    document.getElementById("viewInvoice").focus();
                    $('#Preview_DiscardChanges').show();
                    $('#Preview_InvoiceCreate').show();
                    $("#InvoiceSummary").hide();
                    $("#Preview_PrintInvoice").hide();
                    document.getElementById("Preview_Buttons").scrollIntoView();
                    console.log("Edit Preview layout");

                    break;
                default:
                    console.log("Error setting layout");
            }
        }

        //Reset the modal popup
        function ResetModal() {
            $("#AddCustomers").hide();
            $("#displayCustomers").show();
            $("#AddCustomerFooter").show();

            //Clear all customer details fields
            $('#SearchCustomer').val("");
            $('#displayCustomers').find('input[type=search]').val('').change();
            $('#sameASbilling').prop('checked', false);
            (function () {
                $('#tab1-tab').tab('show');
            })();

            $("#billingInfo").show();
            document.getElementById("Customer").classList.remove("is-invalid");
            edit = false;
            console.log("Reset the customer modal");
        }

        function DisplayInvoicePreview() {
            if ($scope.Invoice.InvoiceHeader.InvoiceAmount == null || $scope.Invoice.Customer.CustomerName == "" || $scope.Invoice.InvoiceHeader.InvoiceAmount == "0.00") {
                Controls.showAlert("Alert", "alert-danger", "Invoice is incomplete!", "message", true);
            } else {
                document.getElementById("NewInvoice").style.display = "none";
                document.getElementById("viewInvoice").style.display = "block";
            }
            document.getElementById("Preview_Buttons").scrollIntoView();
        }

        //***********************************************************************************************************************************************************************************/
        //************************************* Customer Functions ****************************************************************************************************************/
        function GetCustomers() {
            CustomerFunc.getCustomer("", function (customers) {

                if (customers.length == 0) {

                    console.log("No customers found");
                }

                var table = $('#customerRslt').DataTable({
                    data: customers,
                    rowId: "UniqueID",
                    columns: [
                        { 'data': 'CustomerName' },
                        { 'data': 'ContactFname', 'render': function (data, type, row) { return row.ContactFname + " " + row.ContactLname; } },
                        { 'data': 'Phone' }
                    ],
                    bDestroy: true
                });

                $('#customerRslt tbody').unbind("click").on('click', 'tr', function () {
                    var selectedRow = table.row(this).id();
                    console.log(selectedRow);

                    var id = selectedRow;
                    var selectedCustomer = customers.find(function (obj) { return obj.UniqueID === id; });
                    $scope.Invoice.Customer = selectedCustomer;
                    console.log("Customer Currency: " + $scope.Invoice.Customer.Currency);
                    if ($scope.Invoice.Customer.Currency && $scope.Invoice.Customer.Currency != null && $scope.Invoice.Customer.Currency != "") {
                        $scope.Invoice.InvoiceHeader.InvoiceCurrency = $scope.Invoice.Customer.Currency;
                        $scope.CurrencyChange();
                    }

                    $scope.$apply();
                    ResetModal();
                    $('#CustomerModal').modal('hide');
                    table.search('').draw();
                    console.log("$scope.Customer: " + JSON.stringify($scope.Invoice.Customer));
                });

            });
        }
        $scope.AddCustomer = function () {
            GetCustomers();
            $("#AddCustomers").hide();
            $("#displayCustomers").show();
            $("#AddCustomerFooter").show();
            console.log("add Customer btn");
        }

        $scope.SaveCustomer = function () {

            console.log("save customer" + JSON.stringify($scope.Customer));
            CustomerFunc.addCustomer($scope.Customer, function (err, Customer) {
                if (err != null) {
                    console.log("Error creating customer: " + err);
                } else {
                    Controls.showAlert("Alert", "alert-success", "Customer " + "'" + $scope.Customer.CustomerName + "'" + " succesfully created", "message", true);
                    $scope.Invoice.Customer = Customer; //Update Current Customer
                    console.log("Created Customer: " + JSON.stringify(Customer));
                    $scope.Customer = new CustomerFunc.Customer(); //Clear New Customer
                    $scope.$apply();
                    GetCustomers();
                }
            });
        }

        $scope.EditCustomer = function () {

            CustomerFunc.getCustomerById($scope.Invoice.Customer.UniqueID, function (err, docs) {
                if (err != null) {
                    console.log("Error retriving customer: " + err);
                } else {

                    (function () {
                        $('#tab1-tab').tab('show');
                    })();

                    $scope.Customer = docs[0];
                    console.log("Edit: " + $scope.Customer.CustomerName);
                    $scope.$apply();
                    $('#CustomerModal').modal('toggle');
                    $("#AddCustomers").show();
                    $("#displayCustomers").hide();
                    $("#AddCustomerFooter").hide();
                }

            });
        }

        $scope.UpdateCustomer = function () {

            console.log("Update Customer" + JSON.stringify($scope.Customer));

            CustomerFunc.updateCustomer($scope.Customer.UniqueID, $scope.Customer, function (err, num, Customer) {
                if (err != null) {
                    console.log("Error updating customer: " + err);
                } else {
                    Controls.showAlert("Alert", "alert-success", "Customer " + "'" + $scope.Customer.CustomerName + "'" + " succesfully updated", "message", true);
                    $scope.Invoice.Customer = Customer; //Update Current Customer
                    console.log("Updated Customer: " + Customer);
                    $scope.Customer = new CustomerFunc.Customer(); //Clear New Customer
                    $scope.$apply();
                    GetCustomers();
                }
            });
        };

        $scope.SameAsBillingOnChange = function () {

            console.log("Same As Billing" + $scope.Customer.Shipping.SameAsBilling);

            if ($scope.Customer.Shipping.SameAsBilling == true) {

                $scope.Customer.Shipping.AddressLine1 = $scope.Customer.Billing.AddressLine1;
                $scope.Customer.Shipping.AddressLine2 = $scope.Customer.Billing.AddressLine2;
                $scope.Customer.Shipping.City = $scope.Customer.Billing.City;
                $scope.Customer.Shipping.ZipCode = $scope.Customer.Billing.ZipCode;
                $scope.Customer.Shipping.Country = $scope.Customer.Billing.Country;
            }
        }

        $scope.showAddCustomer = function () {

            $("#AddCustomers").show();
            $("#displayCustomers").hide();
            $("#AddCustomerFooter").hide();
            $('#Customer_error').hide();
            document.getElementById("Customer").classList.remove("is-invalid");
            $scope.Customer = new CustomerFunc.Customer();
            (function () {
                $('#tab1-tab').tab('show');
            })();
            console.log("showAddCustomer");
        }
        //************************************************************************************************************************************************************************************
        //***************************************************************************************** Invoice Header Functions *****************************************************************
        function GenerateInvoiceNumber() {

            Date.prototype.yyyymmdd = function () {
                var mm = this.getMonth() + 1; // getMonth() is zero-based
                var dd = this.getDate();

                return [this.getFullYear(),
                (mm > 9 ? '' : '0') + mm,
                (dd > 9 ? '' : '0') + dd
                ].join('');
            };

            var date = new Date();
            getInvoiceCount(date.yyyymmdd());
            function getInvoiceCount(date) {
                InvoiceFunc.CheckInvoiceNum(date, callback);
                function callback(count) {
                    console.log("Invoice Number Count:" + count);
                    $scope.Invoice.InvoiceHeader.InvoiceNumber = date + " - " + (count + 1);
                    $scope.$apply();
                }
            }
        }

        $scope.CurrencyChange = function () {
            $scope.CurrencySymbol = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Invoice.InvoiceHeader.InvoiceCurrency; }).Symbol;
            if ($scope.Invoice.InvoiceHeader.InvoiceCurrency == $scope.Invoice.InvoiceHeader.CompanyCurrency) {
                $scope.Invoice.InvoiceHeader.ExchangeRate = 1
            } else {
                $scope.Invoice.InvoiceHeader.ExchangeRate = "";
            }
            $scope.exchangeRateChange();
        }

        $scope.exchangeRateChange = function (index) {
            if ($scope.Invoice.InvoiceHeader.ExchangeRate == null || $scope.Invoice.InvoiceHeader.ExchangeRate == "" || $scope.Invoice.InvoiceHeader.ExchangeRate == 0) {
                $scope.ConvertedAmount = 0.00;
                Controls.SetError("invoiceExchangeRate", "Invalid Exchange Rate");
            } else {
                $scope.ConvertedAmount = Controls.ToCurrency(Controls.ToNumber($scope.Invoice.InvoiceHeader.InvoiceAmount) * $scope.Invoice.InvoiceHeader.ExchangeRate);
                Controls.SetError("invoiceExchangeRate", "");
            }
        }


        $scope.InvoiceDateOnChange = function () {
            if ($("#invoiceDate").val() == "" && !$scope.Invoice.InvoiceHeader.InvoiceDate) {
                $("#invoiceDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", new Date());
            }
            var dateMomentObject = moment($scope.FormInvoiceDate, "DD/MM/YYYY"); // 1st argument - string, 2nd argument - format
            $scope.Invoice.InvoiceHeader.InvoiceDate = dateMomentObject.toDate(); // convert moment.js object to Date object
            $scope.PaymentTermOnChange()
        }


        $scope.PaymentTermOnChange = function () {
            if ($scope.PaymentTermsList) {

                $scope.Invoice.InvoiceHeader.PaymentTerms = JSON.parse(angular.toJson($scope.PaymentTermsList.find(function (obj) { return obj.UniqueID === $scope.InvoicePaymentTerms.UniqueID })));
                console.log("Payment Terms are: " + JSON.stringify($scope.Invoice.InvoiceHeader.PaymentTerms));
            
                var dateMomentObject = moment($scope.FormInvoiceDate, "DD/MM/YYYY").add($scope.Invoice.InvoiceHeader.PaymentTerms.PaymentPeriod, 'day')
                $scope.Invoice.InvoiceHeader.PaymentDate = dateMomentObject.toDate(); // convert moment.js object to Date object
                $("#paymentDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", dateMomentObject.toDate());
            }

        }

        $scope.PaymentDateOnChange = function () {
            if ($("#paymentDate").val() == "" && !$scope.Invoice.InvoiceHeader.PaymentDate) {
                $("#paymentDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", new Date());
            }
            var dateMomentObject = moment($scope.FormPaymentDate, "DD/MM/YYYY"); // 1st argument - string, 2nd argument - format
            $scope.Invoice.InvoiceHeader.PaymentDate = dateMomentObject.toDate(); // convert moment.js object to Date object
        }

        $scope.PaymentDateChange = function () {
            if ($("#ActualPaymentDate").val() == "") {
                $("#ActualPaymentDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", new Date());
            }
            var dateMomentObject = moment($scope.FormActualPaymentDate, "DD/MM/YYYY"); // 1st argument - string, 2nd argument - format
            $scope.InvoicePayment.PaymentDate = dateMomentObject.toDate(); // convert moment.js object to Date object
        }

        $scope.NextPaymentDateChange = function () {
            console.log("Payment date changed");
            if ($("#NextPaymentDate").val() == "") {
                $("#NextPaymentDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", new Date());
            }
            var dateMomentObject = moment($scope.FormNextPaymentDate, "DD/MM/YYYY"); // 1st argument - string, 2nd argument - format
            $scope.Invoice.InvoicePayment.NextPaymentDate = dateMomentObject.toDate(); // convert moment.js object to Date object
        }


        //*****************************************************************************************************************************************************************************************
        //*************************************************************************************************** Invoice LineItem Functions **********************************************************

        //Add Item from catalog
        function selectProduct(id) {
            var item = null;
            $scope.CurrentProduct = productSrchObj.find(function (obj) { return obj.UniqueID === id; });
            item = new InvoiceFunc.LineItem();
            item.Item = $scope.CurrentProduct.Name;
            item.Description = $scope.CurrentProduct.Description;
            item.Price = $scope.CurrentProduct.Price;
            item.ProductId = $scope.CurrentProduct.UniqueID;
            item.IncomeAccount = $scope.CurrentProduct.IncomeAccount ? $scope.CurrentProduct.IncomeAccount : $scope.SalesAccount[0].UniqueID;
            if ($scope.CurrentProduct.SalesTax != "" && $scope.CurrentProduct.SalesTax != null) {
                item.TaxName = $scope.CurrentProduct.SalesTax;
                item.TaxRate = parseFloat($scope.Tax.find(function (obj) { return obj.Name === item.TaxName; }).TaxRate);
                item.TaxId = $scope.Tax.find(function (obj) { return obj.Name === item.TaxName; }).UniqueID;
                item.TaxAccountId = $scope.Tax.find(function (obj) { return obj.Name === item.TaxName; }).AccountId;
            }

            $scope.InvoiceLineItems.push(item);
            console.log("Added item: " + item);
            $("#SelectItems").hide();
            $("#addrowBtn").show();
            $scope.CalculateLineAmounts();
            $scope.$apply();
        }

        $scope.addItem = function () {

            if ($scope.ProductsTable != null) {
                $scope.ProductsTable.destroy();
                $('#ItemsRslt tbody').off('click');
            }
            GetProducts();

            console.log("show products");
            $("#SelectItems").show();
            $("#addrowBtn").hide();
            document.getElementById("SelectItems").scrollIntoView();
        };

        $scope.AddNonCatalogItem = function () {

            var newLineItem = new InvoiceFunc.LineItem();
            console.log("InvoiceLine Obj: " + JSON.stringify(newLineItem));
            newLineItem.IncomeAccount = $scope.SalesAccount[0].UniqueID;
            $scope.InvoiceLineItems.push(newLineItem);
            console.log("InvoiceLine Obj: " + JSON.stringify(newLineItem));


            $("#SelectItems").hide();
            $("#addrowBtn").show();
        };

        $scope.CancelAddNewPrdct = function () {

            $("#SelectItems").hide();
            $("#addrowBtn").show();
        };

        $scope.deleteItem = function (index) {
            //Remove the item from Array using Index.
            $scope.InvoiceLineItems.splice(index, 1);
            $scope.CalculateLineAmounts();
        }

        $scope.lineValueChange = function (index) {
            $scope.CalculateLineAmounts();
        }

        //Culculate totals and Taxes
        $scope.CalculateLineAmounts = function () {
            var InvoiceSubtotal = 0, InvoiceTotal = 0;
            $scope.Invoice.InvoiceTaxes = [];
            console.log("InvoiceLineItems:" + JSON.stringify($scope.InvoiceLineItems));
            $scope.InvoiceLineItems.forEach(item => {
                var itemTax = item.TaxName;
                var ItemTotal = 0;
                item.Amount = 0;
                var itemPrice = Controls.ToNumber(item.Price);

                if (item.Quantity != 0 && typeof itemPrice == "number" && itemPrice != 0) {

                    ItemTotal = itemPrice * item.Quantity;

                    if ($scope.Tax && item.TaxName != "" && item.TaxName != null) {
                        item.TaxRate = parseFloat($scope.Tax.find(function (obj) { return obj.Name === itemTax; }).TaxRate);
                        item.TaxAmount = (ItemTotal * (item.TaxRate / 100)).toFixed(2);
                        item.TaxId = $scope.Tax.find(function (obj) { return obj.Name === item.TaxName; }).UniqueID;
                        item.TaxAccountId = $scope.Tax.find(function (obj) { return obj.Name === item.TaxName; }).AccountId;
                    } else {
                        item.TaxAmount = (0).toFixed(2);
                        item.TaxName = "";
                        item.TaxRate = "";
                        item.TaxId = "";
                        item.TaxAccountID = "";
                        console.log("Line has no tax");
                    }

                    item.Amount = ItemTotal.toFixed(2);
                    InvoiceSubtotal += parseFloat(item.Amount);
                    InvoiceTotal += parseFloat(item.Amount) + parseFloat(item.TaxAmount);
                    console.log("check InvoiceTotal: " + InvoiceTotal);
                    var itemTax = item.TaxName;
                    var itemTaxObj = $scope.Tax.find(function (obj) { return obj.Name === itemTax; });
                    if (!$scope.Invoice.InvoiceTaxes.includes(itemTaxObj) && itemTaxObj != null && itemTax != "") {
                        itemTaxObj.TotalTaxAmount = parseFloat(item.TaxAmount).toFixed(2);
                        $scope.Invoice.InvoiceTaxes.push(itemTaxObj);

                    } else if ($scope.Invoice.InvoiceTaxes.length > 0 && itemTax != "" && itemTax != null) {
                        itemTaxObj = $scope.Invoice.InvoiceTaxes.find(function (obj) { return obj.Name === itemTax; });
                        console.log("itemTaxObj:" + JSON.stringify(itemTaxObj));
                        itemTaxObj.TotalTaxAmount = (parseFloat(itemTaxObj.TotalTaxAmount) + parseFloat(item.TaxAmount)).toFixed(2);
                    }
                }
            });

            $scope.Invoice.InvoiceHeader.InvoiceNetAmount = parseFloat(InvoiceSubtotal).toFixed(2);
            $scope.Invoice.InvoiceHeader.InvoiceAmount = parseFloat(InvoiceTotal).toFixed(2);

            $scope.AmountDue = parseFloat($scope.Invoice.InvoiceHeader.InvoiceAmount - $scope.AmountPaid).toFixed(2);
            $scope.ConvertedAmount = parseFloat($scope.Invoice.InvoiceHeader.InvoiceAmount * $scope.Invoice.InvoiceHeader.ExchangeRate).toFixed(2);
            console.log("check amount: " + parseFloat(InvoiceTotal).toFixed(2));
            console.log("Invoice taxes: ", JSON.stringify($scope.Invoice.InvoiceTaxes));
            $scope.FormartAllAmounts(Controls.ToCurrency);
            $scope.Invoice.InvoiceTaxes = JSON.parse(angular.toJson($scope.Invoice.InvoiceTaxes));
        }

        //Formart Invoice Ammounts
        $scope.FormartAllAmounts = (Format, action) => {
            var taxLines = $scope.Invoice.InvoiceTaxes
            for (var i = 0; i < Object.keys(taxLines).length; i++) {
                taxLines[i].TaxRate = Controls.ToNumber(taxLines[i].TaxRate)
                taxLines[i].TotalTaxAmount = Format(taxLines[i].TotalTaxAmount); // format amount as currency
            }
            //Retrive Invoice taxes
            var lines = $scope.InvoiceLineItems;
            for (var i = 0; i < Object.keys(lines).length; i++) {
                if (action == "Save") {
                    lines[i].Quantity = Controls.ToNumber(lines[i].Quantity);
                    lines[i].Price = Format(lines[i].Price);
                }
                lines[i].TaxAmount = Format(lines[i].TaxAmount);
                lines[i].Amount = Format(lines[i].Amount); // format amount as currency
            }


            $scope.Invoice.InvoiceHeader.ReferenceCurrencyRate = Controls.ToNumber($scope.Invoice.InvoiceHeader.ReferenceCurrencyRate);
            $scope.Invoice.InvoiceHeader.ExchangeRate = Controls.ToNumber($scope.Invoice.InvoiceHeader.ExchangeRate);

            $scope.Invoice.InvoiceHeader.InvoiceNetAmount = Format($scope.Invoice.InvoiceHeader.InvoiceNetAmount);
            $scope.InvoiceBalance = Format($scope.InvoiceBalance);
            $scope.Invoice.InvoiceHeader.InvoiceAmount = Format($scope.Invoice.InvoiceHeader.InvoiceAmount);
            $scope.AmountPaid = Format($scope.AmountPaid);
            $scope.AmountDue = Format($scope.AmountDue);

            console.log("ConvertedAmount" + $scope.Invoice.InvoiceHeader.InvoiceAmount + " * " + $scope.Invoice.InvoiceHeader.ExchangeRate);
            $scope.ConvertedAmount = Format($scope.ConvertedAmount);
            console.log("Invoice is now formated: " + JSON.stringify($scope.InvoiceLineItems));
        };
        //*****************************************************************************************************************************************************************************************
        //*****************************************************************  Button Functions *****************************************************************************************************

        //Preview Invoice
        $scope.InvoicePreview = function () {
            if ($scope.Invoice.InvoiceState == "To send") {
                formLayout("Edit Preview")
            } else {
                DisplayInvoicePreview();
            }
        };
        //Close Preview Invoice
        $scope.EditInvoice = function () {
            document.getElementById("NewInvoice").style.display = "block";
            document.getElementById("viewInvoice").style.display = "none";
        };


        $scope.DiscardChanges = function () {
            GetInvoice($scope.Invoice.UniqueID);
            if ($scope.Invoice.InvoiceState == "To send") {
                //formLayout("To send")
            }
        };


        //**************************************************** Save invoice as draft **************************************************************************
        $scope.SaveDraft = function () {

            $scope.FormartAllAmounts(Controls.ToNumber, "Save");
            $scope.Invoice.InvoiceLines = $scope.InvoiceLineItems;
            $scope.Invoice.InvoiceState = "Draft";
            //$scope.Invoice.InvoiceHeader.PaymentDate = new Date($scope.Invoice.InvoiceHeader.PaymentDate);
            //$scope.Invoice.InvoiceHeader.InvoiceDate = new Date($scope.Invoice.InvoiceHeader.InvoiceDate);
            $scope.Invoice.InvoicePayment.Balance = $scope.Invoice.InvoiceHeader.InvoiceAmount;
            $scope.Invoice.InvoiceTaxes = JSON.parse(angular.toJson($scope.Invoice.InvoiceTaxes));

            var InvoiceTocreate = JSON.parse(JSON.stringify($scope.Invoice));
            InvoiceTocreate.InvoiceHeader.PaymentDate = new Date(InvoiceTocreate.InvoiceHeader.PaymentDate);
            InvoiceTocreate.InvoiceHeader.InvoiceDate = new Date(InvoiceTocreate.InvoiceHeader.InvoiceDate);
            InvoiceTocreate.InvoiceSendDate = new Date(InvoiceTocreate.InvoiceSendDate);
            $scope.Invoice.UniqueID == null ? InvoiceFunc.CreateInvoice(InvoiceTocreate, InvoiceSaveCallback) : InvoiceFunc.updateInvoice(InvoiceTocreate.UniqueID, InvoiceTocreate, InvoiceSaveCallback);
            console.log("Invoice Object: " + JSON.stringify($scope.Invoice));
        };

        function InvoiceSaveCallback(err, doc) {
            if (err) {
                alerttype = "alert-danger";
                message = "Error Saving Invoice: " + err;
            } else {
                var message = "Invoice: " + "'" + doc.InvoiceHeader.InvoiceNumber + "'" + " succesfully saved",
                    alertId = "Alert",
                    alertType = "alert-success",
                    autoDismiss = true,
                    txtFieldId = "message";

                console.log("Saved Invoice with UniqueID: " + doc.UniqueID);
                $("#InvoiceID").val(doc.UniqueID);
                $("#InvoiceState").val(doc.InvoiceState);
            }
            Controls.showAlert(alertId, alertType, message, txtFieldId, autoDismiss);
            GetInvoice(doc.UniqueID);
        }

        /************************************************************************************************************************************************* */
        /*********************************************************************** Create Invoice *********************************************************** */

        $scope.CreateInvoice = function () {

            var date = new Date();
            console.log("$scope.Invoice.InvoiceHeader.ExchangeRate is: " + $scope.Invoice.InvoiceHeader.ExchangeRate);
            if ($scope.Invoice.Customer.CustomerName != "" && $scope.Invoice.InvoiceHeader.PaymentDate != "" && $scope.Invoice.InvoiceHeader.InvoiceDate != "" && Controls.ToNumber($scope.Invoice.InvoiceHeader.InvoiceAmount) > 0 && !isNaN($scope.Invoice.InvoiceHeader.ExchangeRate) && $scope.Invoice.InvoiceHeader.ExchangeRate > 0) {

                $scope.FormartAllAmounts(Controls.ToNumber, "Save");
                //$scope.Invoice.InvoiceHeader.PaymentDate = new Date($scope.Invoice.InvoiceHeader.PaymentDate);
                //$scope.Invoice.InvoiceHeader.InvoiceDate = new Date($scope.Invoice.InvoiceHeader.InvoiceDate);
                $scope.Invoice.InvoiceLines = $scope.InvoiceLineItems;

                $scope.Invoice.InvoiceState = "To send";
                $scope.Invoice.InvoicePayment.PaymentStatus = "Pending";
                $scope.Invoice.InvoicePayment.Balance = $scope.Invoice.InvoiceHeader.InvoiceAmount;


                $scope.Invoice.InvoiceTaxes = JSON.parse(angular.toJson($scope.Invoice.InvoiceTaxes));
                $scope.Invoice.InvoiceHeader.PaymentTerms = JSON.parse(angular.toJson($scope.Invoice.InvoiceHeader.PaymentTerms));
                console.log("Create Invoice : " + JSON.stringify($scope.Invoice));
                var InvoiceTocreate = JSON.parse(JSON.stringify($scope.Invoice));
                InvoiceTocreate.InvoiceHeader.PaymentDate = new Date(InvoiceTocreate.InvoiceHeader.PaymentDate);
                InvoiceTocreate.InvoiceHeader.InvoiceDate = new Date(InvoiceTocreate.InvoiceHeader.InvoiceDate);
                InvoiceTocreate.InvoiceSendDate = new Date(InvoiceTocreate.InvoiceSendDate);

                InvoiceTocreate.CreatedBy = $rootScope.CurrentUser.Username;

                $scope.Invoice.UniqueID == null ? InvoiceFunc.CreateInvoice(InvoiceTocreate, InvoiceinsertCallback) : InvoiceFunc.updateInvoice(InvoiceTocreate.UniqueID, InvoiceTocreate, InvoiceinsertCallback);
                document.getElementById("AddCstBtn").classList.remove("is-invalid");
                document.getElementById("paymentDate").classList.remove("is-invalid");
                document.getElementById("AddCstBtn").classList.remove("is-invalid");

            } else {
                console.log("$scope.Invoice.Customer.CustomerName: " + $scope.Invoice.Customer.CustomerName);
                if ($scope.Invoice.Customer.CustomerName == "" || $scope.Invoice.Customer.CustomerName == null) {
                    //Controls.showAlert("Alert", "alert-danger", "A customer is required!", "message", true);
                    $("#AddCstBtn").css({ style: "border-style: solid; border-color: red;" });
                }
                else if ($scope.Invoice.InvoiceHeader.PaymentDate == "") {
                    //document.getElementById("paymentDate").classList.add("is-invalid");
                    Controls.showAlert("Alert", "alert-danger", "Payment date is required if payment is not made!", "message", true);
                }
                else if ($scope.Invoice.InvoiceHeader.InvoiceDate == "") {
                    //document.getElementById("invoiceDate").classList.add("is-invalid");
                    Controls.showAlert("Alert", "alert-danger", "Invoice Date is required", "message", true);
                }
                else if ($scope.Invoice.InvoiceHeader.InvoiceAmount <= 0) {
                    //document.getElementById("invoiceDate").classList.add("is-invalid");
                    Controls.showAlert("Alert", "alert-danger", "Invoice amount cannot be " + $scope.CurrencySymbol + "0.00", "message", true);
                } else {
                    //document.getElementById("invoiceDate").classList.add("is-invalid");
                    Controls.showAlert("Alert", "alert-danger", "Check Exchange Rate", "message", true);
                }
                console.log("Error Saving invoice");
            }

            function InvoiceinsertCallback(err, doc) {

                console.log("Invoice Call back")
                if (err) {
                    alerttype = "alert-danger";
                    message = "Error Saving Invoice: " + err;
                } else {
                    console.log("Created Invoice with UniqueID: " + doc.UniqueID);
                    $scope.Invoice.UniqueID = doc.UniqueID;

                    if (doc.InvoiceState != "Draft") {
                        // Create transaction for each line
                        var linesUpdated = 0;
                        doc.InvoiceLines.forEach(function (item, index, arr) {
                            console.log("line :" + JSON.stringify(item));
                            console.log("Ïtem", item.Item);

                            var LineItemTransaction = {
                                AccountId: item.IncomeAccount,
                                Date: doc.InvoiceHeader.InvoiceDate,
                                DocumentReference: doc.InvoiceHeader.InvoiceNumber,
                                DocumentId: doc.UniqueID,
                                Details: item.Item + " " + item.Description,
                                Debit_CompanyCurrencyAmount: "",
                                Debit_AccountCurrencyAmount: "",
                                Debit_ReferenceCurrencyAmount: "",
                                Credit_CompanyCurrencyAmount: item.Amount * doc.InvoiceHeader.ExchangeRate,
                                Credit_AccountCurrencyAmount: item.Amount * doc.InvoiceHeader.ExchangeRate,
                                Credit_ReferenceCurrencyAmount: item.Amount * doc.InvoiceHeader.ReferenceCurrencyRate,
                            }

                            var LineTaxTransaction = {
                                AccountId: item.TaxAccountId,
                                Date: doc.InvoiceHeader.InvoiceDate,
                                DocumentReference: doc.InvoiceHeader.InvoiceNumber,
                                DocumentId: doc.UniqueID,
                                Details: item.Item + " " + item.Description,
                                Debit_CompanyCurrencyAmount: "",
                                Debit_AccountCurrencyAmount: "",
                                Debit_ReferenceCurrencyAmount: "",
                                Credit_CompanyCurrencyAmount: item.TaxAmount * doc.InvoiceHeader.ExchangeRate,
                                Credit_AccountCurrencyAmount: item.TaxAmount * doc.InvoiceHeader.ExchangeRate,
                                Credit_ReferenceCurrencyAmount: item.TaxAmount * doc.InvoiceHeader.ReferenceCurrencyRate,
                            }

                            TransactionFunc.RecordTransaction(LineItemTransaction, LineTaxTransaction, function (err, LineItemTransactionId, LineTaxTransactionId) {

                                if (err) {
                                    console.log("error creating transaction :" + err);
                                } else {
                                    linesUpdated++;
                                    arr[index].TransactionID = LineItemTransactionId;
                                    arr[index].TaxTransactionID = LineTaxTransactionId;
                                    console.log("Updated " + item.Item + " with ID " + item.TransactionID);

                                    //Update Invoice once all items are done
                                    if (linesUpdated == doc.InvoiceLines.length) {
                                        //Set date to correct format
                                        //doc.InvoiceHeader.PaymentDate = new Date(doc.InvoiceHeader.PaymentDate);
                                        //doc.InvoiceHeader.InvoiceDate = new Date(doc.InvoiceHeader.InvoiceDate);
                                        doc.InvoiceCreationDate = new Date();


                                        //Record Accounts RecievableTransaction
                                        var AccountsReceivableTransaction = {
                                            AccountId: $scope.AccountsReceivableAccountId,
                                            Date: doc.InvoiceHeader.InvoiceDate,
                                            DocumentReference: "AR-" + doc.InvoiceHeader.InvoiceNumber,
                                            DocumentId: doc.UniqueID,
                                            Details: doc.Customer.CustomerName + " - " + doc.InvoiceHeader.InvoiceNumber,
                                            Debit_CompanyCurrencyAmount: doc.InvoiceHeader.InvoiceAmount * doc.InvoiceHeader.ExchangeRate,
                                            Debit_AccountCurrencyAmount: doc.InvoiceHeader.InvoiceAmount * doc.InvoiceHeader.ExchangeRate,
                                            Debit_ReferenceCurrencyAmount: doc.InvoiceHeader.InvoiceAmount * doc.InvoiceHeader.ReferenceCurrencyRate,
                                            Credit_CompanyCurrencyAmount: "",
                                            Credit_AccountCurrencyAmount: "",
                                            Credit_ReferenceCurrencyAmount: "",
                                        }

                                        TransactionFunc.RecordTransaction(AccountsReceivableTransaction, null, function (err, TransactionId, TaxTransactionId) {
                                            if (err) {
                                                console.log("error creating Accounts receivable  transaction :" + err);
                                            } else {

                                                doc.AR_TransactionId = TransactionId;
                                                $scope.FormartAllAmounts(Controls.ToNumber, "Save");
                                                InvoiceFunc.updateInvoice(doc.UniqueID, doc, function (err, doc) {
                                                    if (err) {
                                                        console.log("error updating Invoice :" + err);
                                                    } else {
                                                        console.log("Invoice Transaction complete! " + doc);
                                                        DisplayInvoicePreview();
                                                        formLayout("To send");
                                                        Controls.showAlert("Preview_Alert", "alert-success", "Invoice: " + "'" + doc.InvoiceHeader.InvoiceNumber + "'" + " succesfully saved", "Preview_message", true);
                                                        console.log(JSON.stringify(CurrentCustomer));
                                                        GetInvoice(doc.UniqueID);
                                                    }
                                                });
                                            }
                                        });

                                    }
                                }
                            });
                        });
                    }
                }
            }
        }
        //*********************************************************************** Print Invoice *********************************************************** */

        $scope.DownloadInvoice = function () {

            var date = new Date();
            $scope.InvoiceSendDate = date;
            var state = $scope.Invoice.InvoiceState == "To send" ? "Unpaid" : $scope.Invoice.InvoiceState;

            console.log("Generating PDF");
            var doc = new jsPDF('p', 'px', [595, 842], 'a4', true);

            if ($scope.Settings.Logo) {
                doc.addImage($scope.Settings.Logo, 'base64', 250, 50);
            }

            doc.setFontSize(30);
            doc.text("INVOICE", 180, 40);
            doc.autoTable(
                {
                    html: '#print_CompanyInfo',

                    theme: 'plain',
                    headStyles: { fillColor: [255, 255, 255], textColor: [0, 0, 0], fontSize: 14, fontStyle: "bold" },
                    footStyles: { fillColor: [255, 255, 255], fontSize: 10, textColor: [0, 0, 0] },
                    columnStyles: { 0: { halign: 'left', fontSize: 11 } },
                    styles: { cellPadding: 1 },
                    startY: 50,
                    margin: { right: 250 }
                });

            doc.setFontSize(15);
            doc.text("Bill To:", 30, doc.lastAutoTable.finalY + 20);

            doc.autoTable(
                {
                    html: '#print_InvoiceHeaderInfo',

                    didParseCell: data => {
                        if (data.section === 'foot' && data.column.index === 1) {
                            data.cell.styles.halign = 'left';
                        }

                        if (data.section === 'body' && data.row.index === 0) {
                            data.cell.styles.fontStyle = 'bold';
                        }
                    },
                    theme: 'plain',
                    headStyles: { fillColor: [255, 5, 133], fontSize: 17 },
                    footStyles: { fillColor: [255, 255, 255], fontSize: 13, textColor: [0, 0, 0], halign: 'right' },
                    columnStyles: { 0: { halign: 'right', fontSize: 11, cellPadding: { top: 1, right: 1 } }, 1: { halign: 'left', fontSize: 11 } },
                    styles: { cellPadding: 1 },
                    startY: doc.lastAutoTable.finalY + 22,
                    margin: { left: 250 }
                });
            doc.autoTable(
                {
                    html: '#print_InvoiceCustomerInfo',
                    theme: 'plain',
                    headStyles: { fillColor: [255, 255, 255], textColor: [0, 0, 0], fontSize: 14, fontStyle: "bold" },
                    footStyles: { fillColor: [255, 255, 255], fontSize: 11, textColor: [0, 0, 0] },
                    columnStyles: { 0: { halign: 'left', fontSize: 11 } },
                    styles: { cellPadding: 1 },
                    startY: doc.lastAutoTable.finalY - 55,
                    margin: { right: 250 }
                });
            doc.autoTable(
                {
                    html: '#print_InvoiceItems',
                    didParseCell: data => {
                        if (data.section === 'head' && data.column.index != 0) {
                            data.cell.styles.halign = 'right';
                        }
                    },
                    theme: 'striped',
                    headStyles: { fillColor: $scope.Settings.AccentColor, fontSize: 15 },
                    footStyles: { fillColor: [255, 255, 255], fontSize: 10, textColor: [0, 0, 0] },
                    columnStyles: { 1: { halign: 'right' }, 2: { halign: 'right' }, 3: { halign: 'right' } },
                    startY: doc.lastAutoTable.finalY + 10
                });

            doc.autoTable(
                {
                    html: '#print_InvoiceFooter',
                    theme: 'plain',
                    headStyles: { fillColor: [255, 5, 133], fontSize: 15 },
                    footStyles: { fillColor: [255, 255, 255], fontSize: 12, textColor: [0, 0, 0], halign: 'right' },
                    columnStyles: { 0: { halign: 'right', fontSize: 11 }, 1: { halign: 'right', fontSize: 11 } },
                    styles: { cellPadding: { top: 2 } },
                    startY: doc.lastAutoTable.finalY + 10,
                    margin: { left: 200 }
                });

            var invoiceNumber = $scope.Invoice.InvoiceHeader.InvoiceNumber;
            var dateTime = new Date().getTime();
            var file = 'INVOICE-' + invoiceNumber + '-' + dateTime + '.pdf'
            doc.save(file);

            InvoiceFunc.sendInvoice($scope.Invoice.UniqueID, state, date, () => {
                GetInvoice($scope.Invoice.UniqueID);
                $scope.$apply();
            });

        };

        $scope.PrintInvoice = function () {

            var date = new Date();
            $scope.InvoiceSendDate = date;
            var state = $scope.Invoice.InvoiceState == "To send" ? "Unpaid" : $scope.Invoice.InvoiceState;
            var invoiceLines = "";

            $scope.InvoiceLineItems.forEach(item => {

                invoiceLines += "<tbody style=\"font-size: 15px; height: 500px;\">" +
                    "<tr>" +
                    "<td style=\"border-bottom: 1px solid #e6e4e4;\">" +
                    "<p><span style=\"font-weight: bold;\">" + item.Item + "</span><br /><span>" + item.Description + "</span>" +
                    "</p>" +
                    "</td>" +
                    "<td style=\"text-align: right; border-bottom: 1px solid #e6e4e4;\"><span>" + item.Quantity + "</span></td>" +
                    "<td style=\"text-align: right; border-bottom: 1px solid #e6e4e4;\"><span>" + $scope.CurrencySymbol + "" + item.Price + "</span></td>" +
                    "<td style=\"text-align: right; border-bottom: 1px solid #e6e4e4;\"><span>" + $scope.CurrencySymbol + "" + item.Amount + "</span></td>" +
                    "</tr>" +
                    "</tbody>"

            });

            let invoiceTaxes = ""
            $scope.Invoice.InvoiceTaxes.forEach(tax => {
                invoiceTaxes += "<tr>" +
                    "<td>" +
                    "<label style=\"padding-right: 10px; text-align: right\">" + tax.Name + " (" + tax.TaxRate + "%):" +
                    "</label>" +
                    "</td>" +
                    "<td>" +
                    "<label>" + $scope.CurrencySymbol + "" + tax.TotalTaxAmount + "</label>" +
                    "</td>" +
                    "</tr>"

            })

            let displayExchangeRate = $scope.Invoice.InvoiceHeader.InvoiceCurrency != $scope.Settings.Currency && $scope.Settings.DisplayExchangeRate == true ?
                "<tr class=\"text-right\"style=\"font-size: 15px;\">" +
                "<td>Total" +
                "(" + $scope.Invoice.InvoiceHeader.CompanyCurrency + ") at exchange rate" +
                "(" + $scope.Invoice.InvoiceHeader.ExchangeRate + "):</td>" +
                "<td>" + $scope.InvoiceCompanyCurrency.Symbol + $scope.ConvertedAmount + "</td>" +
                "</tr>" : "";

            console.log("display ExchangeRate: " + $scope.Invoice.InvoiceHeader.InvoiceCurrency != $scope.Settings.Currency && $scope.Settings.DisplayExchangeRate == true);
            console.log("$scope.Invoice.InvoiceHeader.InvoiceCurrency: " + $scope.Invoice.InvoiceHeader.InvoiceCurrency);
            console.log("$scope.Settings.Currency: " + $scope.Settings.Currency)
            console.log("$scope.Settings.DisplayExchangeRate: " + $scope.Settings.DisplayExchangeRate)

            let hasCustPhone = $scope.Customer.Phone != "" ? "<span>|</span>" : ""
            const appDataPath = (electron.app || electron.remote.app).getPath('appData');

            

            let invoiceRender = "<!--Invoice PDF Preview -->" +
                "<!DOCTYPE html>" +
                "<html id=\"jbsApp\">" +
                "<head>" +
                "<meta charset=\"utf-8\" />" +
                "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">" +
                "<title>Jucce Finance</title>" +
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">" +                
                bootstrapCss +
                formCss +
                "</head>" +
                "  <body id=\"mainbody\">" +
                "        <div style=\"min-height: 800px;\">" +
                "            <div style=\"padding:15px;\">" +
                "        <div class=\"col-md-12\">" +
                "           <h1 id=\"documentType\" class=\"text-center\" style=\" text-align: center; \">INVOICE</h1>" +
                "        </div>" +
                "        <div class=\" form-group row\" style=\"width: 100%; overflow: hidden;\">" +
                "           <div class=\"col-9\" style=\"padding-left: 0px; width: 50%; float: left;\">" +
                "                <table id=\"print_CompanyInfo\">" +
                "                    <thead>" +
                "                        <tr>" +
                "                          <td style=\"padding: 0;\">" +
                "                              <span style=\"font-size: 18px; font-weight: bold;\"" +
                "                               id=\"companyName\">" + $scope.Settings.CompanyName + "</span>" +
                "                          </td>" +
                "                         </tr>" +
                "                     </thead>" +
                "                 <tbody style=\"font-size: 15px;\">" +
                "                <tr>" +
                "               <td style=\"padding: 0;\">" +
                "                  <span id=\"addrressLine1\">" + $scope.Settings.AddressLine1 + "</span><br />" +
                "                  <span id=\"addrressLine2\">" + $scope.Settings.AddressLine2 + "</span> <br />" +
                "                  <span id=\"addrressLine3\">" + $scope.Settings.ZipCode + " " + $scope.Settings.City + "</span>" +
                "                  <br />" +
                "                  <span id=\"addrressLine4\">" + $scope.Settings.Country + "</span> <br />" +
                "                  <span id=\"contact\">" + $scope.Settings.Phone + "</span> <br />" +
                "                  <span id=\"website\">" + $scope.Settings.Website + "</span>" +
                "               </td>" +
                "               </tr>" +
                "              </tbody>" +
                "              </table>" +
                "</div>" +
                "               <div class=\"col-3\" style=\"width: 50%; float: right;\">" +
                "                       <img alt=\"\" src=\"" + $scope.Settings.Logo + "\" style=\"height: 100px;width: 200px;float: right;\">" +
                "               </div>" +
                "               </div>" +
                "                <div style=\"padding-left: 5px; padding-top: 20px;\">" +
                "                    <span id=\"BillTo\">Bill To:</span>" +
                "                </div>" +
                "                <div class=\"form-group row\" style=\"width: 100%; overflow: hidden;\">" +
                "                    <div class=\"col\" style=\"width: 50%; float: left;\">" +
                "                        <table id=\"print_InvoiceCustomerInfo\">" +
                "                            <thead>" +
                "                                <tr>" +
                "                                    <td>" +
                "                                        <span style=\"font-weight: bold; font-size: 18px;\"" +
                "                                            id=\"cCompanyName\">" + $scope.Invoice.Customer.CustomerName + "</span>" +
                "                                    </td>" +
                "                                </tr>" +
                "                            </thead>" +
                "                            <tbody style=\"font-size: 15px;\">" +
                "                                <tr>" +
                "                                    <td id=\"invoiceHeader\">" +
                "                                            <span id=\"cAddrressLine1\">" + $scope.Invoice.Customer.Billing.AddressLine1 + "</span>" +
                "                                            <br />" +
                "                                            <span id=\"CAddrressLine2\">" + $scope.Invoice.Customer.Billing.AddressLine2 + "</span>" +
                "                                            <br />" +
                "                                            <span id=\"cAddrressLine3\">" + $scope.Invoice.Customer.Billing.ZipCode + "" +
                "                                                " + $scope.Invoice.Customer.Billing.City + "</span><br />" +
                "                                            <span id=\"cAddrressLine4\"> " + $scope.Invoice.Customer.Billing.Country + "</span>" +
                "                                            <br />" +
                "                                            <span id=\"cWebsite\">" + $scope.Invoice.Customer.Phone + "</span>" +
                hasCustPhone +
                "                                            <span id=\"cEmail\">" + $scope.Invoice.Customer.Email + "</span>" +
                "                                    </td>" +
                "                                </tr>" +
                "                            </tbody>" +
                "                        </table>" +
                "                    </div>" +
                "                    <div class=\"col\" style=\"padding-top:20px;\">" +
                "                        <table id=\"print_InvoiceHeaderInfo\" style=\"width: 50%; float: right; text-align: right;\">" +
                "                            <tbody style=\"font-size: 15px; width: fit-content;\">" +
                "                                <tr>" +
                "                                    <td><span class=\"text-bold text-right table-noPadding\">Invoice Number:</span></td>" +
                "                                    <td><span class=\"text-bold\"" +
                "                                            style=\"padding-left:10px\">" + $scope.Invoice.InvoiceHeader.InvoiceNumber + "</span>" +
                "                                    </td>" +
                "                                </tr>" +
                "                                <tr>" +
                "                                    <td><span class=\"text-right\">Invoice Date:</span></td>" +
                "                                    <td><span style=\"padding-left:10px\">" + $scope.InvoiceDisplayDate + "</span>" +
                "                                    </td>" +
                "                                </tr>" +
                "                                <tr>" +
                "                                    <td><span class=\"text-right\">Payment Date:</span></td>" +
                "                                    <td><span style=\"padding-left:10px\">" + $scope.PaymentDisplayDate + "</span>" +
                "                                    </td>" +
                "                                </tr>" +
                "                            </tbody>" +
                "                            <tfoot>" +
                "                                <tr style=\"font-size: 18px;\" class=\"text-bold\">" +
                "                                    <td><span class=\"text-right\">Amount Due" +
                "                                            (" + $scope.Invoice.InvoiceHeader.InvoiceCurrency + "):" +
                "                                        </span></td>" +
                "                                    <td><span " +
                "                                            style=\"padding-left:10px\">" + $scope.CurrencySymbol + "" + $scope.Invoice.InvoiceHeader.InvoiceAmount + "</span>" +
                "                                    </td>" +
                "                                </tr>" +
                "                            </tfoot>" +
                "                        </table>" +
                "                   </div>" +
                "                </div>" +
                "               <div><table id=\"print_InvoiceItems\"  style=\"width: 100%; padding-top:20px; border-spacing: 0px;\">" +
                "                    <thead>" +
                "                        <tr id=\"itemsHeader\" style=\"font-size: 20px; font:bold; color: rgb(0, 0, 0); background-color: "+$scope.Settings.AccentColor+";\">" +
                "                            <th style=\"width: 200px; text-align: left\">Item</th>" +
                "                            <th style=\"text-align: right\">Quantity</th>" +
                "                            <th style=\"text-align: right\">Price</th>" +
                "                            <th style=\"text-align: right\">Amount</th>" +
                "                        </tr>" +
                "                    </thead>" +
                invoiceLines +
                "                </table></div>" +
                "                <div style=\"padding-top: 20px;\"><table id=\"print_InvoiceFooter\"  style=\"width: auto; float: right; padding-right: 5px; border-spacing: 0;\">" +
                "                    <tbody style=\" text-align: right;\">" +
                "                        <tr>" +
                "                            <td>" +
                "                                <label style=\"padding-right: 10px; text-align: right; font-weight: bold;\">Sub" +
                "                                    Total:</label>" +
                "                            </td>" +
                "                            <td>" +
                "                                <label" +
                "                                    id=\"SubTotalAmount\">" + $scope.CurrencySymbol + "" + $scope.Invoice.InvoiceHeader.InvoiceNetAmount + "</label>" +
                "                            </td>" +
                "                        </tr>" +
                invoiceTaxes +
                "                    </tbody>" +
                "                    <tfoot style=\"text-align: right;\">" +
                "                        <tr style=\"font-weight: bold;\">" +
                "                            <td>" +
                "                                <label style=\"padding-right: 10px; text-align: right\">Total Amount Due" +
                "                                    (" + $scope.Invoice.InvoiceHeader.InvoiceCurrency + "): </label>" +
                "                            </td>" +
                "                            <td>" +
                "                                <label" +
                "                                    id=\"TotalAmount\">" + $scope.CurrencySymbol + "" + $scope.Invoice.InvoiceHeader.InvoiceAmount + "</label>" +
                "                            </td>" +
                "                        </tr>" +
                displayExchangeRate +
                "                    </tfoot>" +
                "                </table></div>" +
                "            </div>" +
                "</div>"
            "  </body>"


            const userDataPath = (electron.app || electron.remote.app).getPath('userData');
            fs.writeFile(userDataPath + '/printInvoice.html', invoiceRender, function (err) {
                if (err) throw err;
                console.log('Html File Saved!');

                ipcRenderer.send('printInvoice', 'print')
            });

            ipcRenderer.on('print-reply', (event, arg) => {
                console.log("Printing result: " + arg);
                InvoiceFunc.sendInvoice($scope.Invoice.UniqueID, state, date, function () {
                    $("#Preview_SkipExportInvoice").hide();
                    GetInvoice($scope.Invoice.UniqueID);
                    $scope.$apply();
                    //ipc.send('printInvoice');
                });
            })

        };

        $scope.SkipDownload = function () {
            var date = new Date();
            $scope.InvoiceSendDate = date;
            var state = $scope.Invoice.InvoiceState == "To send" ? "Unpaid" : $scope.Invoice.InvoiceState;
            InvoiceFunc.sendInvoice($scope.Invoice.UniqueID, state, date, function () {
                $("#Preview_SkipExportInvoice").hide();
                GetInvoice($scope.Invoice.UniqueID);
            });

        };
        /***************************************************************************************************************************************************************/
        //************************************************************* Process Invoice Payment **************************************************************************
        $scope.RecordPayment = function () {
            $("#ActualPaymentDate").datepicker({
                format: 'dd/mm/yyyy'
            }).datepicker("setDate", new Date());
            $scope.InvoicePayment.PaymentDate = new Date();
            $("#NextPaymentDate").datepicker({
                format: 'dd/mm/yyyy'
            }).datepicker("setDate", new Date());

            $scope.Invoice.InvoicePayment.NextPaymentDate = new Date();
            $("#PaymentDetails").val("");
            $("#PaymentMethod").val("Select");
            //$scope.InvoicePayment.AmountRecieved = 0;
            $("#NextPaymentDateDiv").hide();
            $("#PaymentError").hide();
            $scope.InvoicePayment.AmountRecieved = $scope.Invoice.InvoicePayment.Balance;
            $scope.DisplayAmountDue = Controls.ToCurrency($scope.Invoice.InvoicePayment.Balance);
            $scope.InvoicePayment.ExchangeRate = $scope.Invoice.InvoiceHeader.ExchangeRate;
        };

        $scope.PaymentAmntOnChange = function () {
            var Balance = Controls.ToNumber($scope.Invoice.InvoicePayment.Balance) - $scope.InvoicePayment.AmountRecieved;
            console.log("Baance is: " + Balance);
            console.log("more than amount: " + $scope.InvoicePayment.AmountRecieved + "   " + $scope.Invoice.InvoicePayment.Balance);
            $("#NextPaymentDateDiv").hide();
            Controls.SetError("AmountRecieved", "");
            if (isNaN($scope.InvoicePayment.AmountRecieved) || $scope.InvoicePayment.AmountRecieved == null) {
                console.log("Is not a number");
                //$scope.InvoicePayment.AmountRecieved = 0.00;
            } else if (Balance < 0) {
                Controls.SetError("AmountRecieved", "Invalid Balance");

            } else if (Balance > 0.00) {
                $("#NextPaymentDateDiv").show();
                Controls.SetError("NextPaymentDate", "Enter Next Payment Date");
            }
            else {
                $("#NextPaymentDateDiv").hide();
                Controls.SetError("NextPaymentDate", "");
            }
        }

        //Record Payment
        $scope.SubmitPayment = function () {

            var Balance = Controls.ToNumber($scope.Invoice.InvoicePayment.Balance) - $scope.InvoicePayment.AmountRecieved;
            Controls.SetError("exchangeRate", "");
            Controls.SetError("AmountRecieved", "");
            Controls.SetError("NextPaymentDate", "");
            Controls.SetError("PaymentMethod", "");

            if ($scope.InvoicePayment.PaymentMethod == null || $scope.InvoicePayment.PaymentMethod == "" || $scope.InvoicePayment.PaymentMethod == "Select") {
                Controls.SetError("PaymentMethod", "Enter Payment Method");
            }
            else if ($scope.InvoicePayment.IncomeAccount == null || $scope.InvoicePayment.IncomeAccount == "" || $scope.InvoicePayment.IncomeAccount == "Select") {
                Controls.SetError("IncomeAccount", "Select Account");
            }
            else if ($scope.InvoicePayment.AmountRecieved <= 0 || $scope.InvoicePayment.AmountRecieved == null) {
                Controls.SetError("AmountRecieved", "Enter Amount Recieved");
            }
            else if (Balance < 0) {
                Controls.SetError("AmountRecieved", "Amount Recieved Is Greater Than Amount Due");
            }
            else if (Balance > 0.00 && ($scope.Invoice.InvoicePayment.NextPaymentDate == "" || $scope.Invoice.InvoicePayment.NextPaymentDate == null)) {
                Controls.SetError("NextPaymentDate", "Enter Next Payment Date");
            }
            else if (isNaN($scope.InvoicePayment.ExchangeRate) || !($scope.InvoicePayment.ExchangeRate > 0)) {
                Controls.SetError("exchangeRate", "Enter a valid exchange rate");
            }
            else {
                $scope.Invoice.InvoicePayment.Balance = Controls.ToNumber($scope.Invoice.InvoicePayment.Balance) - $scope.InvoicePayment.AmountRecieved;
                $scope.Invoice.InvoicePayment.AmountRecieved = Controls.ToNumber($scope.Invoice.InvoiceHeader.InvoiceAmount) - $scope.Invoice.InvoicePayment.Balance

                $scope.InvoicePayment.InvoiceCurrency = $scope.Invoice.InvoiceHeader.InvoiceCurrency;
                $scope.InvoicePayment.CompanyCurrency = $scope.Invoice.InvoiceHeader.CompanyCurrency;
                $scope.InvoicePayment.ReferenceCurrencyRate = $scope.Settings.DailyExchangeRate;
                $scope.InvoicePayment.ReferenceCurrency = $scope.Settings.ReferenceCurrency;
                console.log("Invoice payment info:" + JSON.stringify($scope.InvoicePayment));

                if ($scope.Invoice.InvoicePayment.Balance > 0.00) {
                    $scope.Invoice.InvoicePayment.PaymentStatus = "Unpaid";
                    $scope.InvoicePayment.PaymentType = "Partial Payment";
                    $scope.Invoice.InvoiceState = "Unpaid";
                    $scope.Invoice.InvoicePayment.NextPaymentDate = new Date($scope.Invoice.InvoicePayment.NextPaymentDate);
                    console.log("partial payment");
                } else {
                    $scope.Invoice.InvoicePayment.PaymentStatus = "Paid";
                    $scope.Invoice.InvoiceState = "Paid";
                    $scope.InvoicePayment.PaymentType = "Full Payment";
                    console.log("Payment Date: " + $scope.InvoicePayment.PaymentDate)
                    $scope.Invoice.InvoicePayment.PaymentDate = new Date($scope.InvoicePayment.PaymentDate);
                    $scope.Invoice.InvoicePayment.NextPaymentDate = "",
                        console.log("Full payment:");
                }
                $scope.InvoicePayment.Balance = $scope.Invoice.InvoicePayment.Balance;
                $scope.InvoicePayment.PaymentDate = new Date($scope.InvoicePayment.PaymentDate);
                console.log("Payment Date: " + $scope.InvoicePayment.PaymentDate)

                $scope.InvoicePayment.InvoiceNumber = $scope.Invoice.InvoiceHeader.InvoiceNumber;
                $scope.InvoicePayment.InvoiceID = $scope.Invoice.UniqueID;
                $scope.InvoicePayment.CustomerID = $scope.Invoice.Customer.UniqueID;
                $scope.InvoicePayment.AmountDue = $scope.Invoice.InvoiceHeader.InvoiceAmount;

                console.log("Payment details:" + JSON.stringify($scope.Invoice.InvoicePayment));
                console.log("Invoice payment info:" + JSON.stringify($scope.InvoicePayment));

                $scope.InvoicePayment.CreatedBy = $rootScope.CurrentUser.Username;

                InvPaymentFunc.recordPayment($scope.InvoicePayment, RecordPaymentCallBack, $scope.Invoice.InvoicePayment, $scope.Invoice.InvoiceState);
            }

        };

        function RecordPaymentCallBack(payment, PaymentInfo, InvoiceState, error) {
            if (error) {
                Controls.showAlert("PaymentError", "alert-danger", "Error updating payment: " + error, "message", false);
                GetInvoice();

            } else {

                console.log("payment info is: " + payment.IncomeAccount);
                var PaymentTransaction = {
                    AccountId: payment.IncomeAccount,
                    Date: payment.PaymentDate,
                    DocumentReference: payment.InvoiceNumber,
                    DocumentId: payment.UniqueID,
                    Details: "Payment for Invoice:" + payment.InvoiceNumber + "\n" + (payment.PaymentDetails || ""),
                    Debit_CompanyCurrencyAmount: payment.AmountRecieved * payment.ExchangeRate,
                    Debit_AccountCurrencyAmount: payment.AmountRecieved * payment.ExchangeRate,
                    Debit_ReferenceCurrencyAmount: payment.AmountRecieved * payment.ReferenceCurrencyRate,
                    Credit_CompanyCurrencyAmount: "",
                    Credit_AccountCurrencyAmount: "",
                    Credit_ReferenceCurrencyAmount: "",
                }



                TransactionFunc.RecordTransaction(PaymentTransaction, null, function (err, PaymentTransactionId) {

                    if (err) {
                        console.log("error creating transaction :" + err);
                    } else {
                        console.log("Updated payment with ID " + PaymentTransactionId);

                        //Record Accounts Receivable transaction
                        var AccountsReceivableTransaction = {
                            AccountId: $scope.AccountsReceivableAccountId,
                            Date: payment.PaymentDate,
                            DocumentReference: PaymentTransactionId,
                            DocumentId: payment.UniqueID,
                            Details: "Payment for Invoice:" + payment.InvoiceNumber + "\n" + (payment.PaymentDetails || ""),
                            Debit_CompanyCurrencyAmount: "",
                            Debit_AccountCurrencyAmount: "",
                            Debit_ReferenceCurrencyAmount: "",
                            Credit_CompanyCurrencyAmount: payment.AmountRecieved * payment.ExchangeRate,
                            Credit_AccountCurrencyAmount: payment.AmountRecieved * payment.ExchangeRate,
                            Credit_ReferenceCurrencyAmount: payment.AmountRecieved * payment.ReferenceCurrencyRate,
                        }
                        TransactionFunc.RecordTransaction(AccountsReceivableTransaction, null, function (err, ARTransactionId) {

                            if (err) {
                                console.log("error creating Accounts Receivable Transaction :" + err);
                            } else {
                                console.log("Recorded Accounts Receivable Transaction with ID " + ARTransactionId);
                                InvPaymentFunc.updatePaymentTransactionID(payment.UniqueID, PaymentTransactionId, function (err, doc) {
                                    if (err) {
                                        console.log("error updating Invoice :" + err);
                                    } else {
                                        console.log("Payment Transaction complete! " + JSON.stringify(doc));

                                    }
                                });
                            }
                        });
                    }
                });

                InvoiceFunc.invoicePayment($scope.Invoice.UniqueID, PaymentInfo, InvoiceState, UpdateInvoicePaymentCallback);

            }
        }

        function UpdateInvoicePaymentCallback(id, error) {
            if (error) {
                Controls.showAlert("PaymentError", "alert-danger", "Error updating payment: " + error, "message", false);
                GetInvoice();

            } else {
                $('#InvoicePaymentModal').modal('toggle');
                Controls.showAlert("Preview_Alert", "alert-success", "Invoice: " + "'" + $scope.Invoice.InvoiceHeader.InvoiceNumber + "'" + " payment recorded", "Preview_message", true);
                GetInvoice();
            }
        }
        //Remove Payment
        $scope.RemovePayment = function () {

            $scope.Invoice.InvoicePayment.Balance = Controls.ToNumber($scope.Invoice.InvoicePayment.Balance) + Controls.ToNumber($scope.DeletePayment.AmountRecieved);
            //$scope.Invoice.InvoicePayment.AmountRecieved = $scope.Invoice.InvoicePayment.AmountRecieved - $scope.DeletePayment.AmountRecieved;

            if ($scope.Invoice.InvoicePayment.Balance > 0.00) {
                $scope.Invoice.InvoicePayment.PaymentStatus = "Unpaid";
                $scope.Invoice.InvoiceState = "Unpaid";
                $scope.Invoice.InvoicePayment.NextPaymentDate = $scope.Invoice.InvoiceHeader.PaymentDate;//new Date($scope.Invoice.InvoiceHeader.PaymentDate);
                console.log("partial payment");
            } else {
                $scope.Invoice.InvoicePayment.PaymentStatus = "Paid";
                $scope.Invoice.InvoiceState = "Paid";
                $scope.Invoice.InvoicePayment.NextPaymentDate = "";
            }

            console.log("Payment details:" + JSON.stringify($scope.Invoice.InvoicePayment));
            TransactionFunc.deleteTransaction({ UniqueID: $scope.DeletePayment.TransactionId }, function (err, numRemoved) {

                if (err) {
                    console.log("error deleting transaction :" + err);
                } else {
                    console.log("Removed " + numRemoved + " Transactions");
                    //Delete Accounts Receivable transaction
                    TransactionFunc.deleteTransaction({ DocumentReference: $scope.DeletePayment.TransactionId, DocumentId: $scope.DeletePayment.TransactionId }, function (err, numRemoved) {
                        if (err) {
                            console.log("error deleting transaction :" + err);
                        } else {
                            console.log("Removed " + numRemoved + " Transactions");
                        }
                    });
                    InvPaymentFunc.deletePayment($scope.DeletePayment.UniqueID, RemovePaymentCallBack, $scope.Invoice.InvoicePayment, $scope.Invoice.InvoiceState)
                }
            });


        };

        function RemovePaymentCallBack(numRemoved, PaymentInfo, InvoiceState, error) {
            if (error || numRemoved == 0) {
                Controls.showAlert("Alert", "alert-danger", "Error deleting payment: " + error, "message", false);
                GetInvoice();

            } else {
                InvoiceFunc.invoicePayment($scope.Invoice.UniqueID, PaymentInfo, InvoiceState, DeleteInvoicePaymentCallback);
            }
        }

        function DeleteInvoicePaymentCallback(error) {
            if (error) {
                Controls.showAlert("Alert", "alert-danger", "Error updating payment: " + error, "message", false);
                GetInvoice();
            } else {
                $('#DeletePayment').modal('toggle');
                Controls.showAlert("Alert", "alert-success", "Invoice: " + "'" + $scope.Invoice.InvoiceHeader.InvoiceNumber + "'" + " payment deleted", "message", true);
                GetInvoice();
            }
        }
        function GetPayments() {

            InvPaymentFunc.getPayments(InvoiceID, function (payments) {

                $scope.PaymentsTable = $('#InvoicePayments').DataTable({

                    data: payments,
                    rowId: "UniqueID",
                    columns: [
                        { 'data': 'PaymentDate', 'render': function (date) { return Controls.FormatDate(date); } },
                        { 'data': 'PaymentMethod', 'render': function (method) { return method ? method : ""; } },
                        { 'data': 'PaymentDetails', 'render': function (PaymentDetails) { return PaymentDetails ? PaymentDetails : ""; } },
                        { 'data': 'AmountRecieved', 'render': function (amount) { return $scope.CurrencySymbol + Controls.ToCurrency(amount); } },
                        {
                            'data': 'UniqueID', 'sortable': false, 'render': function (id) {

                                return '<div>' +
                                    '<i id="delete_' + id + '"><span class="glyphicon glyphicon-trash"></span></i></div>'
                            }
                        }
                    ],
                    paging: false,
                    searching: false,
                    lengthChange: false,
                    info: false,
                    bDestroy: true
                });
                $('#InvoicePayments tbody').unbind("click").on('click', 'i', function () {
                    //var supplierID = table.row(this).id();
                    console.log("button clicked is:" + $(this).attr('id'));
                    var buttonType = $(this).attr('id').split("_")[0];
                    var paymentId = $(this).attr('id').split("_")[1];
                    console.log(buttonType);
                    console.log("payment id is: " + paymentId);

                    switch (buttonType) {
                        case "delete":
                            console.log("delete action");
                            InvPaymentFunc.getPaymentById(paymentId, function (err, docs) {
                                if (err != null) {
                                    console.log("Error retriving payment: " + JSON.stringify(err));
                                } else {
                                    $scope.DeletePayment = docs[0];
                                    $scope.DeletePayment.AmountRecieved = Controls.ToCurrency($scope.DeletePayment.AmountRecieved);
                                    $scope.$apply();
                                    $('#DeletePayment').modal('toggle');
                                    console.log("delete payment: " + $scope.DeletePayment.AmountRecieved);
                                }
                            });
                            break;
                        default:
                            console.log("Unknown action");
                            break;
                    }
                });
            });
        }
        //******************************************************************************************************************************************************************************************
    }
});