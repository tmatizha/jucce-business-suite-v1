jbsApp.controller("invoicesController", function ($scope, $rootScope, $timeout, $location) {

    //Check if user is signed-in
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {
        var CurrenciesList = require('./app/config/appfiles/Currencies.js');
        var InvoiceFunc = require('./app/models/invoice.js');
        var CustomerFunc = require('./app/models/Customer.js');
        var InvPaymentFunc = require('./app/models/InvoicePayments.js');
        var AccountFunc = require('./app/models/ChartOfAccounts.js');
        var TransactionFunc = require('./app/models/Transactions.js');
        var LatePayments = require('./app/lib/LatePayments.js');
        var moment = require('moment');

        var Chart = require("chart.js");

        $("#Customer").select2({
            placeholder: "Select Customer",
            allowClear: true
        });

        //load app settings
        Settings.getSettings(function (err, AppSettings) {
            $scope.Settings = AppSettings;
            $scope.Settings.DailyExchangeRate = ($scope.Settings.DailyExchangeRate == null || $scope.Settings.DailyExchangeRate == "") ? 1 : $scope.Settings.DailyExchangeRate;
            GetInvoices();
            GetCustomers();
            GetAccounts();
            GetARAccount()
            $scope.$apply();
            $("#Alert").hide();
        });

        function GetInvoices() {
            InvoiceFunc.getInvoice("", GetInvoicesCallback);
        }

        //Get Income Accounts values
        function GetAccounts() {
            AccountFunc.getAccount({ AccountType: "Cash and Bank" }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Cash and Bank Accounts found");
                    $scope.IncomeAccounts = [];
                } else {

                    $scope.IncomeAccounts = Accounts;
                }
            });
        }

        //Get Account for Accounts Receivable
        function GetARAccount() {
            AccountFunc.getAccount({ AccountName: "Accounts Receivable" }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Accounts Receivable Account found");
                    AccountFunc.CheckAccounts(GetARAccount);
                } else {
                    $scope.AccountsReceivableAccountId = Accounts[0].UniqueID;
                    console.log("Accounts Receivable Accounts found:" + Accounts[0].UniqueID);
                }
            });
        }

        $scope.Applyfilter = function () {
            var search = {};
            var searchArray = [];
            var filter = null;
            var includefilter = false;

            if ($scope.Customer) {
                searchArray.push({ "Customer.UniqueID": $scope.Customer });
            }

            if ($scope.Status) {
                if ($scope.Status != "All") {
                    searchArray.push({ "InvoiceState": $scope.Status });
                }
            }

            if ($scope.FromDate) {
                searchArray.push({ "InvoiceHeader.InvoiceDate": { $gte: moment(new Date($scope.FromDate)).startOf('day') } });
            }

            if ($scope.ToDate) {
                searchArray.push({ "InvoiceHeader.InvoiceDate": { $lte: moment(new Date($scope.ToDate)).endOf('day') } });
            }

            if ($scope.InvoiceNumber) {
                searchArray.push({ "InvoiceHeader.InvoiceNumber": $scope.InvoiceNumber });

            }
            searchArray.length > 0 ? filter = { $and: searchArray } : filter = "";

            console.log("Search Filter is: " + JSON.stringify(filter));

            InvoiceFunc.searchInvoice(filter, GetInvoicesCallback);
        }

        $scope.ClearFilters = function () {


            InvoiceFunc.searchInvoice({}, GetInvoicesCallback);
            $scope.Status = "";
            $scope.FromDate = "";
            $scope.ToDate = "";
            $scope.InvoiceNumber = "";
            $scope.Customer = "";

            $timeout(function () {
                $("#Customer").select2("val", "");
            }, 1);

            //$('#Customer').val(null).trigger('change');


        }

        function GetInvoicesCallback(err, invoices) {

            var LatepaymentAmounts = [];

            if (err != null) {
                console.log("Error Getting Invoices:" + err);
            } else {

                var item = 0;
                invoices.forEach((invoice, index) => {
                    var invoiceLatePayment = {
                        invoiceUniqueID: "",
                        latePaymentFee: 0,
                        amountDue: 0
                    }

                    LatePayments.CalculateLatePaymentCharge(invoice.Customer.UniqueID, invoice.InvoicePayment.Balance, invoice.InvoiceHeader.PaymentDate, invoice.InvoiceHeader.PaymentTerms, "Invoice", function (LatePaymentDetails) {

                        
                        invoiceLatePayment.UniqueID = invoice.UniqueID;
                        invoiceLatePayment.latePaymentFee = LatePaymentDetails.LateFee;
                        invoiceLatePayment.amountDue = invoice.InvoicePayment.Balance + LatePaymentDetails.LateFee;

                        LatepaymentAmounts.push(invoiceLatePayment);
                        console.log("Found Late Payment Details: " + JSON.stringify(LatePaymentDetails));

                        console.log("item: " + item);
                        console.log("invoices.length: " + invoices.length);
                        item++
                        if (item == invoices.length) {
                            console.log("All Late Payment Details: " + JSON.stringify(LatepaymentAmounts));
                            callback();
                        }
                        
                    });





                });

                function callback() {
                    $('#InvoicesTable').dataTable({
                        data: invoices,
                        columns: [
                            {
                                'data': 'InvoiceState', 'render': function (data, type, row) {
                                    var type;
                                    var status = row.InvoiceState;
                                    if (row.InvoiceHeader.PaymentDate < moment().subtract(1, 'days').endOf('day') && status == "Unpaid") {
                                        status = "Overdue";
                                    }
                                    switch (status) {
                                        case "Draft":
                                            type = "info";
                                            break;
                                        case "To send":
                                            type = "primary";
                                            break;
                                        case "Unpaid":
                                            type = "warning";
                                            break;
                                        case "Paid":
                                            type = "success";
                                            break;
                                        case "Overdue":
                                            type = "danger";
                                            break;
                                        case "Canceled":
                                            type = "";
                                            break;
                                        default: type = "default";
                                            break;
                                    }
                                    return Controls.AsLink("createinvoice", row.UniqueID, '<span class="badge badge-pill badge-' + type + '" style="width:100%">' + status + '</span>');
                                }
                            },
                            { 'data': 'InvoiceHeader.InvoiceDate', 'render': function (data, type, row) { return Controls.AsLink("createinvoice", row.UniqueID, row.InvoiceHeader.InvoiceDate != null ? Controls.FormatDate(row.InvoiceHeader.InvoiceDate) : ""); } },
                            { 'data': 'InvoiceHeader.PaymentDate', 'render': function (data, type, row) { return Controls.AsLink("createinvoice", row.UniqueID, row.InvoiceHeader.PaymentDate != null ? Controls.FormatDate(row.InvoiceHeader.PaymentDate) : ""); } },
                            { 'data': 'InvoiceHeader.InvoiceNumber', 'render': function (data, type, row) { return Controls.AsLink("createinvoice", row.UniqueID, row.InvoiceHeader.InvoiceNumber); } },
                            { 'data': 'Customer.CustomerName', 'render': function (data, type, row) { return row.Customer ? Controls.AsLink("createinvoice", row.UniqueID, row.Customer.CustomerName) : ""; } },
                            { 'data': 'InvoiceHeader.InvoiceAmount', 'render': function (data, type, row) { return Controls.AsLink("createinvoice", row.UniqueID, CurrenciesList.Currencies.find(function (obj) { return obj.ISO === row.InvoiceHeader.InvoiceCurrency; }).Symbol + Controls.ToCurrency(row.InvoiceHeader.InvoiceAmount) + '</a>'); } },
                            {
                                'data': 'InvoiceHeader.InvoiceAmount', 'render': function (data, type, row) {

                                    var lateFee = LatepaymentAmounts.find(function (obj) {
                                        return obj.UniqueID == row.UniqueID;
                                    });
                                    lateFee = lateFee ? lateFee.latePaymentFee : 0;

                                    return Controls.AsLink("createinvoice", row.UniqueID, CurrenciesList.Currencies.find(function (obj) { return obj.ISO === row.InvoiceHeader.InvoiceCurrency; }).Symbol + Controls.ToCurrency(lateFee) + '</a>');
                                }
                            },
                            {
                                'data': 'InvoicePayment.Balance', 'render': function (data, type, row) {

                                    var amountDue = LatepaymentAmounts.find(function (obj) {
                                        return obj.UniqueID == row.UniqueID;
                                    });
                                    amountDue = amountDue ? amountDue.amountDue : row.InvoicePayment.Balance;

                                    return Controls.AsLink("createinvoice", row.UniqueID, CurrenciesList.Currencies.find(function (obj) { return obj.ISO === row.InvoiceHeader.InvoiceCurrency; }).Symbol + Controls.ToCurrency(amountDue));
                                }
                            },
                            {
                                'data': 'UniqueID', 'sortable': false, 'render': function (data, type, row) {

                                    var disablepayment = row.InvoiceState == "Paid" ? "disabled" : "";
                                    var mainButton;

                                    if (row.InvoiceState == "Paid" || row.InvoiceState == "Draft") {
                                        mainButton = '<a ng-reflect-router-link = "#!createinvoice/" href="#!createinvoice/' + row.UniqueID + '/view" class="btn btn-outline-default" style="color:#007bff;padding-left: 62px;">View</a>';
                                        view = '';
                                    } else {

                                        mainButton = '<button type="button" id="pay_' + row.UniqueID + '" ' + disablepayment + ' class="btn btn-outline-default" style="color: green;"><span class="glyphicon glyphicon-plus"style="margin-right: 5px;"></span><span >Payment</span></button>';
                                        view = '<li><a ng-reflect-router-link = "#!createinvoice/" href="#!createinvoice/' + row.UniqueID + '/view">View</a></li>';
                                    }

                                    return '<div class="btn-group">' +
                                        mainButton +
                                        '<button class="btn btn-sm btn-outline-default" type="button" data-toggle="dropdown">' +
                                        '<span class="glyphicon glyphicon-chevron-down"></span></button>' +
                                        '<ul class="dropdown-menu jc-dropdown link">' +
                                        view +
                                        '<li><a ng-reflect-router-link = "#!createinvoice/" href="#!createinvoice/' + row.UniqueID + '/duplicate">duplicate</a></li>' +
                                        '<li class="divider"></li>' +
                                        '<li><button type="button" id="delete_' + row.UniqueID + '" class="btn btn-outline-default" style="color: red; padding:0"><span class="glyphicon glyphicon-trash"style="margin-right: 5px;"></span><span >delete</span></button></div></li>' +
                                        '</ul>' +
                                        '</div></div>';
                                }
                            },
                        ],
                        bDestroy: true
                    });
                    $('#InvoicesTable tbody').unbind("click").on('click', 'button', function () {
                        if ($(this).attr('id') && $(this).attr('id').split("_")[0]) {
                            console.log("button clicked is:" + $(this).attr('id'));
                            var buttonType = $(this).attr('id').split("_")[0];
                            var InvoiceId = $(this).attr('id').split("_")[1];
                            console.log(buttonType);
                            console.log("Customer id is: " + InvoiceId);


                            switch (buttonType) {
                                case "pay":
                                    InvoiceFunc.getInvoiceByID(InvoiceId, function (err, docs) {
                                        if (err != null) {
                                            console.log("Error retriving invoice: " + err);
                                        } else {


                                            $scope.InvoicePayment = new InvPaymentFunc.InvoicePayment;
                                            $scope.Invoice = docs[0];


                                            //Handle late payment fees
                                            LatePayments.CalculateLatePaymentCharge($scope.Invoice.Customer.UniqueID, $scope.Invoice.InvoicePayment.Balance, $scope.Invoice.InvoiceHeader.PaymentDate, $scope.Invoice.InvoiceHeader.PaymentTerms, "Invoice", function (LatePaymentDetails) {
                                                $scope.DaysOverDue = LatePaymentDetails.DaysOverDue;
                                                $scope.LateFee = LatePaymentDetails.LateFee;
                                                $scope.InvoiceBalance = $scope.Invoice.InvoicePayment.Balance + LatePaymentDetails.LateFee;
                                                console.log("Late Payment Details: " + JSON.stringify(LatePaymentDetails));
                                                $scope.Invoice.InvoicePayment.Balance = $scope.InvoiceBalance;

                                                $("#ActualPaymentDate").datepicker({
                                                    format: 'dd/mm/yyyy'
                                                }).datepicker("setDate", new Date());
                                                $scope.InvoicePayment.PaymentDate = new Date();
                                                $("#NextPaymentDate").datepicker({
                                                    format: 'dd/mm/yyyy'
                                                }).datepicker("setDate", new Date());
                                                $scope.Invoice.InvoicePayment.NextPaymentDate = new Date();
                                                $("#PaymentDetails").val("");
                                                $("#PaymentMethod").val("Select");
                                                $scope.InvoicePayment.AmountRecieved = $scope.Invoice.InvoicePayment.Balance;
                                                $("#NextPaymentDateDiv").hide();
                                                $("#PaymentError").hide();
                                                Controls.SetError("exchangeRate", "");
                                                Controls.SetError("AmountRecieved", "");
                                                Controls.SetError("NextPaymentDate", "");
                                                Controls.SetError("PaymentMethod", "");
                                                Controls.SetError("IncomeAccount", "");
                                                $scope.CurrencySymbol = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Invoice.InvoiceHeader.InvoiceCurrency; }).Symbol;
                                                $scope.Invoice.InvoicePayment.Balance = Controls.ToCurrency($scope.Invoice.InvoicePayment.Balance);
                                                $scope.InvoicePayment.ExchangeRate = $scope.Invoice.InvoiceHeader.ExchangeRate;
                                                $scope.$apply();
                                                $('#InvoicePaymentModal').modal('toggle');
                                            });                                            
                                        }
                                    });
                                    break;
                                case "delete":
                                    console.log("delete action");
                                    InvoiceFunc.getInvoiceByID(InvoiceId, function (err, docs) {
                                        if (err != null) {
                                            console.log("Error retriving invoice: " + err);
                                        } else {
                                            $scope.DeleteInvoice = docs[0];
                                            $scope.$apply();
                                            $('#DeleteInvoice').modal('toggle');
                                            console.log("Delete Invoice" + $scope.DeleteInvoice.InvoiceHeader.InvoiceNumber);
                                        }
                                    });

                                    break;
                                default:
                                    console.log("Unknown action");
                                    break;
                            }
                        }
                    });
                    $scope.fillCounters();
                }
            }
        }

        $scope.RemoveInvoice = function () {

            var InvoiceID = $scope.DeleteInvoice.UniqueID;

            console.log("Deleting Invoice" + JSON.stringify($scope.DeleteInvoice));
            InvoiceFunc.deleteInvoice($scope.DeleteInvoice, function (err, numRemoved) {
                if (err != null) {
                    console.log("Error deleting Invoice: " + err);
                } else {

                    console.log("Deleted " + numRemoved + " Invoice");
                    $scope.DeleteInvoice.DeletedBy = $rootScope.CurrentUser.Username;
                    TransactionFunc.deleteRelatedTransactions($scope.DeleteInvoice.UniqueID, function (err, numRemoved) {

                        if (err != null) {
                            console.log("Error deleting Invoice Transactions: " + err);
                        } else {
                            $('#DeleteInvoice').modal('toggle');
                            console.log("Deleted " + numRemoved + " Transactions");
                            Controls.showAlert("Alert", "alert-success", "Invoice #" + "'" + $scope.DeleteInvoice.InvoiceHeader.InvoiceNumber + "'" + " succesfully deleted", "message", true);
                            $scope.DeleteInvoice = new InvoiceFunc.Invoice(); //Clear New Invoice
                            $scope.$apply();
                            InvoiceFunc.getInvoice("", GetInvoicesCallback);
                        }
                    });
                }
            });

            InvPaymentFunc.getPayments(InvoiceID, function (payments) {

                if (payments == null) {
                    console.log("Error getting Invoice payments: " + err);
                } else {

                    InvPaymentFunc.deleteAllPayments(InvoiceID, function (err, numRemoved) {
                        if (err != null) {
                            console.log("Error deleting Invoice payments: " + err);
                        } else {

                            console.log("Deleted " + numRemoved + " Invoice payments");
                            payments.forEach(payment => {

                                TransactionFunc.deleteRelatedTransactions(payment.UniqueID, function (err, numRemoved) {

                                    if (err != null) {
                                        console.log("Error deleting payment Transactions: " + err);
                                    } else {
                                        console.log("Deleted " + numRemoved + " Transactions");
                                    }

                                });
                            });
                        }
                    });
                }
            });
        };

        function GetCustomers() {
            CustomerFunc.getCustomer("", function (customers) {
                if (customers.length == 0) {
                    console.log("No customers found");
                } else {
                    $scope.Customers = customers;
                    $scope.$apply();
                }
            });
        }




        $scope.fillCounters = function () {

            //All Invoices
            InvoiceFunc.CountInvoices({}, function (err, total) {
                if (err) {
                    console.log("Error:" + err);
                } else {
                    Controls.updateCounters(total, "AllInvoices");
                    console.log("All Invoices: " + total);
                }
            });

            //Draft Invoices
            InvoiceFunc.CountInvoices({ $or: [{ InvoiceState: "Draft" }, { InvoiceState: "To send" }] }, function (err, total) {
                if (err) {
                    console.log("Error:" + err);
                } else {
                    Controls.updateCounters(total, "DraftInvoices");
                    console.log("Draft Invoices: " + total);
                }
            });

            //Paid Invoices
            InvoiceFunc.CountInvoices({ InvoiceState: "Paid" }, function (err, total) {
                if (err) {
                    console.log("Error:" + err);
                } else {
                    Controls.updateCounters(total, "PaidInvoices");
                    console.log("Paid Invoices: " + total);
                }
            });

            //Unpaid Invoices
            InvoiceFunc.CountInvoices({ InvoiceState: "Unpaid" }, function (err, total) {
                if (err) {
                    console.log("Error:" + err);
                } else {
                    Controls.updateCounters(total, "UnpaidInvoices");
                    console.log("Unpaid Invoices: " + total);
                }
            });

            //Late Invoices
            InvoiceFunc.CountInvoices({ $and: [{ "InvoiceState": "Unpaid" }, { "InvoiceHeader.PaymentDate": { $lt: moment().startOf('day') } }] }, function (err, total) {
                if (err) {
                    console.log("Error:" + err);
                } else {
                    Controls.updateCounters(total, "LateInvoices");
                    console.log("Late Invoices: " + total);
                }
            });
        }

        $scope.NewInvoice = function () {
            $location.path('createinvoice');
        };
        //************************************************************* Process Invoice Payment **************************************************************************
        $scope.RecordPayment = function () {
            $("#ActualPaymentDate").datepicker().datepicker("setDate", new Date());
            $scope.Invoice.InvoicePayment.NextPaymentDate = "";
            $("#PaymentDetails").val("");
            $("#PaymentMethod").val("Select");
            $scope.InvoicePayment.AmountRecieved = $scope.Invoice.InvoicePayment.Balance;
            console.log("Amount Due " + $scope.Invoice.InvoicePayment.Balance);
            $("#NextPaymentDateDiv").hide();
            $("#PaymentError").hide();

        };

        $scope.PaymentAmntOnChange = function () {

            var Balance = Controls.ToNumber($scope.Invoice.InvoicePayment.Balance) - $scope.InvoicePayment.AmountRecieved;
            console.log("Baance is: " + Balance);
            console.log("more than amount: " + $scope.InvoicePayment.AmountRecieved + "   " + $scope.Invoice.InvoicePayment.Balance);
            $("#NextPaymentDateDiv").hide();
            Controls.SetError("AmountRecieved", "");
            if (isNaN($scope.InvoicePayment.AmountRecieved) || $scope.InvoicePayment.AmountRecieved == null) {
                console.log("Is not a number");
                //$scope.InvoicePayment.AmountRecieved = 0.00;
            } else if (Balance < 0) {
                Controls.SetError("AmountRecieved", "Invalid Balance");

            } else if (Balance > 0.00) {
                $("#NextPaymentDateDiv").show();
                Controls.SetError("NextPaymentDate", "Enter Next Payment Date");
            }
            else {
                $("#NextPaymentDateDiv").hide();
                Controls.SetError("NextPaymentDate", "");
            }
        }

        $scope.PaymentDateChange = function () {
            if ($("#ActualPaymentDate").val() == "") {
                $("#ActualPaymentDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", new Date());
            }
            var dateMomentObject = moment($scope.FormActualPaymentDate, "DD/MM/YYYY"); // 1st argument - string, 2nd argument - format
            $scope.InvoicePayment.PaymentDate = dateMomentObject.toDate(); // convert moment.js object to Date object
        }

        $scope.NextPaymentDateChange = function () {
            console.log("Payment date changed");
            if ($("#NextPaymentDate").val() == "") {
                $("#NextPaymentDate").datepicker({
                    format: 'dd/mm/yyyy'
                }).datepicker("setDate", new Date());
            }
            var dateMomentObject = moment($scope.FormNextPaymentDate, "DD/MM/YYYY"); // 1st argument - string, 2nd argument - format
            $scope.Invoice.InvoicePayment.NextPaymentDate = dateMomentObject.toDate(); // convert moment.js object to Date object
        }

        //Record Payment
        $scope.SubmitPayment = function () {

            var Balance = Controls.ToNumber($scope.Invoice.InvoicePayment.Balance) - $scope.InvoicePayment.AmountRecieved;
            Controls.SetError("exchangeRate", "");
            Controls.SetError("AmountRecieved", "");
            Controls.SetError("NextPaymentDate", "");
            Controls.SetError("PaymentMethod", "");

            if ($scope.InvoicePayment.PaymentMethod == null || $scope.InvoicePayment.PaymentMethod == "" || $scope.InvoicePayment.PaymentMethod == "Select") {
                Controls.SetError("PaymentMethod", "Enter Payment Method");
            }
            else if ($scope.InvoicePayment.IncomeAccount == null || $scope.InvoicePayment.IncomeAccount == "" || $scope.InvoicePayment.IncomeAccount == "Select") {
                Controls.SetError("IncomeAccount", "Select Account");
            }
            else if ($scope.InvoicePayment.AmountRecieved <= 0 || $scope.InvoicePayment.AmountRecieved == null) {
                Controls.SetError("AmountRecieved", "Enter Amount Recieved");
            }
            else if (Balance < 0) {
                Controls.SetError("AmountRecieved", "Amount Recieved Is Greater Than Amount Due");
            }
            else if (Balance > 0.00 && ($scope.Invoice.InvoicePayment.NextPaymentDate == "" || $scope.Invoice.InvoicePayment.NextPaymentDate == null)) {
                Controls.SetError("NextPaymentDate", "Enter Next Payment Date");
            }
            else if (isNaN($scope.InvoicePayment.ExchangeRate) || !($scope.InvoicePayment.ExchangeRate > 0)) {
                Controls.SetError("exchangeRate", "Enter a valid exchange rate");
            }
            else {

                $scope.Invoice.InvoicePayment.Balance = Controls.ToNumber($scope.Invoice.InvoicePayment.Balance) - Controls.ToNumber($scope.InvoicePayment.AmountRecieved);
                $scope.Invoice.InvoicePayment.AmountRecieved = Controls.ToNumber($scope.Invoice.InvoiceHeader.InvoiceAmount) - Controls.ToNumber($scope.Invoice.InvoicePayment.Balance);

                $scope.InvoicePayment.InvoiceCurrency = $scope.Invoice.InvoiceHeader.InvoiceCurrency;
                $scope.InvoicePayment.CompanyCurrency = $scope.Invoice.InvoiceHeader.CompanyCurrency;
                $scope.InvoicePayment.ReferenceCurrencyRate = $scope.Settings.DailyExchangeRate;
                $scope.InvoicePayment.ReferenceCurrency = $scope.Settings.ReferenceCurrency;

                if ($scope.Invoice.InvoicePayment.Balance > 0.00) {
                    $scope.Invoice.InvoicePayment.PaymentStatus = "Unpaid";
                    $scope.InvoicePayment.PaymentType = "Partial Payment";
                    $scope.Invoice.InvoiceState = "Unpaid";
                    $scope.Invoice.InvoicePayment.NextPaymentDate = new Date($scope.Invoice.InvoicePayment.NextPaymentDate);
                    console.log("partial payment");
                } else {
                    $scope.Invoice.InvoicePayment.PaymentStatus = "Paid";
                    $scope.Invoice.InvoiceState = "Paid";
                    $scope.InvoicePayment.PaymentType = "Full Payment";
                    console.log("Payment Date: " + $scope.InvoicePayment.PaymentDate)
                    $scope.Invoice.InvoicePayment.PaymentDate = new Date($scope.InvoicePayment.PaymentDate);
                    $scope.Invoice.InvoicePayment.NextPaymentDate = "",
                        console.log("Full payment:");
                }
                $scope.InvoicePayment.Balance = $scope.Invoice.InvoicePayment.Balance;
                $scope.InvoicePayment.PaymentDate = new Date($scope.InvoicePayment.PaymentDate);
                console.log("Payment Date: " + $scope.InvoicePayment.PaymentDate)

                $scope.InvoicePayment.InvoiceNumber = $scope.Invoice.InvoiceHeader.InvoiceNumber;
                $scope.InvoicePayment.InvoiceID = $scope.Invoice.UniqueID;
                $scope.InvoicePayment.CustomerID = $scope.Invoice.Customer.UniqueID;
                $scope.InvoicePayment.AmountDue = $scope.Invoice.InvoiceHeader.InvoiceAmount;


                console.log("Payment details:" + JSON.stringify($scope.Invoice.InvoicePayment));
                console.log("Invoice payment info:" + JSON.stringify($scope.InvoicePayment));

                $scope.InvoicePayment.CreatedBy = $rootScope.CurrentUser.Username;

                InvPaymentFunc.recordPayment($scope.InvoicePayment, RecordPaymentCallBack, $scope.Invoice.InvoicePayment, $scope.Invoice.InvoiceState);
            }

        };



        function RecordPaymentCallBack(payment, PaymentInfo, InvoiceState, error) {
            if (error) {
                Controls.showAlert("PaymentError", "alert-danger", "Error updating payment: " + error, "message", false);
                GetInvoice();

            } else {

                console.log("payment info is: " + payment.IncomeAccount);
                var PaymentTransaction = {
                    AccountId: payment.IncomeAccount,
                    Date: payment.PaymentDate,
                    DocumentReference: payment.InvoiceNumber,
                    DocumentId: payment.UniqueID,
                    Details: "Payment for Invoice:" + payment.InvoiceNumber + "\n" + (payment.PaymentDetails || ""),
                    Debit_CompanyCurrencyAmount: payment.AmountRecieved * payment.ExchangeRate,
                    Debit_AccountCurrencyAmount: payment.AmountRecieved * payment.ExchangeRate,
                    Debit_ReferenceCurrencyAmount: payment.AmountRecieved * payment.ReferenceCurrencyRate,
                    Credit_CompanyCurrencyAmount: "",
                    Credit_AccountCurrencyAmount: "",
                    Credit_ReferenceCurrencyAmount: "",
                }

                TransactionFunc.RecordTransaction(PaymentTransaction, null, function (err, PaymentTransactionId) {

                    if (err) {
                        console.log("error creating transaction :" + err);
                    } else {
                        console.log("Updated payment with ID " + PaymentTransactionId);

                        //Record Accounts Receivable transaction
                        var AccountsReceivableTransaction = {
                            AccountId: $scope.AccountsReceivableAccountId,
                            Date: payment.PaymentDate,
                            DocumentReference: PaymentTransactionId,
                            DocumentId: payment.UniqueID,
                            Details: "Payment for Invoice:" + payment.InvoiceNumber + "\n" + (payment.PaymentDetails || ""),
                            Debit_CompanyCurrencyAmount: "",
                            Debit_AccountCurrencyAmount: "",
                            Debit_ReferenceCurrencyAmount: "",
                            Credit_CompanyCurrencyAmount: payment.AmountRecieved * payment.ExchangeRate,
                            Credit_AccountCurrencyAmount: payment.AmountRecieved * payment.ExchangeRate,
                            Credit_ReferenceCurrencyAmount: payment.AmountRecieved * payment.ReferenceCurrencyRate,
                        }
                        TransactionFunc.RecordTransaction(AccountsReceivableTransaction, null, function (err, ARTransactionId) {

                            if (err) {
                                console.log("error creating Accounts Receivable Transaction :" + err);
                            } else {
                                console.log("Recorded Accounts Receivable Transaction with ID " + ARTransactionId);
                                InvPaymentFunc.updatePaymentTransactionID(payment.UniqueID, PaymentTransactionId, function (err, doc) {
                                    if (err) {
                                        console.log("error updating Invoice :" + err);
                                    } else {
                                        console.log("Payment Transaction complete! " + JSON.stringify(doc));

                                    }
                                });
                            }
                        });
                    }
                });
                InvoiceFunc.invoicePayment($scope.Invoice.UniqueID, PaymentInfo, InvoiceState, UpdateInvoicePaymentCallback);

            }
        }

        function UpdateInvoicePaymentCallback(id, error) {
            if (error) {
                Controls.showAlert("PaymentError", "alert-danger", "Error updating payment: " + error, "message", false);
                GetInvoices();

            } else {
                $('#InvoicePaymentModal').modal('hide');
                Controls.showAlert("Alert", "alert-success", "Invoice: " + "'" + $scope.Invoice.InvoiceHeader.InvoiceNumber + "'" + " payment recorded", "message", true);
                GetInvoices();
            }
        }
        function GetPayments() {

            InvPaymentFunc.getPayments(InvoiceID, function (payments) {

                $scope.PaymentsTable = $('#InvoicePayments').DataTable({

                    data: payments,
                    rowId: "UniqueID",
                    columns: [
                        { 'data': 'PaymentDate', 'render': function (date) { return date.toLocaleDateString(); } },
                        { 'data': 'PaymentType' },
                        { 'data': 'PaymentMethod' },
                        { 'data': 'AmountRecieved', 'render': function (amount) { return $scope.CurrencySymbol + parseFloat(amount).toFixed(2); } }
                    ],
                    paging: false,
                    searching: false,
                    lengthChange: false,
                    info: false


                });
            });
        }
        //******************************************************************************************************************************************************************************************
    }

});

//*********************************************************************************************************************************************************** */

// Controller variables:


$(document).ready(function () {

});



