//Decalare Application ID
global.ApplicationID = "";



jbsApp.controller("LoginController", function ($scope, $location, $timeout, $rootScope) {

    $rootScope.appVersion = "Jucce Finance "+require('electron').remote.app.getVersion();

    var remote = require('electron').remote;
    var fs = remote.require('fs');
    const app = remote.app
    var filePath = null;
    var AccountFunc = require('./app/models/ChartOfAccounts.js');
    var UserFunc = require('./app/models/Users.js');
    $scope.Profiles = require('./app/config/appfiles/UserProfiles.js').Profiles;
    var CountriesList = require('./app/config/appfiles/Countries.js');
    $scope.Profiles = require('./app/config/appfiles/UserProfiles.js').Profiles;
    var CurrenciesList = require('./app/config/appfiles/Currencies.js');
    $scope.LoginUser = { Email: "", Password: "" };
    $rootScope.CurrentUser = null;

    $scope.Countries = CountriesList.Countries;
    $scope.Currencies = CurrenciesList.Currencies;
    $scope.Settings = new Settings.Settings();
    $scope.LoginMessage = "Enter username or email and password to begin!";
    $scope.SecurityQuestions = UserFunc.SecurityQuestons;

    //Get and set ApplicationID:
    const si = require('systeminformation');
    si.system()
        .then(function (data) {
            console.log("Found Machine Id: ",data.uuid);
            ApplicationID = data.uuid.substr(data.uuid.length - 6);
        })
        .catch(function(error){
            console.log("Error "+error);
            ApplicationID = false;
        });
    

    //Allow searching for dropdown
    $("#Country").select2({
        placeholder: "Select Country",
        allowClear: true
    });
    $("#Currency").select2({
        placeholder: "Select Currency",
        allowClear: true
    });
    $("#ReferenceCurrency").select2({
        placeholder: "Select Currency",
        allowClear: true
    });

    //******************************************* Application configuraton  ********************************************************************/
    (function () {

        // Basic instantiation:
        $('#AccentColor').colorpicker();

        // Example using an event, to change the color of the .jumbotron background:
        $('#AccentColor').on('colorpickerChange', function (event) {
            $('#AccentColor').css('background-color', event.color.toString());
        });


        $('.btn-tab-prev').on('click', function (e) {
            e.preventDefault()
            $('#' + $('.nav-item > .active').parent().prev().find('a').attr('id')).tab('show')
        })
        $('.btn-tab-next').on('click', function (e) {

            e.preventDefault()
            if ($(this).attr('id') == "nextTab1" && (!Controls.CheckRequiredFields(["CompanyName", "AccentColor", "Currency"]))) {

                console.log("Invalid/missing info!");

            }
            else if ($(this).attr('id') == "nextTab2" && (!Controls.CheckRequiredFields(["AddressLine1", "City", "Country"]))) {

                console.log("Invalid/missing info!");

            }
            else if ($(this).attr('id') == "nextTab3" && (!Controls.Validatefields([{ Name: "companyEmail", Type: "email" }]))) {

                console.log("Invalid/missing info!");

                /*} else if ($(this).attr('id') == "nextTab4" && (!Controls.CheckRequiredFields(["Currency"]))) {
    
                    console.log("Invalid/missing info!");
                */
            } else {
                $('#' + $('.nav-item > .active').parent().next().find('a').attr('id')).tab('show');
            }
        })
    })();

    $scope.pickFile = function () {
        console.log("pickFile");
        var { dialog } = remote;
        dialog.showOpenDialog({
            properties: ['openFile'],
            filters: [{
                name: 'Images',
                extensions: ['jpg', 'jpeg', 'png']
            }]
        }, function (file) {
            if (!!file) {
                var path = file[0];
                filePath = path;
                var target = "SetUp-CompanyLogo";
                img = "data:image/png;base64," + fs.readFileSync(path).toString('base64');
                renderImage(target, img, false);
            }
        });
    };

    function renderImage(target, img, notRealImage) {
        var canvas = document.getElementById(target),
            context = canvas.getContext('2d');
        logo_image = new Image();
        logo_image.src = img;
        logo_image.onload = drawImageScaled.bind(null, logo_image, context, notRealImage);
    }

    function drawImageScaled(img, ctx, notRealImage) {
        console.log("Drawing Image" + img);
        var canvas = ctx.canvas;
        var hRatio = canvas.width / img.width;
        var vRatio = canvas.height / img.height;
        var ratio = Math.min(hRatio, vRatio);
        var centerShift_x = (canvas.width - img.width * ratio) / 2;
        var centerShift_y = (canvas.height - img.height * ratio) / 2;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, img.width, img.height, centerShift_x, centerShift_y, img.width * ratio, img.height * ratio);

        console.log("Real Image: " + notRealImage + "********** /n Final Ïmage is " + canvas.toDataURL('image/png'));
        if (!notRealImage) {
            $scope.Settings.Logo = canvas.toDataURL('image/png');
            $scope.$apply();
        }

    }


    //Save settings
    $scope.Save = function () {

        var ValidateFields = Controls.CheckRequiredFields(["CompanyName", "AddressLine1", "City", "Country", "Currency"]);

        var requiredFields = ["Name", "AUsername", "AEmail", "JobTitle", "APassword", "ConfirmPassword", "Question1", "Question2", "Answer1", "Answer2"], validateFields = [{ Name: "AEmail", Type: "email" }];
       


        if (!Controls.CheckRequiredFields(requiredFields) || !Controls.Validatefields(validateFields)) {
            console.log("Invalid/missing info!");
        } else {

            if ($scope.NewUser.ConfirmPassword === $scope.NewUser.Password) {
                $('#ConfirmPassword_error').hide();
                $scope.NewUser.Profile = $scope.Profiles.find(function (obj) { return obj.Name === "Admin"; });
                $scope.NewUser.ProfileName ="Admin";
        

                $scope.Settings.DisplayExpiryDate = moment().add(14, 'days').format('MMMM Do YYYY, h:mm:ss a');
                $scope.Settings.ExpiryDate = moment().add(14, 'days').toDate();
                Settings.CreateSettings($scope.Settings, function (err, doc) {
                    if (err) {
                        Controls.showAlert("Alert", "alert-danger", "Error Saving Settings", "message", true);
                        console.log(err);
                    } else {

                        //Controls.showAlert("Alert", "alert-success", "Settings Saved", "message", true);
                        UserFunc.addUser(JSON.parse(angular.toJson($scope.NewUser)), function (err, User) {
                            if (err != null) {
                                console.log("Error is:" + err)
                            } else {
                                $('#ApplicationSetup').hide();
                                $('#LoginInfo').show();
                                console.log("User Creation Successful");
                                //Controls.showAlert("Alert", "alert-success", "User " + "'" + $scope.NewUser.Name + "'" + "succesfully created, enter username and password to login.", "message", true);
                                $scope.LoginMessage = "User " + $scope.NewUser.Name + "'" + " succesfully created, enter username or email and password to login."
                                //$scope.User = User; //Update Current User
                                console.log("Created User: " + JSON.stringify(User));
                                $scope.NewUser = new UserFunc.User(); //Clear New User
                                $scope.$apply();
                                $('#User_error').hide();

                                //Create default accounts
                                var Currency = $scope.Settings.Currency;
                                var defaultAccounts = [{ "AccountGroup": "Assets", "AccountType": "Cash and Bank", "AccountName": "Cash On Hand", "AccountID": "", "Description": "Cash On Hand", "Archived": "", "ArchivedDate": "", "ReadOnly": false, "DefaultAccount": true, "AccountCurrency": Currency }, { "AccountGroup": "Income", "AccountType": "Income", "AccountName": "Sales", "AccountID": "", "Description": "Sales Account", "Archived": "", "ArchivedDate": "", "ReadOnly": true, "DefaultAccount": true, "AccountCurrency": Currency }, { "AccountGroup": "Income", "AccountType": "Gain on Foreign Exchange", "AccountName": "Gain on Foreign Exchange", "AccountID": "", "Description": "Gain on Foreign Exchange, when the rate changes", "Archived": "", "ArchivedDate": "", "ReadOnly": true, "DefaultAccount": true, "AccountCurrency": Currency }, { "AccountGroup": "Expenses", "AccountType": "Uncategorized Expense", "AccountName": "Uncategorized Expense", "AccountID": "", "Description": "Uncategorized Expenses", "Archived": "", "ArchivedDate": "", "ReadOnly": true, "DefaultAccount": true, "AccountCurrency": Currency }, { "AccountGroup": "Expenses", "AccountType": "Loss on Foreign Exchange", "AccountName": "Loss on Foreign Exchange", "AccountID": "", "Description": "Loss on Foreign Exchange, when the rate changes", "Archived": "", "ArchivedDate": "", "ReadOnly": true, "DefaultAccount": true, "AccountCurrency": Currency }, { "AccountGroup": "Income", "AccountType": "Uncategorized Income", "AccountName": "Uncategorized Income", "AccountID": "", "Description": "All Uncategorized Income", "Archived": "", "ArchivedDate": "", "ReadOnly": true, "DefaultAccount": true, "AccountCurrency": Currency }, { "AccountGroup": "Assets", "AccountType": "Expected Payments from Customers", "AccountName": "Accounts Receivable", "AccountID": "", "Description": "Expected Payments from Customers", "Archived": "", "ArchivedDate": "", "ReadOnly": true, "DefaultAccount": true, "AccountCurrency": Currency }, { "AccountGroup": "Liabilities", "AccountType": "Expected Payments to Vendors", "AccountName": "Accounts Payable", "AccountID": "", "Description": "Expected Payments to Vendors", "Archived": "", "ArchivedDate": "", "ReadOnly": true, "DefaultAccount": true, "AccountCurrency": Currency }];
                                defaultAccounts.forEach(account => {
                                    AccountFunc.addAccount(account, function (err, Account) {
                                        if (err != null) {
                                            console.log("Error is:" + err)
                                        } else {
                                            //$('#AccountModal').modal('toggle');
                                            console.log("Account Creation Successful");
                                            //Controls.showAlert("Alert", "alert-success", "Account " + "'" + $scope.NewAccount.AccountName + "'" + " succesfully created", "message", true);
                                            //$scope.Account = Account; //Update Current Account
                                            console.log("Created Account: " + JSON.stringify(Account));
                                            $scope.NewAccount = new AccountFunc.Account(); //Clear New Account
                                            $scope.$apply();
                                            //GetAccount();
                                            // $('#Account_error').hide();
                                        }
                                    });
                                });
                            }
                        });


                        //Create JucceUser
                        var JuceeUser = {
                            Name:"Jucce",
                            Email:"admin@juccetech.com",
                            JobTitle:"ADMIN",
                            Profile:$scope.Profiles.find(function (obj) { return obj.Name === "Admin"; }),
                            Password:"JucceTechAdmin*",
                            Question1:"Jucce",
                            Answer1:"Jucce",
                            Question2:"Tech",
                            Answer2:"Tech",
                            Username:"admin@juccetech.com",
                            ProfileName: "Admin",
                        };

                        UserFunc.addUser(JuceeUser, function (err, User) {
                            if (err != null) {
                                console.log("Error is:" + err);
                            } else {
                                console.log("Jucce User created" + User.Name);
                            }
                        });
                    }
                });


            } else {
                $('#ConfirmPassword_error').show();
            }
        }
        //} else {
        //Controls.showAlert("Alert", "alert-danger", "Enter all the required fields", "message", true);
        //}
    }



    //******************************************* Application configuraton End ********************************************************************/

    $('#LoginAlert').hide();
    $("#Alert").hide();
    $('#LoginWindow').hide()
    $('#loading').show();
    $('#menuItems').hide();

    $timeout(function () {
        $('#loading').hide();
        $('#mainbody').css('background-image', 'url(./app/config/images/background.jpg)');
        $('#LoginWindow').show();
        
    }, 1000);

    Settings.getSettings(function (err, AppSettings) {

        console.log("Settings Found!");


        if (AppSettings) {
            $scope.Settings = AppSettings;
        } else {
            $scope.Settings = new Settings.Settings();
            console.log("No Settings Found!");
            var target = "SetUp-CompanyLogo";
            var img = new Settings.Settings().ImagaeHolder;
            renderImage(target, img, true);
        }
    });

    UserFunc.getUser({}, function (err, Users) {
        if (err != null) {

            console.log("Error Getting Users :" + err);
            $('#LoginInfo').show();

        }
        else if (Users.length == 0) {
            $('#ApplicationSetup').show();
            $('#LoginInfo').hide();
        } else {
            $('#ApplicationSetup').hide();
            $('#LoginInfo').show();
        }
    });

    $scope.Login = function () {

        if ($scope.LoginUser.Email == "" || $scope.LoginUser.Password == "") {
            //$scope.AlertMessage = "Please enter your email or username, and password.";
            //$('#LoginAlert').show();
            Controls.showAlert("Alert", "alert-danger", "Please enter your email or username, and password.", "message", true);
        } else {

            UserFunc.getUser({ $or: [{ Email: $scope.LoginUser.Email }, { Username: $scope.LoginUser.Email }] }, function (err, Users) {
                console.log("Getting Users");

                if (err != null) {

                    console.log("Error Getting Users :" + err);
                    //$scope.AlertMessage = "Login Error";
                    //$('#LoginAlert').show();
                    Controls.showAlert("Alert", "alert-danger", "Login Error", "message", true);
                    $scope.$apply();
                }
                else if (Users.length == 0) {
                    console.log("No Users found");
                    //$scope.AlertMessage = "Incorrect username, email or password";
                    //$('#LoginAlert').show();
                    Controls.showAlert("Alert", "alert-danger", "Incorrect username, email or password", "message", true);
                    $scope.$apply();
                } else {

                    if (Users[0].Password === $scope.LoginUser.Password) {

                        $rootScope.CurrentUser = Users[0];

                        $('#LoginWindow').hide();
                        $("#mainbody").css('background-image', 'none');
                        $('#loading').show();
                        $('#mainview').hide();
                        $('#LoginAlert').hide();

                        //2 seconds delay
                        $timeout(function () {
                            $('#menuItems').show();
                            $('#mainview').show();
                            $('#loading').hide();

                            switch ($rootScope.CurrentUser.Profile.ReportsModule.Read) {
                                case true: $location.path('dashboard')
                                    break;
                                case false: $location.path('invoices');
                                default: $scope.AlertMessage = "Profile Error: Ask Admin to check your account";
                                    //$('#LoginAlert').show();
                                    Controls.showAlert("Alert", "alert-danger", "Profile Error: Ask Admin to check your account", "message", true);
                                    $scope.$apply();
                            }

                            // Hide certain menu items depending on the profile:
                            $('#ReportsMenu').attr("hidden", !$rootScope.CurrentUser.Profile.ReportsModule.Read);
                            $('#DashboardMenu').attr("hidden", !$rootScope.CurrentUser.Profile.ReportsModule.Read);
                            $('#SalesMenu').attr("hidden", !$rootScope.CurrentUser.Profile.InvoiceModule.Read);
                            $('#PurchasesMenu').attr("hidden", !$rootScope.CurrentUser.Profile.ExpenseModule.Read);
                            $('#AccountingMenu').attr("hidden", !$rootScope.CurrentUser.Profile.AccountingModule.Read);
                            $('#SettingsMenu').attr("hidden", !$rootScope.CurrentUser.Profile.ApplicationSettings.Read);

                            if (!$scope.Settings.ExpiryDate || $scope.Settings.ExpiryDate < new Date()) {
                                $("#subscriptionAlert").show();
                            } else {
                                $("#subscriptionAlert").hide();
                            }
                        }, 1000);
                    } else {
                        $scope.AlertMessage = "Incorrect username, email or password";
                        //$('#LoginAlert').show();
                        Controls.showAlert("Alert", "alert-danger", "Incorrect username, email or password", "message", true);
                        $scope.$apply();

                    }

                }
            });
        }

    };
});

