const { data } = require('jquery');

jbsApp.controller("mainDashboardController", function ($scope, $location, $rootScope) {

    var CurrenciesList = require('./app/config/appfiles/Currencies.js');
    var InvoiceFunc = require('./app/models/invoice.js');
    var CustomerFunc = require('./app/models/Customer.js');
    var ExpenseFunc = require('./app/models/Expense.js');
    var AccountFunc = require('./app/models/ChartOfAccounts.js');
    var TransactionFunc = require('./app/models/Transactions.js');
    var TransactionData = require('./app/lib/TransactionData.js');
    var moment = require('moment');

    //Check User Data Session 
    console.log("Current User is: " + JSON.stringify($rootScope.CurrentUser))
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {

        $("#Customer").select2({
            placeholder: "Select Customer",
            allowClear: true
        });

        $('#ExpensePeriod').on('change', function () {
            GetExpenseDistribution(this.value)
            console.log('GetExpenseDistribution')
        });

        //load app settings
        Settings.getSettings(function (err, AppSettings) {
            $scope.Settings = AppSettings;
            $scope.CurrencySymbol = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Settings.Currency; }).Symbol;
            //GetInvoices();
            $scope.fillCounters();
            GetAccounts();
            GetExpenseDistribution('This Month');
            $scope.$apply();
            $("#Alert").hide();
        });

        $scope.Applyfilter = function () {
            var search = {};
            var searchArray = [];
            var filter = null;
            var includefilter = false;

            if ($scope.Customer) {
                searchArray.push({ "Customer.UniqueID": $scope.Customer });
            }

            if ($scope.Status) {
                if ($scope.Status != "All") {
                    searchArray.push({ "InvoiceState": $scope.Status });
                }
            }

            if ($scope.FromDate) {
                searchArray.push({ "InvoiceHeader.InvoiceDate": { $gte: moment(new Date($scope.FromDate)).startOf('day') } });
            }

            if ($scope.ToDate) {
                searchArray.push({ "InvoiceHeader.InvoiceDate": { $lte: moment(new Date($scope.ToDate)).endOf('day') } });
            }

            if ($scope.InvoiceNumber) {
                searchArray.push({ "InvoiceHeader.InvoiceNumber": $scope.InvoiceNumber });
            }
            searchArray.length > 0 ? filter = { $and: searchArray } : filter = "";

            console.log("Search Filter is: " + JSON.stringify(filter));
        }

        $scope.fillCounters = function () {


            //Paid Invoices
            InvoiceFunc.searchInvoice({ "InvoicePayment.AmountRecieved": { $gt: 0 } }, function (err, invoices) {
                if (err) {
                    console.log("Error:" + err);
                } else {

                    $scope.TotalPaidInvoiceAmount = 0;
                    invoices.forEach(invoice => {
                        $scope.TotalPaidInvoiceAmount += (invoice.InvoicePayment.AmountRecieved * invoice.InvoiceHeader.ExchangeRate);
                    });
                    $scope.TotalPaidInvoiceAmount = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, $scope.TotalPaidInvoiceAmount);
                    $scope.$apply();
                }
            });

            //Unpaid Invoices
            InvoiceFunc.searchInvoice({ "InvoicePayment.Balance": { $gt: 0 } }, function (err, invoices) {
                if (err) {
                    console.log("Error:" + err);
                } else {
                    $scope.TotalUnpaidInvoiceAmount = 0;
                    invoices.forEach(invoice => {
                        $scope.TotalUnpaidInvoiceAmount += (invoice.InvoicePayment.Balance * invoice.InvoiceHeader.ExchangeRate);
                    });
                    $scope.TotalUnpaidInvoiceAmount = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, $scope.TotalUnpaidInvoiceAmount);
                    $scope.$apply();
                }
            });

            //Late Invoices
            InvoiceFunc.searchInvoice({ $and: [{ "InvoicePayment.Balance": { $gt: 0 } }, { "InvoiceHeader.PaymentDate": { $lt: moment().startOf('day') } }] }, function (err, invoices) {
                if (err) {
                    console.log("Error:" + err);
                } else {
                    console.log("Unpaid invoices: " + JSON.stringify(invoices));
                    var InvoicesData = [];

                    if (invoices.length > 7) {
                        for (var i = 0; i < 7; i++) {

                            InvoicesData.push(invoices[i]);
                        }
                    } else {
                        InvoicesData = invoices;
                    }

                    $('#InvoicesTable').dataTable({
                        data: InvoicesData,
                        columns: [
                            { 'data': 'Customer.CustomerName', 'render': function (data, type, row) { return row.Customer ? Controls.AsLink("createinvoice", row.UniqueID, row.Customer.CustomerName) : ""; } },
                            { 'data': 'InvoiceHeader.InvoiceAmount', 'render': function (data, type, row) { return row.UniqueID, CurrenciesList.Currencies.find(function (obj) { return obj.ISO === row.InvoiceHeader.InvoiceCurrency; }).Symbol + Controls.ToCurrency(row.InvoiceHeader.InvoiceAmount) } },

                        ],
                        bDestroy: true,
                        "paging": false,
                        "ordering": false,
                        "info": false,
                        "filter": false,
                        scrollY: 350
                    });
                }
            });


            //Paid Expenses
            ExpenseFunc.searchExpense({ "ExpensePayment.AmountRecieved": { $gt: 0 } }, function (err, expenses) {
                if (err) {
                    console.log("Error:" + err);
                } else {
                    console.log("Paid Expenses: " + JSON.stringify(expenses));
                    $scope.TotalPaidExpenseAmount = 0;
                    expenses.forEach(expense => {
                        $scope.TotalPaidExpenseAmount += (expense.ExpensePayment.AmountRecieved * expense.ExpenseHeader.ExchangeRate);
                    });
                    $scope.TotalPaidExpenseAmount = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, $scope.TotalPaidExpenseAmount);
                    $scope.$apply();
                }
            });

            //Unpaid Expenses
            ExpenseFunc.searchExpense({ "ExpensePayment.Balance": { $gt: 0 } }, function (err, expenses) {
                if (err) {
                    console.log("Error:" + err);
                } else {
                    console.log("Unpaid Expenses: " + JSON.stringify(expenses));
                    $scope.TotalUnpaidExpenseAmount = 0;
                    expenses.forEach(expense => {
                        $scope.TotalUnpaidExpenseAmount += (expense.ExpensePayment.Balance * expense.ExpenseHeader.ExchangeRate);
                    });
                    $scope.TotalUnpaidExpenseAmount = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, $scope.TotalUnpaidExpenseAmount);
                    $scope.$apply();
                }
            });

            //Late Expenses
            ExpenseFunc.searchExpense({ $and: [{ "ExpensePayment.Balance": { $gt: 0 } }, { "ExpenseHeader.PaymentDate": { $lt: moment().startOf('day') } }] }, function (err, expenses) {
                if (err) {
                    console.log("Error:" + err);
                } else {

                    var ExpensesData = [];
                    if (expenses.length > 7) {
                        for (var i = 0; i < 7; i++) {

                            ExpensesData.push(expenses[i]);
                        }
                    } else {
                        ExpensesData = expenses;
                    }

                    console.log("Late Expenses: " + JSON.stringify(ExpensesData));
                    $('#ExpensesTable').dataTable({
                        data: ExpensesData,
                        columns: [
                            { 'data': 'Supplier.SupplierName', 'render': function (data, type, row) { return row.Supplier ? Controls.AsLink("addexpense", row.UniqueID, row.Supplier.SupplierName) : ""; } },
                            { 'data': 'ExpenseHeader.ExpenseAmount', 'render': function (data, type, row) { return row.UniqueID, CurrenciesList.Currencies.find(function (obj) { return obj.ISO === row.ExpenseHeader.ExpenseCurrency; }).Symbol + Controls.ToCurrency(row.ExpenseHeader.ExpenseAmount) } },

                        ],
                        bDestroy: true,
                        "paging": false,
                        "ordering": false,
                        "info": false,
                        "filter": false,
                        scrollY: 350
                    });
                }
            });

        }

        //Expense distribution
        function GetAccounts() {

            //var FromDate = moment(new Date($scope.FromDate)).startOf('day');
            //var ToDate = moment(new Date($scope.ToDate)).endOf('day');
            var Months = [];
            var dateStart = moment().subtract(11, 'months');
            var dateEnd = moment().add(1, 'months');
            while (dateEnd > dateStart) {
                var MonthRange = {
                    label: dateStart.format('MMM') + " " + dateStart.format('YYYY'),
                    startDate: moment(dateStart).startOf('month'),
                    endDate: moment(dateStart).endOf('month')
                };
                Months.push(MonthRange);
                dateStart.add(1, 'month');
            }

            //$scope.Months = Months;
            console.log("Months Filter is" + JSON.stringify(Months));


            //Get All Accounts
            TransactionData.GetTransactionData(Months, function (data) {
                console.log("Final Transaction Data :" + JSON.stringify(data));

                $scope.PandLData = {
                    lables: [],
                    Income: [],
                    Expenses: [],
                    NetChange: []
                }

                data.forEach(month => {

                    var income = month.Data.find(function (obj) { return obj.AccountType === "Cash and Bank"; }).AccountDebitTotal;
                    var expense = month.Data.find(function (obj) { return obj.AccountType === "Cash and Bank"; }).AccountCreditTotal;
                    $scope.PandLData.lables.push(month.title);
                    $scope.PandLData.Income.push(income);
                    $scope.PandLData.Expenses.push(expense * -1);
                    $scope.PandLData.NetChange.push(income - expense);
                });

                console.log("$scope.PandLData" + JSON.stringify($scope.PandLData));
                $scope.$apply();
                renderCharts($('#ProfitAndLoss'), 10, 5, '#1E90FF', '', 'Draft Invoices', 'bar');

            });
        }



        function GetExpenseDistribution(period) {

            var startDate = moment().subtract(11, 'months').startOf('month');
            var endDate = moment().add(1, 'months').endOf('month');

            console.log("GetExpenseDistribution: "+ period);

            switch (period) {
                case "This Month":
                    startDate = moment().subtract(11, 'months').startOf('month');
                    endDate = moment().add(1, 'months').endOf('month');
                    break;
                case "This Quarter":
                    startDate = moment().startOf('quarter')
                    endDate=  moment().endOf('quarter')


                    break;
                case "This Year":
                    startDate = moment().startOf('year')
                    endDate = moment().endOf('year')
                    break;
                case "All":
                    console.log("Loading all expenses")
                    break;
                default:
                    console.log("Unknown action");
                    break;
            }
           

            var filter =  period=="All"? {}:{ $and: [{ Date: { $gte: startDate } }, { Date: { $lte: endDate } }] }
            TransactionData.GetExpenseDistribution(filter, function (ExpenseDistributionData) {
                $scope.ExpenseDistributionData = {
                    data: [],
                    labels: [],
                    color: []
                }
                var i = 0;
                var k = 0;
                var j = 0;
                ExpenseDistributionData.forEach(item => {

                    var colors = [
                        "#5F9EA0",
                        "#4682B4",
                        "#B0C4DE",
                        "#ADD8E6",
                        "#87CEFA",
                        "#D3D3D3",
                        "#696969",
                        "#778899",
                        "#808080",
                        "#87CEEB",
                        "#6495ED",
                        "#00BFFF",
                        "#1E90FF",
                        "#4169E1",
                        "#008080",
                        "#20B2AA",
                        "#00FA9A",
                    ]

                    var color = function () {

                        var resultColor;
                        if (i < colors.length) {
                            resultColor = colors[i];
                            i++
                            return resultColor;
                        }
                        else if (i >= colors.length) {
                            resultColor = colors[k];
                            k++
                            return resultColor;
                        } else if (k >= colors.length) {
                            resultColor = colors[j];
                            j++;
                            return resultColor;
                        } else {
                            return "#008080";
                        }
                    }
                    $scope.ExpenseDistributionData.data.push(item.AccountTotal);
                    $scope.ExpenseDistributionData.labels.push(item.AccountName);
                    $scope.ExpenseDistributionData.color.push(color());
                });

                $scope.HasExpenseData = ExpenseDistributionData.length > 0 ? true : false;
                $scope.$apply();

                renderCharts($('#ExpenseBreakdown'), 10, 5, '#1E90FF', document.getElementById('ExpenseBreakdownLabel'), 'Draft Invoices', 'doughnut');
            });

        }

        $scope.NewInvoice = function () {
            $location.path('createinvoice');
        };
        //******************************************************************************************************************************************************************************************

        //*********************************************************** Render All the Charts ************************************************************************ */
        function renderCharts(ctx, total, value, color, div, type, chartType) {

            switch (chartType) {
                case "doughnut":
                    var pieChart = new Chart(ctx, {
                        type: 'doughnut',
                        data: {
                            datasets: [
                                {
                                    data: $scope.ExpenseDistributionData.data,
                                    backgroundColor: $scope.ExpenseDistributionData.color
                                }],

                            // These labels appear in the legend and in the tooltips when hovering different arcs
                            // These labels appear in the legend and in the tooltips when hovering different arcs
                            labels: $scope.ExpenseDistributionData.labels,

                        },
                        options: {
                            cutoutPercentage: 40,
                            animation: {
                                animateScale: true
                            },
                            rotation: 0.2 * Math.PI,
                            legend: {
                                display: true,
                                position: 'bottom',
                                align: 'center'
                            },
                            tooltips: {
                                callbacks: {
                                    label: function (tooltipItem, data) {
                                        var indice = tooltipItem.index;
                                        return data.labels[indice] + ': ' + Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, data.datasets[0].data[indice]) + '';
                                    }
                                }
                            },/*
                            plugins: {
                                labels: [
                                    {
                                        render: function (args) {
                                            return args.label + ' (' + args.percentage + '%)';
                                        },
                                        position: 'outside',
                                        precision: 1
                                    },
                                ]
                            }*/
                        }
                    });
                    break;
                case "bar":
                    var barChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            datasets: [{
                                label: 'Total Money In',
                                data: $scope.PandLData.Income,
                                backgroundColor: 'rgba(7, 52, 255, 0.5)',
                                borderColor: 'rgba(7, 52, 255, 0.5)',
                                borderWidth: 1
                            },
                            {
                                label: 'Total Money Out',
                                data: $scope.PandLData.Expenses,
                                backgroundColor: 'rgba(119, 119, 120, 0.5)',
                                borderColor: 'rgba(119, 119, 120, 0.5)',
                                borderWidth: 1
                            },
                            {
                                label: 'Money Left',
                                data: $scope.PandLData.NetChange,
                                borderColor: 'rgba(40, 167, 69, 1)',
                                backgroundColor: 'rgba(0, 0, 0, 0)',
                                // Changes this dataset to become a line
                                type: 'line'
                            }
                            ],


                            // These labels appear in the legend and in the tooltips when hovering different arcs
                            labels: $scope.PandLData.lables,

                        },
                        options: {
                            animation: {
                                animateScale: true
                            },
                            legend: {
                                display: true
                            },
                            scales: {
                                xAxes: [{
                                    stacked: true,
                                    gridLines: { display: false },
                                    ticks: {
                                        min: $scope.PandLData.lables[0],
                                        max: $scope.PandLData.lables[11]
                                    }
                                }],
                                yAxes: [{
                                    stacked: true,
                                    ticks: {
                                        // Include a dollar sign in the ticks
                                        callback: function (value, index, values) {
                                            return $scope.CurrencySymbol + value;
                                        }
                                    }
                                }
                                ]
                            },
                            tooltips: {
                                callbacks: {
                                    label: function (tooltipItem, data) {
                                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                        if (label) {
                                            label += ': ';
                                        }
                                        label += Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, tooltipItem.yLabel);
                                        return label;
                                    }
                                }
                            },
                            plugins: {
                                labels: {
                                    render: function (args) {
                                        return '';
                                    }
                                }
                            }
                        },

                    });

                    break;
                default:
                    console.log("Unknown chart");
                    break;
            }

        }

        //************************************************************************************************************************************************************ */
    }

});

//*********************************************************************************************************************************************************** */

// Controller variables:


$(document).ready(function () {

});



