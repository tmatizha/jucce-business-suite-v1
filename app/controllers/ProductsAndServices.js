jbsApp.controller("ProductsAndServicesController", function ($scope, $routeParams, $rootScope, $location) {

    //Check if user is signed-in
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {
        var ProdctFunc = require('./app/models/ProductsAndServices.js');
        var SalesTaxFunc = require('./app/models/SalesTax.js');
        var CurrenciesList = require('./app/config/appfiles/Currencies.js');
        var AccountFunc = require('./app/models/ChartOfAccounts.js');

        //load Invoice settings
        Settings.getSettings(function (err, AppSettings) {
            $scope.Settings = AppSettings;
            $scope.Currency = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Settings.Currency; });
            $scope.CurrencySymbol = $scope.Currency.Symbol;
            console.log("Currency is " + $scope.CurrencySymbol);

            Run();
            $scope.$apply();
        });


        //Initialise scope variables
        //$scope.Countries = CountriesList.Countries;
        $scope.NewProduct = new ProdctFunc.Product();
        function Run() {
            $("#Alert").hide();
            $("#Preview_Alert").hide();            

            //Get Tax values
            SalesTaxFunc.getSalesTax("", function (SalesTax) {
                if (SalesTax.length == 0) {

                    console.log("No SalesTax found");
                    $scope.Tax = [];
                } else {

                    $scope.Tax = SalesTax;
                }
            });

            //Get Income Accounts values
            AccountFunc.getAccount({ AccountGroup: "Income" }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Expenses Accounts found");
                    $scope.IncomeAccount = [];
                } else {

                    $scope.IncomeAccount = Accounts;
                }
            });

            //Get Expense Accounts values
            AccountFunc.getAccount({ AccountGroup: "Expenses" }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Expenses Accounts found");
                    $scope.ExpenseAccount = [];
                } else {

                    $scope.ExpenseAccount = Accounts;
                }
            });

            //field validation
            //document.getElementById("Product").classList.add("is-invalid")
            //document.getElementById("Product").classList.remove("is-invalid");
            GetProducts();
        }

        function GetProducts() {

            if ($scope.ProductsTable != null) {
                $scope.ProductsTable.destroy();
                $('#ProductsTable tbody').off('click');
            }

            ProdctFunc.getProduct("", function (Products) {
                console.log("Getting Products");

                if (Products.length == 0) {

                    console.log("No Products found");
                }

                $scope.ProductsTable = $('#ProductsTable').DataTable({
                    data: Products,
                    rowId: "UniqueID",
                    columns: [
                        { 'data': 'Name' },
                        { 'data': 'Description' },
                        { 'data': 'Price', 'render': function (amount) { return $scope.CurrencySymbol + parseFloat(amount).toFixed(2); } },
                        { 'data': 'Cost', 'render': function (amount) { return $scope.CurrencySymbol + parseFloat(amount).toFixed(2); } },
                        {
                            'data': 'UniqueID', 'sortable': false, 'render': function (id) {

                                return '<div>' +
                                    '<button type="button" id="edit_' + id + '"  style="margin-right:10px;" class="btn btn-outline-info"><span class="glyphicon glyphicon-pencil"></span></button>' +
                                    '<button type="button" id="delete_' + id + '" class="btn btn-outline-danger"><span class="glyphicon glyphicon-trash"></span></button></div>'
                            }
                        }
                    ],
                    bDestroy: true
                });


                $('#ProductsTable tbody').unbind("click").on('click', 'button', function () {
                    //var ProductID = table.row(this).id();

                    console.log("button clicked is:" + $(this).attr('id'));
                    var buttonType = $(this).attr('id').split("_")[0];
                    var ProductId = $(this).attr('id').split("_")[1];
                    console.log(buttonType);
                    console.log("Product id is: " + ProductId);


                    switch (buttonType) {
                        case "edit":
                            ProdctFunc.getProductById(ProductId, function (err, docs) {
                                if (err != null) {
                                    console.log("Error retriving Product: " + err);
                                } else {
                                    $scope.NewProduct = docs[0];
                                    $scope.$apply();
                                    $('#ProductModal').modal('toggle');
                                }

                            });
                            break;
                        case "delete":
                            console.log("delete action");
                            ProdctFunc.getProductById(ProductId, function (err, docs) {
                                if (err != null) {
                                    console.log("Error retriving Product: " + JSON.stringify(err));
                                } else {
                                    $scope.DeleteProduct = docs[0];
                                    $scope.$apply();
                                    $('#DeleteProduct').modal('toggle');
                                }
                            });

                            break;
                        default:
                            console.log("Unknown action");
                            break;
                    }
                });
            });
        }

        $scope.AddProduct = function () {
            $scope.NewProduct = new ProdctFunc.Product();
        }

        $scope.SaveProduct = function () {

            var requiredFields = ["Product"];

            console.log("save Product" + JSON.stringify($scope.NewProduct));

            if (Controls.CheckRequiredFields(requiredFields)) {
                ProdctFunc.addProduct($scope.NewProduct, function (err, Product) {
                    if (err != null) {
                        console.log("Error is:" + err)
                    } else {
                        $('#ProductModal').modal('toggle');
                        console.log("Product Creation Successful");
                        Controls.showAlert("Alert", "alert-success", "Product " + "'" + $scope.NewProduct.Name + "'" + " succesfully created", "message", true);
                        //$scope.Product = Product; //Update Current Product
                        console.log("Created Product: " + JSON.stringify(Product));
                        $scope.NewProduct = new ProdctFunc.Product(); //Clear New Product
                        $scope.$apply();
                        GetProducts();
                        $('#Product_error').hide();
                    }
                });
            } else {
                console.log("Missing fileds/invalid data entered");
            }
        }


        $scope.UpdateProduct = function () {

            var requiredFields = ["Product"];
            console.log("Update Product" + JSON.stringify($scope.NewProduct));

            if (Controls.CheckRequiredFields(requiredFields)) {
                ProdctFunc.updateProduct($scope.NewProduct.UniqueID, $scope.NewProduct, function (err, num, Product) {
                    err ? console.log("Error is:" + err) : console.log("Product Update Successful");
                    if (err != null) {
                        console.log("Error deleting Product: " + err);
                    } else {
                        $('#ProductModal').modal('toggle');
                        Controls.showAlert("Alert", "alert-success", "Product " + "'" + $scope.NewProduct.Name + "'" + " succesfully updated", "message", true);
                        $scope.Product = Product; //Update Current Product
                        console.log("Updated Product: " + Product);
                        $scope.NewProduct = new ProdctFunc.Product(); //Clear New Product
                        $scope.$apply();
                        GetProducts();
                    }
                });
            } else {
                console.log("Missing fileds/invalid data entered");
            }
        };

        $scope.RemoveProduct = function () {

            console.log("Delete Product" + JSON.stringify($scope.DeleteProduct));
            ProdctFunc.deleteProduct($scope.DeleteProduct.UniqueID, function (err, numRemoved) {
                if (err != null) {
                    console.log("Error deleting Product: " + err);
                } else {
                    $('#DeleteProduct').modal('toggle');
                    console.log("Deleted " + numRemoved + " Products");
                    Controls.showAlert("Alert", "alert-success", "Product " + "'" + $scope.DeleteProduct.Name + "'" + " succesfully deleted", "message", true);
                    $scope.DeleteProduct = new ProdctFunc.Product(); //Clear New Product
                    $scope.$apply();
                    GetProducts();
                }
            });
        };

        $scope.PriceOnChange = function () {

            $scope.NewProduct.Price = parseFloat(Controls.ToNumber($scope.NewProduct.Price)).toFixed(2);
            console.log("Price in now " + $scope.NewProduct.Price);

        }

        $scope.CostOnChange = function () {

            $scope.NewProduct.Cost = parseFloat(Controls.ToNumber($scope.NewProduct.Cost)).toFixed(2);
            console.log("Cost in now " + $scope.NewProduct.Cost);

        }

    }
});