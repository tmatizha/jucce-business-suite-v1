jbsApp.controller("ProfitAndLossController", function ($scope, $routeParams, $rootScope, $location) {

    //Check if user is signed-in
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {

        var TransactionFunc = require('./app/models/Transactions.js');
        var CountriesList = require('./app/config/appfiles/Countries.js');
        var CurrenciesList = require('./app/config/appfiles/Currencies.js');
        var AccountFunc = require('./app/models/ChartOfAccounts.js');
        var DateRangeSet = false;
        //var moment = require('moment'); 


        //Initialise scope variables
        $scope.Countries = CountriesList.Countries;
        $scope.Currencies = CurrenciesList.Currencies;
        $scope.NewTransaction = new TransactionFunc.AccountTransaction();

        Settings.getSettings(function (err, AppSettings) {
            $scope.Settings = AppSettings;
            $scope.CurrencySymbol = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Settings.Currency; }).Symbol;
            Run();
        });


        function Run() {
            $("#Alert").hide();
            $("#Preview_Alert").hide();

            $("#Account").select2({
                placeholder: "Select Account",
                allowClear: true
            });

            GetAccounts();
        }

        function GetAccounts() {
            var FromDate = moment(new Date($scope.FromDate)).startOf('day');
            var ToDate = moment(new Date($scope.ToDate)).endOf('day');

            //Income Accounts
            AccountFunc.getAccount({ AccountGroup: "Income" }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Income Accounts found");
                    //$scope.IncomeAccounts = [];
                } else {
                    //console.log("Income Accounts found");
                    //$scope.IncomeAccounts = Accounts;
                    $scope.Accounts = Accounts.concat($scope.Accounts);
                    var filterArrary = [];
                    Accounts.forEach(account => {
                        var xfilter;
                        if ($scope.FromDate && $scope.ToDate) {
                            xfilter = { $and: [{ AccountId: account.UniqueID }, { Date: { $gte: FromDate } }, { Date: { $lte: ToDate } }] };
                        } else if ($scope.FromDate && !$scope.ToDate) {
                            xfilter = { $and: [{ AccountId: account.UniqueID }, { Date: { $gte: FromDate } }] };
                        } else if ($scope.ToDate && !$scope.FromDate) {
                            xfilter = { $and: [{ AccountId: account.UniqueID }, { Date: { $lte: ToDate } }] };
                        } else {
                            xfilter = { AccountId: account.UniqueID };
                        }
                        filterArrary.push(xfilter);
                    });

                    var filter = { $or: filterArrary }
                    $scope.IncomeChecked = false;
                    GetTransactions(filter, "Income");

                }
            });

            //Cost Of Goods sold Accounts
            AccountFunc.getAccount({ $and: [{ AccountGroup: "Expenses" }, { AccountType: "Cost of Goods Sold" }] }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No COGS Accounts found");
                    //$scope.COGSAccounts = [];
                } else {
                    console.log("COGS Accounts found");
                    //$scope.COGSAccounts = Accounts;
                    $scope.Accounts = Accounts.concat($scope.Accounts);
                    var filterArrary = [];
                    Accounts.forEach(account => {
                        var xfilter;
                        if ($scope.FromDate && $scope.ToDate) {
                            xfilter = { $and: [{ AccountId: account.UniqueID }, { Date: { $gte: FromDate } }, { Date: { $lte: ToDate } }] };
                        } else if ($scope.FromDate && !$scope.ToDate) {
                            xfilter = { $and: [{ AccountId: account.UniqueID }, { Date: { $gte: FromDate } }] };
                        } else if ($scope.ToDate && !$scope.FromDate) {
                            xfilter = { $and: [{ AccountId: account.UniqueID }, { Date: { $lte: ToDate } }] };
                        } else {
                            xfilter = { AccountId: account.UniqueID };
                        }
                        filterArrary.push(xfilter);
                    });

                    var filter = { $or: filterArrary }
                    $scope.COGSChecked = false;
                    GetTransactions(filter, "COGS");
                }
            });

            //Expense Accounts
            AccountFunc.getAccount({ AccountGroup: "Expenses", $not: [{ AccountType: "Cost of Goods Sold" }] }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Expenses Accounts found");
                } else {
                    console.log("Expenses Accounts found");
                    $scope.Accounts = Accounts.concat($scope.Accounts);
                    var filterArrary = [];
                    Accounts.forEach(account => {
                        var xfilter;
                        if ($scope.FromDate && $scope.ToDate) {
                            xfilter = { $and: [{ AccountId: account.UniqueID }, { Date: { $gte: FromDate } }, { Date: { $lte: ToDate } }] };
                        } else if ($scope.FromDate && !$scope.ToDate) {
                            xfilter = { $and: [{ AccountId: account.UniqueID }, { Date: { $gte: FromDate } }] };
                        } else if ($scope.ToDate && !$scope.FromDate) {
                            xfilter = { $and: [{ AccountId: account.UniqueID }, { Date: { $lte: ToDate } }] };
                        } else {
                            xfilter = { AccountId: account.UniqueID };
                        }
                        filterArrary.push(xfilter);
                    });

                    var filter = { $or: filterArrary }
                    $scope.ExpensesChecked = false;
                    GetTransactions(filter, "Expenses");
                }
            });

            //Get Tax Accounts        
            AccountFunc.getAccount({ $and: [{ AccountGroup: "Liabilities" }, { AccountType: "Sales Taxes" }] }, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Tax Accounts found");
                } else {
                    console.log("Tax Accounts found");
                    $scope.Accounts = Accounts.concat($scope.Accounts);
                    var filterArrary = [];
                    Accounts.forEach(account => {
                        var xfilter;
                        if ($scope.FromDate && $scope.ToDate) {
                            xfilter = { $and: [{ AccountId: account.UniqueID }, { Date: { $gte: FromDate } }, { Date: { $lte: ToDate } }] };
                        } else if ($scope.FromDate && !$scope.ToDate) {
                            xfilter = { $and: [{ AccountId: account.UniqueID }, { Date: { $gte: FromDate } }] };
                        } else if ($scope.ToDate && !$scope.FromDate) {
                            xfilter = { $and: [{ AccountId: account.UniqueID }, { Date: { $lte: ToDate } }] };
                        } else {
                            xfilter = { AccountId: account.UniqueID };
                        }
                        filterArrary.push(xfilter);
                    });

                    var filter = { $or: filterArrary }
                    $scope.TaxChecked = false;
                    GetTransactions(filter, "Tax");
                }
            });

        }

        //Calculate Date Range Filter
        function SetDateRageFilter(date) {
            //Range of years
            function GetYEARS() {
                var Years = []
                var dateStart = moment(date);
                var dateEnd = moment().add(1, 'year');
                while (dateEnd >= dateStart) {

                    var YearRange = {
                        label: dateStart.format('YYYY'),
                        startDate: moment(dateStart).startOf('year'),
                        endDate: moment(dateStart).endOf('year'),
                    };
                    Years.push(YearRange);
                    dateStart.add(1, 'year');
                }
                $scope.Years = Years;
                console.log("Years Filter is" + JSON.stringify(Years));

                $scope.DateRangeFilters = Years[0].label;
                $scope.$apply();
                $scope.DateRangeChange(Years[0].startDate, Years[0].endDate);
            };


            //Rage of financial quarters
            function GetQUARTERS() {
                var Quarters = []
                var dateStart = moment(date);
                var dateEnd = moment();

                while (dateEnd > dateStart) {

                    var quarterNum = dateStart.quarter();
                    var QuarterRange = {
                        label: dateStart.format('YYYY') + " Q" + quarterNum,
                        startDate: moment(dateStart).quarter(quarterNum).startOf('quarter'),
                        endDate: moment(dateStart).quarter(quarterNum).endOf('quarter')
                    }
                    Quarters.push(QuarterRange);
                    dateStart.add(1, 'quarter');
                }
                $scope.Quarters = Quarters;
                console.log("Quarters Filter is" + JSON.stringify(Quarters));
            };

            //Range of months
            function GetMONTHS() {
                var Months = [];
                var dateStart = moment(date);
                var dateEnd = moment()
                while (dateEnd > dateStart) {
                    var MonthRange = {
                        label: dateStart.format('YYYY') + " " + dateStart.format('MMMM'),
                        startDate: moment(dateStart).startOf('month'),
                        endDate: moment(dateStart).endOf('month')
                    };
                    Months.push(MonthRange);
                    dateStart.add(1, 'month');
                }

                $scope.Months = Months;
                console.log("Months Filter is" + JSON.stringify(Months));
            };

            GetYEARS();
            GetMONTHS();
            GetQUARTERS();

            var filter = [{
                label: "Years",
                filters: $scope.Years,
            }, {
                label: "Quarters",
                filters: $scope.Quarters,
            }, {
                label: "Months",
                filters: $scope.Months
            }];

            $scope.DateRangeFilter = filter;
            console.log("Filter are" + JSON.stringify($scope.DateRangeFilter));
        }

        function GetTransactions(filter, AccountType) {

            TransactionFunc.getTransaction(filter, function (transaction) {

                //****************** Initialise P&L scope  variables ************************** */

                $scope.TotalOperatingExpenses = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, (0));
                $scope.TotalCOGS = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, 0);
                $scope.TotalIncome = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, 0);
                $scope.GrossProfit = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, 0);
                $scope.NetProfit = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, 0);
                $scope.IsProfit = true;

                /*********************************************************************/


                var AccountsArray = [];
                var TotalAccountDifference = 0;

                if (!transaction || transaction.length == 0) {
                    console.log("No transaction found for :" + AccountType);
                    switch (AccountType) {
                        case "Income":
                            $scope.IncomeAccounts = 0
                            $scope.IncomeAccountsData = {};
                            $scope.IncomeChecked = true;
                            break;
                        case "COGS":
                            $scope.COGSAccounts = 0;
                            $scope.COGSAccountsData = {};
                            $scope.COGSChecked = true;
                            break;
                        case "Expenses":
                            $scope.ExpensesAccounts = 0;
                            $scope.ExpensesAccountsData = {};
                            $scope.ExpensesChecked = true;
                            break;
                        case "Tax":
                            $scope.TaxAccounts = 0;
                            $scope.TaxAccountsData = {};
                            $scope.TaxChecked = true;
                            break;
                        default: ""
                            break;
                    }
                    $scope.CalculateReportAmounts();
                } else {

                    //Check and set Date Range Filter
                    if (!DateRangeSet) {
                        SetDateRageFilter(transaction[0].Date);
                        DateRangeSet = true;
                    }

                    var groupedTransactions = _.groupBy(transaction, function (transaction) {
                        return transaction.AccountId;
                    });

                    //console.log("Transactions for Account" +AccountType +" "+ JSON.stringify(groupedTransactions));

                    var arrayOfKyes = [];
                    for (var key in groupedTransactions) {
                        arrayOfKyes.push(key);
                    }

                    var z = 0;
                    Calculate(groupedTransactions[arrayOfKyes[z]]);


                    function Calculate(Transactions) {
                        var AccountData = $scope.Accounts.find(function (obj) { return obj.UniqueID === arrayOfKyes[z]; })
                        var AccountName = AccountData.AccountName;
                        var AccountGroup = AccountData.AccountGroup;
                        var AccountBalance = 0;

                        //Get Account balance
                        var firstTransaction = Transactions[0];

                        balanceFilter = { AccountId: key, Date: { $lt: moment(new Date(firstTransaction.Date)) } }

                        TransactionFunc.getTransaction(balanceFilter, function (balanceTransactions) {

                            var startingAccountBalance = 0;
                            balanceTransactions.forEach(item => {

                                var debit = Controls.ToNumber(item.Debit_CompanyCurrencyAmount);
                                var credit = Controls.ToNumber(item.Credit_CompanyCurrencyAmount);

                                var itemBalance = 0;

                                if (AccountGroup == "Assets" || AccountGroup == "Liabilities") {
                                    itemBalance = credit - debit;
                                } else {
                                    itemBalance = credit + debit;
                                }

                                startingAccountBalance += itemBalance;
                            });

                            AccountBalance = startingAccountBalance;
                            console.log("startingAccountBalance: " + startingAccountBalance)

                            var AccountTransactions = {
                                AccountName: AccountName,
                                AccountStartingBalance: startingAccountBalance,
                                AccountTotal: 0,
                                AccountDifference: 0
                            };

                            Transactions.forEach(item => {
                                var debit = Controls.ToNumber(item.Debit_CompanyCurrencyAmount);
                                var credit = Controls.ToNumber(item.Credit_CompanyCurrencyAmount);

                                var itemBalance = 0;

                                console.log("Account Group is: " + AccountGroup);

                                if (AccountGroup == "Assets" || AccountGroup == "Liabilities") {
                                    itemBalance = credit - debit;
                                } else {
                                    itemBalance = credit + debit;
                                }
                                AccountBalance += itemBalance;
                                AccountTransactions.AccountTotal += itemBalance;
                            });

                            //Calculate Account Difference
                            AccountTransactions.AccountDifference = AccountTransactions.AccountStartingBalance + AccountTransactions.AccountTotal;
                            TotalAccountDifference += AccountTransactions.AccountDifference;
                            AccountTransactions.AccountTotal = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, AccountTransactions.AccountTotal);
                            AccountTransactions.AccountStartingBalance = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, AccountTransactions.AccountStartingBalance);
                            AccountTransactions.AccountDifference = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, AccountTransactions.AccountDifference);

                            AccountsArray.push(AccountTransactions);

                            console.log("AccountTransactions", JSON.stringify(AccountTransactions));
                            z++;
                            if (z < arrayOfKyes.length) {
                                Calculate(groupedTransactions[arrayOfKyes[z]]);

                            } else {

                                var AccountData = {
                                    TotalAccountDifference: TotalAccountDifference,
                                    Accounts: AccountsArray
                                }

                                switch (AccountType) {
                                    case "Income":
                                        $scope.IncomeAccounts = AccountData.TotalAccountDifference;
                                        $scope.IncomeAccountsData = AccountData;
                                        $scope.IncomeChecked = true;
                                        break;
                                    case "COGS":
                                        $scope.COGSAccounts = AccountData.TotalAccountDifference;
                                        $scope.COGSAccountsData = AccountData;
                                        $scope.COGSChecked = true;
                                        break;
                                    case "Expenses":
                                        $scope.ExpensesAccounts = AccountData.TotalAccountDifference;
                                        $scope.ExpensesAccountsData = AccountData;
                                        $scope.ExpensesChecked = true;
                                        break;
                                    case "Tax":
                                        $scope.TaxAccounts = AccountData.TotalAccountDifference;
                                        $scope.TaxAccountsData = AccountData;
                                        $scope.TaxChecked = true;
                                        break;
                                    default: ""
                                        break;
                                }
                                $scope.CalculateReportAmounts();
                            }

                        });
                    }
                }
            });

        }

        $scope.CalculateReportAmounts = function () {


            if (!$scope.IncomeChecked) {
                $scope.IncomeAccounts = 0;
            }

            if (!$scope.COGSChecked) {
                $scope.COGSAccounts = 0;
            }

            if (!$scope.TaxChecked) {
                $scope.TaxAccounts = 0;
            }

            if (!$scope.ExpensesChecked) {
                $scope.ExpensesAccounts = 0;
            }

            console.log("IncomeAccounts", JSON.stringify($scope.IncomeAccounts));
            console.log("COGSAccounts", JSON.stringify($scope.COGSAccounts));
            console.log("ExpensesAccounts", JSON.stringify($scope.ExpensesAccounts));
            console.log("TaxAccounts", JSON.stringify($scope.TaxAccounts));

            $scope.TotalOperatingExpenses = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, ($scope.TaxAccounts + $scope.ExpensesAccounts));
            $scope.TotalCOGS = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, $scope.COGSAccounts);
            $scope.TotalIncome = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, $scope.IncomeAccounts);
            $scope.GrossProfit = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, $scope.IncomeAccounts - $scope.COGSAccounts);
            $scope.NetProfit = $scope.IncomeAccounts - ($scope.COGSAccounts + $scope.TaxAccounts + $scope.ExpensesAccounts);
            $scope.NetProfit < 0 ? $scope.IsProfit = false : $scope.IsProfit = true;
            $scope.NetProfit = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, $scope.NetProfit)
            $scope.$apply();

        }

        $scope.Applyfilter = function () {

            var searchArray = [];
            var filter = null;
            if ($scope.FromDate) {
                searchArray.push({ Date: { $gte: moment(new Date($scope.FromDate)).startOf('day') } });
            }

            if ($scope.ToDate) {
                searchArray.push({ Date: { $lte: moment(new Date($scope.ToDate)).endOf('day') } });
            }

            searchArray.length > 0 ? filter = { $and: searchArray } : filter = {};
            GetAccounts(filter);

        }

        $scope.DateRangeChange = function (start, end) {

            var startDate = $("#DateRange option:selected").attr("data-startDate") || start;
            var endDate = $("#DateRange option:selected").attr("data-endDate") || end;
            $("#FromDate").datepicker().datepicker("setDate", new Date(startDate));
            $("#ToDate").datepicker().datepicker("setDate", new Date(endDate));
            GetAccounts();
        }


        $scope.ClearFilters = function () {
            $scope.DateRangeFilters = $scope.Years[0].label;
            $("#FromDate").datepicker().datepicker("setDate", new Date($scope.Years[0].startDate));
            $("#ToDate").datepicker().datepicker("setDate", new Date($scope.Years[0].endDate));
            GetAccounts();
        }




        //*********************************************************************** export Statement *********************************************************** */

        $scope.ExportPDFStatement = function () {
            console.log("Generating PDF");
            var doc = new jsPDF('p', 'px', [595, 842], 'a4', true);

            console.log("Generating PDF");
            if ($scope.Settings.Logo) {
                doc.addImage($scope.Settings.Logo, 'base64', 250, 25);
            }

            doc.setFontSize(20);
            doc.text("Profit and Loss", 30, 50);

            doc.setFontSize(15);
            doc.text($scope.Settings.CompanyName, 30, 70);

            doc.setFontSize(12);

            var FromDate = Controls.FormatDateString(new Date($scope.FromDate));
            var ToDate = Controls.FormatDateString(new Date($scope.ToDate));

            doc.text("Date Range: " + FromDate + " to " + ToDate, 30, 85);

            doc.text("Accounts", 30, 120);

            doc.autoTable(
                {
                    html: '#IncomeAccounts',
                    didParseCell: data => {
                        if (data.section === 'foot' && data.column.index != 0) {
                            data.cell.styles.halign = 'right';
                        }
                    },
                    theme: 'striped',
                    headStyles: { fillColor: $scope.Settings.AccentColor, fontSize: 12 },
                    footStyles: { fillColor: [255, 255, 255], fontSize: 10, textColor: [0, 0, 0] },
                    columnStyles: { 1: { halign: 'right' }, 2: { halign: 'right' } },
                    startY: 130
                });

            doc.autoTable(
                {
                    html: '#COGS',
                    didParseCell: data => {
                        if (data.section === 'foot' && data.column.index != 0) {
                            data.cell.styles.halign = 'right';
                        }
                    },
                    theme: 'striped',
                    headStyles: { fillColor: $scope.Settings.AccentColor, fontSize: 12 },
                    footStyles: { fillColor: [255, 255, 255], fontSize: 10, textColor: [0, 0, 0] },
                    columnStyles: { 1: { halign: 'right' }, 2: { halign: 'right' } },
                    startY: doc.lastAutoTable.finalY + 10
                });


            doc.autoTable(
                {
                    html: '#OperatingExpense',
                    didParseCell: data => {
                        if (data.section === 'foot' && data.column.index != 0) {
                            data.cell.styles.halign = 'right';
                        }
                    },
                    theme: 'striped',
                    headStyles: { fillColor: $scope.Settings.AccentColor, fontSize: 12 },
                    footStyles: { fillColor: [255, 255, 255], fontSize: 10, textColor: [0, 0, 0] },
                    columnStyles: { 1: { halign: 'right' }, 2: { halign: 'right' } },
                    startY: doc.lastAutoTable.finalY + 10
                });

            var dateTime = new Date();
            var file = $scope.Settings.CompanyName + '- Profit and Loss -' + dateTime.toDateString() + '.pdf'
            doc.page = 1;
            for (var i = 1; i <= doc.internal.getNumberOfPages(); i++) {
                // Go to page i
                doc.setPage(i);
                //Print Page 1 of 4 for example
                addFooters(i)
            }
            doc.save(file);




            function addFooters(page) {
                const pageCount = doc.internal.getNumberOfPages();
                const currentDate = Controls.FormatDateString(new Date());
                doc.setFontSize(10);
                doc.text("Profit and Loss - " + $scope.Settings.CompanyName, 20, 610);
                doc.text("Created on: " + currentDate, 20, 620);
                doc.text("Page " + page + "/" + String(pageCount), 400, 620);
                doc.page++;
            }
        }

        /***************************************************************************************************************************************************************/
    }

});