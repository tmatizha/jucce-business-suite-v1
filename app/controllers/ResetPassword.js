
jbsApp.controller("ResetPasswordController", function ($scope, $location, $timeout, $rootScope) {

    var remote = require('electron').remote;
    var fs = remote.require('fs');
    const app = remote.app
    var filePath = null;
    var AccountFunc = require('./app/models/ChartOfAccounts.js');
    var UserFunc = require('./app/models/Users.js');
    $scope.Profiles = require('./app/config/appfiles/UserProfiles.js').Profiles;
    var CountriesList = require('./app/config/appfiles/Countries.js');
    $scope.Profiles = require('./app/config/appfiles/UserProfiles.js').Profiles;
    var CurrenciesList = require('./app/config/appfiles/Currencies.js');
    $scope.LoginUser = { Email: "", Password: "" };
    $rootScope.CurrentUser = null;
    $scope.ResetUser = new UserFunc.User();

    $scope.Countries = CountriesList.Countries;
    $scope.Currencies = CurrenciesList.Currencies;
    $scope.Settings = new Settings.Settings();
    $scope.ResetPasswordMessage = "Enter username or email to begin!";
    $scope.SecurityQuestions = UserFunc.SecurityQuestons;

    //******************************************* Password Reset ********************************************************************/

    $('#questions').hide();
    $('#ResetAlert').hide();
    $('#ResetAlertSuccess').hide();
    $scope.GetUserDetails = function () {

        UserFunc.getUser({ $or: [{ Email: $scope.ResetUser.Email }, { Username: $scope.ResetUser.Email }] }, function (err, Users) {
            console.log("Getting Users");

            if (err != null) {

                console.log("Error Getting Users :" + err);
                //$scope.AlertMessage = "Login Error";
                //$('#LoginAlert').show();
                Controls.showAlert("Alert", "alert-danger", "Login Error", "message", true);
                $scope.$apply();
            }
            else if (Users.length == 0) {
                console.log("No Users found");
                $('#ResetUser_error').show();
                $scope.$apply();
            } else {

                $('#ResetUser_error').hide();

                $scope.ResetPasswordMessage = "Answer the following questions to reset you password!";
                $scope.ResetUser = Users[0];
                $('#questions').show();
                $('#UserToReset').hide();
                $scope.$apply();
            }
        });
    };

    $scope.ResetPassword = function () {

        if ($scope.ResetUser.Question1 == "" || $scope.ResetUser.Question2 == "") {
            //$scope.AlertMessage = "Please enter your email or username, and password.";
            //$('#LoginAlert').show();
            Controls.showAlert("ResetAlert", "alert-danger", "Please enter your email or username, and password.", "message", true);
        } else {

            if ($scope.ResetUser.Answer1 === $scope.ResetUserAnswer1 && $scope.ResetUser.Answer2 === $scope.ResetUserAnswer2 && $scope.NewPassword == $scope.ConfirmNewPassword) {

                console.log("Answer1: "+ ResetUser.Answer1 +" Answer2: "+ ResetUser.Answer2);

                $scope.ResetUser.Password = $scope.NewPassword;

                UserFunc.updateUser($scope.ResetUser.UniqueID, JSON.parse(angular.toJson($scope.ResetUser)), function (err, num, User) {
                    err ? console.log("Error is:" + err) : console.log("User Update Successful");
                    if (err != null) {
                        console.log("Error deleting User: " + err);
                    } else {
                        //Controls.showAlert("ResetAlertSuccess", "alert-success", "Password for user " + "'" + User.Name + "'" + " succesfully updated", "message", true);
                        //2 seconds delay
                        $timeout(function () {
                            $location.path('login');
                        }, 1000);
                        
                    }
                });

            } else {
                if ($scope.NewPassword != $scope.ConfirmNewPassword) {
                    $('#ConfirmNewPassword_error').show();
                } else {
                    Controls.showAlert("ResetAlert", "alert-danger", "Incorrect answers provided!", "message", true);
                }

            }
        }
    };
    //******************************************* Password Reset End ********************************************************************/
});

