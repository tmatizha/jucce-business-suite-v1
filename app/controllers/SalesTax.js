jbsApp.controller("SalesTaxController", function ($scope, $rootScope, $location) {

    //Check if user is signed-in
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {


        var SalesTaxFunc = require('./app/models/SalesTax.js');
        var CountriesList = require('./app/config/appfiles/Countries.js');
        var AccountFunc = require('./app/models/ChartOfAccounts.js');

        //Initialise scope variables
        $scope.Countries = CountriesList.Countries;
        $scope.NewSalesTax = new SalesTaxFunc.SalesTax();
        function Run() {
            $("#Alert").hide();
            $("#Preview_Alert").hide();
            GetSalesTax();

            //load Invoice settings
            Settings.getSettings(function (err, AppSettings) {
                $scope.Settings = AppSettings;
                $scope.Currency = $scope.Settings.Currency;
            });
        };
        Run();

        function GetSalesTax() {

            if ($scope.SalesTaxTable != null) {
                $scope.SalesTaxTable.destroy();
                $('#SalesTaxTable tbody').off('click');
            }

            SalesTaxFunc.getSalesTax("", function (SalesTax) {
                console.log("Getting SalesTax");

                if (SalesTax.length == 0) {

                    console.log("No SalesTax found");
                }

                $scope.SalesTaxTable = $('#SalesTaxTable').DataTable({
                    data: SalesTax,
                    rowId: "UniqueID",
                    columns: [
                        { 'data': 'Name' },
                        { 'data': 'Description' },
                        { 'data': 'TaxRate', 'render': function (rate) { return parseFloat(rate).toFixed(2) + "%"; } },
                        {
                            'data': 'UniqueID', 'sortable': false, 'render': function (id) {

                                return '<div>' +
                                    '<button type="button" id="edit_' + id + '"  style="margin-right:10px;" class="btn btn-outline-info"><span class="glyphicon glyphicon-pencil"></span></button>' +
                                    '<button type="button" id="delete_' + id + '" class="btn btn-outline-danger"><span class="glyphicon glyphicon-trash"></span></button></div>'
                            }
                        }
                    ],
                    bDestroy: true
                });


                $('#SalesTaxTable tbody').unbind("click").on('click', 'button', function () {
                    //var SalesTaxID = table.row(this).id();

                    console.log("button clicked is:" + $(this).attr('id'));
                    var buttonType = $(this).attr('id').split("_")[0];
                    var SalesTaxId = $(this).attr('id').split("_")[1];
                    console.log(buttonType);
                    console.log("SalesTax id is: " + SalesTaxId);


                    switch (buttonType) {
                        case "edit":
                            SalesTaxFunc.getSalesTaxById(SalesTaxId, function (err, docs) {
                                if (err != null) {
                                    console.log("Error retriving SalesTax: " + err);
                                } else {
                                    $scope.NewSalesTax = docs[0];
                                    $scope.$apply();
                                    $('#SalesTaxModal').modal('toggle');
                                }

                            });
                            break;
                        case "delete":
                            console.log("delete action");
                            SalesTaxFunc.getSalesTaxById(SalesTaxId, function (err, docs) {
                                if (err != null) {
                                    console.log("Error retriving SalesTax: " + JSON.stringify(err));
                                } else {
                                    $scope.DeleteSalesTax = docs[0];
                                    $scope.$apply();
                                    $('#DeleteSalesTax').modal('toggle');
                                }
                            });

                            break;
                        default:
                            console.log("Unknown action");
                            break;
                    }
                });
            });
        }

        $scope.AddSalesTax = function () {
            $scope.NewSalesTax = new SalesTaxFunc.SalesTax();
        }

        $scope.SaveSalesTax = function () {

            var requiredFields = ["Name", "Abbreviation", "Rate"];

            if (Controls.CheckRequiredFields(requiredFields)) {

                var TaxAccount = {
                    AccountGroup: "Liabilities",
                    AccountType: "Sales Taxes",
                    AccountName: $scope.NewSalesTax.Name,
                    AccountCurrency: $scope.Currency,
                    AccountID: "",
                    Description: $scope.NewSalesTax.Description,
                    Archived: "",
                    ArchivedDate: "",
                    ReadOnly: true,
                }

                AccountFunc.addAccount(TaxAccount, function (err, Account) {
                    if (err != null) {
                        console.log("Error is:" + err)
                    } else {
                        console.log("Account Creation Successful");
                        //Controls.showAlert("Alert", "alert-success", "Account " + "'" + Account.AccountName + "'" + " succesfully created", "message", true);
                        console.log("Created Account: " + JSON.stringify(Account));

                        $scope.NewSalesTax.AccountId = Account;
                        console.log("save SalesTax" + JSON.stringify($scope.NewSalesTax));

                        SalesTaxFunc.addSalesTax($scope.NewSalesTax, function (err, SalesTax) {
                            if (err != null) {
                                console.log("Error is:" + err)
                            } else {
                                $('#SalesTaxModal').modal('toggle');
                                console.log("SalesTax Creation Successful");
                                Controls.showAlert("Alert", "alert-success", "SalesTax " + "'" + $scope.NewSalesTax.Name + "'" + " succesfully created", "message", true);
                                //$scope.SalesTax = SalesTax; //Update Current SalesTax
                                console.log("Created SalesTax: " + JSON.stringify(SalesTax));
                                $scope.NewSalesTax = new SalesTaxFunc.SalesTax(); //Clear New SalesTax
                                $scope.$apply();
                                GetSalesTax();
                                $('#SalesTax_error').hide();
                            }
                        });
                    }
                });
            } else {
                console.log("Missing fileds/invalid data entered");
            }
        }

        /*
        $scope.EditSalesTax = function () {
    
            console.log("Edit.NewSalesTax");
            
        }
        */

        $scope.UpdateSalesTax = function () {

            var requiredFields = ["Name", "Abbreviation", "Rate"];

            if (Controls.CheckRequiredFields(requiredFields)) {
                console.log("Update SalesTax" + JSON.stringify($scope.NewSalesTax));
                SalesTaxFunc.updateSalesTax($scope.NewSalesTax.UniqueID, $scope.NewSalesTax, function (err, num, SalesTax) {
                    err ? console.log("Error is:" + err) : console.log("SalesTax Update Successful");
                    if (err != null) {
                        console.log("Error deleting SalesTax: " + err);
                    } else {
                        $('#SalesTaxModal').modal('toggle');
                        Controls.showAlert("Alert", "alert-success", "SalesTax " + "'" + SalesTax.Name + "'" + " succesfully updated", "message", true);
                        $scope.SalesTax = SalesTax; //Update Current SalesTax
                        console.log("Updated SalesTax: " + SalesTax);
                        $scope.NewSalesTax = new SalesTaxFunc.SalesTax(); //Clear New SalesTax
                        $scope.$apply();
                        GetSalesTax();

                        var TaxAccount = {
                            AccountGroup: "Liabilities",
                            AccountType: "Sales Taxes",
                            AccountName: $scope.NewSalesTax.Name,
                            AccountCurrency: $scope.Currency,
                            AccountID: "",
                            Description: $scope.NewSalesTax.Description,
                            Archived: "",
                            ArchivedDate: "",
                            ReadOnly: true,
                        }

                        AccountFunc.updateAccount($scope.NewSalesTax.AccountId, TaxAccount, function (err, num, Account) {

                            if (err != null) {
                                console.log("Error deleting Account: " + err);
                            } else {
                                console.log("Updated Account: " + Account);
                            }
                        });
                    }
                });
            } else {
                console.log("Missing fileds/invalid data entered");
            }
        };

        $scope.RemoveSalesTax = function () {

            console.log("Delete SalesTax" + JSON.stringify($scope.DeleteSalesTax));
            SalesTaxFunc.deleteSalesTax($scope.DeleteSalesTax.UniqueID, function (err, numRemoved) {
                if (err != null) {
                    console.log("Error deleting SalesTax: " + err);
                } else {
                    $('#DeleteSalesTax').modal('toggle');
                    console.log("Deleted " + numRemoved + " SalesTax");
                    Controls.showAlert("Alert", "alert-success", "SalesTax " + "'" + $scope.DeleteSalesTax.Name + "'" + " succesfully deleted", "message", true);
                    AccountFunc.archiveAccount($scope.DeleteSalesTax.AccountId, function (err, num, Account) {
                        if (err != null) {
                            console.log("Error deleting Account: " + err);
                        } else {
                            console.log("Updated Account: " + Account);
                        }
                    });
                    $scope.DeleteSalesTax = new SalesTaxFunc.SalesTax(); //Clear New SalesTax
                    $scope.$apply();
                    GetSalesTax();
                }
            });
        };

        $scope.xRateOnChange = function () {

            $scope.NewSalesTax.TaxRate = $scope.NewSalesTax.TaxRate
            console.log("Rate in now " + $scope.NewSalesTax.TaxRate);

        }
    }
});