
jbsApp.controller("SuppliersController", function ($scope, $routeParams, $rootScope, $location) {

    //Check if user is signed-in
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {
        var SupplierFunc = require('./app/models/Suppliers.js');
        var CountriesList = require('./app/config/appfiles/Countries.js');
        var CurrenciesList = require('./app/config/appfiles/Currencies.js');


        //Initialise scope variables
        $scope.Countries = CountriesList.Countries;
        $scope.Currencies = CurrenciesList.Currencies;
        $scope.NewSupplier = new SupplierFunc.Supplier();
        function Run() {
            $("#Alert").hide();
            $("#Preview_Alert").hide();           
            GetSuppliers();
        }

        Run();

        function GetSuppliers() {

            if ($scope.SuppliersTable != null) {
                $scope.SuppliersTable.destroy();
                $('#SuppliersTable tbody').off('click');
            }

            SupplierFunc.getSupplier("", function (suppliers) {
                console.log("Getting suppliers");

                if (suppliers.length == 0) {

                    console.log("No suppliers found");
                }

                $scope.SuppliersTable = $('#SuppliersTable').DataTable({
                    data: suppliers,
                    rowId: "UniqueID",
                    columns: [
                        { 'data': 'SupplierName' },
                        { 'data': 'ContactFname', 'render': function (data, type, row) { return row.ContactFname + " " + row.ContactLname; } },
                        { 'data': 'Phone' },
                        { 'data': 'Currency' },
                        {
                            'data': 'UniqueID', 'sortable': false, 'render': function (id) {

                                return '<div>' +
                                    '<button type="button" id="edit_' + id + '"  style="margin-right:10px;" class="btn btn-outline-info"><span class="glyphicon glyphicon-pencil"></span></button>' +
                                    '<button type="button" id="delete_' + id + '" class="btn btn-outline-danger"><span class="glyphicon glyphicon-trash"></span></button></div>'
                            }
                        }
                    ],
                    bDestroy: true
                });


                $('#SuppliersTable tbody').unbind("click").on('click', 'button', function () {
                    //var supplierID = table.row(this).id();

                    console.log("button clicked is:" + $(this).attr('id'));
                    var buttonType = $(this).attr('id').split("_")[0];
                    var supplierId = $(this).attr('id').split("_")[1];
                    console.log(buttonType);
                    console.log("supplier id is: " + supplierId);


                    switch (buttonType) {
                        case "edit":
                            SupplierFunc.getSupplierById(supplierId, function (err, docs) {
                                if (err != null) {
                                    console.log("Error retriving supplier: " + err);
                                } else {
                                    $scope.NewSupplier = docs[0];
                                    $("#Currency").val($scope.NewSupplier.Currency).change();
                                    $scope.$apply();
                                    $('#SupplierModal').modal('toggle');
                                }

                            });
                            break;
                        case "delete":
                            console.log("delete action");
                            SupplierFunc.getSupplierById(supplierId, function (err, docs) {
                                if (err != null) {
                                    console.log("Error retriving supplier: " + JSON.stringify(err));
                                } else {
                                    $scope.DeleteSupplier = docs[0];
                                    $scope.$apply();
                                    $('#DeleteSupplier').modal('toggle');
                                }
                            });

                            break;
                        default:
                            console.log("Unknown action");
                            break;
                    }
                });
            });
        }

        $scope.AddSupplier = function () {
            $scope.NewSupplier = null;
            $scope.NewSupplier = new SupplierFunc.Supplier();
        }

        $scope.SaveSupplier = function () {

            var requiredFields = ["SupplierName", "Currency"], validateFields = [{ Name: "Email", Type: "email" }, { Name: "Phone", Type: "phone" }]

            if (!Controls.CheckRequiredFields(requiredFields) || !Controls.Validatefields(validateFields)) {
                console.log("Invalid/missing info!")
            } else {
                console.log("save supplier" + JSON.stringify($scope.NewSupplier));
                SupplierFunc.addSupplier($scope.NewSupplier, function (err, Supplier) {
                    if (err != null) {
                        console.log("Error is:" + err)
                    } else {
                        $('#SupplierModal').modal('toggle');
                        console.log("Supplier Creation Successful");
                        Controls.showAlert("Alert", "alert-success", "Supplier " + "'" + $scope.NewSupplier.SupplierName + "'" + " succesfully created", "message", true);
                        //$scope.Supplier = Supplier; //Update Current Supplier
                        console.log("Created Supplier: " + JSON.stringify(Supplier));
                        $scope.NewSupplier = new SupplierFunc.Supplier(); //Clear New Supplier
                        $scope.$apply();
                        GetSuppliers();
                        $('#Supplier_error').hide();
                    }
                });
            }
        }

        $scope.EditSupplier = function () {

            console.log("Edit.NewSupplier");

        }

        $scope.UpdateSupplier = function () {

            console.log("Update Supplier" + JSON.stringify($scope.NewSupplier));
            var requiredFields = ["SupplierName", "Currency"], validateFields = [{ Name: "Email", Type: "email" }, { Name: "Phone", Type: "phone" }]

            if (!Controls.CheckRequiredFields(requiredFields) || !Controls.Validatefields(validateFields)) {
                console.log("Invalid/missing info!")
            } else {
                SupplierFunc.updateSupplier($scope.NewSupplier.UniqueID, $scope.NewSupplier, function (err, num, Supplier) {
                    if (err != null) {
                        console.log("Error is:" + err)
                    } else {
                        $('#SupplierModal').modal('toggle');
                        Controls.showAlert("Alert", "alert-success", "Supplier " + "'" + $scope.NewSupplier.SupplierName + "'" + " succesfully updated", "message", true);
                        $scope.Supplier = Supplier; //Update Current Supplier
                        console.log("Updated Supplier: " + Supplier);
                        $('#SupplierModal').modal('toggle');
                        $scope.NewSupplier = new SupplierFunc.Supplier(); //Clear New Supplier
                        $scope.$apply();
                        GetSuppliers();
                    }
                });
            }
        };

        $scope.RemoveSupplier = function () {

            console.log("Delete Supplier" + JSON.stringify($scope.DeleteSupplier));
            SupplierFunc.deleteSupplier($scope.DeleteSupplier.UniqueID, function (err, numRemoved) {
                if (err != null) {
                    console.log("Error deleting supplier: " + err);
                } else {
                    $('#DeleteSupplier').modal('toggle');
                    console.log("Deleted " + numRemoved + " suppliers");
                    Controls.showAlert("Alert", "alert-success", "Supplier " + "'" + $scope.DeleteSupplier.SupplierName + "'" + " succesfully deleted", "message", true);
                    $scope.DeleteSupplier = new SupplierFunc.Supplier(); //Clear New Supplier
                    $scope.$apply();
                    GetSuppliers();
                }
            });
        };
    }

});

