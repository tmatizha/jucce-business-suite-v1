
jbsApp.controller("TransctionsController", function ($scope, $routeParams, $timeout, $rootScope, $location) {

    //Check if user is signed-in
    if ($rootScope.CurrentUser == null) {
        $location.path('login');
    } else {

        var TransactionFunc = require('./app/models/Transactions.js');
        var CountriesList = require('./app/config/appfiles/Countries.js');
        var CurrenciesList = require('./app/config/appfiles/Currencies.js');
        var AccountFunc = require('./app/models/ChartOfAccounts.js');
        var DateRangeSet = false;
        //var moment = require('moment'); 


        //Initialise scope variables
        $scope.Countries = CountriesList.Countries;
        $scope.Currencies = CurrenciesList.Currencies;
        $scope.NewTransaction = new TransactionFunc.AccountTransaction();

        Settings.getSettings(function (err, AppSettings) {
            $scope.Settings = AppSettings;
            $scope.CurrencySymbol = CurrenciesList.Currencies.find(function (obj) { return obj.ISO === $scope.Settings.Currency; }).Symbol;
            Run();
        });


        function Run() {
            $("#Alert").hide();
            $("#Preview_Alert").hide();

            $("#Account").select2({
                placeholder: "Select Account",
                allowClear: true
            });

            GetAccounts()
        }

        function GetAccounts() {
            AccountFunc.getAccount({}, function (Accounts) {
                if (Accounts.length == 0) {
                    console.log("No Expenses Accounts found");
                    $scope.Accounts = [];
                } else {

                    $scope.Accounts = Accounts;
                    $scope.$apply();
                    GetTransactions({});
                }
            });
        }

        //Calculate Date Range Filter
        function SetDateRageFilter(date) {
            //Range of years
            function GetYEARS() {
                var Years = []
                var dateStart = moment(date);
                var dateEnd = moment().add(1, 'year');
                while (dateEnd >= dateStart) {

                    var YearRange = {
                        label: dateStart.format('YYYY'),
                        startDate: moment(dateStart).startOf('year'),
                        endDate: moment(dateStart).endOf('year'),
                    };
                    Years.push(YearRange);
                    dateStart.add(1, 'year');
                }
                $scope.Years = Years;
                $scope.DateRangeFilters = Years[0].label;
                $scope.$apply();
                $scope.DateRangeChange(Years[0].startDate, Years[0].endDate);
                console.log("Years Filter is" + JSON.stringify(Years));

            };


            //Rage of financial quarters
            function GetQUARTERS() {
                var Quarters = []
                var dateStart = moment(date);
                var dateEnd = moment();

                while (dateEnd > dateStart) {

                    var quarterNum = dateStart.quarter();
                    var QuarterRange = {
                        label: dateStart.format('YYYY') + " Q" + quarterNum,
                        startDate: moment(dateStart).quarter(quarterNum).startOf('quarter'),
                        endDate: moment(dateStart).quarter(quarterNum).endOf('quarter')
                    }
                    Quarters.push(QuarterRange);
                    dateStart.add(1, 'quarter');
                }
                $scope.Quarters = Quarters;
                console.log("Quarters Filter is" + JSON.stringify(Quarters));
            };

            //Range of months
            function GetMONTHS() {
                var Months = [];
                var dateStart = moment(date);
                var dateEnd = moment()
                while (dateEnd > dateStart) {
                    var MonthRange = {
                        label: dateStart.format('YYYY') + " " + dateStart.format('MMMM'),
                        startDate: moment(dateStart).startOf('month'),
                        endDate: moment(dateStart).endOf('month')
                    };
                    Months.push(MonthRange);
                    dateStart.add(1, 'month');
                }

                $scope.Months = Months;
                console.log("Months Filter is" + JSON.stringify(Months));
            };

            GetYEARS();
            GetMONTHS();
            GetQUARTERS();

            var filter = [{
                label: "Years",
                filters: $scope.Years,
            }, {
                label: "Quarters",
                filters: $scope.Quarters,
            }, {
                label: "Months",
                filters: $scope.Months
            }];

            $scope.DateRangeFilter = filter;

            console.log("Filter are" + JSON.stringify($scope.DateRangeFilter));
        }

        function GetTransactions(filter) {
            console.log("Search Filter is: " + JSON.stringify(filter));

            TransactionFunc.getTransaction(filter, function (transaction) {
                console.log("Getting transaction");
                $scope.AccountTableTransactions = [];


                $scope.AccountsArray = [];

                if (!transaction || transaction.length == 0) {
                    console.log("No transaction found");
                    $scope.$apply();
                } else {

                    //Check and set Date Range Filter
                    if (!DateRangeSet) {
                        SetDateRageFilter(transaction[0].Date);
                        DateRangeSet = true;
                    }

                    var groupedTransactions = _.groupBy(transaction, function (transaction) {
                        return transaction.AccountId;
                    });

                    console.log("Transactions" + JSON.stringify(groupedTransactions));

                    var arrayOfKyes = [];
                    for (var key in groupedTransactions) {
                        arrayOfKyes.push(key);
                    }

                    var z = 0;
                    Calculate(groupedTransactions[arrayOfKyes[z]]);


                    function Calculate(Transactions) {
                        var AccountData = $scope.Accounts.find(function (obj) { return obj.UniqueID === arrayOfKyes[z]; })
                        var AccountName = AccountData.AccountName;
                        var AccountGroup = AccountData.AccountGroup;
                        var AccountIDx = AccountData._id
                        var AccountBalance = 0;

                        //Get Account balance
                        var firstTransaction = Transactions[0];

                        console.log("firstTransaction", JSON.stringify(firstTransaction));
                        //var AccountStartingBalance = Controls.ToNumber(firstTransaction.Credit_CompanyCurrencyAmount) - Controls.ToNumber(firstTransaction.Debit_CompanyCurrencyAmount);
                        balanceFilter = { AccountId: key, Date: { $lt: moment(new Date(firstTransaction.Date)) } }

                        TransactionFunc.getTransaction(balanceFilter, function (balanceTransactions) {

                            var startingAccountBalance = 0;
                            balanceTransactions.forEach(item => {

                                var debit = Controls.ToNumber(item.Debit_CompanyCurrencyAmount);
                                var credit = Controls.ToNumber(item.Credit_CompanyCurrencyAmount);
                                var itemBalance = 0;

                                console.log("Account Group is: " + AccountGroup);
                                if (AccountGroup == "Assets" || AccountGroup == "Liabilities") {
                                    itemBalance = debit - credit;
                                } else {
                                    itemBalance = credit + debit;
                                }
                                startingAccountBalance += itemBalance;
                            });

                            AccountBalance = startingAccountBalance;
                            console.log("startingAccountBalance: " + startingAccountBalance)

                            var AccountTransactions = {
                                AccountName: AccountName,
                                AccountStartingBalance: startingAccountBalance,
                                CreditTotal: 0,
                                DebitTotal: 0,
                                AccountTotal: 0,
                                AccountValues: [],
                                AccountIDx: AccountIDx
                            };

                            Transactions.forEach(item => {

                                var debit = Controls.ToNumber(item.Debit_CompanyCurrencyAmount);
                                var credit = Controls.ToNumber(item.Credit_CompanyCurrencyAmount);

                                var itemBalance = 0;

                                console.log("Account Group is: " + AccountGroup);
                                if (AccountGroup == "Assets" || AccountGroup == "Liabilities") {
                                    itemBalance = debit - credit;
                                } else {
                                    itemBalance = credit + debit;
                                }

                                AccountBalance += itemBalance;

                                var transactionRecord = {
                                    date: item.Date.toDateString(),
                                    Description: item.Details,
                                    Debit: debit > 0 ? (Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, debit)) : "",
                                    Credit: credit > 0 ? (Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, credit)) : "",
                                    Balance: Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, AccountBalance)
                                }

                                AccountTransactions.AccountValues.push(transactionRecord);
                                AccountTransactions.CreditTotal += Controls.ToNumber(credit);
                                AccountTransactions.DebitTotal += Controls.ToNumber(debit);
                                AccountTransactions.AccountTotal += itemBalance;
                            });

                            //Calculate Account Difference
                            AccountTransactions.AccountDifference = AccountTransactions.AccountStartingBalance + AccountTransactions.AccountTotal;

                            AccountTransactions.CreditTotal = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, AccountTransactions.CreditTotal);
                            AccountTransactions.DebitTotal = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, AccountTransactions.DebitTotal);
                            AccountTransactions.AccountTotal = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, AccountTransactions.AccountTotal);
                            AccountTransactions.AccountStartingBalance = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, AccountTransactions.AccountStartingBalance);
                            AccountTransactions.AccountDifference = Controls.ToCurrencyWithSymbol($scope.CurrencySymbol, AccountTransactions.AccountDifference);
                            $scope.AccountsArray.push(AccountTransactions);

                            console.log("AccountTransactions", JSON.stringify(AccountTransactions));
                            z++;
                            if (z < arrayOfKyes.length) {
                                Calculate(groupedTransactions[arrayOfKyes[z]]);

                            } else {

                                $scope.$apply();
                            }

                        });
                    }
                }
            });

        }

        $scope.Applyfilter = function () {

            var searchArray = [];
            var filter = null;

            if ($scope.Account) {
                searchArray.push({ AccountId: $scope.Account });
            }

            if ($scope.FromDate) {
                searchArray.push({ Date: { $gte: moment(new Date($scope.FromDate)).startOf('day') } });
            }

            if ($scope.ToDate) {
                searchArray.push({ Date: { $lte: moment(new Date($scope.ToDate)).endOf('day') } });
            }

            searchArray.length > 0 ? filter = { $and: searchArray } : filter = {};
            GetTransactions(filter);

        }

        $scope.DateRangeChange = function (start, end) {

            var startDate = $("#DateRange option:selected").attr("data-startDate") || start;
            var endDate = $("#DateRange option:selected").attr("data-endDate") || end;
            $("#FromDate").datepicker().datepicker("setDate", new Date(startDate));
            $("#ToDate").datepicker().datepicker("setDate", new Date(endDate));

        }


        $scope.ClearFilters = function () {
            $scope.DateRangeFilters = $scope.Years[0].label;
            $("#FromDate").datepicker().datepicker("setDate", new Date($scope.Years[0].startDate));
            $("#ToDate").datepicker().datepicker("setDate", new Date($scope.Years[0].endDate));
            $timeout(function () {
                $("#Account").select2("val", "");
            }, 1);
            //$scope.Applyfilter();
        }


        //*********************************************************************** export Statement *********************************************************** */

        $scope.ExportPDFStatement = function () {
            console.log("Generating PDF");
            var doc = new jsPDF('l', 'px', [842,595], 'a4', true);

            if ($scope.Settings.Logo) {
                doc.addImage($scope.Settings.Logo, 'base64', 420, 25);
            }

            doc.setFontSize(20);
            doc.text("Account Transactions", 30, 50);

            doc.setFontSize(15);
            doc.text($scope.Settings.CompanyName, 30, 70);

            doc.setFontSize(12);

            var FromDate = Controls.FormatDateString(new Date($scope.FromDate));
            var ToDate = Controls.FormatDateString(new Date($scope.ToDate));

            doc.text("Date Range: " + FromDate + " to " + ToDate, 30, 85);

            

            $scope.AccountsArray.forEach(function (account, index) {

                //doc.text(account.AccountName, 30, index == 0? 120:doc.lastAutoTable.finalY + 22);
                doc.autoTable(
                    {
                        html: '#TransactionsTable_' + account.AccountIDx,
                        didParseCell: data => {
                            if (data.section === 'foot' && data.column.index != 0) {
                                data.cell.styles.halign = 'right';
                            }
                        },
                        theme: 'striped',
                        headStyles: { fillColor: $scope.Settings.AccentColor, fontSize: 12 },
                        footStyles: { fillColor: [255, 255, 255], fontSize: 10, textColor: [0, 0, 0] },
                        columnStyles: { 1: { halign: 'left' }, 2: { halign: 'left' }, 3: { halign: 'right' }, 4: { halign: 'right' }, 5: { halign: 'right' } },
                        startY: index == 0? 130: index==1? doc.lastAutoTable.finalY:doc.lastAutoTable.finalY + 22,
                        showFoot:'lastPage'

                    });

            });

            var file = $scope.Settings.CompanyName + '- Account Transactions -' + Controls.FormatDateString(new Date()) + '.pdf'
            doc.page=1;
            for(var i = 1; i <= doc.internal.getNumberOfPages(); i++) {
                // Go to page i
               doc.setPage(i);
                //Print Page 1 of 4 for example
                addFooters(i)
           }
            doc.save(file);

            function addFooters(page) {
                const pageCount = doc.internal.getNumberOfPages();
                const currentDate = Controls.FormatDateString(new Date());
                
                doc.setFontSize(10);

                doc.text("Account Transactions - " + $scope.Settings.CompanyName, 20, 420);
                doc.text("Created on: " + currentDate, 20, 430);
                doc.text("Page " + page+ "/" + String(pageCount), 580, 430);
                doc.page ++;
                
            }
        }

        /***************************************************************************************************************************************************************/

    }

});

