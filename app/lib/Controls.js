//Data Formating 

const { uniqueId } = require('lodash');

//format amounts for currency
exports.ToCurrency = function (value) {
    var n = new Number(value);
    var myObj = {
        style: "currency",
        currency: "USD"
    }
    return n.toLocaleString("en-GB", myObj).replace("US$", "");
};

exports.ToCurrencyWithSymbol = function (symbol, value) {
    var n = new Number(value);
    var myObj = {
        style: "currency",
        currency: "USD"
    }

    var newValue = symbol + n.toLocaleString("en-GB", myObj).replace("US$", "")
    if (value < 0) {
        newValue = "-" + newValue.replace("-", "");
    }

    return newValue;
};

exports.ToNumber = function (value) {

    console.log("Original Amount: " + value);
    var amount = 0;
    if (value != "" && typeof value == "string") {
        amount = new String(value).replace(",", "");
        amount = parseFloat(amount)
    } else if (value != "" && typeof value == "number") {
        amount = value;
    } else {
        amount = 0;
    }

    console.log("Formated Amount: " + amount);

    return amount;
};

exports.AsLink = function (form, Id, value) {

    return '<a ng-reflect-router-link = "#!' + form + '/" href="#!' + form + '/' + Id + '/view" class="tableLink">' + value + '</a>';
}

//Form validation
exports.SetError = function (Control, error) {
    if (error == "") {
        document.getElementById(Control).classList.remove("is-invalid");
        hideError(Control);
    } else {
        console.log("Setting error: " + error + " on field: " + Control);
        document.getElementById(Control).classList.add("is-invalid");
        showError(Control, "value required!");
    }
};

exports.CheckRequiredFields = function (fields) {
    var isValid = true;

    if (fields.length != 0) {
        fields.forEach(Control => {
            console.log("Checking field:" + Control + $('#' + Control).val() + " test");
            if ($('#' + Control).val() == "" || $('#' + Control).val() == null || $('#' + Control).val() == "? string: ?" || $('#' + Control).val() == "? undefined:undefined ?") {
                showError(Control, "value required!");
                isValid = false;
            } else {
                hideError(Control);
            }
        });
    } else {
        isValid = true;
    }
    return isValid;
}

exports.Validatefields = function (fields) {
    var isValid = true;
    var EmailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var PhoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

    if (fields.length != 0) {
        fields.forEach(Control => {

            console.log("Checking field: " + Control.Name);
            if ($('#' + Control.Name).val() != "" && $('#' + Control.Name).val() != null) {

                switch (Control.Type) {
                    case "email":
                        if (EmailRegex.test($('#' + Control.Name).val())) {
                            hideError(Control.Name);
                        } else {
                            showError(Control.Name, "Enter a valid email address!");
                            isValid = false
                        }
                        break;
                    case "phone":
                        if (PhoneRegex.test($('#' + Control.Name).val())) {
                            hideError(Control.Name);
                        } else {
                            showError(Control.Name, "Enter a valid phome number!");
                            isValid = false
                        }
                        break;
                    default:
                        break;
                }

            } else {
                hideError(Control.Name);
            }
        });
    } else {
        isValid = true;
    }
    return isValid;
}

function showError(field, error) {
    document.getElementById(field).classList.add("is-invalid");
    $('#' + field + '_error').show();
    $('#' + field + '_error').text(error);
}

function hideError(field) {
    document.getElementById(field).classList.remove("is-invalid");
    $('#' + field + '_error').hide();
    $('#' + field + '_error').text('');
}

//Alert Controlls
exports.showAlert = function (alertId, type, message, txtFieldId, autoDismiss) {
    console.log(type + " " + message);
    var alertIdx = "#" + alertId;
    var txtFieldId = "#" + txtFieldId;
    $(txtFieldId).text(message);
    document.getElementById(alertId).classList.add(type);
    $(alertIdx).show();
    window.scrollTo(0, 0);
    if (autoDismiss) {
        setTimeout(function () {
            $(alertIdx).fadeTo(500, 0).slideUp(500, function () {
                $(this).removeClass("ng-scope alert-success");
                $(this).removeAttr("style");
                $(this).css({ "text-align": "center;", "display": "none;" });
                $(alertIdx).hide();
            });
        }, 5000);
    }
}

//Aninimate counters
exports.updateCounters = function (total, id) {
    $('#' + id).animate({
        counter: total
    }, {
        duration: 1000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        },
        complete: this.updateCounters
    });
};

//Format dates
exports.FormatDate = function (date) {
    console.log("Formating Date: " +date );
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();

    var dateString = (d <= 9 ? '0' + d : d) + '/' + (m <= 9 ? '0' + m : m) + '/' + y;

    console.log("dateString: " +dateString );

    return dateString;
};

exports.FormatDateString = function (date) {
    var fDate = date.toDateString().split(" ");
    var day = fDate[2];
    var month = fDate[1]
    var year = fDate[3]
    var dateString = day+" "+ month+", "+year;

    return dateString;
};
