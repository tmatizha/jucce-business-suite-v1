const CustomerFunc = require('../models/Customer.js');
const SupplierFunc = require('../models/Suppliers.js');
const PaymentTermFunc = require('../models/PaymentTerms.js');
//const settings



exports.CalculateLatePaymentCharge = (CustomerID, DocAmount, DueDate, PaymentTerms, Type, callback) => {

    let latePaymentDetails = {
        LateFee: 0,
        DaysOverDue: 0,
        error: null
    }
    
    if (DocAmount > 0) {
        
        if (PaymentTerms) {
            callback(calculateAmount(DueDate, PaymentTerms.LatePaymentRate, DocAmount, PaymentTerms.PaymentPeriod));
        } else if (Type == "Invoice") {
            Settings.getSettings(function (err, AppSettings) {

                if (err != null) {
                    console.log("Error retriving settings: " + err);
                    latePaymentDetails.error = err;
                    callback(latePaymentDetails);

                } else {

                    if (AppSettings.DefaultPaymentTerm.length > 0) {

                        //Get Payment Terms
                        PaymentTermFunc.getPaymentTermById(AppSettings.DefaultPaymentTerm, function (err, PaymentTerms) {
                            console.log("Getting PaymentTerms");
                            if (err != null) {
                                console.log("Error Getting PaymentTerms :" + err);
                                latePaymentDetails.error = err;
                                callback(latePaymentDetails);
                            }
                            else if (PaymentTerms.length == 0) {
                                console.log("No PaymentTerms found");
                                latePaymentDetails.error = "No PaymentTerms found";
                                callback(latePaymentDetails);
                            } else {

                                console.log("latePaymentDetails: " + JSON.stringify(latePaymentDetails));
                                callback(calculateAmount(DueDate, PaymentTerms[0].LatePaymentRate, DocAmount, PaymentTerms[0].PaymentPeriod));
                            }
                        });

                    } else {
                        console.log("No PaymentTerms found");
                        latePaymentDetails.error = "No PaymentTerms found";
                    }

                }
            });
        } else {
            latePaymentDetails.error = "No PaymentTerms found";
            callback(latePaymentDetails);
        }
    }else {
        latePaymentDetails.error = "No PaymentTerms found";
        callback(latePaymentDetails);
    }
}

function calculateAmount(DueDate, rate, Amount, period) {

    let latePaymentDetails = {
        LateFee: 0,
        DaysOverDue: 0,
        error: null
    }

    console.log("DueDate" + DueDate);
    let today = moment().startOf('day');
    let dateDue = moment(new Date(DueDate)).startOf('day');
    let numOfDays = moment.duration(today.diff(dateDue)).asDays();
    let factor = Math.floor(numOfDays / period);
    let fee = 0;

    console.log("today" + today);
    console.log("dateDue" + dateDue);
    console.log("numOfDays" + numOfDays);
    console.log("factor" + factor);
    console.log("rate" + rate);

    if (numOfDays > 0 && factor >= 1) {
        fee = (((rate / 100) * Amount) * factor);
        latePaymentDetails.LateFee = fee;
        latePaymentDetails.DaysOverDue = numOfDays;
    }

    console.log("latePaymentDetails" + JSON.stringify(latePaymentDetails));
    return latePaymentDetails;
}
