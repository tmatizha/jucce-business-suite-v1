var AccountFunc = require('../models/ChartOfAccounts.js');
var AccountTypes = require('../config/appfiles/ChartOfAccounts.js').ChartOfAccounts;
var TransactionFunc = require('../models/Transactions.js');
var Controls = require('./Controls');


exports.GetTransactionData = function (mainFilter, callback) {
    //console.log("Search Filter is: " + JSON.stringify(mainFilter));

    var TransactionDataArray = [];
    var AccountTypesArray = [];

    AccountTypes.forEach(account => {
        account.value.forEach(accountType => {
            AccountTypesArray.push(accountType.Type);
        });
    });

    console.log("Accounts Array" + AccountTypesArray);

    if (mainFilter != "") {

        mainFilter.forEach(filter => {

            var Result = {
                title: filter.label,
                Data: []
            }
            var xFilter = { $and: [{ Date: { $gte: filter.startDate } }, { Date: { $lte: filter.endDate } }] };
            GetAccountInfo(xFilter, callback, false, Result, mainFilter.length, AccountTypesArray);
            console.log("Filter is:" + JSON.stringify(Result));
        });

    } else {
        filter = {};
        GetAccountInfo(filter, callback, false);
    }



    function GetAccountInfo(filter, callback, withStartingBalance, result, length, AccountTypesArray) {
        var Accounts, AccountsArray = [];
        var AccountDataArray = [];
        var FinalAccountDataArray = [];
        AccountFunc.getAccount({}, function (AccountData) {
            if (AccountData.length == 0) {
                console.log("No Accounts found");
                Accounts = [];
            } else {

                Accounts = AccountData;

                TransactionFunc.getTransaction(filter, function (transaction) {
                    console.log("Found transactions: " + JSON.stringify(transaction));

                    if (!transaction || transaction.length == 0) {
                        console.log("No transaction found");
                        var xFinalAccountDataArray = [];
                        AccountTypesArray.forEach(type => {

                            xFinalAccountDataArray.push({
                                "AccountType": type,
                                "AccountBalance": 0,
                                "AccountStartingBal": 0,
                                "AccountDebitTotal": 0,
                                "AccountCreditTotal": 0,
                                "AccountDifference": 0
                            })
                        });

                        TransactionDataArray.push({
                            title: result.title,
                            Data: xFinalAccountDataArray
                        });



                        if (length == TransactionDataArray.length) {
                            callback(TransactionDataArray);
                            console.log("done computing");
                        } else {

                            console.log("no record, still computing: " + TransactionDataArray.length + " of " + length);
                        }
                    } else {

                        var groupedTransactions = _.groupBy(transaction, function (transaction) {
                            return transaction.AccountId;
                        });

                        console.log("All Group Transactions" + JSON.stringify(groupedTransactions));

                        var arrayOfKyes = [];
                        for (var key in groupedTransactions) {
                            arrayOfKyes.push(key);
                        }

                        var z = 0;
                        Calculate(groupedTransactions[arrayOfKyes[z]]);


                        function Calculate(Transactions) {
                            console.log("arrayOfKyes[z] "+arrayOfKyes[z]);
                            var AccountData = Accounts.find(function (obj) { return obj.UniqueID === arrayOfKyes[z]; })

                            if (AccountData) {
                                var AccountName = AccountData.AccountName;
                                var AccountGroup = AccountData.AccountGroup;
                                var AccountType = AccountData.AccountType;
                                var AccountBalance = 0;

                                if (withStartingBalance) {
                                    //Get Account balance
                                    var firstTransaction = Transactions[0];

                                    //console.log("firstTransaction", JSON.stringify(firstTransaction));
                                    balanceFilter = { AccountId: key, Date: { $lt: moment(new Date(firstTransaction.Date)) } }

                                    TransactionFunc.getTransaction(balanceFilter, function (balanceTransactions) {

                                        var startingAccountBalance = 0;
                                        balanceTransactions.forEach(item => {

                                            var debit = Controls.ToNumber(item.Debit_CompanyCurrencyAmount);
                                            var credit = Controls.ToNumber(item.Credit_CompanyCurrencyAmount);
                                            var itemBalance = 0;

                                            //console.log("Account Group is: " + AccountGroup);
                                            if (AccountGroup == "Assets" || AccountGroup == "Liabilities") {
                                                itemBalance = credit - debit;
                                            } else {
                                                itemBalance = credit + debit;
                                            }
                                            startingAccountBalance += itemBalance;
                                        });

                                        GenarateData(startingAccountBalance);
                                    });
                                } else {
                                    GenarateData(0);
                                }

                                function GenarateData(startingAccountBalance) {

                                    AccountBalance = startingAccountBalance;
                                    //console.log("startingAccountBalance: " + startingAccountBalance)

                                    var AccountTransactions = {
                                        AccountName: AccountName,
                                        AccountGroup: AccountGroup,
                                        AccountType: AccountType,
                                        AccountStartingBalance: startingAccountBalance,
                                        CreditTotal: 0,
                                        DebitTotal: 0,
                                        AccountTotal: 0,
                                        AccountValues: []
                                    };

                                    Transactions.forEach(item => {

                                        var debit = Controls.ToNumber(item.Debit_CompanyCurrencyAmount);
                                        var credit = Controls.ToNumber(item.Credit_CompanyCurrencyAmount);

                                        var itemBalance = 0;

                                        //console.log("Account Group is: " + AccountGroup);
                                        if (AccountGroup == "Assets" || AccountGroup == "Liabilities") {
                                            itemBalance = credit - debit;
                                        } else {
                                            itemBalance = credit + debit;
                                        }

                                        AccountBalance += itemBalance;

                                        var transactionRecord = {
                                            date: item.Date.toDateString(),
                                            Description: item.Details,
                                            Debit: debit,
                                            Credit: credit,
                                            Balance: AccountBalance
                                        }

                                        AccountTransactions.AccountValues.push(transactionRecord);
                                        AccountTransactions.CreditTotal += Controls.ToNumber(credit);
                                        AccountTransactions.DebitTotal += Controls.ToNumber(debit);
                                        AccountTransactions.AccountTotal += itemBalance;
                                        AccountTransactions.Month = result.title;
                                    });

                                    //Calculate Account Difference
                                    AccountTransactions.AccountDifference = AccountTransactions.AccountStartingBalance + AccountTransactions.AccountTotal;

                                    AccountsArray.push(AccountTransactions);

                                    //console.log("AccountTransactions", JSON.stringify(AccountTransactions));
                                    //console.log("groupedTransactions", JSON.stringify(groupedTransactions));

                                    z++;
                                    if (z < arrayOfKyes.length) {
                                        Calculate(groupedTransactions[arrayOfKyes[z]]);

                                    } else {

                                        console.log("Accounts Array: ", JSON.stringify(AccountsArray));
                                        //return callback(AccountsArray);

                                        //******************************* To Re-enable ****************************************************/



                                        //console.log("Accounts Array: ", JSON.stringify(AccountsArray));
                                        var xGroupedTransactions = _.groupBy(AccountsArray, function (account) {
                                            return account.AccountType;
                                        });
                                        //console.log("Transaction Data :" + JSON.stringify(xGroupedTransactions));
                                        for (var key in xGroupedTransactions) {

                                            var AccountData = {
                                                AccountType: key,
                                                AccountBalance: 0,
                                                AccountStartingBal: 0,
                                                AccountDebitTotal: 0,
                                                AccountCreditTotal: 0,
                                                AccountDifference: 0,
                                            }

                                            xGroupedTransactions[key].forEach(account => {

                                                //console.log("Account Id's are : " + account.AccountGroup + " " + account.AccountType + " " + account.AccountTotal);
                                                AccountData.AccountBalance += account.AccountStartingBalance;
                                                AccountData.AccountStartingBal += account.AccountStartingBalance;
                                                AccountData.AccountDebitTotal += account.DebitTotal;
                                                AccountData.AccountCreditTotal += account.CreditTotal;
                                                AccountData.AccountDifference += account.AccountDifference;

                                            });

                                            AccountDataArray.push(AccountData);

                                        }

                                        if (result) {

                                            // Sort and Check if all data is there for all account groups
                                            //var accountGroups = ["Income","Liabilities","Assets","Expenses"];

                                            AccountTypesArray.forEach(type => {

                                                var accountInfo = AccountDataArray.find(function (obj) { return obj.AccountType === type; });
                                                if (accountInfo) {

                                                    FinalAccountDataArray.push(accountInfo);
                                                } else {
                                                    FinalAccountDataArray.push({
                                                        "AccountType": type,
                                                        "AccountBalance": 0,
                                                        "AccountStartingBal": 0,
                                                        "AccountDebitTotal": 0,
                                                        "AccountCreditTotal": 0,
                                                        "AccountDifference": 0
                                                    })
                                                }

                                            });
                                            console.log("Passing transaction data: " + JSON.stringify(AccountDataArray));



                                            TransactionDataArray.push({ title: result.title, Data: FinalAccountDataArray });
                                            if (length == TransactionDataArray.length) {
                                                callback(TransactionDataArray);
                                            } else {

                                                console.log("still computing: " + TransactionDataArray.length + " of " + length);
                                            }
                                        }
                                        else {
                                            callback(AccountDataArray);
                                        }


                                        //************************************************************************************************************************************** */
                                    }

                                }

                            } else {
                                console.log("No Acccount data found")
                            }
                        }
                    }
                });
            }
        });
    }
}

exports.GetExpenseDistribution = function (filter, callback) {

    var ExpenseDistribution = [];
    AccountFunc.getAccount({ AccountGroup: "Expenses" }, function (AccountData) {
        if (AccountData.length == 0) {
            console.log("No Expenses Accounts found");
            Accounts = [];
        } else {

            var filters = [];

            AccountData.forEach(account => {
                var filter = { "AccountId": account.UniqueID };
                filters.push(filter);
            });

            TransactionFunc.getTransaction({ $and: [filter, { $or: filters }] }, function (transactions) {
                //console.log("Found Expenses transaction" + JSON.stringify(transactions));

                var groupedTransactions = _.groupBy(transactions, function (transaction) {
                    return transaction.AccountId;
                });

                for (var key in groupedTransactions) {
                    var AccountDetails = AccountData.find(function (obj) { return obj.UniqueID === key; });

                    var ExpenseData = {
                        AccountName: AccountDetails.AccountName,
                        AccountTotal: 0
                    }

                    groupedTransactions[key].forEach(transaction => {

                        ExpenseData.AccountTotal += transaction.Debit_CompanyCurrencyAmount
                    });

                    ExpenseDistribution.push(ExpenseData);
                }

                console.log("Expenses transaction Data" + JSON.stringify(ExpenseDistribution));
                callback(ExpenseDistribution);

            });
        }
    });
}