exports.Account = function () {
    this.AccountGroup = "";
    this.AccountType = "";
    this.AccountName = "";
    this.AccountCurrency = "";
    this.AccountID = "";
    this.Description = "";
    this.Archived = "";
    this.ArchivedDate = "";
    this.ReadOnly = false;
    this.DefaultAccount = false;
    this.LastTransactionDate = "";
};

exports.AccountTransaction = function () {
    this.Date = new Date();
    this.DocumentId = "";
    this.Details = "";
    this.CompanyCurrencyAmount = 0;
    this.AccountCurrencyAmount = 0;
    this.ReferenceCurrencyAmount = 0;    
};

var Datastore = require('nedb');
var alertType, message;

const electron = require('electron');
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
var db = new Datastore({ filename: userDataPath+'/jucce/accounts.db', autoload: true });
//var transactionDB = new Datastore({ filename: app.getPath('userData')+'jucce/transactions.db', autoload: true });


exports.addAccount = function (Account, callback) {
    var id;
    if (Account._id) { delete Account._id };
    Account.UniqueID = SystemData.UniqueID();
    db.insert(Account, function (err, doc) {
        console.log(err);
        id = doc.UniqueID
        console.log(id);
        callback(err, id);
    });
};

exports.getAccount = function (filter, callback) {
  
    db.find(filter, function (err, docs) {
        console.log(filter);
        if(err){
            console.log("Error getting accounts for filter"+JSON.stringify(filter)+" "+ err);
        }else{
            callback(docs);            
            //console.log(docs);
        }
    });
};

exports.getAccountById = function (filter, callback) {
    filter = filter.replace(" ", "|");
    var xfilter = new RegExp(filter, "gi");
    console.log("Account ID is:" + xfilter);
    var myfilter = { UniqueID: xfilter };
    db.find(myfilter, function (err, docs) {

        callback(err, docs);
        console.log(myfilter);
        console.log(docs);
    });
};

exports.updateAccount = function (id, Account, callback) {
    delete Account._id;
    console.log(Account);
    db.update({ UniqueID: id }, Account, { multi: false, returnUpdatedDocs: true }, function (err, num, doc) {
        callback(err, num, doc);
    });
};


exports.archiveAccount = function (id, callback) {
    db.update({ UniqueID: id }, {$set:{Archived:true, ArchivedDate: new Date()}}, { multi: false, returnUpdatedDocs: true }, function (err, num, doc) {
        callback(err, num, doc);
    });
};

exports.deleteAccount = function (id, callback) {
    db.remove({ UniqueID: id }, function (err, numRemoved) {
        callback();
    });
};


//Default Accounts Defination
var Settings = require('./ApplicationSettings.js');

var DefaultAccounts = [
    {
        "AccountGroup": "Assets",
        "AccountType": "Expected Payments from Customers",
        "AccountName": "Accounts Receivable",
        "AccountID": "",
        "Description": "Expected Payments from Customers",
        "Archived": "",
        "ArchivedDate": "",
        "ReadOnly": true,
        "DefaultAccount": true
    },
    {
        "AccountGroup": "Liabilities",
        "AccountType": "Expected Payments to Vendors",
        "AccountName": "Accounts Payable",
        "AccountID": "",
        "Description": "Expected Payments to Vendors",
        "Archived": "",
        "ArchivedDate": "",
        "ReadOnly": true,
        "DefaultAccount": true
    },
    {
        "AccountGroup": "Income",
        "AccountType": "Income",
        "AccountName": "Sales",
        "AccountID": "",
        "Description": "Sales Account",
        "Archived": "",
        "ArchivedDate": "",
        "ReadOnly": true,
        "DefaultAccount": true
    },
    {
        "AccountGroup": "Income",
        "AccountType": "Uncategorized Income",
        "AccountName": "Uncategorized Income",
        "AccountID": "",
        "Description": "All Uncategorized Income",
        "Archived": "",
        "ArchivedDate": "",
        "ReadOnly": true,
        "DefaultAccount": true
    },
    {
        "AccountGroup": "Income",
        "AccountType": "Gain on Foreign Exchange",
        "AccountName": "Gain on Foreign Exchange",
        "AccountID": "",
        "Description": "Gain on Foreign Exchange, when the rate changes",
        "Archived": "",
        "ArchivedDate": "",
        "ReadOnly": true,
        "DefaultAccount": true
    },
    {
        "AccountGroup": "Expenses",
        "AccountType": "Uncategorized Expense",
        "AccountName": "Uncategorized Expense",
        "AccountID": "",
        "Description": "Uncategorized Expenses",
        "Archived": "",
        "ArchivedDate": "",
        "ReadOnly": true,
        "DefaultAccount": true
    },
    {
        "AccountGroup": "Expenses",
        "AccountType": "Loss on Foreign Exchange",
        "AccountName": "Loss on Foreign Exchange",
        "AccountID": "",
        "Description": "Loss on Foreign Exchange, when the rate changes",
        "Archived": "",
        "ArchivedDate": "",
        "ReadOnly": true,
        "DefaultAccount": true
    },
];


//format amounts for currency
exports.CheckAccounts = function (callback) {
    var CompanyCurrency;
    var index = 0;
    Settings.getSettings(function (err, AppSettings) {
        if (err != null) {
            console.log("Error Getting Settings:" + err);
        } else {
            CompanyCurrency = AppSettings.Currency;

            DefaultAccounts.forEach(item => {
                db.findOne({ AccountGroup: item.AccountGroup, AccountName: item.AccountName }, function (err, Doc) {
                    index++;
                    if (err != null) {
                        console.log("Error is:" + err);
                        index == DefaultAccounts.length ? callback() : "checking!!";
                    } else if (Doc == null) {
                        item.AccountCurrency = CompanyCurrency;
                        db.insert(item, function (err, account) {
                            if (err != null) {
                                console.log("Error Creating Account is:" + err);
                            } else {
                                console.log("Created Account: " + JSON.stringify(account));
                                index == DefaultAccounts.length ? callback() : "checking!!";
                            }
                        });
                    } else {
                        console.log("Account :" + Doc.AccountName + "check complete");
                        index == DefaultAccounts.length ? callback() : "checking!!";
                    }
                });
            });
        }
    });
}