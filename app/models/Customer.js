exports.Customer = function () {
  this.CustomerName = "";
  this.Email = "";
  this.Phone = "";
  this.ContactFname = "";
  this.ContactLname = "";
  this.Currency = "";

  this.Billing = {
    AddressLine1: "",
    AddressLine2: "",
    City: "",
    ZipCode: "",
    Country: ""
  };

  this.Shipping = {
    SameAsBilling: false,
    AddressLine1: "",
    AddressLine2: "",
    City: "",
    ZipCode: "",
    Country: ""
  };
};

var Datastore = require('nedb');
var alertType, message;

const electron = require('electron');
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
var db = new Datastore({ filename: userDataPath+'/jucce/customers.db', autoload: true });


exports.addCustomer = function (customer, callback) {

  if (customer._id) { delete customer._id };
  customer.UniqueID = SystemData.UniqueID();
  db.insert(customer, function (err, doc) {
    callback(err, doc);
    console.log("Doc ID" + doc.UniqueID);
  });
};

exports.getCustomer = function (filter, callback) {
  filter = filter.replace(" ", "|");
  var xfilter = new RegExp(filter, "gi");
  console.log(xfilter);
  var myfilter = { $or: [{ UniqueID: xfilter }, { CustomerName: xfilter }, { Email: xfilter }, { ContactFname: xfilter }, { ContactLname: xfilter }, { Phone: xfilter }, { "Billing.Country": xfilter }, { "Billing.City": xfilter }, { "Shipping.Country": xfilter }, { "Shipping.City": xfilter }] };
  db.find(myfilter, function (err, docs) {

    callback(docs);
    console.log(myfilter);
    console.log(docs);
  });
};

exports.getCustomerById = function (filter, callback) {
  filter = filter.replace(" ", "|");
  var xfilter = new RegExp(filter, "gi");
  console.log("Customer ID is:" + xfilter);
  var myfilter = { UniqueID: xfilter };
  db.find(myfilter, function (err, docs) {

    callback(err, docs);
    console.log(myfilter);
    console.log(docs);
  });
};

exports.updateCustomer = function (id, customer, callback) {
  delete customer._id;
  console.log(customer);
  db.update({ UniqueID: id }, customer, { multi: false, returnUpdatedDocs: true }, function (err, num, doc) {
    callback(err, num, doc);
  });
};

exports.deleteCustomer = function (id, callback) {
  db.remove({ UniqueID: id }, function (err, numRemoved) {
    callback(err, numRemoved);
  });
};