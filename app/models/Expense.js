exports.LineItem = function(){
    this.Item= "";
    this.Description= "";
    this.Quantity= 1;
    this.Cost= 0.00;
    this.Amount= 0;
    this.TaxName= "";
    this.TaxRate= 0;
    this.TaxAmount= 0;
    this.ProductId= "";
    this.ExpenseAccount="";    
    this.TransactionID = "";
    this.TaxTransactionID = "";
};

exports.Expense = function () {
    this.Supplier = {
        SupplierName : "",
        Email : "",
        Phone : "",
        ContactFname : "",
        ContactLname : "",
        AddressLine1 : "",
        AddressLine2 : "",
        City : "",
        ZipCode : "",
        Country : "",
        UniqueID : ""
    };

    this.ExpenseHeader= {
        ExpenseNumber: "",
        OrderNumber :"",
        ExpenseDate :"",
        PaymentDate :"",
        ExpenseCurrency :"",
        CompanyCurrency : "",
        ExchangeRate : 1,
        ReferenceCurrency : "",
        ReferenceCurrencyRate : 1,
        ExpenseNetAmount :0.00,
        ExpenseAmount :0.00,
        PaymentTerms:{
            PaymentTerm:"",
            PaymentPeriod:0,
            LatePaymentRate:0
        }
    };

    this.ExpensePayment= {
        Balance : "",
        AmountRecieved : 0.00,
        PaymentDate : "",
        PaymentStatus : "Unpaid",
        NextPaymentDate : ""
    };

    this.ExpenseLines = [];
    this.ExpenseTaxes = [];
    this.ExpenseCreationDate = "";
};


var Datastore = require('nedb');

const electron = require('electron');
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
console.log("userDataPath: ",userDataPath);
var db = new Datastore({ filename: userDataPath+'/jucce/expenses.db', autoload: true });

var deletedDB = new Datastore({ filename: userDataPath+'/jucce/deletedExpenses.db', autoload: true });

exports.CreateExpense = function (expense, callback) {
    expense.UniqueID = SystemData.UniqueID();
    db.insert(expense, function (err, doc) {
        callback(err, doc);
    });
};

exports.getExpense = function (searchString, callback) {

    // Take out spaces and replace with pipes
    searchString = searchString.split(' ').join('|');

    // Use searchString to build rest of regex
    // -> Note: 'i' for case insensitive
    var regex = new RegExp(searchString, 'i');

    // Build query, using regex for each searchable field
    var query = {
        $or: [
            {
                "UniqueID": {
                    "$regex": regex,
                },
            },

            {
                "Supplier.SupplierName": {
                    "$regex": regex,
                },
            },

            {
                "Email": {
                    "$regex": regex,
                },
            },

            {
                "ContactFname": {
                    "$regex": regex,
                },
            },
            {
                "ContactLname": {
                    "$regex": regex,
                },
            },
            {
                "Phone": {
                    "$regex": regex,
                },
            },
            {
                "Country": {
                    "$regex": regex,
                },
            },
            {
                "City": {
                    "$regex": regex,
                },
            }
        ]
    };
    console.log("my filter is: " + JSON.stringify(query));

    db.find(query, function (err, docs) {

        callback(err, docs);
        console.log("my filter is: " + JSON.stringify(query));
        console.log(JSON.stringify(docs));
    });
};

exports.getAllExpenses = function (callback) {
    db.find({}, function (err, docs) {
        callback(err,docs);
        console.log("All expenses: "+ JSON.stringify(docs));
    });
};

exports.searchExpense = function (filter, callback) {
    console.log("search filter is: " + JSON.stringify(filter));
    db.find(filter, function (err, docs) {
        callback(err, docs);        
        console.log("Result is: "+JSON.stringify(docs));
    });
};

exports.getExpenseByID = function (filter, callback) {

    var myfilter = { UniqueID: filter };
    console.log(myfilter);
    db.find(myfilter, function (err, docs) {
        callback(err,docs);
        console.log(docs);
    });
};

exports.CheckExpenseNum = function (filter, callback) {
    //filter = filter.replace(" ", "|");
    var xfilter = new RegExp(filter);
    console.log(xfilter);
    var myfilter = { "ExpenseHeader.ExpenseNumber": { "$regex": xfilter } };
    db.count(myfilter, function (err, count) {
        callback(count);
        console.log(myfilter);
        console.log(count);
        console.log(err);
    });
};

//only for draft expenses
exports.updateExpense = function (id, expense, callback) {
    delete expense._id;
    db.update({ UniqueID: id }, expense, { multi: false, returnUpdatedDocs: true }, function (err, num, doc) {
         if (err) {
            console.log("Invoice Update Failed: " + err)
        } else {
            console.log("Updated Expense is: ", JSON.stringify(doc));
            console.log("Updated " + num + " documents");
            callback(err, doc);
        }
    });
};


//Expense Payament
exports.expensePayment = function (id, PaymentInfo, ExpenseState, callback) {
    //delete expense.UniqueID;
    console.log(JSON.stringify("Payment Info: " + PaymentInfo));
    db.update({ UniqueID: id }, {
        $set: {
            "ExpensePayment.Balance": PaymentInfo.Balance,
            "ExpensePayment.AmountRecieved": PaymentInfo.AmountRecieved,
            "ExpensePayment.PaymentDate": PaymentInfo.PaymentDate,
            "ExpensePayment.PaymentStatus": PaymentInfo.PaymentStatus,
            "ExpensePayment.NextPaymentDate": PaymentInfo.NextPaymentDate,
            "ExpenseState": ExpenseState
        }
    },
        function (err, doc) {
            callback(err);
        });
};

//only for draft expenses
exports.deleteExpense = function (expense, callback) {
    db.remove({ UniqueID: expense.UniqueID }, function (err, numRemoved) {
        callback(err, numRemoved);
    });

    delete expense._id;
    deletedDB.insert(expense, function (err, doc) {
        if(err){
            console.log("error recording expense as deleted: "+ err);
        }else{
            console.log("expense recorded as deleted: "+ JSON.stringify(doc));
        }
        
    });
};

exports.CountExpenses = function (filter, callback) {
    db.count({ "ExpensePayment.PaymentStatus": filter }, function (err, count) {
        callback(err, count);
    });
};
exports.CountExpensesByState = function (filter, callback) {
    db.count({ "ExpenseState": filter }, function (err, count) {
        callback(err, count);
    });
};
exports.CountAllExpenses = function (callback) {
    db.count({}, function (err, count) {
        callback(err, count);
    });
};

exports.CountLateExpenses = function (callback) {
    db.count({ "ExpenseHeader.PaymentDate": { $gte: new Date() }, "ExpenseState": "To pay" }, function (err, count) {
        callback(err, count);
    });
};