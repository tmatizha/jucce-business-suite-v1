exports.InvoicePayment = function () {
  this.InvoiceNumber = "";
  this.InvoiceID = "";
  this.CustomerID = "";
  this.PaymentDate = "";
  this.AmountRecieved = "";
  this.AmountDue = "";
  this.InvoiceCurrency = "";
  this.CompanyCurrency = "";
  this.ExchangeRate = 1;
  this.RefrenceCurrency = "";
  this.ReferenceCurrencyRate = "";
  this.PaymentType = "";
  this.PaymentMethod = "";
  this.Balance = 0;
  this.PaymentDetails = "";
  this.CreatedBy = "";
  this.TransactionId = "";
};


var Datastore = require('nedb');
var alertType, message;

const electron = require('electron');
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
var db = new Datastore({ filename: userDataPath+'/jucce/invoicePayments.db', autoload: true });


exports.recordPayment = function (payment, callback, PaymentInfo, InvoiceState) {
  var id;
  if (payment._id) { delete payment._id };
  payment.UniqueID = SystemData.UniqueID();
  db.insert(payment, function (err, doc) {
    console.log(err);
    id = doc.UniqueID
    console.log(id);
    callback(doc, PaymentInfo, InvoiceState, err);
  });
};

exports.getPayments = function (filter, callback) {
  filter = filter.replace(" ", "|");
  var xfilter = new RegExp(filter, "gi");
  console.log(xfilter);
  var myfilter = { $or: [{ UniqueID: xfilter }, { CustomerID: xfilter }, { InvoiceID: xfilter }, { PaymentDate: xfilter }, { PaymentMethod: xfilter }, { InvoiceNumber: xfilter }, { CreatedBy: xfilter }] };
  db.find(myfilter, function (err, docs) {
    callback(docs);
    console.log(myfilter);
    console.log(docs);
  });
};

exports.getPaymentById = function (id, callback) {
  console.log("Payment ID is:" + id);
  db.find({ UniqueID: id }, function (err, docs) {
    callback(err,docs);
    console.log("Payment found:" + JSON.stringify(docs));
  });
};

exports.updatePaymentTransactionID = function (id, TransactionId, callback) {
  //delete payment.UniqueID;
  console.log("Update Payment:" + TransactionId);
  db.update({ UniqueID: id }, {$set:{TransactionId:TransactionId}},{ multi: false, returnUpdatedDocs: true }, function (err, num, doc) {
    callback(err, doc);
  });
};

exports.deletePayment = function (id, callback, PaymentInfo, InvoiceState) {
  console.log("Removing ID: " + id);
  db.remove({ UniqueID: id }, function (err, numRemoved) {
    callback(numRemoved, PaymentInfo, InvoiceState, err);
  });
};

exports.deleteAllPayments = function(InvoiceID, callback){

  db.remove({InvoiceID: InvoiceID }, { multi: true }, function (err, numRemoved) {

      if (err) {
          console.log("error deleting previous payments: " + err);
      } else {
          console.log("Removed: " + numRemoved + " payments");
          callback(err,numRemoved);
      }
  });
};