exports.License = function () {
    this.Main = "";
    this.Log1 = "";
    this.Log2 = new Date();
};

var Datastore = require('nedb');

const electron = require('electron');
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
var db = new Datastore({ filename: userDataPath+'/jucce/appfiles.db', autoload: true });

exports.SaveLicense = function (License, callback) {
    delete License._id;
    db.insert(License, function (err, doc) {
        callback(err, doc);
    });
};

exports.CheckLicense = function (filter, callback) {
    db.find(filter, function (err, docs) {
        callback(err, docs);
        console.log(docs);
    });
};