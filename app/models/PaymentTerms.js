exports.PaymentTerm = function()
{
  this.PaymentTerm="";
  this.Description="";
  this.PaymentPeriod="";
  this.LatePaymentRate="";  
  this.IsDefault=false;
};

var Datastore = require('nedb');

const electron = require('electron');
const paymentTermDataPath = (electron.app || electron.remote.app).getPath('userData');
var db = new Datastore({ filename: paymentTermDataPath+'/jucce/PaymentTerm.db', autoload: true });


exports.addPaymentTerm = function (paymentTerm, callback) {
  var id;
  if (paymentTerm._id) { delete paymentTerm._id };
  paymentTerm.UniqueID = SystemData.UniqueID();
  db.insert(paymentTerm, function (err, doc) {
    console.log(err);
    id = doc.UniqueID
    console.log(id);
    callback(err, id);
  });
};

exports.getPaymentTerms = function (filter, callback) {
 
  console.log("Get PaymentTerm: " + JSON.stringify(filter));
  db.find(filter, function (err, docs) {
    callback(err,docs);
  });
};

exports.getPaymentTermById = function (filter, callback) {
  filter = filter.replace(" ", "|");
  var xfilter = new RegExp(filter, "gi");
  console.log("PaymentTerm ID is:" + xfilter);
  var myfilter = { UniqueID: xfilter };
  db.find(myfilter, function (err, docs) {

    callback(err, docs);
    console.log(myfilter);
    console.log(docs);
  });
};

exports.updatePaymentTerm = function (id, paymentTerm, callback) {
  delete paymentTerm._id;
  console.log(paymentTerm);
  db.update({ UniqueID: id }, paymentTerm,{ multi: false, returnUpdatedDocs: true }, function (err,num, doc) {
    callback(err, num, doc);
  });  
};

exports.deletePaymentTerm = function (id, callback) {
  db.remove({ UniqueID: id }, function (err, numRemoved) {
    callback();
  });
};