exports.Product = function()
{
  this.Name="";
  this.Description="";
  this.Price= 0,
  this.Cost=0,
  this.SalesTax="";
  this.Sell=false;
  this.Buy=false;
  this.IncomeAccount="";
  this.ExpenseAccount="";
};

var Datastore = require('nedb');
var alertType, message;

const electron = require('electron');
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
var db = new Datastore({ filename: userDataPath+'/jucce/productsAndServices.db', autoload: true });


exports.addProduct = function (product, callback) {
  var id;
  if (product._id) { delete product._id };
  product.UniqueID = SystemData.UniqueID();
  db.insert(product, function (err, doc) {
    console.log(err);
    id = doc.UniqueID
    console.log(id);
    callback(err, id);
  });
};

exports.getProduct = function (filter, callback) {
  filter = filter.replace(" ", "|");
  var xfilter = new RegExp(filter, "gi");
  console.log(xfilter);
  var myfilter = { $or: [{ UniqueID: xfilter }, { Name: xfilter }, { Description: xfilter }] };
  db.find(myfilter, function (err, docs) {

    callback(docs);
    console.log(myfilter);
    console.log(docs);
  });
};

exports.getProductToSell = function (filter,callback) {
  filter = filter.replace(" ", "|");
  var xfilter = new RegExp(filter, "gi");
  console.log(xfilter);
  var myfilter = { Sell:true};
  db.find(myfilter, function (err, docs) {

    callback(docs);
    console.log(myfilter);
    console.log(docs);
  });
};

exports.getProductToBuy = function (filter,callback) {
  filter = filter.replace(" ", "|");
  var xfilter = new RegExp(filter, "gi");
  console.log(xfilter);
  var myfilter = {Buy:true};
  db.find(myfilter, function (err, docs) {

    callback(docs);
    console.log(myfilter);
    console.log(docs);
  });
};

exports.getProductById = function (filter, callback) {
  filter = filter.replace(" ", "|");
  var xfilter = new RegExp(filter, "gi");
  console.log("Product ID is:" + xfilter);
  var myfilter = { UniqueID: xfilter };
  db.find(myfilter, function (err, docs) {

    callback(err, docs);
    console.log(myfilter);
    console.log(docs);
  });
};

exports.updateProduct = function (id, product, callback) {
  delete product._id;
  console.log(product);
  db.update({ UniqueID: id }, product,{ multi: false, returnUpdatedDocs: true }, function (err,num, doc) {
    callback(err, num, doc);
  });  
};

exports.deleteProduct = function (id, callback) {
  db.remove({ UniqueID: id }, function (err, numRemoved) {
    callback();
  });
};