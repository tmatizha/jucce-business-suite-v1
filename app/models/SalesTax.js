exports.SalesTax = function () {
    this.Name = "";
    this.Abbreviation = "";
    this.Description = "";
    this.TaxNumber = "";
    this.TaxRate = "";
    this.RateHistory = {
        Rate: "",
        EffectiveDate: ""
    };
    this.AccountId = "";
};

var Datastore = require('nedb');
var alertType, message;

const electron = require('electron');
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
var db = new Datastore({ filename: userDataPath+'/jucce/SalesTax.db', autoload: true });


exports.addSalesTax = function (SalesTax, callback) {
    var id;
    if (SalesTax._id) { delete SalesTax._id };
    SalesTax.UniqueID = SystemData.UniqueID();
    db.insert(SalesTax, function (err, doc) {
        console.log(err);
        id = doc.UniqueID
        console.log(id);
        callback(err, id);
    });
};

exports.getSalesTax = function (filter, callback) {
    filter = filter.replace(" ", "|");
    var xfilter = new RegExp(filter, "gi");
    console.log(xfilter);
    var myfilter = { $or: [{ UniqueID: xfilter }, { Name: xfilter }, { Description: xfilter }, { Abbreviation: xfilter }] };
    db.find(myfilter, function (err, docs) {

        callback(docs);
        console.log(myfilter);
        console.log(docs);
    });
};

exports.getSalesTaxById = function (filter, callback) {
    filter = filter.replace(" ", "|");
    var xfilter = new RegExp(filter, "gi");
    console.log("SalesTax ID is:" + xfilter);
    var myfilter = { UniqueID: xfilter };
    db.find(myfilter, function (err, docs) {

        callback(err, docs);
        console.log(myfilter);
        console.log(docs);
    });
};

exports.updateSalesTax = function (id, SalesTax, callback) {
    delete SalesTax._id;
    console.log(SalesTax);
    db.update({ UniqueID: id }, SalesTax, { multi: false, returnUpdatedDocs: true }, function (err, num, doc) {
        callback(err, num, doc);
    });
};

exports.deleteSalesTax = function (id, callback) {
    db.remove({ UniqueID: id }, function (err, numRemoved) {
        callback();
    });
};