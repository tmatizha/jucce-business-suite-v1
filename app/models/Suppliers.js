exports.Supplier = function()
{
  this.SupplierName= "";
  this.Email= "";
  this.Phone= "";
  this.ContactFname= "";
  this.ContactLname= "";
  this.Currency= "";
  this.AddressLine1= "";
  this.AddressLine2= "";
  this.City= "";
  this.ZipCode= "";
  this.Country= "";
  this.PaymentTerms= "";
  this.PaymentPeriod=0;
  this.LatePaymentRate=0;
};


var Datastore = require('nedb');
var alertType, message;

const electron = require('electron');
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
var db = new Datastore({ filename: userDataPath+'/jucce/suppliers.db', autoload: true });


exports.addSupplier = function (supplier, callback) {

  if (supplier._id) { delete supplier._id };
  supplier.UniqueID = SystemData.UniqueID();
  db.insert(supplier, function (err, doc) {
    callback(err, doc);
    console.log("Doc ID" + doc.UniqueID);
  });
};

exports.getSupplier = function (filter, callback) {
  filter = filter.replace(" ", "|");
  var xfilter = new RegExp(filter, "gi");
  console.log(xfilter);
  var myfilter = { $or: [{ UniqueID: xfilter }, { SupplierName: xfilter }, { Email: xfilter }, { ContactFname: xfilter }, { ContactLname: xfilter }, { Phone: xfilter }, { "Billing.Country": xfilter }, { "Billing.City": xfilter }, { "Shipping.Country": xfilter }, { "Shipping.City": xfilter }] };
  db.find(myfilter, function (err, docs) {

    callback(docs);
    console.log(myfilter);
    console.log(docs);
  });
};

exports.getSupplierById = function (filter, callback) {
  filter = filter.replace(" ", "|");
  var xfilter = new RegExp(filter, "gi");
  console.log("Supplier ID is:" + xfilter);
  var myfilter = { UniqueID: xfilter };
  db.find(myfilter, function (err, docs) {

    callback(err, docs);
    console.log(myfilter);
    console.log(docs);
  });
};

exports.updateSupplier = function (id, supplier, callback) {
  delete supplier._id;
  console.log(supplier);
  db.update({ UniqueID: id }, supplier, { multi: false, returnUpdatedDocs: true }, function (err, num, doc) {
    callback(err, num, doc);
  });
};

exports.deleteSupplier = function (id, callback) {
  db.remove({ UniqueID: id }, function (err, numRemoved) {
    callback(err, numRemoved);
  });
};