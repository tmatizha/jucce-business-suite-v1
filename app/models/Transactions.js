exports.AccountTransaction = function () {
    this.AccountId = "";
    this.DocumentReference = "";
    this.Date = new Date();
    this.DocumentId = "";
    this.Details = "";
    this.Debit_CompanyCurrencyAmount = 0;
    this.Debit_AccountCurrencyAmount = 0;
    this.Debit_ReferenceCurrencyAmount = 0;
    this.Credit_CompanyCurrencyAmount = 0;
    this.Credit_AccountCurrencyAmount = 0;
    this.Credit_ReferenceCurrencyAmount = 0;
    this.IsTax = false;
};

var Datastore = require('nedb');
var alertType, message;

const electron = require('electron');
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
var db = new Datastore({ filename: userDataPath+'/jucce/transactions.db', autoload: true });

exports.RecordTransaction = function (MainTransaction, TaxTransaction, callback) {

    console.log("MainTransaction.TransactionID " + MainTransaction.TransactionID == undefined);
    var TransactionID = "", TaxTransactionID = "";

    db.remove({ DocumentReference: MainTransaction.DocumentReference, DocumentId: MainTransaction.DocumentId }, { multi: true }, function (err, numRemoved) {

        if (err) {
            console.log("error deleting previous transactions: " + err);
        } else {
            console.log("Removed: " + numRemoved + " transactions");
            MainTransaction.UniqueID = SystemData.UniqueID();
            db.insert(MainTransaction, function (err, doc) {
                if (err) {
                    console.log("error saving line Item transaction: " + err);
                } else {
                    console.log("Recorded New Trasaction: " + JSON.stringify(doc));
                    TransactionID = doc.UniqueID;
                    if (TaxTransaction && TaxTransaction.AccountId != null && TaxTransaction.AccountId != "") {
                        db.insert(TaxTransaction, function (err, doc) {
                            if (err) {
                                console.log("error saving tax Item transaction: " + err);
                            } else {
                                console.log("Recorded New Trasaction: " + JSON.stringify(doc));
                                TaxTransactionID = doc.UniqueID;
                                callback(err, TransactionID, TaxTransactionID);
                            }
                        });
                    } else {
                        callback(err, TransactionID, TaxTransactionID);
                    }
                }
            });
        }
    });
};


exports.getTransaction = function (filter, callback) {
    
    db.find(filter).sort({ Date: 1 }).exec(function (err, docs) {
        callback(docs);
      });
};

exports.deleteTransaction = function (Transaction, callback) {
    db.remove(Transaction, { multi: true }, function (err, numRemoved) {

        if (err) {
            console.log("error deleting transactions: " + err);
        } else {
            console.log("Removed: " + numRemoved + " transactions");
            callback(err,numRemoved);
        }
    });
};

exports.deleteRelatedTransactions = function(DocumentId, callback){

    db.remove({DocumentId: DocumentId }, { multi: true }, function (err, numRemoved) {

        if (err) {
            console.log("error deleting previous transactions: " + err);
        } else {
            console.log("Removed: " + numRemoved + " transactions");
            callback(err,numRemoved);
        }
    });
};