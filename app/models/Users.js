exports.User = function()
{
  this.Name="";
  this.Email="";
  this.JobTitle="";  
  this.Profile="";
  this.Password="";
  this.Question1="";
  this.Answer1="";
  this.Question2="";
  this.Answer2="";
  Username="";
};

exports.SecurityQuestons = [
  "What was the house number and street name you lived in as a child?",
  "What were the last four digits of your childhood telephone number?",
  "What primary school did you attend?",
  "In what town or city was your first full time job?",
  "In what town or city did you meet your spouse or partner?",
  "What is the middle name of your oldest child?",
  "What are the last five digits of your driver's license number?",
  "What is your grandmother's (on your mother's side) maiden name?",
  "What is your spouse or partner's mother's maiden name?",
  "In what town or city did your parents meet?"];

var Datastore = require('nedb');

const electron = require('electron');
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
var db = new Datastore({ filename: userDataPath+'/jucce/User.db', autoload: true });


exports.addUser = function (user, callback) {
  var id;
  if (user._id) { delete user._id };
  user.UniqueID = SystemData.UniqueID();
  db.insert(user, function (err, doc) {
    console.log(err);
    id = doc.UniqueID
    console.log(id);
    callback(err, id);
  });
};

exports.getUser = function (filter, callback) {
 
  console.log("Get User: " + JSON.stringify(filter));
  db.find(filter, function (err, docs) {
    callback(err,docs);
  });
};

exports.getUserById = function (filter, callback) {
  filter = filter.replace(" ", "|");
  var xfilter = new RegExp(filter, "gi");
  console.log("User ID is:" + xfilter);
  var myfilter = { UniqueID: xfilter };
  db.find(myfilter, function (err, docs) {

    callback(err, docs);
    console.log(myfilter);
    console.log(docs);
  });
};

exports.updateUser = function (id, user, callback) {
  delete user._id;
  console.log(user);
  db.update({ UniqueID: id }, user,{ multi: false, returnUpdatedDocs: true }, function (err,num, doc) {
    callback(err, num, doc);
  });  
};

exports.deleteUser = function (id, callback) {
  db.remove({ UniqueID: id }, function (err, numRemoved) {
    callback();
  });
};