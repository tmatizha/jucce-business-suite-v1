exports.Invoice = function () {
    this.Customer = {
        CustomerName: "",
        Email: "",
        Phone: "",
        ContactFname: "",
        ContactLname: "",
        Currency:"",
        Billing: {
            AddressLine1: "",
            AddressLine2: "",
            City: "",
            ZipCode: "",
            Country: ""
        },
        Shipping: {
            SameAsBilling: "",
            AddressLine1: "",
            AddressLine2: "",
            City: "",
            ZipCode: "",
            Country: ""
        },
        UniqueID: ""
    };
    this.InvoiceHeader = {
        InvoiceNumber: "",
        OrderNumber: "",
        InvoiceDate: "",
        PaymentDate: "",
        InvoiceCurrency: "",
        CompanyCurrency: "",
        ExchangeRate: 1,
        ReferenceCurrency: "",
        ReferenceCurrencyRate: 1,
        InvoiceNetAmount: 0.00,
        InvoiceAmount: 0.00,
        PaymentTerms:{
            PaymentTerm:"",
            Description:"",
            PaymentPeriod:0,
            LatePaymentRate:0,
            UniqueID: "" 
        }
    };
    this.InvoicePayment = {
        Balance: 0,
        AmountRecieved: 0,
        PaymentDate: "",
        PaymentStatus: "",
        NextPaymentDate: ""
    };
    this.InvoiceLines = [];
    this.InvoiceTaxes = [];
    this.InvoiceState = "Draft";
    this.InvoiceCreationDate = "";
    this.InvoiceSendDate = "";
}

exports.LineItem = function () {
    this.Item = "";
    this.Description = "";
    this.Quantity = 1;
    this.Price = 0.00;
    this.Amount = 0;
    this.TaxName = "";
    this.TaxRate = 0;
    this.TaxAmount = 0;
    this.TaxId = "";
    this.ProductId = "";
    this.IncomeAccount = "";
    this.TransactionID = "";
    this.TaxTransactionID = "";
    this.TaxAccountId = "";
}

var Datastore = require('nedb');
var alertType, message;

const electron = require('electron');
const userDataPath = (electron.app || electron.remote.app).getPath('userData');
var db = new Datastore({ filename: userDataPath+'/jucce/invoices.db', autoload: true });

var deletedDB = new Datastore({ filename: userDataPath+'/jucce/deletedInvoices.db', autoload: true });



exports.CreateInvoice = function (invoice, callback) {
    //console.log("Create Invoice");
    invoice.UniqueID = SystemData.UniqueID();
    db.insert(invoice, function (err, doc) {
        callback(err, doc);
    });
};

//only for draft invoices
exports.updateInvoice = function (id, invoice, callback) {
    //console.log("Updating Invoice: "+ JSON.stringify(invoice));
    //delete invoice.UniqueID;
    db.update({ UniqueID: id }, invoice, { multi: false, returnUpdatedDocs: true }, function (err, num, doc) {
        if (err) {
            console.log("Invoice Update Failed: " + err)
        } else {
            console.log("Updated Invoice is: ", JSON.stringify(doc));
            console.log("Updated " + num + " documents");
            callback(err, doc);
        }
    });
};

exports.getInvoice = function (searchString, callback) {

    // Take out spaces and replace with pipes
    searchString = searchString.split(' ').join('|');

    // Use searchString to build rest of regex
    // -> Note: 'i' for case insensitive
    var regex = new RegExp(searchString, 'i');

    // Build query, using regex for each searchable field
    var query = {
        $or: [
            {
                "UniqueID": {
                    "$regex": regex,
                },
            },

            {
                "Customer.CustomerName": {
                    "$regex": regex,
                },
            },

            {
                "Email": {
                    "$regex": regex,
                },
            },

            {
                "ContactFname": {
                    "$regex": regex,
                },
            },
            {
                "ContactLname": {
                    "$regex": regex,
                },
            },
            {
                "Phone": {
                    "$regex": regex,
                },
            },
            {
                "Country": {
                    "$regex": regex,
                },
            },
            {
                "S_Country": {
                    "$regex": regex,
                },
            },
            {
                "City": {
                    "$regex": regex,
                },
            },
            {
                "S_City": {
                    "$regex": regex,
                },
            },
        ]
    };
    console.log("my filter is: " + JSON.stringify(query));

    db.find(query, function (err, docs) {

        callback(err,docs);
        console.log("my filter is: " + JSON.stringify(query));
        console.log(JSON.stringify(docs));
    });
};

exports.searchInvoice = function (filter, callback) {
    //console.log("search filter is: " + JSON.stringify(filter));
    db.find(filter, function (err, docs) {        
        //console.log("Result is: " + JSON.stringify(docs));
        callback(err, docs);
    });
};

exports.getInvoiceByID = function (filter, callback) {

    var myfilter = { UniqueID: filter };
    console.log(myfilter);
    db.find(myfilter, function (err, docs) {
        callback(err, docs);
        console.log(docs);
    });
};

exports.CheckInvoiceNum = function (filter, callback) {
    //filter = filter.replace(" ", "|");
    var xfilter = new RegExp(filter);
    //console.log(xfilter);
    var myfilter = { "InvoiceHeader.InvoiceNumber": { "$regex": xfilter } };
    db.count(myfilter, function (err, count) {
        callback(count);
        console.log(myfilter);
        console.log(count);
        console.log(err);
    });
};




//Invoice Payament
exports.invoicePayment = function (id, PaymentInfo, InvoiceState, callback) {
    //delete invoice.UniqueID;
    //console.log(JSON.stringify("Payment Info: " + PaymentInfo));
    db.update({ UniqueID: id }, {
        $set: {
            "InvoicePayment.Balance": PaymentInfo.Balance,
            "InvoicePayment.AmountRecieved": PaymentInfo.AmountRecieved,
            "InvoicePayment.PaymentDate": PaymentInfo.PaymentDate,
            "InvoicePayment.PaymentStatus": PaymentInfo.PaymentStatus,
            "InvoicePayment.NextPaymentDate": PaymentInfo.NextPaymentDate,
            "InvoiceState": InvoiceState
        }
    },
        function (err, doc) {
            callback(err);
        });
};

//Update invoice state
exports.sendInvoice = function (id, invoiceState, sendDate, callback) {
    db.update({ UniqueID: id }, {
        $set: {
            InvoiceState: invoiceState,
            InvoiceSendDate: sendDate,
        }
    },
        function (err, doc) {
            callback(doc, err);
        });
};

//only for draft invoices
exports.deleteInvoice = function (invoice, callback) {
    db.remove({ UniqueID: invoice.UniqueID }, function (err, numRemoved) {
        callback(err, numRemoved);
    });

    delete invoice._id;
    deletedDB.insert(invoice, function (err, doc) {
        if(err){
            console.log("error recording invoice as deleted: "+ err);
        }else{
            console.log("invoice recorded as deleted: "+ JSON.stringify(doc));
        }
        
    });
};

//only for draft expenses
exports.deleteExpense = function (expense, callback) {
    
};

exports.CountInvoices = function (filter, callback) {
    console.log("filter is: " + JSON.stringify(filter))
    db.count(filter, function (err, count) {
        callback(err, count);
    });
};
exports.CountInvoicesByState = function (filter, callback) {
    db.count({ "InvoiceState": filter }, function (err, count) {
        callback(err, count);
    });
};
exports.CountAllInvoices = function (callback) {
    db.count({}, function (err, count) {
        callback(err, count);
    });
};

exports.CountLateInvoices = function (callback) {
    db.count({ "InvoiceHeader.PaymentDate": { $lte: new Date() }, "InvoiceState": "Unpaid" }, function (err, count) {
        callback(err, count);
    });
};