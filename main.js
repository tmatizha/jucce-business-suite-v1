const electron = require('electron');
const url = require('url');
const path = require('path');
const { app, BrowserWindow, Menu, ipcMain } = electron;
const _ = require('lodash');
const moment = require('moment');
//const ipcMain = electron
const os = require('os');
const fs = require('fs');


let mainWindow;

app.on('ready', () => {

    mainWindow = new BrowserWindow({ minWidth: 1280, minHeight: 800, show: false, icon: __dirname + '/app/config/images/JBS_Icon.ico' })// frame: false})
    //mainWindow.webContents.openDevTools();
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file',
        slashes: true
    }));


    mainWindow.maximize();
    mainWindow.show();

    // Build menu from template
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
    //Insert menu
    //Over ride the menu by enabling the bellow:
    //Menu.setApplicationMenu(mainMenu);    
});

ipcMain.on('printInvoice', (event, arg) => {

    console.log(arg + ' Initiated');

    var options = {
        silent: false,
        printBackground: true,
        color: true,
        margin: {
            marginType: 'printableArea'
        },
        landscape: false,
        pagesPerSheet: 1,
        collate: false,
        copies: 1,
        header: 'Header of the Page',
        footer: 'Footer of the Page',
        printSelectionOnly: true
    }

    let win = new BrowserWindow({
        show: false,
        webPreferences: {
            nodeIntegration: true
        }
    });

    const userDataPath = (electron.app || electron.remote.app).getPath('userData');
    win.loadURL('file:///'+userDataPath+'/printInvoice.html');
    
    

    win.webContents.on('did-finish-load', () => {
        win.webContents.print(options, (success, failureReason) => {
            if (!success) {
                console.log("Print Failed"+ failureReason);
            } else {

                console.log('Print Initiated');
                event.sender.send('print-reply', 'printed');
            }
        });
    });
})

// Create menu template
const mainMenuTemplate = [
    {
        label: 'File',
        submenu: [
            {
                label: 'Quit',
                click() {
                    app.quit();
                }
            }
        ]
    }
];

